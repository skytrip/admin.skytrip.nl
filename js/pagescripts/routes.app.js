var base_url = "http://skyadmin.erdinckaraman.com";
var app = angular.module("routesItems", [ 'angularMoment','ngTouch', 'ui.grid', 'ui.grid.selection', 'ui.grid.exporter']);
app.run(function(amMoment) {
    amMoment.changeLocale('tr');
});
app.filter('iif', function () {
    return function(input, trueValue, falseValue) {
        return input ? trueValue : falseValue;
    };
});

app.controller('routesList', ['$scope', 'uiGridConstants', '$http', 'moment', function ($scope, uiGridConstants, $http,moment) {
    $scope.routesGrid = {
        showGridFooter: true,
        showColumnFooter: true,
        enableFiltering: true,
        enableSorting: true,
        enableGridMenu: true,
        columnDefs: [
            {field: 'ucus_id', name:'Uçuş ID', cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope"><a href="#index.php/routes/update/{{row.entity.ucus_id}}" *data-toggle="modal" *data-target="#detailView">{{row.entity.ucus_id}}</a></div>', width:80},
            {field:'ucus_kod',name:'Uçuş Kodu',cellTemplate:'<div class="ui-grid-cell-contents ng-binding ng-scope">{{row.entity.ucus_kod}} {{row.entity.ucus_no}}</div>'},
            {field:'ai1name',name:'Kalkış Kalkış',cellTemplate:'<div class="ui-grid-cell-contents ng-binding ng-scope">{{row.entity.ai1name}} ({{row.entity.ai1airportcode}})</div>'},
            {field:'ai2name',name:'Varış Yeri',cellTemplate:'<div class="ui-grid-cell-contents ng-binding ng-scope">{{row.entity.ai2name}} ({{row.entity.ai2airportcode}})</div>'},
            {field:'ucus_tarih'},
            {field:'ucus_saat1',name:'Kalkış Saati'},
            {field:'ucus_saat2',name:'Varış Saati'}
        ]
    };
    $scope.table = [];
    $scope.page = 1;
    $scope.countall = 0;
    loadData();
    var tablouzunluk = 10;
    var firstinpage = 1;
    var lastinpage = tablouzunluk;
    $scope.firstinpage = firstinpage;
    $scope.lastinpage = lastinpage;

    $scope.pageFirst = function () {
        $scope.page = 1;
        $scope.firstinpage = 1;
        $scope.lastinpage = tablouzunluk;
        loadData();
    };

    $scope.pageLast = function () {
        $scope.page = $scope.countall;
        $scope.firstinpage = $scope.tumu - $scope.tumu%tablouzunluk + 1;
        $scope.lastinpage = $scope.tumu;
        loadData();
    };

    $scope.setPage = function (p) {
        $scope.page = p;
        if(p != $scope.countall){
            $scope.lastinpage = (p - 1) * tablouzunluk + tablouzunluk;
            $scope.firstinpage = (p - 1) * tablouzunluk + 1;
        }else{
            $scope.lastinpage = (p - 1) * tablouzunluk + $scope.tumu%tablouzunluk;
            $scope.firstinpage = (p - 1) * tablouzunluk + 1;
        }
        loadData();
    };

    $scope.range = function(min, max, step) {
        step = step || 1;
        var input = [];
        if (min<1) {min = 1;}
        if (max>$scope.countall) {max = $scope.countall;}
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    };

    function loadData() {
        $http({
            url: base_url + '/index.php/routes/getRoutesList/'+$scope.page,
            method: 'GET'
        }).success(function(data, status) {
            $scope.table = data.result;
            $scope.routesGrid.data = data.result;
            $scope.tumu = data.count_all;
            $scope.countall = Math.ceil(data.count_all / 10)
        });
    }
}]);

angular.element(document).ready(function() {
    angular.bootstrap(document.getElementById('appRoutesItems'), ['routesItems']);
});
