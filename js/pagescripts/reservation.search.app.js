
angular.module("skytripPages", [])
	.directive('myEnter', function () {
		return function (scope, element, attrs) {
	        element.bind("keydown keypress", function (event) {
	            if(event.which === 13) {
	                scope.$apply(function (){
	                    scope.$eval(attrs.myEnter);
	                });
	                event.preventDefault();
	            }
	        });
	    };
	})
    .controller("skytripSearch", ['$scope', function($scope) {
        $scope.FormElements = {
            rezid: "",
            rezno: "",
            pnr: "",
            kayitno_pre: "",
            kayitno_post: "",
            musteriid: "",
            kayittar_start: "",
            kayittar_end: "",
            ucusno_pre: "",
            ucusno_post: "",
            ucustar_start: "",
            ucustar_end: "",
            turop: "",
            ekstraucr: "",
            kalan: "",
            iade: "",
            acente: "",
            aciklama: "",
            kaydeden: "",
            durum: "",
            yon: "",
            webapp: "",
            kayit: "",
            mailbilgisi: "",
            degisen: "",
            sifirucret :""
        };
        $scope.FormSearch = function() {
            referance_dt_filter = $scope.FormElements;
            referance_dt_rezdt.draw();
        };
        $scope.FormClear = function() {
            $scope.FormElements.rezid = "";
            $scope.FormElements.rezno = "";
            $scope.FormElements.pnr = "";
            $scope.FormElements.kayitno_pre = "";
            $scope.FormElements.kayitno_post = "";
            $scope.FormElements.musteriid = "";
            $scope.FormElements.kayittar_start = "";
            $scope.FormElements.kayittar_end = "";
            $scope.FormElements.ucusno_pre = "";
            $scope.FormElements.ucusno_post = "";
            $scope.FormElements.ucustar_end = "";
            $scope.FormElements.ucustar_start = "";
            $scope.FormElements.turop = "";
            $scope.FormElements.ekstraucr = "";
            $scope.FormElements.kalan = "";
            $scope.FormElements.iade = "";
            $scope.FormElements.acente = "";
            $scope.FormElements.aciklama = "";
            $scope.FormElements.kaydeden = "";
            $scope.FormElements.durum = "";
            $scope.FormElements.yon = "";
            $scope.FormElements.webapp = "";
            $scope.FormElements.kayit = "";
            $scope.FormElements.mailbilgisi = "";
            $scope.FormElements.degisen = "";
            $scope.FormElements.sifirucret = "";
            $scope.FormSearch();
        }
    }]);

    angular.element(document).ready(function() {
        angular.bootstrap(document.getElementById('appSkytripPages'), ['skytripPages']);
    });
