var base_url = "http://skyadmin.erdinckaraman.com";
var app = angular.module("dashboardItems", ["angularMoment", 'ngTouch', 'ui.grid', 'ui.grid.selection', 'ui.grid.exporter']);
app.run(function(amMoment) {
     amMoment.changeLocale('tr');
});
app.filter('iif', function () {
     return function(input, trueValue, falseValue) {
          return input ? trueValue : falseValue;
     };
});
app.controller('optionList', ['$scope', 'uiGridConstants', '$http', 'moment', function ($scope, uiGridConstants, $http,moment) {
     $scope.optionGrid = {
          showGridFooter: true,
          showColumnFooter: true,
          enableFiltering: true,
          enableSorting: true,
          enableGridMenu: true,
          columnDefs: [
               { field: 'rez_kayitno', name:'KayıtNo', cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope"><a href="index.php/reservation/simple/{{row.entity.rez_id}}" data-toggle="modal" data-target="#detailView">{{row.entity.rez_kayitno}}</a></div>', width:80},
               { field: 'rez_turoperatorpnr', name:'RezNo', width:80},
               { field: 'acente_ad', name:'Acente & Müşteri', width:140},
               { field: 'turop_ad', name:'Tur Opr.', width:80},
               { field: 'telefon', name:'Telefon', width:100},
               { field: 'rez_opsiyontarih', name:'Opsiyon', cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope" style="{{row.entity.opsiyontarihstyle}}">{{row.entity.rez_opsiyontarih}}</div>', width:140},
               {
                    field: 'sum_tutar',
                    name:'Tutar',
                    cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope" style="{{row.entity.kalanstyle}}">{{row.entity.sum_tutar}}€</div>',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    width:130,
                    footerCellTemplate: '<div>Toplam: {{col.getAggregationValue() | number:2}}€</div>',
                    customTreeAggregationFinalizerFn: function( aggregation ) {
                         aggregation.rendered = aggregation.value;
                    }
               }
          ]
     };
     $scope.table = [];
     $scope.page = 1;
     $scope.countall = 0;
     loadData();
     var tablouzunluk = 10;
     var firstinpage = 1;
     var lastinpage = tablouzunluk;
     $scope.firstinpage = firstinpage;
     $scope.lastinpage = lastinpage;

     $scope.pageFirst = function () {
          $scope.page = 1;
          $scope.firstinpage = 1;
          $scope.lastinpage = tablouzunluk;
          loadData();
     }

     $scope.pageLast = function () {
          $scope.page = $scope.countall;
          $scope.firstinpage = $scope.tumu - $scope.tumu%tablouzunluk + 1;
          $scope.lastinpage = $scope.tumu;
          loadData();
     }

     $scope.setPage = function (p) {
          $scope.page = p;
          if(p != $scope.countall){
               $scope.lastinpage = (p - 1) * tablouzunluk + tablouzunluk;
               $scope.firstinpage = (p - 1) * tablouzunluk + 1;
          }else{
               $scope.lastinpage = (p - 1) * tablouzunluk + $scope.tumu%tablouzunluk;
               $scope.firstinpage = (p - 1) * tablouzunluk + 1;
          }
          loadData();
     }

     $scope.range = function(min, max, step) {
          step = step || 1;
          var input = [];
          if (min<1) {min = 1;}
          if (max>$scope.countall) {max = $scope.countall;}
          for (var i = min; i <= max; i += step) {
               input.push(i);
          }
          return input;
     };

     function loadData() {
          $http({
               url: base_url + '/index.php/dashboard/dashboard_optionlist/'+$scope.page,
               method: 'GET'
          }).success(function(data, status) {
               var obje = data.result;
               var objects = [];
               for(i in obje){
                    var tarih = moment(obje[i]["rez_opsiyontarih"], 'YYYY-MM-DD HH-mm-ss').format('DD/MM/YYYY dd hh:mm');
                    var tarihkontrol = moment(obje[i]["rez_opsiyontarih"], 'YYYY-MM-DD HH-mm-ss');
                    var currenttime = new Date();

                    var opsiyontarihstyle = "";

                    if(tarihkontrol.isBefore(currenttime)){
                         opsiyontarihstyle = "color:red;font-weight:bold;"
                    }
                    if(tarihkontrol.diff(currenttime, 'days') == 0){
                         opsiyontarihstyle = "color:black;font-weight:bold;"
                    }

                    var kalanstyle = "";

                    if(obje[i]["kalan"] > 0){
                         kalanstyle = "color:red;";
                    }else{
                         if(obje[i]["kalan"] == 0){
                              kalanstyle = "color:green";
                         }
                         if(obje[i]["kalan"] < 0){
                              kalanstyle = "color:blue;";
                         }
                    }
                    objects.push({
                         "rez_id":obje[i]["rez_id"],
                         "rez_kayitno":obje[i]["rez_kayitno"],
                         "rez_turoperatorpnr":obje[i]["rez_turoperatorpnr"],
                         "acente_ad":obje[i]["acente_ad"],
                         "turop_ad":obje[i]["turop_ad"],
                         "telefon":obje[i]["telefon"],
                         "rez_opsiyontarih":tarih,
                         "opsiyontarihstyle":opsiyontarihstyle,
                         "sum_tutar":obje[i]["sum_tutar"],
                         "kalanstyle":kalanstyle
                    });
               };
               $scope.table = objects;//data.result;
               // $scope.optionGrid.data = [];
               $scope.optionGrid.data = objects;
               $scope.tumu = data.count_all;
               $scope.countall = Math.ceil(data.count_all / 10)
          });
     }

}]);
app.controller('cancelList', ['$scope', '$http', function ($scope, $http) {
     $scope.cancelGrid = {
          showGridFooter: true,
          showColumnFooter: true,
          enableFiltering: true,
          enableSorting: true,
          enableGridMenu: true,
          columnDefs: [
               { field: 'rez_kayitno', name:'KayıtNo', cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope"><a href="index.php/reservation/simple/{{row.entity.rez_id}}" data-toggle="modal" data-target="#detailView">{{row.entity.rez_kayitno}}</a></div>'},
               { field: 'acente_ad', name:'Acente & Müşteri'},
               { field: 'telefon', name:'Telefon'},
          ]
     };
     $scope.table = [];
     $scope.page = 1;
     $scope.countall = 0;
     loadData();
     var tablouzunluk = 10;
     var firstinpage = 1;
     var lastinpage = tablouzunluk;
     $scope.firstinpage = firstinpage;
     $scope.lastinpage = lastinpage;

     $scope.pageFirst = function () {
          $scope.page = 1;
          $scope.firstinpage = 1;
          $scope.lastinpage = tablouzunluk;
          loadData();
     }

     $scope.pageLast = function () {
          $scope.page = $scope.countall;
          $scope.firstinpage = $scope.tumu - $scope.tumu%tablouzunluk + 1;
          $scope.lastinpage = $scope.tumu;
          loadData();
     }

     $scope.setPage = function (p) {
          $scope.page = p;
          if(p != $scope.countall){
               $scope.lastinpage = (p - 1) * tablouzunluk + tablouzunluk;
               $scope.firstinpage = (p - 1) * tablouzunluk + 1;
          }else{
               $scope.lastinpage = (p - 1) * tablouzunluk + $scope.tumu%tablouzunluk;
               $scope.firstinpage = (p - 1) * tablouzunluk + 1;
          }
          loadData();
     }

     $scope.range = function(min, max, step) {
          step = step || 1;
          var input = [];
          if (min<1) {min = 1;}
          if (max>$scope.countall) {max = $scope.countall;}
          for (var i = min; i <= max; i += step) {
               input.push(i);
          }
          return input;
     };

     function loadData() {
          $http({
               url: base_url + '/index.php/dashboard/dashboard_cancellist/' + $scope.page,
               method: 'GET'
          }).success(function(data, status) {
               // $scope.table = data.result;
               $scope.cancelGrid.data = data.result;
               $scope.tumu = data.count_all;
               $scope.countall = Math.ceil(data.count_all / 10)
          });
     }

}]);
app.controller('waitList', ['$scope', 'uiGridConstants', '$http', function ($scope, uiGridConstants, $http) {
     $scope.waitGrid = {
          showGridFooter: true,
          showColumnFooter: true,
          enableFiltering: true,
          enableSorting: true,
          enableGridMenu: true,
          columnDefs: [
               { field: 'rez_kayitno', name:'KayıtNo', cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope"><a href="index.php/reservation/simple/{{row.entity.rez_id}}" data-toggle="modal" data-target="#detailView">{{row.entity.rez_kayitno}}</a></div>', width:80},
               { field: 'rez_turoperatorpnr', name:'RezNo', width:80},
               { field: 'acente_ad', name:'Acente & Müşteri', width:140},
               { field: 'turop_ad', name:'Tur Opr.', width:80},
               { field: 'telefon', name:'Telefon', width:100},
               { field: 'rez_opsiyontarih', name:'Opsiyon', width:140},
               {
                    field: 'sum_tutar',
                    name:'Tutar',
                    cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope" style="{{row.entity.kalanstyle}}">{{row.entity.sum_tutar}}€</div>',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    width:130,
                    footerCellTemplate: '<div>Toplam: {{col.getAggregationValue() | number:2}}€</div>',
                    customTreeAggregationFinalizerFn: function( aggregation ) {
                         aggregation.rendered = aggregation.value;
                    }
               }
          ]
     };
     $scope.table = [];
     $scope.page = 1;
     $scope.countall = 0;
     loadData();
     var tablouzunluk = 10;
     var firstinpage = 1;
     var lastinpage = tablouzunluk;
     $scope.firstinpage = firstinpage;
     $scope.lastinpage = lastinpage;

     $scope.pageFirst = function () {
          $scope.page = 1;
          $scope.firstinpage = 1;
          $scope.lastinpage = tablouzunluk;
          loadData();
     }

     $scope.pageLast = function () {
          $scope.page = $scope.countall;
          $scope.firstinpage = $scope.tumu - $scope.tumu%tablouzunluk + 1;
          $scope.lastinpage = $scope.tumu;
          loadData();
     }

     $scope.setPage = function (p) {
          $scope.page = p;
          if(p != $scope.countall){
               $scope.lastinpage = (p - 1) * tablouzunluk + tablouzunluk;
               $scope.firstinpage = (p - 1) * tablouzunluk + 1;
          }else{
               $scope.lastinpage = (p - 1) * tablouzunluk + $scope.tumu%tablouzunluk;
               $scope.firstinpage = (p - 1) * tablouzunluk + 1;
          }
          loadData();
     }

     $scope.range = function(min, max, step) {
          step = step || 1;
          var input = [];
          if (min<1) {min = 1;}
          if (max>$scope.countall) {max = $scope.countall;}
          for (var i = min; i <= max; i += step) {
               input.push(i);
          }
          return input;
     };

     function loadData() {
          $http({
               url: base_url + '/index.php/dashboard/dashboard_waitlist/'+$scope.page,
               method: 'GET'
          }).success(function(data, status) {
               // $scope.table = data.result;
               $scope.waitGrid.data = data.result;
               $scope.tumu = data.count_all;
               $scope.countall = Math.ceil(data.count_all / 10)
          });
     }

}]);
app.controller('specmessage', ['$scope', '$http', function($scope, $http) {
     $scope.specmessageGrid = {
          showGridFooter: true,
          showColumnFooter: true,
          enableFiltering: true,
          enableSorting: true,
          enableGridMenu: true,
          columnDefs: [
               { field: 'acentekul_kullanici', name:'Gönderen'},
               { field: 'tarih', name:'Tarih'},
               { field: 'mesaj_konu', name:'Konu'}
          ]
     };
     $scope.table = [];

     function loadData() {
          $http({
               url: base_url + '/index.php/dashboard/dashboard_specialmessage',
               method: 'GET'
          }).success(function (data, status) {
               // $scope.table = data;
               $scope.specmessageGrid.data = data;
          });
     }

     loadData();

}]);
app.controller('resmessage', ['$scope', '$http', function($scope, $http) {
     $scope.resmessageGrid = {
          showGridFooter: true,
          showColumnFooter: true,
          enableFiltering: true,
          enableSorting: true,
          enableGridMenu: true,
          columnDefs: [
               { field: 'gonderen', name:'Gönderen'},
               { field: 'alan', name:'Alan'},
               { field: 'rezmesaj_mesaj', name:'Mesaj'},
               { field: 'tarih', name:'Tarih'},
          ]
     };
     $scope.table = [];

     function loadData() {
          $http({
               url: base_url + '/index.php/dashboard/dashboard_reservationmessage',
               method: 'GET'
          }).success(function (data, status) {
               if(data.length > 0)
                    $scope.resmessage.data = data;
          });
     }

     loadData();

}]);
app.controller('flightwithpassenger', ['$scope', '$http', function($scope, $http) {
     $scope.flightwithpassengerGrid = {
          showGridFooter: true,
          showColumnFooter: true,
          enableFiltering: true,
          enableSorting: true,
          enableGridMenu: true,
          columnDefs: [
               { field: 'ucus_id', name:'Uçuş ID', width:80},
               { field: 'parkur', name:'Parkur', width:80},
               { field: 'ucus_no', name:'Uçuş No', width:140},
               { field: 'tarih', name:'Tarih', width:80},
               { field: 'yolcu', name:'Yolcu', width:100}
          ]
     };
     $scope.table = [];
     var date = new Date();
     //    $scope.selectedDate = moment().format('DD-MM-YYYY');
     var day = date.getDate();
     var month = parseInt(date.getMonth()) + parseInt(1);
     var year = date.getFullYear();
     if(day < 10){
          day = 0 + day;
     }
     if(month < 10){
          month = 0 + month;
     }
     $scope.selectedDate = day + "-" + month + "-" + year;

     $scope.dateChanged = function() {
          loadData();
     };

     function loadData() {
          $http({
               url: base_url + '/index.php/dashboard/dashboard_flightwithpassengercount/' + $scope.selectedDate,
               method: 'GET'
          }).success(function (data, status) {
               $scope.flightwithpassengerGrid.data = data;
          });
     }

     loadData();

}]);

angular.element(document).ready(function() {
    angular.bootstrap(document.getElementById('appDashboardItems'), ['dashboardItems']);
});
