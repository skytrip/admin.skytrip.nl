<?php
	/**
	 * Created by erkinduran
	 * Corp: MEK Teknoloji
	 * http://mekteknoloji.com
	 */
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once  APPPATH.'/third_party/Dompdf/autoload.inc.php';
	use Dompdf\Dompdf as Dom;
	class Dompdf extends Dom
	{}