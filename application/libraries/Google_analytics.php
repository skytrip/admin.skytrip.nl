<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of google_analytics
 *
 * @author halit
 */

class Google_analytics {
    
    private $m_ci;
    private $m_client;
    
    function __construct() {
        $this->m_ci =& get_instance();
        
        $this->m_client = new Google_Client();
        $this->m_client->setAuthConfigFile(dirname(__FILE__)
                ."/../config/client_secret_802045768363-nv3b4g571idvc5p6kq1uc12dt7v3s94l.apps.googleusercontent.com.json");
        $this->m_client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
        
    }
    
    public function daily() {
        if ($this->m_ci->session->has_userdata("access_token")) {
            try {
            
            $this->m_client->setAccessToken($this->m_ci->session->userdata("access_token"));
            
            $analytics = new Google_Service_Analytics($this->m_client);
            
            $profile_id = NULL;
            
            $accounts = $analytics->management_accounts->listManagementAccounts();
            if (count($accounts->getItems()) > 0) {
                $items = $accounts->getItems();
                $first_account_id = $items[1]->getId();
                
                $properties = $analytics->management_webproperties->listManagementWebproperties($first_account_id);
                if (count($properties->getItems() > 0)) {
                    $items = $properties->getItems();
                    $first_property_id = $items[0]->getId();
                    
                    $profiles = $analytics->management_profiles->listManagementProfiles($first_account_id, $first_property_id);
                    if (count($profiles->getItems()) > 0) {
                        $items = $profiles->getItems();
                        $profile_id = $items[0]->getId();
                    } else {
                        throw new Exception("No profile found");
                    }
                } else {
                    throw new Exception("No properties found");
                }
            } else {
                throw new Exception("No accounts found");
            }
            
            return $analytics->data_ga->get('ga:'.$profile_id, 'today', 'today', 'ga:visits', array('dimensions' => 'ga:hour'));
            
            } catch (Google_Service_Exception $e) {
                header("Location: /index.php/auth/oauth2callback");
            }
            
        } else {
            header("Location: /index.php/auth/oauth2callback");
        }
    }
    
    public function auth() {
        if ($this->m_ci->input->get("code") == NULL) {
            $auth_url = $this->m_client->createAuthUrl();
            header("Location: " . filter_var($auth_url, FILTER_SANITIZE_URL));
        } else {
            $this->m_client->authenticate($this->m_ci->input->get("code"));
            $this->m_ci->session->set_userdata("access_token", $this->m_client->getAccessToken());
            header("Location: /");
        }
    }
    
}
