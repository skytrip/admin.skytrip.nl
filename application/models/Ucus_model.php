<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 20.11.2018
 * Time: 21:51
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Ucus_model extends CI_Model{

    public function getUcusList($limit)
    {
        $this->db->from("ucus");
        $count = $this->db->count_all_results();

        $this->db->select(
            "uc.ucus_id, date_format(uc.ucus_tarih, '%d/%m/%y %a') as ucus_tarih, uc.ucus_kod, " .
            "uc.ucus_no, date_format(uc.ucus_varistarih, '%d/%m/%y %a') as ucus_varistarih, " .
            "Substr(uc.ucus_saat1, 1, 5) as ucus_saat1, Substr(uc.ucus_saat2, 1, 5) as ucus_saat2, " .
            "ai1.city as ai1name, ai2.city as ai2name, ai1.code as ai1airportcode, " .
            " al.name as alname," .
            "ai2.code as ai2airportcode"
        );
        $this->db->from("ucus uc");
        $this->db->join("airports ai1", "uc.ucus_parkur1 = ai1.code");
        $this->db->join("airports ai2", "uc.ucus_parkur2 = ai2.code");
        $this->db->join("airlines al", "uc.ucus_kod = al.code");
        $this->db->group_by(
            "uc.ucus_id, uc.ucus_tarih, uc.ucus_kod, uc.ucus_no, uc.ucus_varistarih, uc.ucus_saat1, uc.ucus_saat2, "
            . "ai1.city, ai2.city, ai1.code, ai2.code, al.name"
        );
        $this->db->order_by("uc.ucus_id, uc.ucus_saat1","DESC");
        $this->db->limit($limit[ 1 ], $limit[ 0 ]);
        $query = $this->db->get();
        return array(
            "count_all" => $count, "result" => $query->result()
        );
    }

    public function getById($id)
    {
        $this->db->select('*');
        $this->db->from('ucus');
        $this->db->where('ucus_id', $id);  // Also mention table name here
        $query = $this->db->get();
        return $query->row();
    }

    public function insert($ucusData)
    {
        try{
            $ucusData['ucus_duzentarih'] = '0000-00-00 00:00:00';
            $ucusData['ucus_dkilittarih'] = '0000-00-00 00:00:00';
            $ucusData['ucus_duzenleyen'] = '0';
            $ucusData['ucus_duzenseviye'] = '0';
            $ucusData['ucus_dkilitleyen'] = '0';
            $ucusData['ucus_dkilitseviye'] = '0';
            $ucusData['ucus_araparkur'] = '0';
            $ucusData['ucus_parkur1_old'] = '0';
            $ucusData['ucus_parkur2_old'] = '0';
            $ucusData['ucus_kod_old'] = '0';
            $this->db->insert('ucus',$ucusData);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }catch (Exception $e){
            return false;
        }
    }

    public function getDays($code,$c = false)
    {
        $this->db->select("DATE_FORMAT(u.ucus_tarih,'%Y-%m-%d') AS ucus_tarih");
        $this->db->from("ucus u");
        $this->db->join('ucussegment us','us.ucus_id=u.ucus_id');
        $this->db->join('ucusroute ur','us.route_id=ur.route_id');
        $this->db->where("u.ucus_tarih >=",date('Y-m-d'));
        if($c) {
            $this->db->where("u.ucus_parkur1 in(SELECT code FROM airports WHERE countrycode=" . $this->db->escape($code) . ")");
        }else{
            $this->db->where("u.ucus_parkur1",$code);
        }
        $this->db->order_by('ucus_tarih','ASC');
        $this->db->group_by('ucus_tarih');
        $query = $this->db->get();
        return $query->result();
    }

}