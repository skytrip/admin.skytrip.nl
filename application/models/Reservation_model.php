<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation_model extends CI_Model
{
     public function rezervasyonlist_daycount($condition)
     {
          // print_r($condition);
          $this->db->select("COUNT(date_format(r.rez_kayittarih, '%Y-%m-%d')) as count, date_format(r.rez_kayittarih, '%Y-%m-%d') as reztarih");//, date_format(r.rez_kayittarih, '%Y-%m-%d') as reztarih
          $this->db->from("rezervasyon r");
          $this->db->group_by("date_format(r.rez_kayittarih, '%Y-%m-%d')");
          $this->db->where($condition);
          $query = $this->db->get();

          $counts = array();
          foreach ($query->result() as $a) {
               array_push($counts, $a->count);
          }
          $tarih = array();
          foreach ($query->result() as $b) {
               array_push($tarih, $a->reztarih);
          }

          return array(
               "counts" => $counts,
               "tarih" => $tarih,
               "result" => $query->result()
          );
     }

     public function bilet_daycount($condition)
     {
          // print_r($condition);
          $this->db->select("COUNT(IF((r.rez_durum = 'OK'), 1, 0)) as count, date_format(r.rez_kayittarih, '%Y-%m-%d') as reztarih");
          $this->db->from("rezervasyon r");
          $this->db->group_by("date_format(r.rez_kayittarih, '%Y-%m-%d')");
          $this->db->where($condition);
          $query = $this->db->get();

          $counts = array();
          foreach ($query->result() as $a) {
               array_push($counts, $a->count);
          }
          $tarih = array();
          foreach ($query->result() as $b) {
               array_push($tarih, $a->reztarih);
          }

          return array(
               "counts" => $counts,
               "tarih" => $tarih,
               "result" => $query->result()
          );
     }

     public function rezervasyonlist_monthcount($condition)
     {
          $this->db->select('yolcu_rezid, COUNT(yolcu_rezid) as yolcu_sayi, yolcu_satisfiyat, yolcu_vergi, yolcu_koltukfiyat');
          $this->db->from('yolcu');
          $this->db->group_by("yolcu_rezid");
          $this->db->last_query();
          $subquery = $this->db->_compile_select();
          $this->db->_reset_select();

          $this->db->select("SUM(CASE WHEN r.rez_durum = 'OK' THEN 1 ELSE 0 END) as countok, COUNT(date_format(r.rez_kayittarih, '%Y-%m-%d')) as count, date_format(r.rez_kayittarih, '%Y-%m') as reztarih, y.yolcu_sayi, sum(y.yolcu_satisfiyat - y.yolcu_vergi - y.yolcu_koltukfiyat) as kazanc");
          $this->db->from("rezervasyon r");
          $this->db->join("($subquery) y", "y.yolcu_rezid = r.rez_id", "left");
          $this->db->join('gelirgider g', 'r.rez_id = g.gelirgider_rezid', 'left');
          $this->db->group_by("MONTH(r.rez_kayittarih)");
          $this->db->where($condition);
          $this->db->order_by("r.rez_kayittarih", "asc");
          $query = $this->db->get();

          return $query->result();
     }

     public function rezervasyonlist_turopcount()
     {
          $this->db->select("tur.turop_ad, COUNT(tur.turop_id) as turopcount");
          $this->db->from("rezervasyon r");
          $this->db->join('turoperator tur', 'r.rez_turoperator = tur.turop_id', 'left')->group_by("r.rez_turoperator");
          $query = $this->db->get();

          return $query->result();
     }

     public function rezervasyonlist_gidiscount($condition)
     {
          $this->db->select('ucus_id, COUNT(ucus_id) as ucusadet, ucus_parkur2 as parkur');
          $this->db->from('ucus');
          $this->db->group_by("ucus_parkur2");
          $this->db->last_query();
          $subquery = $this->db->_compile_select();
          $this->db->_reset_select();

          $this->db->select("IF((r.rez_yon = 'g'), uc.ucusadet, 0) as adet, IF((r.rez_yon = 'g'), uc.parkur, 0) as parkur");
          $this->db->from("rezervasyon r");
          $this->db->join('yolcu y', 'r.rez_id = y.yolcu_rezid', 'left');
          $this->db->join("yolcuucus yu", "y.yolcu_id = yu.yucus_yolcuid", "left");
          $this->db->join("($subquery) uc", "yu.yucus_ucusid = uc.ucus_id", "left");
          $this->db->group_by("uc.parkur");
          $this->db->where($condition);
          $query = $this->db->get();

          $result = array();
          foreach ($query->result() as $a) {
               if($a->adet != 0) {
                    array_push($result, array($a->adet, $a->parkur));
               }
          }

          return $result;
     }

     public function rezervasyonlist_donuscount($condition)
     {
          $this->db->select('ucus_id, COUNT(ucus_id) as ucusadet, ucus_parkur2 as parkur');
          $this->db->from('ucus');
          $this->db->group_by("ucus_parkur2");
          $this->db->last_query();
          $subquery = $this->db->_compile_select();
          $this->db->_reset_select();

          $this->db->select("IF((r.rez_yon = 'd'), uc.ucusadet, 0) as adet, IF((r.rez_yon = 'd'), uc.parkur, 0) as parkur");
          $this->db->from("rezervasyon r");
          $this->db->join('yolcu y', 'r.rez_id = y.yolcu_rezid', 'left');
          $this->db->join("yolcuucus yu", "y.yolcu_id = yu.yucus_yolcuid", "left");
          $this->db->join("($subquery) uc", "yu.yucus_ucusid = uc.ucus_id", "left");
          $this->db->group_by("uc.parkur");
          $this->db->where($condition);
          $query = $this->db->get();

          $result = array();
          foreach ($query->result() as $a) {
               if($a->adet != 0) {
                    array_push($result, array($a->adet, $a->parkur));
               }
          }

          return $result;
     }

     public function rezervasyonlist_guzergaheniyi($condition)
     {
          $this->db->select('concat(ucus_parkur1, " ", ucus_parkur2) as parkur, COUNT(ucus_id) as count');
          $this->db->from('ucus');
          $this->db->group_by("parkur");
          // $this->db->last_query();
          // $subquery = $this->db->_compile_select();
          // $this->db->_reset_select();
          //
          // $this->db->select('uc.parkur, uc.count');
          // $this->db->from("rezervasyon r");
          // $this->db->join('yolcu y', 'r.rez_id = y.yolcu_rezid','left');
          // $this->db->join("yolcuucus yu", "y.yolcu_id = yu.yucus_yolcuid", "left");
          // $this->db->join("($subquery) uc","yu.yucus_ucusid = uc.ucus_id", "left");
          // // $this->db->group_by("uc.parkur");
          $this->db->where($condition);
          $query = $this->db->get();

          $result = array();
          for ($i=0; $i < count($query->result()); $i++) {
               $result[$query->result()[$i]->parkur] = $query->result()[$i]->count;
          }
          arsort($result);

          // print_r(array_slice($result,0,10));

          return array_slice($result, 0, 10);
     }

     public function reservation_list_count($condition, $likes)
     {
         $this->db->select("rezervasyon.*");
          $this->db->from("rezervasyon");
          $this->db->join('yolcu y', 'rezervasyon.rez_id = y.yolcu_rezid', 'left')->group_by("rezervasyon.rez_id");
          $this->db->join('acente a', 'rezervasyon.rez_acente = a.acente_id', 'left')->group_by("rezervasyon.rez_acente");
          $this->db->join('turoperator tur', 'rezervasyon.rez_turoperator = tur.turop_id', 'left')->group_by("rezervasyon.rez_turoperator");
          $this->db->join('rezervasyonek ekuc', 'rezervasyon.rez_id = ekuc.rezek_rezid', 'left')->group_by("rezervasyon.rez_id");
          $this->db->join('gelirgider g', 'rezervasyon.rez_id = g.gelirgider_rezid', 'left')->group_by("rezervasyon.rez_id");
          $this->db->join("yolcuucus yu", "y.yolcu_id = yu.yucus_yolcuid", "left")->group_by("y.yolcu_id");
          $this->db->join("ucus uc", "yu.yucus_ucusid = uc.ucus_id", 'left')->group_by("yu.yucus_ucusid");
          $this->db->join("musteri mu", "rezervasyon.rez_kaydeden = mu.musteri_id", "left");
          $this->db->where($condition);
          if($likes != "") {
               $this->db->like($likes);
          }
          $query = $this->db->get();
          $kontrol = array();
          foreach ($query->result() as $a) {
               if(!in_array($a->rez_id, $kontrol)) {
                    array_push($kontrol, $a->rez_id);
               }
          }
          $count = count($kontrol);
          return array(
               "count_all" => $count
          );
     }

     public function reservation_list($condition, $likes, $limit, $order)
     {
          $this->db->select("r.*, COUNT(y.yolcu_rezid) AS `yolcu_sayi`, a.acente_ad, tur.turop_ad, y.yolcu_satisfiyat AS sum_tutar, ekuc.rezek_tutar, Round(AVG(IF((g.gelirgider_grup = 'g'), g.gelirgider_tutar, 0)), 2) as alinan, Round(AVG(IF((g.gelirgider_grup = 'c'), g.gelirgider_tutar, 0)), 2) as odenen, (y.yolcu_satisfiyat - Round(AVG(IF((g.gelirgider_grup = 'g' AND g.gelirgider_grup IS NOT NULL), g.gelirgider_tutar, 0)), 2) + Round(AVG(IF((g.gelirgider_grup = 'c' AND g.gelirgider_grup IS NOT NULL), g.gelirgider_tutar, 0)), 2)) as kalan, y.yolcu_id, SUM(CASE WHEN y.yolcu_tipi = 'ADT' THEN 1 ELSE 0 END) AS yolcu_adt, SUM(CASE WHEN y.yolcu_tipi = 'CHD' THEN 1 ELSE 0 END) AS yolcu_chd, SUM(CASE WHEN y.yolcu_tipi = 'INF' THEN 1 ELSE 0 END) AS yolcu_inf, uc.ucus_tarih");
          $this->db->from("rezervasyon r");
          $this->db->join("(SELECT `yolcu_id`, `yolcu_rezid`, SUM(yolcu_satisfiyat) AS yolcu_satisfiyat, `yolcu_tipi` FROM `yolcu` GROUP BY `yolcu_rezid`) y", 'r.rez_id = y.yolcu_rezid', 'left');
          $this->db->join('acente a', 'r.rez_acente = a.acente_id', 'left')->group_by("r.rez_acente,a.acente_ad");
          $this->db->join('turoperator tur', 'r.rez_turoperator = tur.turop_id', 'left')->group_by("r.rez_turoperator,tur.turop_ad");
          $this->db->join('rezervasyonek ekuc', 'r.rez_id = ekuc.rezek_rezid', 'left')->group_by("r.rez_id");
          $this->db->join('gelirgider g', 'r.rez_id = g.gelirgider_rezid', 'left')->group_by("r.rez_id");
          $this->db->join("yolcuucus yu", "y.yolcu_id = yu.yucus_yolcuid", "left");
          $this->db->join("(SELECT `ucus_tarih`, `ucus_id`, `ucus_kod`, `ucus_no` FROM `ucus`) uc", "yu.yucus_ucusid = uc.ucus_id", "left");
          $this->db->join("musteri mu", "r.rez_kaydeden = mu.musteri_id", "left");
          $this->db->where($condition);
          $this->db->like($likes);
          $this->db->order_by($order);
          if($limit !== ""){
               $this->db->limit($limit[1], $limit[0]);
          }
         $query = $this->db->get();

         return array(
               //   "count_all" => $count,
               "result" => $query->result()
          );
     }

     public function reservation_list_simple($aramaarr, $limit)
     {
          $this->db->select('ucus_id, ucus_parkur1, ucus_parkur2, ucus_tarih');
          $this->db->from('ucus');
          $this->db->last_query();
          $subquery = $this->db->_compile_select();
          $this->db->_reset_select();

          $this->db->select("r.rez_id, r.rez_durum, r.rez_yon, uc.ucus_parkur1, uc.ucus_parkur2, y.yolcu_ad, y.yolcu_soyad, uc.ucus_tarih, r.rez_pnr, r.rez_kayittarih, y.yolcu_satisfiyat as fiyat");
          $this->db->from("rezervasyon r");
          $this->db->join('yolcu y', 'r.rez_id = y.yolcu_rezid', 'left')->group_by("r.rez_id");
          $this->db->join('acente a', 'r.rez_acente = a.acente_id', 'left')->group_by("r.rez_acente");
          $this->db->join('turoperator tur', 'r.rez_turoperator = tur.turop_id', 'left')->group_by("r.rez_turoperator");
          $this->db->join('rezervasyonek ekuc', 'r.rez_id = ekuc.rezek_rezid', 'left')->group_by("r.rez_id");
          $this->db->join('gelirgider g', 'r.rez_id = g.gelirgider_rezid', 'left')->group_by("r.rez_id");
          $this->db->join("yolcuucus yu", "y.yolcu_id = yu.yucus_yolcuid", "left")->group_by("y.yolcu_id");
          $this->db->join("($subquery) uc", "yu.yucus_ucusid = uc.ucus_id", "left");
          $this->db->join("musteri mu", "r.rez_kaydeden = mu.musteri_id", "left");
          $this->db->where($aramaarr);
          $this->db->limit($limit);
          $query = $this->db->get();

          return $query->result();
     }

     public function kisayolarama($likes, $sayfano, $tip)
     {
          if($tip == "pnr" or $tip == "reznot") {
               $this->db->select("r.rez_id, r.rez_kayitno, r.rez_pnr, r.rez_not");
          }
          if($tip == "soyad" or $tip == "ad" or $tip == "extra" or $tip == "yolcunot") {
               $this->db->select("r.rez_id, r.rez_kayitno, r.rez_pnr, y.yolcu_ad, y.yolcu_soyad, y.yolcu_aciklama");
          }
          if($tip == "musteri") {
               $this->db->select("m.musteri_id, m.musteri_ad, m.musteri_soyad, m.musteri_dtarih, m.musteri_tel1, m.musteri_tel2");
          }
          if($tip != "musteri") {
               $this->db->from("rezervasyon r");
          }
          if($tip == "musteri") {
               $this->db->from("musteri m");
          }
          if($tip == "soyad" or $tip == "ad" or $tip == "extra" or $tip == "yolcunot") {
               $this->db->join('yolcu y', 'r.rez_id = y.yolcu_rezid', 'left')->group_by("r.rez_id");
          }
          $this->db->like($likes);
          $this->db->limit(20, $sayfano);
          $query = $this->db->get();
          if($query->num_rows() > 0) {
               return $query->result();
          }else{
               return null;
          }
     }

     public function ucustarih($yolcuid)
     {
          $this->db->select("yu.yucus_yolcuid, yu.yucus_ucusid, uc.ucus_tarih");
          $this->db->from("yolcuucus yu");
          $this->db->join("ucus uc", "yu.yucus_ucusid = uc.ucus_id", 'left')->group_by("yu.yucus_ucusid");
          $this->db->where("yu.yucus_yolcuid", $yolcuid);
          $this->db->limit(1, 0);
          $query = $this->db->get();

          if($query->num_rows() > 0) {
               echo json_decode(json_encode($query->result()), true)[ 0 ][ "ucus_tarih" ];
          }else {
               echo "0000-00-00";
          }
     }

     public function turop_list()
     {
          $this->db->select("turop_id as id, turop_ad as title");
          $this->db->from("turoperator");
          $query = $this->db->get();
          return $query->result();
     }

     public function extraucret_list()
     {
          $this->db->select("ekstraucret_id as id, ekstraucret_tr as title");
          $this->db->order_by("ekstraucret_id");
          $this->db->from("ekstraucrettanim");
          $query = $this->db->get();
          return $query->result();
     }

     public function acente_list()
     {
          $this->db->select("acente_id as id, acente_ad as title");
          $this->db->from("acente");
          $query = $this->db->get();
          return $query->result();
     }

     public function yonetici_list()
     {
          $this->db->select("yonetici_id as id, concat(yonetici_ad, ' ', yonetici_soyad) as title");
          $this->db->where(array("yonetici_onay" => "E"));
          $this->db->from("yonetici");
          $query = $this->db->get();
          return $query->result();
     }

     public function webapp_list()
     {
          $this->db->select("webapp_kod as id, webapp_isim as title");
          $this->db->where(array("webapp_onay" => "E"));
          $this->db->from("webapp");
          $query = $this->db->get();
          return $query->result();
     }

     public function rezervasyon_yil_list()
     {
          $this->db->from("rezervasyon");
          $this->db->select("rez_kayittarih");
          $sorgu = $this->db->get();

          $yillar = array();
          foreach ($sorgu->result() as $a) {
               $sonuchas = explode(" ", $a->rez_kayittarih);
               $yil = explode("-", $sonuchas[0]);
               if(!in_array($yil[0], $yillar)) {
                    array_push($yillar, $yil[0]);
               }
          }
          return $yillar;
     }

     public function musteri_email()
     {
          $this->db->from("musteri");
          $this->db->select("musteri_email1");
          $sorgu = $this->db->get();

          $email = array();
          foreach ($sorgu->result() as $a) {
               if($a->musteri_email1 != "") {
                    if(!in_array($a->musteri_email1, $email)) {
                         array_push($email, $a->musteri_email1);
                    }
               }
          }
          return $email;
     }

     public function rezervasyon_simple($id)
     {
          $this->db->select(
               "rz.*, rz.rez_kayittarih as kayittarih, ".
               "rz.rez_duzentarih as duzentarih, date_format(rz.rez_opsiyontarih, '%d/%m/%y %H:%i') as opsiyontarih, " .
               "concat(yo.yonetici_ad, ' ', yo.yonetici_soyad) as kayitedenyonetici, " .
               "concat(mu.musteri_ad, ' ', mu.musteri_soyad) as kayitedenmusteri, to.turop_ad as turoperator, " .
               "ac.acente_ad as acentead"
          );
          $this->db->where(array("rz.rez_id" => $id));
          $this->db->from("rezervasyon rz");
          $this->db->join("yonetici yo", "rz.rez_kaydeden = yo.yonetici_id", "left");
          $this->db->join("musteri mu", "rz.rez_kaydeden = mu.musteri_id", "left");
          $this->db->join("turoperator to", "rz.rez_turoperator = to.turop_id", "left");
          $this->db->join("acente ac", "rz.rez_acente = ac.acente_id", "left");
          $query = $this->db->get();
          return $query->first_row();
     }

     public function rezervasyon_simple_ucus($id)
     {
          $this->db->select(
               "uc.ucus_id, date_format(uc.ucus_tarih, '%d/%m/%y %a') as ucus_tarih, uc.ucus_kod, " .
               "uc.ucus_no, date_format(uc.ucus_varistarih, '%d/%m/%y %a') as ucus_varistarih, " .
               "Substr(uc.ucus_saat1, 1, 5) as ucus_saat1, Substr(uc.ucus_saat2, 1, 5) as ucus_saat2, " .
               "ai1.city as ai1name, ai2.city as ai2name, ai1.code as ai1airportcode, " .
               "Group_Concat(yo.yolcu_id) as yolcuad, al.name as alname, us.sinif_tanim ," .
               "ai2.code as ai2airportcode, yu.yucus_durum as ucusdurum"
          );
          $this->db->where(array("rz.rez_id" => $id));
          $this->db->from("ucus uc");
          $this->db->join("yolcuucus yu", "uc.ucus_id = yu.yucus_ucusid");
          $this->db->join("yolcu yo", "yu.yucus_yolcuid = yo.yolcu_id");
          $this->db->join("airports ai1", "uc.ucus_parkur1 = ai1.code");
          $this->db->join("airports ai2", "uc.ucus_parkur2 = ai2.code");
          $this->db->join("rezervasyon rz", "rz.rez_id = yo.yolcu_rezid");
          $this->db->join("airlines al", "uc.ucus_kod = al.code");
          $this->db->join("ucussinifad us", "yu.yucus_sinifid = us.sinif_id", "left");
          $this->db->group_by(
               "uc.ucus_id, uc.ucus_tarih, uc.ucus_kod, uc.ucus_no, uc.ucus_varistarih, uc.ucus_saat1, uc.ucus_saat2, "
               . "ai1.city, ai2.city, ai1.code, ai2.code, yu.yucus_durum, al.name, us.sinif_tanim"
          );
          $this->db->order_by("uc.ucus_tarih, uc.ucus_saat1, yo.yolcu_id");
          $query = $this->db->get();
          return $query->result();
     }

     public function rezervasyon_simple_yolcutotal($acenteid)
     {
          $this->db->select("yo.yolcu_id, yo.yolcu_cinsiyet, yo.yolcu_ad, yo.yolcu_soyad, date_format(yo.yolcu_dtarih, '%d/%m/%Y') as yolcu_dtarih, yo.yolcu_biletno, yo.yolcu_koltukfiyat, yo.yolcu_vergi, yo.yolcu_satisfiyat, yo.yolcu_id, yo.yolcu_tipi");
          $this->db->where(array("ac.acente_id" => $acenteid));
          $this->db->from("acente ac");
          // $this->db->join("acente ac", "rz.rez_acente = ac.acente_id", "left")->group_by("rz.rez_acente");
          $this->db->join("rezervasyon r", "ac.acente_id = r.rez_acente", "left");
          $this->db->join('yolcu yo', 'r.rez_id = yo.yolcu_rezid', 'left');
          $this->db->order_by("yo.yolcu_id");
          $query = $this->db->get();
          return $query->result();
     }

     public function rezervasyon_simple_yolcu($id)
     {
          $this->db->select("yo.yolcu_cinsiyet, yo.yolcu_ad, yo.yolcu_soyad, date_format(yo.yolcu_dtarih, '%d/%m/%Y') as yolcu_dtarih, yo.yolcu_biletno, yo.yolcu_koltukfiyat, yo.yolcu_vergi, yo.yolcu_satisfiyat, yo.yolcu_id, yo.yolcu_tipi");
          $this->db->where(array("yo.yolcu_rezid" => $id));
          $this->db->from("yolcu yo");
          $this->db->order_by("yo.yolcu_id");
          $query = $this->db->get();
          return $query->result();
     }

     public function rezervasyon_simple_ek($id)
     {
          $this->db->select("rezek_aciklama, rezek_tutar, rezek_durum");
          $this->db->where(array("rezek_rezid" => $id));
          $this->db->from("rezervasyonek");
          $query = $this->db->get();
          return $query->result();
     }

     public function rezervasyon_simple_gelirgider($id)
     {
          $this->db->select(
               "Round(AVG(IF((gelirgider_grup = 'g'), gelirgider_tutar, 0)), 2) as gelir, " .
               "Round(AVG(IF((gelirgider_grup = 'c'), gelirgider_tutar, 0)), 2) as gider"
          );
          $this->db->where(array("gelirgider_rezid" => $id));
          $this->db->from("gelirgider");
          $query = $this->db->get();
          return $query->first_row();
     }

     public function rezervasyon_toplam_gelirgider($acenteid)
     {
          $this->db->select("g.gelirgider_grup, g.gelirgider_tutar"
               // "SUM(IF((g.gelirgider_grup = 'g'), g.gelirgider_tutar, 0)) as gelir, " .
               // "SUM(IF((g.gelirgider_grup = 'c'), g.gelirgider_tutar, 0)) as gider, r.rez_id"
          );
          $this->db->where(array("ac.acente_id" => $acenteid));
          $this->db->from("acente ac");
          $this->db->join("rezervasyon r", "ac.acente_id = r.rez_acente", "left");
          $this->db->join("gelirgider g", "r.rez_id = g.gelirgider_rezid", "left")->group_by("r.rez_id");
          // $this->db->join('yolcu yo', 'r.rez_id = yo.yolcu_rezid', 'left');
          $query = $this->db->get();
          return $query->result();
     }

     public function rezervasyon_detail($id)
     {
          $this->db->select("rz.*, rz.rez_kayittarih as kayittarih, rz.rez_duzentarih as duzentarih, date_format(rz.rez_opsiyontarih, '%d/%m/%y %H:%i') as opsiyontarih, concat(yo.yonetici_ad, ' ', yo.yonetici_soyad) as kayitedenyonetici, concat(mu.musteri_ad, ' ', mu.musteri_soyad) as kayitedenmusteri, concat(mu.musteri_ad, ' ', mu.musteri_soyad) as musteriadsoyad, mu.musteri_tel1 as musteritel1, mu.musteri_tel2 as musteritel2, mu.musteri_gsm as  musterigsm, mu.musteri_email1 as musteriemail1, mu.musteri_email2 as musteriemail2, mu.musteri_cinsiyet as mustericinsiyet, mu.musteri_adres as musteriadres, mu.musteri_sehir, mu.musteri_ulke, to.turop_ad as turoperator, ac.acente_ad as acentead, ac.acente_kod as acentekod, ac.acente_id, ac.acente_tel1 as acentetel, ac.acente_gsm as acentegsm, ac.acente_email as acenteemail, ac.acente_sahipcinsiyet as acentecinsiyet, (ac.acente_sahipad + ' ' + ac.acente_sahipsoyad) as acenteadsoyad, ak.acentekul_kullanici as acentekullanici, COUNT(CASE WHEN r.rez_durum = 'OK' THEN 1 ELSE 0 END) as youlcucount");
          $this->db->where(array("rz.rez_id" => $id));
          $this->db->from("rezervasyon rz");
          $this->db->join("yonetici yo", "rz.rez_kaydeden = yo.yonetici_id", "left");
          $this->db->join("musteri mu", "rz.rez_kaydeden = mu.musteri_id", "left");
          $this->db->join("turoperator to", "rz.rez_turoperator = to.turop_id", "left");
          $this->db->join("acente ac", "rz.rez_acente = ac.acente_id", "left")->group_by("rz.rez_acente");
          $this->db->join("rezervasyon r", "ac.acente_id = r.rez_acente", "left")->group_by("ac.acente_id");
          $this->db->join('yolcu y', 'r.rez_id = y.yolcu_rezid', 'left')->group_by("r.rez_acente");
          $this->db->join('rezervasyonek ekuc', 'r.rez_id = ekuc.rezek_rezid', 'left')->group_by("r.rez_acente");
          $this->db->join('gelirgider g', 'r.rez_id = g.gelirgider_rezid', 'left')->group_by("r.rez_acente");
          $this->db->join("acentekullanici ak", "ac.acente_id = ak.acentekul_acenteid", "left");
          $query = $this->db->get();
          return $query->first_row();
     }

     public function rezervasyon_detail_contact($id)
     {
          $this->db->select("rc.*");
          $this->db->where(array("rc.contact_rezervation_id" => $id));
          $this->db->from("reservation_contact rc");
          $query = $this->db->get();
          return $query->first_row();
     }

     public function rezervasyon_detail_ucus($id)
     {
          return $this->rezervasyon_simple_ucus($id);
     }

     public function rezervasyon_detail_yolcu($id)
     {
          return $this->rezervasyon_simple_yolcu($id);
     }

     public function rezervasyon_detail_gelirgider( $id )
     {
          return $this->rezervasyon_simple_gelirgider($id);
     }

     public function flight_cupon( $id )
     {
          $this->db->select(
               "uc.ucus_id, date_format(uc.ucus_tarih, '%d/%m/%Y') as ucus_tarih, uc.ucus_kod, uc.ucus_no, " .
               "Substr(uc.ucus_saat1, 1, 5) as ucus_saat1, Substr(uc.ucus_saat2, 1, 5) as ucus_saat2, " .
               "ai1.name as ai1name, ai2.name as ai2name, yu.yucus_bagaj as bagaj, " .
               "CONCAT(yo.yolcu_ad, ' ', yo.yolcu_soyad) as yolcuad, yo.yolcu_cinsiyet as cinsiyet, " .
               "al.name as alname, rz.rez_turoperatorpnr as pnr, date_format(yo.yolcu_dtarih, '%d/%m/%Y') as dogumtarih, " .
               "rz.rez_pnr as ucusno, ac.acente_ad as acente, yo.yolcu_biletno as biletno, " .
               "ai2.code as ai2airportcode, yu.yucus_durum as ucusdurum"
          );
          $this->db->where(array( "yo.yolcu_id" => $id ));
          $this->db->from("ucus uc");
          $this->db->join("yolcuucus yu", "uc.ucus_id = yu.yucus_ucusid");
          $this->db->join("yolcu yo", "yu.yucus_yolcuid = yo.yolcu_id");
          $this->db->join("airports ai1", "uc.ucus_parkur1 = ai1.code");
          $this->db->join("airports ai2", "uc.ucus_parkur2 = ai2.code");
          $this->db->join("rezervasyon rz", "rz.rez_id = yo.yolcu_rezid");
          $this->db->join("airlines al", "uc.ucus_kod = al.code");
          $this->db->join("acente ac", "rz.rez_acente = ac.acente_id", "left");
          $query = $this->db->get();

          return $query->result();
     }

     public function flight_ticket( $id )
     {
          $this->db->select(
               "uc.ucus_id, date_format(uc.ucus_tarih, '%d/%m/%Y') as ucus_tarih, uc.ucus_kod, uc.ucus_no, " .
               "Substr(uc.ucus_saat1, 1, 5) as ucus_saat1, Substr(uc.ucus_saat2, 1, 5) as ucus_saat2, " .
               "ai1.name as ai1name, ai2.name as ai2name, yu.yucus_bagaj as bagaj, " .
               "CONCAT(yo.yolcu_ad, ' ', yo.yolcu_soyad) as yolcuad, yo.yolcu_cinsiyet as cinsiyet, " .
               "al.name as alname, rz.rez_turoperatorpnr as pnr, date_format(yo.yolcu_dtarih, '%d/%m/%Y') as dogumtarih, " .
               "rz.rez_pnr as ucusno, ac.acente_ad as acente, yo.yolcu_biletno as biletno, " .
               "ai2.code as ai2airportcode, yu.yucus_durum as ucusdurum, date_format(rz.rez_kayittarih, '%d/%m/%Y %H:%i:%s') as reztarih"
          );
          $this->db->where(array( "yo.yolcu_id" => $id ));
          $this->db->from("ucus uc");
          $this->db->join("yolcuucus yu", "uc.ucus_id = yu.yucus_ucusid");
          $this->db->join("yolcu yo", "yu.yucus_yolcuid = yo.yolcu_id");
          $this->db->join("airports ai1", "uc.ucus_parkur1 = ai1.code");
          $this->db->join("airports ai2", "uc.ucus_parkur2 = ai2.code");
          $this->db->join("rezervasyon rz", "rz.rez_id = yo.yolcu_rezid");
          $this->db->join("airlines al", "uc.ucus_kod = al.code");
          $this->db->join("acente ac", "rz.rez_acente = ac.acente_id", "left");
          $query = $this->db->get();

          return $query->result();
     }

     public function dashboard_opsionlist( $limit )
     {
          $this->db->from("rezervasyon");
          $this->db->where(array( 'rez_durum' => 'OP' ));
          $count = $this->db->count_all_results();

          $this->db->select("r.rez_id, r.rez_kayitno, r.rez_turoperatorpnr, ac.acente_ad, tur.turop_ad, mu.musteri_tel1 as telefon, r.rez_opsiyontarih, y.yolcu_satisfiyat AS sum_tutar, (y.yolcu_satisfiyat - Round(AVG(IF((g.gelirgider_grup = 'g'), g.gelirgider_tutar, 0)), 2) + Round(AVG(IF((g.gelirgider_grup = 'c'), g.gelirgider_tutar, 0)), 2)) as kalan");
          $this->db->from("rezervasyon r");
          $this->db->join('yolcu y', 'r.rez_id = y.yolcu_rezid', 'left')->group_by("r.rez_id");
          $this->db->join('acente ac', 'r.rez_acente = ac.acente_id', 'left')->group_by("r.rez_acente");
          $this->db->join('turoperator tur', 'r.rez_turoperator = tur.turop_id', 'left')->group_by("r.rez_turoperator");
          $this->db->join('gelirgider g', 'r.rez_id = g.gelirgider_rezid', 'left')->group_by("r.rez_id");
          $this->db->join("musteri mu", "r.rez_kaydeden = mu.musteri_id", "left");
          $this->db->where(array( 'rez_durum' => 'OP' ));
          $this->db->order_by('rez_id', 'DESC');
          $this->db->limit($limit[ 1 ], $limit[ 0 ]);
          $query = $this->db->get();

          return array(
               "count_all" => $count, "result" => $query->result()
          );
     }

     public function dashboard_cancellist( $limit )
     {
          $this->db->from("rezervasyon");
          $this->db->where(array( 'rez_durum' => 'CA' ));
          $count = $this->db->count_all_results();

          $this->db->select("r.rez_id, r.rez_kayitno, r.rez_turoperatorpnr, ac.acente_ad, tur.turop_ad, mu.musteri_tel1 as telefon, r.rez_opsiyontarih, y.yolcu_satisfiyat AS sum_tutar");
          $this->db->from("rezervasyon r");
          $this->db->join('yolcu y', 'r.rez_id = y.yolcu_rezid', 'left')->group_by("r.rez_id");
          $this->db->join('acente ac', 'r.rez_acente = ac.acente_id', 'left')->group_by("r.rez_acente");
          $this->db->join('turoperator tur', 'r.rez_turoperator = tur.turop_id', 'left')->group_by("r.rez_turoperator");
          $this->db->join("musteri mu", "r.rez_kaydeden = mu.musteri_id", "left");
          $this->db->where(array( 'rez_durum' => 'CA' ));
          $this->db->order_by('rez_id', 'DESC');
          $this->db->limit($limit[ 1 ], $limit[ 0 ]);
          $query = $this->db->get();

          return array(
               "count_all" => $count, "result" => $query->result()
          );
     }

     public function dashboard_waitlist( $limit )
     {
          $this->db->from("rezervasyon");
          $this->db->where(array( 'rez_durum' => 'WA' ));
          $count = $this->db->count_all_results();

          $this->db->select("r.rez_id, r.rez_kayitno, r.rez_turoperatorpnr, ac.acente_ad, tur.turop_ad, mu.musteri_tel1 as telefon, r.rez_opsiyontarih, y.yolcu_satisfiyat AS sum_tutar");
          $this->db->from("rezervasyon r");
          $this->db->join('yolcu y', 'r.rez_id = y.yolcu_rezid', 'left')->group_by("r.rez_id");
          $this->db->join('acente ac', 'r.rez_acente = ac.acente_id', 'left')->group_by("r.rez_acente");
          $this->db->join('turoperator tur', 'r.rez_turoperator = tur.turop_id', 'left')->group_by("r.rez_turoperator");
          $this->db->join("musteri mu", "r.rez_kaydeden = mu.musteri_id", "left");
          $this->db->where(array( 'rez_durum' => 'WA' ));
          $this->db->order_by('rez_id', 'DESC');
          $this->db->limit($limit[ 1 ], $limit[ 0 ]);
          $query = $this->db->get();

          return array(
               "count_all" => $count, "result" => $query->result()
          );
     }

    public function insert($data)
    {
//        return $this->db->set($data)->get_compiled_insert('rezervasyon');
        $this->db->insert('rezervasyon',$data);
        return $this->db->insert_id();
    }

}
