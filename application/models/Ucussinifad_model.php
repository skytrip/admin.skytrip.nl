<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 21.11.2018
 * Time: 02:20
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Ucussinifad_model extends CI_Model
{
    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("ucussinifad");
        $this->db->where("sinif_durum",'A');
        $this->db->order_by('sinif_tanim','ASC');
        $query = $this->db->get();
        return $query->result();
    }
}