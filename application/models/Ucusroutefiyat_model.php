<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 21.11.2018
 * Time: 02:18
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Ucusroutefiyat_model extends CI_Model
{

    public function insert($data)
    {
        $this->db->insert('ucusroutefiyat',$data);
        return $this->db->insert_id();
    }

    public function getByRoute($route_id)
    {
        $this->db->select('*');
        $this->db->from('ucusroutefiyat');
        $this->db->where('ucusfiyat_routeid',$route_id);
        $query = $this->db->get();
        return $query->row();
    }

    public function get($id)
    {
        $this->db->select('*');
        $this->db->from('ucusroutefiyat');
        $this->db->where('ucusfiyat_id',$id);
        $query = $this->db->get();
        return $query->row();
    }

}