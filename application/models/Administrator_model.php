<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator_model extends CI_Model {
    
    public function get_yonetici($username, $password) {
        $this->db->select("*");
        $this->db->where(array("yonetici_kullanici" => $username));
        $this->db->from("yonetici");
        $result = $this->db->get();
        $result_object = $result->row_object();
        if ($result_object->yonetici_sifre == sha1($password)) {
            return $result_object;
        }
        else
        {
            return null;
        }
    }
    
    public function admin_list() {
        $this->db->select("*");
        $this->db->from("yonetici");
        $result = $this->db->get();
        return $result->result();
    }
    
    
    
}