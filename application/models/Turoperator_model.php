<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 25.11.2018
 * Time: 05:35
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Turoperator_model extends CI_Model
{

    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("turoperator");
        $this->db->order_by('turop_ad','ASC');
        $query = $this->db->get();
        return $query->result();
    }
}