<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mongolog_model extends CI_Model
{

    private $clientSettings;
    private $mongodb;
    private $mongodbsearch;
    private $mongo;
    private $manager;

    public function __construct()
    {
        parent::__construct();
        $this->clientSettings = $this->config->item('mongosettings');
        $this->manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
        $this->mongo = new MongoDB\Client($this->clientSettings["dsn"]);
        $this->mongodb = $this->mongo->selectDatabase($this->clientSettings["database"]);
        $this->mongodbsearch = $this->mongo->selectDatabase($this->clientSettings["databasesearcg"]);
    }

    public function get_data()
    {
        $collection = new MongoDB\Client($this->mongodb, $this->clientSettings["document"]);
        return $collection->find();
    }

    public function get_pagdata($limit = null,$start = null,$draw = null,$aramax,$sortx)
    {
        $collection = new MongoDB\Collection($this->manager, $this->mongodb, $this->clientSettings["document"]);

        $anaarama = array();
        $sort = array('detaylar.trnxtime' => -1);

        //   array('$match' => array('SERVICE_NAME' => 'SUNEX-FlightSearch')),
        if(empty($aramax)) {
            $anaarama = array('TRNX_ID' => array('$gt' => ''));
        }else{
            $anaarama = $aramax;
            // print_r($anaarama);
        }
        if(empty($sortx)) {
            $sort = array('detaylar.trnxtime' => -1);
        }else{
            $sort = $sortx;
            // print_r($anaarama);
        }
        // $time = new \MongoDB\BSON\UTCDateTime(strtotime(str_replace('/', '-', '14-11-2016')));
        // $time2 = new \MongoDB\BSON\UTCDateTime(strtotime(str_replace('/', '-', '14-11-2016 +1 day')));
        $result = $collection->aggregate(
            array(
                array('$match' => $anaarama),//1479164400000$anaarama
                array('$group' => array(
                    '_id' => '$TRNX_ID',
                    'detaylar' => array(
                        '$push' => array(
                            'servicename' => '$SERVICE_NAME',
                            'error' => '$ERROR_STATU',
                            'trnxtime' => '$TRNX_TIME',
                            'trnx_year' => array('$year' => '$TRNX_TIME'),
                            'trnx_month' => array('$month' => '$TRNX_TIME'),
                            'trnx_day' => array('$dayOfMonth' => '$TRNX_TIME'),
                            'requestfile' => '$SERVICE_REQUEST_FILE_ID',
                            'responsefile' => '$SERVICE_RESPONSE_FILE_ID'
                        )
                    ),
                    'detaycount' => array('$sum' => 1))
                ),
                array('$sort' => $sort),
                array('$skip' => intval($start)),
                array('$limit' => intval($limit))
            ), ["useCursor" => false]
        );

        $resultarr = array();
        $a = $start + 1;
        foreach ($result as $value) {
            $detaylar = array();

            $i = -1;
            foreach ($value->detaylar as $val) {
                $i++;

                $detaylar[$i]["servicename"] = $val->servicename;
                $detaylar[$i]["error"] = $val->error;
                // $detaylar[$i]["trnx_ham"] = (string) new MongoDB\BSON\UTCDateTime($val->trnxtime);
                $detaylar[$i]["trnx_year"] = $val->trnx_year;
                $detaylar[$i]["trnx_month"] = $val->trnx_month;
                $detaylar[$i]["trnx_day"] = $val->trnx_day;
                $detaylar[$i]["requestfile"] = (string) new MongoDB\BSON\ObjectId($val->requestfile);
                $detaylar[$i]["responsefile"] = (string) new MongoDB\BSON\ObjectId($val->responsefile);
            }

            array_push(
                $resultarr, array(
                    "trnxid" => $value->_id,
                    "detaylar" => $detaylar,
                    "detaycount" => $value->detaycount,
                    "rownum" => $a++,
                    "sayfarowbaslangic" => $start,
                    "sayfarowlength" => $limit
                )
            );
        }

        $resultcount = $collection->aggregate(
            array(
                array('$match' => $anaarama),
                array('$group' => array(
                    '_id' => '$TRNX_ID',
                    'count' => array('$sum' => 1))
                )
            ), ["useCursor" => false]
        );

        $resultcountnum = 0;
        foreach ($resultcount as $value) {
            $resultcountnum = $resultcountnum + 1;
        }

        return '{"data": ' . json_encode($resultarr) . ',"draw":' . $draw . ',"recordsFiltered":' . $resultcountnum . ',"recordsTotal":' . $resultcountnum . '}';
    }

    public function get_pagdatasearch($limit = null, $start = null, $draw = null, $aramax, $sortx)
    {
        $collection = new MongoDB\Collection($this->manager, "searchResults", $this->clientSettings["document"]);

        $day1 = date("Y-01-01");
        $day2 = date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day'));

        if(empty($aramax)) {
            $anaarama = array('TRNX_ID' => array('$gt' => ''), 'INSERT_DATE' => array('$gte' => new \MongoDB\BSON\UTCDateTime(strtotime($day1) * 1000), '$lte' => new \MongoDB\BSON\UTCDateTime(strtotime($day2) * 1000)));
        }else{
            $anaarama = $aramax;
        }
        $result = $collection->aggregate(
            array(
                array('$match' => $anaarama),
                array('$group' =>array('_id' => array(
                    'trnx_id' => '$TRNX_ID',
                    'trnx_year' => array('$year' => '$INSERT_DATE'),
                    'trnx_month' => array('$month' => '$INSERT_DATE'),
                    'trnx_day' => array('$dayOfMonth' => '$INSERT_DATE'),
                    'trnx_hour' => array('$hour' => '$INSERT_DATE'),
                    'trnx_minute' => array('$minute' => '$INSERT_DATE'),
                    'trnx_second' => array('$second' => '$INSERT_DATE'),
                    'searchresult' => '$SEARCH_RESULT_JSON',
                    'insertdate' => '$INSERT_DATE'
                ))),
                array('$group' => array('_id' => array('trnx_year' => '$_id.trnx_year', 'trnx_month' => '$_id.trnx_month', 'trnx_day' => '$_id.trnx_day', 'trnx_hour' => '$_id.trnx_hour', 'trnx_minute' => '$_id.trnx_minute', 'trnx_second' => '$_id.trnx_second', 'searchresult' => '$_id.searchresult', 'insertdate' => '$_id.insertdate'),'count' => array('$sum' => 1))),
                array('$sort' => $sortx),//array('_id.insertdate' => -1)
                array('$skip' => intval($start)),
                array('$limit' => intval($limit))
            ), ["useCursor" => false, "allowDiskUse" => true]
        );

        $resultcount = $collection->aggregate(
            array(
                array('$match' => $anaarama),
                array('$group' => array(
                    '_id' => '$TRNX_ID',
                    'count' => array('$sum' => 1))
                )
            ), ["useCursor" => false]
        );

        $resultcountnum = 0;
        foreach ($resultcount as $value) {
            $resultcountnum = $resultcountnum + 1;
        }

        $resultarr = array();
        $r = $resultcountnum - $start + 1;
        foreach ($result as $value) {
            $r--;
            $value->rownum = $r;
            array_push($resultarr, $value);
        }

        //    return $resultarr;
        return array(
            "result" => $resultarr,
            "draw" => $draw,
            "recordsFiltered" => $resultcountnum,
            "recordsTotal" => $resultcountnum
        );
    }

    public function get_trnx_group_daily_counts($day1, $day2 = null)
    {
        $collection = new MongoDB\Collection($this->manager, $this->mongodb, $this->clientSettings["document"]);

        if($day2 == null) {
            $day2 = date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day'));
        }else{
            $day2 = date("Y-m-d", strtotime($day2 . ' +1 day'));
        }
        $result = $collection->aggregate(
            array(
                array('$match' => array('TRNX_TIME' => array('$gte' => new \MongoDB\BSON\UTCDateTime(strtotime($day1) * 1000), '$lte' => new \MongoDB\BSON\UTCDateTime(strtotime($day2) * 1000)))),
                array('$group' =>array('_id' => array(
                    'trnx_id' => '$TRNX_ID',
                    'trnx_year' => array('$year' => '$TRNX_TIME'),
                    'trnx_month' => array('$month' => '$TRNX_TIME'),
                    'trnx_day' => array('$dayOfMonth' => '$TRNX_TIME')
                ))),
                array('$group' => array('_id' => array('trnx_year' => '$_id.trnx_year', 'trnx_month' => '$_id.trnx_month', 'trnx_day' => '$_id.trnx_day'),'count' => array('$sum' => 1))),
                array('$sort' => array('_id.trnx_year' => 1, '_id.trnx_month' => 1, '_id.trnx_day' => 1))
            ), ["useCursor" => false]
        );
        // print_r($result[0]);//->count
        $resultarr = array();
        foreach ($result as $value) {
            //    print_r($value);
            array_push($resultarr, $value);
        }

        return $resultarr;
    }

    public function get_trnx_group_sorgu($day1, $day2 = null)
    {
        $collection = new MongoDB\Collection($this->manager, "searchResults", $this->clientSettings["document"]);
        // print_r($collection);
        if($day2 == null) {
            $day2 = date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day'));
        }else{
            $day2 = date("Y-m-d", strtotime($day2 . ' +1 day'));
        }
        $result = $collection->aggregate(
            array(
                array('$match' => array('INSERT_DATE' => array('$gte' => new \MongoDB\BSON\UTCDateTime(strtotime($day1) * 1000), '$lte' => new \MongoDB\BSON\UTCDateTime(strtotime($day2) * 1000)))),
                array('$group' =>array('_id' => array(
                    'trnx_id' => '$TRNX_ID',
                    'trnx_year' => array('$year' => '$INSERT_DATE'),
                    'trnx_month' => array('$month' => '$INSERT_DATE'),
                    'trnx_day' => array('$dayOfMonth' => '$INSERT_DATE'),
                    'trnx_hour' => array('$hour' => '$INSERT_DATE'),
                    'trnx_minute' => array('$minute' => '$INSERT_DATE'),
                    'trnx_second' => array('$second' => '$INSERT_DATE'),
                    'searchresult' => '$SEARCH_RESULT_JSON',
                    'insertdate' => '$INSERT_DATE'
                ))),
                array('$group' => array('_id' => array('trnx_year' => '$_id.trnx_year', 'trnx_month' => '$_id.trnx_month', 'trnx_day' => '$_id.trnx_day', 'trnx_hour' => '$_id.trnx_hour', 'trnx_minute' => '$_id.trnx_minute', 'trnx_second' => '$_id.trnx_second', 'searchresult' => '$_id.searchresult', 'insertdate' => '$_id.insertdate'),'count' => array('$sum' => 1))),
                array('$sort' => array('_id.insertdate' => -1)),
                array('$skip' => 0),
                array('$limit' => 10)
            ), ["useCursor" => false, "allowDiskUse" => true]
        );
        $resultarr = array();
        foreach ($result as $value) {
            array_push($resultarr, $value);
        }

        return $resultarr;
    }

    public function get_trnx_last15_days()
    {
        $time = strtotime(date("Y-m-d") . " -15days");
        $collection = new MongoDB\Collection($this->manager, $this->mongodb, $this->clientSettings["document"]);

        $result = $collection->aggregate(
            array(
                array('$match' => array('TRNX_TIME' => array('$gt' => new MongoDB\BSON\UTCDateTime($time * 1000)))),
                array('$group' => array('_id' => array(
                    'trnx_id' => '$TRNX_ID',
                    'trnx_year' => array('$year' => '$TRNX_TIME'),
                    'trnx_month' => array('$month' => '$TRNX_TIME'),
                    'trnx_day' => array('$dayOfMonth' => '$TRNX_TIME')
                ))),
                array('$group' => array('_id' => array( 'trnx_year' => '$_id.trnx_year', 'trnx_month' => '$_id.trnx_month', 'trnx_day' => '$_id.trnx_day' ), 'count' => array('$sum' => 1))),
                array('$sort' => array('_id.trnx_year' => 1, '_id.trnx_month' => 1, '_id.trnx_day' => 1))
            ), ["useCursor" => false]
        );

        $resultarr = array();

        foreach ($result as $value) {
            array_push($resultarr, $value->count);
        }

        return $resultarr;
    }

    public function get_trnx_last30_days_sum()
    {
        $time = strtotime(date("Y-m-d") . " -1month");
        $collection = new MongoDB\Collection($this->manager, $this->mongodb, $this->clientSettings["document"]);
        return $collection->aggregate(
            array(
                array('$match' => array('TRNX_TIME' => array('$gt' => new MongoDB\BSON\UTCDateTime($time * 1000))) ),
                array('$group' => array('_id' => '$TRNX_ID', 'count' => array('$sum' => 1))),
                array('$group' => array('_id' => 1, 'count' => array('$sum' => '$count')))
            ), ["useCursor" => false]
        );
    }

    public function get_trnx_group_monthly_counts($month1, $month2 = null)
    {
        $collection = new MongoDB\Collection($this->manager, $this->mongodb, $this->clientSettings["document"]);

        if($month2 == null) {
            $month2 = date("Y-m-31");
        }else{
            $month2 = date("Y-m-31", strtotime($month2));
        }

        $result = $collection->aggregate(
            array(
                array('$match' =>
                    array('TRNX_TIME' => array('$gte' => new \MongoDB\BSON\UTCDateTime(strtotime($month1) * 1000), '$lte' => new \MongoDB\BSON\UTCDateTime(strtotime($month2) * 1000)))
                ),
                array('$group' =>array('_id' => array(
                    'trnx_id' => '$TRNX_ID',
                    'trnx_year' => array('$year' => '$TRNX_TIME'),
                    'trnx_month' => array('$month' => '$TRNX_TIME')
                ))),
                array('$group' =>
                    array('_id' => array('trnx_year' => '$_id.trnx_year', 'trnx_month' => '$_id.trnx_month'),'count' => array('$sum' => 1))),array('$sort' => array('_id.trnx_year' => 1, '_id.trnx_month' => 1, '_id.trnx_day' => 1))), ["useCursor" => false]
        );
        // print_r($result[0]);//->count
        $resultarr = array();
        foreach ($result as $value) {
            //    print_r($value);
            array_push($resultarr, $value);
        }

        return $resultarr;
    }

    public function get_trnx_yearly_processed_calendar()
    {
        $time = strtotime(date("Y-01-01"));//değiştirildi = date("Y-01-01")
        $collection = new \MongoDB\Collection($this->manager, $this->mongodb, $this->clientSettings["document"]);
        $result = $collection->aggregate(
            array(
                array('$match' => array('TRNX_TIME' => array('$gt' => new MongoDB\BSON\UTCDateTime($time * 1000)))),
                array('$group' => array('_id' => array(
                    'trnx_id' => '$TRNX_ID',
                    'trnx_year' => array('$year' => '$TRNX_TIME'),
                    'trnx_month' => array('$month' => '$TRNX_TIME'),
                    'trnx_day' => array('$dayOfMonth' => '$TRNX_TIME')
                ))),
                array('$group' => array('_id' => array(
                    'trnx_year' => '$_id.trnx_year',
                    'trnx_month' => '$_id.trnx_month',
                    'trnx_day' => '$_id.trnx_day'),
                    'count' => array('$sum' => 1),
                    'procount' => ['$sum' =>['$cond' => [['$or' =>[['$eq' => ['$SERVICE_NAME', 'applyBook']],['$eq' => ['$_id.operator', 'Booking']],]], 1,0]]])
                ),
                array('$sort' => array('_id.trnx_year' => 1, '_id.trnx_month' => 1, '_id.trnx_day' => 1))
            ),
            ['useCursor'=>false]
        );
        return $result;
    }
}
