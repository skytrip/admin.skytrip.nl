<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Model {

    private $ajax = "#index.php/";

    public function get() {
        return array(
            "dashboard" => array(
                "title" => "Dashboard",
                "icon" => "fa-home",
                "url" => $this->ajax."dashboard"
            ),
            "reservation" => array(
                "title" => "Reservation",
                "icon" => "fa-calendar",
                "url" => $this->ajax."reservation",
                "sub" => array(
                    "analytics" => array(
                        "title" => "Add Reservation",
                        "url" => $this->ajax."reservation/addReservation"
                    ),
                ),
            ),
            "statistics" => array(
                "title" => "Statistics",
                "icon" => "fa-bar-chart",
                // "icon" => "fa fa-line-chart",
                "url" => $this->ajax."statistic"
            ),
            "searchresults" => array(
                "title" => "Search Results",
                "icon" => "fa-list-alt",
                // "icon" => "fa fa-line-chart",
                "url" => $this->ajax."searchresults"
            ),
            "webservicelog" => array(
                "title" => "Web Service Logs",
                "icon" => "fa-list-alt",
                // "icon" => "fa fa-line-chart",
                "url" => $this->ajax."webservicelog"
            ),
            "routes" => array(
                "title" => "Routes",
                "icon" => "fa-plane",
                // "icon" => "fa fa-line-chart",
                "url" => $this->ajax."routes",

                "sub" => array(
                    "analytics" => array(
                        "title" => "Add Route",
                        "url" => $this->ajax."routes/addRoute"
                    ),
//                       "marketing" => array(
//                           "title" => "Sell Route",
//                           "url" => $this->ajax."routes/sellRoute"
//                       ),
                )
            ),
        );
    }

}
