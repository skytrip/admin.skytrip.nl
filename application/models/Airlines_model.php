<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 3.12.2018
 * Time: 02:31
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Airlines_model extends CI_Model
{
    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("airlines");
        $this->db->where("active",'Y');
        $this->db->order_by('name','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getByKey($key)
    {
        $sql = "SELECT ap.code AS id,concat(ap.name,' - ',ap.country) AS text FROM airlines ap WHERE (code like '%$key%' OR name like '%$key%') AND active='Y'";
        $query = $this->db->query($sql);
        return $query->result();
    }

}