<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 3.12.2018
 * Time: 02:31
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Airports_model extends CI_Model
{
    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("airports");
        $this->db->order_by('country,name','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllExistsRoute()
    {
        $this->db->select("ap.code,ap.country,ap.name,ap.countrycode");
        $this->db->from("airports ap");
        $this->db->join("ucus u","u.ucus_parkur1=ap.code");
        $this->db->where("u.ucus_tarih >=",date('Y-m-d'));
        $this->db->order_by('ap.country,ap.name','ASC');
        $this->db->group_by('ap.code,ap.country,ap.name,ap.countrycode');
        $query = $this->db->get();
        return $query->result();
    }

    public function getGidis()
    {
        $this->db->select("ap.code,ap.city,ap.country,ap.name,ap.countrycode,DATE_FORMAT(ucus_tarih,'%Y-%m-%d') AS ucus_tarih");
        $this->db->from("airports ap");
        $this->db->join("ucus u","u.ucus_parkur1=ap.code");
        $this->db->join('ucussegment us','us.ucus_id=u.ucus_id');
        $this->db->join('ucusroute ur','us.route_id=ur.route_id');
        $this->db->where("u.ucus_tarih >=",date('Y-m-d'));
        $this->db->where("ap.countrycode is not null");
        $this->db->order_by('ucus_tarih,ap.name','ASC');
        $this->db->group_by('ap.code,ap.country,ap.name,ap.countrycode');
        $query = $this->db->get();
        return $query->result();
    }


    public function getVaris()
    {
        $this->db->select("ap.code,ap.country,ap.name,ap.countrycode,DATE_FORMAT(ucus_tarih,'%Y-%m') AS ucus_tarih");
        $this->db->from("airports ap");
        $this->db->join("ucus u","u.ucus_parkur2=ap.code");
        $this->db->join('ucussegment us','us.ucus_id=u.ucus_id');
        $this->db->join('ucusroute ur','us.route_id=ur.route_id');
        $this->db->where("u.ucus_tarih >=",date('Y-m-d'));
        $this->db->where("ap.countrycode is not null");
        $this->db->order_by('ap.country,ap.name','ASC');
        $this->db->group_by('ap.code,ap.country,ap.name,ap.countrycode');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllByParkurForVaris($parkur)
    {
        $this->db->select("ap.code,ap.city,ap.country,ap.name,ap.countrycode,DATE_FORMAT(ucus_tarih,'%Y-%m-%d') AS ucus_tarih");
        $this->db->from("airports ap");
        $this->db->join("ucus u","u.ucus_parkur2=ap.code AND u.ucus_parkur1='$parkur'");
        $this->db->join('ucussegment us','us.ucus_id=u.ucus_id');
        $this->db->join('ucusroute ur','us.route_id=ur.route_id');
        $this->db->where("u.ucus_tarih >=",date('Y-m-d'));
        $this->db->where("ap.countrycode is not null");
        $this->db->order_by('ucus_tarih,ap.name','ASC');
        $this->db->group_by('ap.code,ap.country,ap.name,ap.countrycode');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllByParkurForDonus($parkur,$varis)
    {
        $this->db->select("ap.code,ap.city,ap.country,ap.name,ap.countrycode,DATE_FORMAT(ucus_tarih,'%Y-%m-%d') AS ucus_tarih");
        $this->db->from("airports ap");
        $this->db->join("ucus u","u.ucus_parkur2=ap.code");
        $this->db->join('ucussegment us','us.ucus_id=u.ucus_id');
        $this->db->join('ucusroute ur','us.route_id=ur.route_id');
        $this->db->where("u.ucus_tarih >=",date('Y-m-d'));
        $this->db->where("u.ucus_parkur1",$parkur);
        $this->db->where("u.ucus_parkur2",$varis);
        $this->db->where("ap.countrycode is not null");
        $this->db->order_by('ucus_tarih,ap.name','ASC');
        $this->db->group_by('ap.code,ap.country,ap.name,ap.countrycode');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllByCountryForDonus($parkur,$c1,$varis,$c2)
    {
        $this->db->select("ap.code,ap.city,ap.country,ap.name,ap.countrycode,DATE_FORMAT(ucus_tarih,'%Y-%m-%d') AS ucus_tarih");
        $this->db->from("airports ap");
        $this->db->join("ucus u","u.ucus_parkur1=ap.code");
        $this->db->join('ucussegment us','us.ucus_id=u.ucus_id');
        $this->db->join('ucusroute ur','us.route_id=ur.route_id');
        $this->db->where("u.ucus_tarih >=",date('Y-m-d'));
        if($c1){
            $this->db->where("u.ucus_parkur2 in(SELECT code FROM airports WHERE countrycode=". $this->db->escape(str_replace('country-','',$parkur)) .")");
        }else{
            $this->db->where("u.ucus_parkur2 in(SELECT code FROM airports WHERE countrycode in(SELECT countrycode FROM airports WHERE code=".$this->db->escape($parkur)." ) )");
        }
        if($c2){
            $this->db->where("u.ucus_parkur1 in(SELECT code FROM airports WHERE countrycode=". $this->db->escape(str_replace('country-','',$varis)) .")");
        }else{
            $this->db->where("u.ucus_parkur1 in(SELECT code FROM airports WHERE countrycode in(SELECT countrycode FROM airports WHERE code=".$this->db->escape($varis)." ) )");
        }
        $this->db->where("ap.countrycode is not null");
        $this->db->order_by('ucus_tarih,ap.name','ASC');
        $this->db->group_by('ap.code,ap.country,ap.name,ap.countrycode');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllByCountryForDonusVaris($parkur,$c1,$varis,$c2)
    {
        $this->db->select("ap.code,ap.city,ap.country,ap.name,ap.countrycode,DATE_FORMAT(ucus_tarih,'%Y-%m-%d') AS ucus_tarih");
        $this->db->from("airports ap");
        $this->db->join("ucus u","u.ucus_parkur2=ap.code");
        $this->db->join('ucussegment us','us.ucus_id=u.ucus_id');
        $this->db->join('ucusroute ur','us.route_id=ur.route_id');
        $this->db->where("u.ucus_tarih >=",date('Y-m-d'));
        if($c1){
            $this->db->where("u.ucus_parkur1 in(SELECT code FROM airports WHERE countrycode=". $this->db->escape(str_replace('country-','',$parkur)) .")");
        }else{
            $this->db->where("u.ucus_parkur1 in(SELECT code FROM airports WHERE countrycode in(SELECT countrycode FROM airports WHERE code=".$this->db->escape($parkur)." ) )");
        }
        if($c2){
            $this->db->where("u.ucus_parkur2 in(SELECT code FROM airports WHERE countrycode=". $this->db->escape(str_replace('country-','',$varis)) .")");
        }else{
            $this->db->where("u.ucus_parkur2 in(SELECT code FROM airports WHERE countrycode in(SELECT countrycode FROM airports WHERE code=".$this->db->escape($varis)." ) )");
        }
        $this->db->where("ap.countrycode is not null");
        $this->db->order_by('ucus_tarih,ap.name','ASC');
        $this->db->group_by('ap.code,ap.country,ap.name,ap.countrycode');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllByCountryForVaris($country)
    {
        $this->db->select("ap.code,ap.city,ap.country,ap.name,ap.countrycode,DATE_FORMAT(u.ucus_tarih,'%Y-%m-%d') AS ucus_tarih");
        $this->db->from("airports ap");
        $this->db->join("ucus u","u.ucus_parkur2=ap.code");
        $this->db->join('ucussegment us','us.ucus_id=u.ucus_id');
        $this->db->join('ucusroute ur','us.route_id=ur.route_id');
        $this->db->where("u.ucus_tarih >=",date('Y-m-d'));
        $this->db->where("u.ucus_parkur1 in(SELECT code FROM airports WHERE countrycode=". $this->db->escape($country) .")");
        $this->db->order_by('ucus_tarih,ap.name','ASC');
        $this->db->group_by('ap.code,ap.country,ap.name,ap.countrycode');
        $query = $this->db->get();
        return $query->result();
    }

    public function getByKod($kod,$byCountry = false)
    {
        $this->db->select("*");
        $this->db->from("airports ap");
        if($byCountry){
            $this->db->where("code",$kod);
        }else{
            $this->db->where("code",$kod);
        }
        $query = $this->db->get();
        return $query->row();
    }

    public function getByKey($key)
    {
        $sql = "SELECT ap.code AS id,ap.country,concat(ap.name,' - ',ap.city) AS text,ap.countrycode FROM airports ap WHERE (code like '%$key%' OR name like '%$key%') AND countrycode is not null";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getCountry($key)
    {
        $this->db->select("ap.country");
        $this->db->from("airports ap");
        $this->db->where("countrycode",$key);
        $query = $this->db->get();
        return $query->row()->country;
    }

}