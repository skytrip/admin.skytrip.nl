<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_datas extends CI_Model {

    public function passenger_count($day1 = null, $day2 = null) {
        $this->db->select("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d') as reztarih, count(yolcu.yolcu_id) as yolcu_count");
        $this->db->from('yolcu');
        $this->db->join('rezervasyon', 'yolcu.yolcu_rezid = rezervasyon.rez_id');
        if($day1 != null && $day2 != null){
             $this->db->where("rezervasyon.rez_kayittarih >=", $day1);
             $this->db->where("rezervasyon.rez_kayittarih <=", $day2);
        }else{
             $this->db->where("rezervasyon.rez_kayittarih >=", date("Y-m-01"));
        }
        $this->db->group_by("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d')");
        $query = $this->db->get();
        return $query->result();
    }

    public function revenue_summary($day1 = null, $day2 = null) {
        $this->db->select("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d') as reztarih, sum(yolcu.yolcu_satisfiyat - yolcu.yolcu_vergi - yolcu.yolcu_koltukfiyat) as kazanc");
        $this->db->from('yolcu');
        $this->db->join('rezervasyon', 'yolcu.yolcu_rezid = rezervasyon.rez_id');
        if($day1 != null && $day2 != null){
             $this->db->where("rezervasyon.rez_kayittarih >=", $day1);
             $this->db->where("rezervasyon.rez_kayittarih <=", $day2);
        }else{
             $this->db->where("rezervasyon.rez_kayittarih >=", date("Y-m-01"));
        }
        $this->db->group_by("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d')");
        $query = $this->db->get();
        return $query->result();
    }

    public function revenue_last15days() {
        $this->db->select("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d') as reztarih, sum(yolcu.yolcu_satisfiyat - yolcu.yolcu_vergi - yolcu.yolcu_koltukfiyat) as kazanc");
        $this->db->from('yolcu');
        $this->db->join('rezervasyon', 'yolcu.yolcu_rezid = rezervasyon.rez_id');
        $this->db->where("rezervasyon.rez_kayittarih >=", date("Y-m-d", strtotime("-30 day")));
        $this->db->group_by("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d')");
        $this->db->order_by("MAX(rezervasyon.rez_kayittarih)", "DESC");
        $this->db->limit(15);
        $query = $this->db->get();
        return $query->result();
    }

    public function passenger_last15days() {
        $this->db->select("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d') as reztarih, count(yolcu.yolcu_id) as yolcu_count");
        $this->db->from('yolcu');
        $this->db->join('rezervasyon', 'yolcu.yolcu_rezid = rezervasyon.rez_id');
        $this->db->where("rezervasyon.rez_kayittarih >=", date("Y-m-d", strtotime("-30 day")));
        $this->db->group_by("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d')");
        $this->db->order_by("MAX(rezervasyon.rez_kayittarih)", "DESC");
        $this->db->limit(15);
        $query = $this->db->get();
        return $query->result();
    }

    public function revenue_last30days_sum() {
        $this->db->select("sum(yolcu.yolcu_satisfiyat - yolcu.yolcu_vergi - yolcu.yolcu_koltukfiyat) as kazanc");
        $this->db->from('yolcu');
        $this->db->join('rezervasyon', 'yolcu.yolcu_rezid = rezervasyon.rez_id');
        $this->db->where("rezervasyon.rez_kayittarih >=", date("Y-m-d", strtotime("-30 day")));
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->kazanc;
    }

    public function passenger_last30days_sum() {
        $this->db->select("count(yolcu.yolcu_id) as yolcu_count");
        $this->db->from('yolcu');
        $this->db->join('rezervasyon', 'yolcu.yolcu_rezid = rezervasyon.rez_id');
        $this->db->where("rezervasyon.rez_kayittarih >=", date("Y-m-d", strtotime("-30 day")));
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->yolcu_count;
    }

    public function calendar_summary() {
        $this->db->select("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d') as rezdate, SUM(IF(rez_durum='OK',1,0)) as statusok, SUM(IF(rez_durum='OP',1,0)) as statusop, SUM(IF(rez_durum='CA',1,0)) as statusca");
        $this->db->from('rezervasyon');
        $this->db->where("rez_kayittarih >=", date("Y-01-01"));
        $this->db->group_by("date_format(rezervasyon.rez_kayittarih, '%Y-%m-%d')");
        $query = $this->db->get();
        return $query->result();
    }

    public function special_message($id) {
        $this->db->select("ms.mesaj_id, ac.acentekul_kullanici, date_format(ms.mesaj_gonderimtarih, '%d-%m-%Y %H:%i') as tarih, ms.mesaj_konu, ");
        $this->db->from('mesaj ms');
        $this->db->where(array("ms.mesaj_alan" => $id));
        $this->db->join('acentekullanici ac', 'ms.mesaj_gonderen = ac.acentekul_id');
        $this->db->order_by("ms.mesaj_gonderimtarih", "DESC");
        $query = $this->db->get();
        return $query->result();
    }

    public function reservation_message($id) {
        $this->db->select("ms.rezmesaj_rezid, gn.acentekul_kullanici as gonderen, al.acentekul_kullanici as alan, ms.rezmesaj_mesaj, date_format(ms.rezmesaj_kayittarih, '%d-%m-%Y %H:%i') as tarih");
        $this->db->from('rezervasyonmesaj ms');
        $this->db->where(array("ms.rezmesaj_alan" => $id));
        $this->db->join('acentekullanici gn', 'ms.rezmesaj_kaydeden = gn.acentekul_id');
        $this->db->join('acentekullanici al', 'ms.rezmesaj_alan = al.acentekul_id');
        $this->db->order_by("ms.rezmesaj_kayittarih", "DESC");
        $query = $this->db->get();
        return $query->result();
    }

    public function flight_with_passengercount($date) {
        $this->db->select("uc.ucus_id, concat(uc.ucus_parkur1, ' ', uc.ucus_parkur2) as parkur, uc.ucus_no, date_format(uc.ucus_tarih, '%d-%m-%Y') as tarih, count(yu.id) as yolcu");
        $this->db->from('ucus uc');
        $this->db->join('yolcuucus yu', 'uc.ucus_id = yu.yucus_ucusid');
        $this->db->where(array('uc.ucus_tarih' => $date));
        $this->db->group_by("uc.ucus_id, concat(uc.ucus_parkur1, ' ', uc.ucus_parkur2), uc.ucus_no, date_format(uc.ucus_tarih, '%d-%m-%Y')");
        $query = $this->db->get();
        return $query->result();
    }
}
