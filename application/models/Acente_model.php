<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 21.11.2018
 * Time: 02:19
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Acente_model extends CI_Model
{
    public function getAll()
    {
        $this->db->select('*');
        $this->db->from('acente a');
        $this->db->where('a.acente_onay','E');
        $this->db->order_by('a.acente_ad');
        $query = $this->db->get();
        return $query->result();
    }
}