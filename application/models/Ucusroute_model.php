<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 20.11.2018
 * Time: 21:45
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Ucusroute_model extends CI_Model
{

    public function dashboard_opsionlist( $limit )
    {
        $this->db->from("ucusroute");
        $count = $this->db->count_all_results();

        $this->db->select("ur.*");
        $this->db->from("ucusroute ur");
        $this->db->order_by('route_id', 'DESC');
        $this->db->limit($limit[ 1 ], $limit[ 0 ]);
        $query = $this->db->get();

        return array(
            "count_all" => $count, "result" => $query->result()
        );
    }

    public function getAllByUcusId($id)
    {
        $this->db->select('urf.*,ur.*');
        $this->db->from('ucusroutefiyat urf');
        $this->db->join('ucusroute ur','ur.route_id=urf.ucusfiyat_routeid');
        $this->db->join('ucussegment us','us.route_id=ur.route_id');
        $this->db->where('us.ucus_id',$id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get($id)
    {
        $this->db->select('ur.*,us.ucus_id');
        $this->db->from('ucusroute ur');
        $this->db->join('ucussegment us','us.route_id=ur.route_id');
        $this->db->where('ur.route_id',$id);
        $query = $this->db->get();
        return $query->row();
    }

    public function getWithUcus($id)
    {
        $this->db->select('ur.*,us.ucus_id,u.ucus_kod');
        $this->db->from('ucusroute ur');
        $this->db->join('ucussegment us','us.route_id=ur.route_id');
        $this->db->join('ucus u','us.ucus_id=u.ucus_id');
        $this->db->where('ur.route_id',$id);
        $query = $this->db->get();
        return $query->row();
    }

    public function insert($ucusRouteData)
    {
//        return $this->db->set($ucusRouteData)->get_compiled_insert('ucusroute');
        $this->db->insert('ucusroute',$ucusRouteData);
        return $this->db->insert_id();
    }

    public function getByUcusKod($gidis,$varis,$gidis_tarih,$byCountry1 = false,$byCountry2 = false,$toplam_koltuk = 1,$page = 0)
    {
        $this->db->select('urf.ucusfiyat_id,u.ucus_tarih, u.ucus_saat1, u.ucus_saat2, u.ucus_kod, u.ucus_no, ap1.code AS ap1code, ap1.name AS ap1name,ap1.city AS ap1city, ap2.code AS ap2code, ap2.name AS ap2name,ap2.city AS ap2city, sum(urf.ucusfiyat_koltuksayi) AS ucusfiyat_koltuksayi, sum(urf.ucusfiyat_kalankoltuksayi) AS ucusfiyat_kalankoltuksayi, urf.ucusfiyat_koltukmaliyet, urf.ucusfiyat_yetiskinsatisfiyat, urf.ucusfiyat_cocuksatisfiyat, urf.ucusfiyat_bebeksatisfiyat,ur.route_farklituropizin,ur.route_farklihsirketizin,ur.route_tekyonizin');
        $this->db->from('ucusroute ur');
        $this->db->join('ucussegment us','us.route_id=ur.route_id');
        $this->db->join('ucus u', 'us.ucus_id=u.ucus_id');
        $this->db->join('ucusroutefiyat urf','urf.ucusfiyat_routeid=ur.route_id');
        $this->db->join('airlines al','al.code=u.ucus_kod');
        $this->db->join('airports ap1','ap1.code=u.ucus_parkur1');
        $this->db->join('airports ap2','ap2.code=u.ucus_parkur2');
        if($byCountry1)
            $this->db->where('ap1.countrycode',$byCountry1);
        else
            $this->db->where('u.ucus_parkur1',$gidis);
        if($byCountry2)
            $this->db->where('ap2.countrycode',$byCountry2);
        else
            $this->db->where('u.ucus_parkur2',$varis);

        $gidis_tarih = date('Y-m-d',strtotime('-3 days',strtotime($gidis_tarih)));
        if($gidis_tarih <  date('Y-m-d'))
            $gidis_tarih = date('Y-m-d');

        $this->db->where('u.ucus_tarih>=',$gidis_tarih);
        $this->db->where('ur.route_goruntulemeizin in(2,1)');
        $this->db->group_by('urf.ucusfiyat_sinifid,urf.ucusfiyat_yetiskinsatisfiyat, urf.ucusfiyat_cocuksatisfiyat, urf.ucusfiyat_bebeksatisfiyat,u.ucus_tarih');
        $this->db->order_by('ucus_tarih','ASC');

        $limit = 10;
        $offset = $page * $limit;
        $this->db->limit($limit,$offset);

        $query = $this->db->get();
        return $query->result();
    }

}