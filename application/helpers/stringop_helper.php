<?php
if (!function_exists("is_nullempty")) {
    function is_nullempty($s) {
        return $s === NULL || $s == trim("");
    }
}

if (!function_exists("is_contain")) {
    function is_contain($s, $k) {
        return stristr($s, $k) !== FALSE;
    }
}

if (!function_exists("is_contain_array")) {
    function is_contain_array($s, $a) {
        foreach ($a as $k) {
            if (stristr($s, $k) !== FALSE) {
                return TRUE;
            } 
        }
        return FALSE;
    }
}
?>