<?php

if (!function_exists("mdywsl_to_sqldate")) {

    function mdywsl_to_sqldate($datestr) {
        $datearr = explode("/", $datestr);
        if (count($datearr) > 1) {
            return "{$datearr[2]}-{$datearr[1]}-{$datearr[0]}";
        } else {
            return $datestr;
        }
    }

}

if (!function_exists("dmywsl_to_sqldate")) {

    function dmywsl_to_sqldate($datestr) {
        $datearr = explode("/", $datestr);
        if (count($datearr) > 1) {
            return "{$datearr[2]}-{$datearr[1]}-{$datearr[0]}";
        } else {
            return $datestr;
        }
    }

}

if (!function_exists("dmywdt_to_sqldate")) {

    function dmywdt_to_sqldate($datestr) {
        $datearr = explode(".", $datestr);
        if (count($datearr) > 1) {
            return "{$datearr[2]}-{$datearr[1]}-{$datearr[0]}";
        } else {
            return $datestr;
        }
    }

}

if (!function_exists("sqldate_to_dmya")) {

    function sqldate_to_dmya($datestr) {
        $datenames = array("Mon" => "Pt", "Tue" => "Sa", "Wed" => "Ça", "Thu" => "Pe", "Fri" => "Cu", "Sat" => "Ct", "Sun" => "Pa");
        $datearr = explode(" ", $datestr);
        $datearr[1] = $datenames[$datearr[1]];
        return implode(" ", $datearr);
    }

}

if (!function_exists("sqldate_to_longdate")) {

    function sqldate_to_longdate($datestr) {
        $datenames = array("Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi");
        $time = strtotime($datestr);
        $dateformatted = date("d/m/Y ", $time) . $datenames[date("w", $time)] . date(" H:i", $time);
        return $dateformatted;
    }
}
