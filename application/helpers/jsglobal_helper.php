<?php

if (!function_exists("activemenu")) {
    function activemenu($menuitem) {
        return "$(\"a[title='" . $menuitem . "']\").each(function(){
             $(this).parent().addClass('active');
        });";
    }
}
