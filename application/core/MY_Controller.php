<?php



class MY_Controller extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->auth_control();
    }
    
    private function auth_control() {
        if (!$this->session->has_userdata("logged_user")) {
            redirect(base_url() . "index.php/auth/login");
            exit();
        }
    }
    
}