<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 21.11.2018
 * Time: 02:07
 */
$globalRoute = [];
if($model->ucus_id > 0 && count($ucusRoutes)){
    $globalRoute = [
        'route_opsiyontarih'=>$ucusRoutes[0]->route_opsiyontarih,
        'route_opsiyonizin'=>$ucusRoutes[0]->route_opsiyonizin,
        'route_donussureizintur'=>$ucusRoutes[0]->route_donussureizintur,
        'route_donussureizinmin'=>$ucusRoutes[0]->route_donussureizinmin,
        'route_donussureizinmax'=>$ucusRoutes[0]->route_donussureizinmax,
        'route_donussureizintarih'=>$ucusRoutes[0]->route_donussureizintarih,
        'route_farklituropizin'=>$ucusRoutes[0]->route_farklituropizin,
        'route_farklihsirketizin'=>$ucusRoutes[0]->route_farklihsirketizin,
        'route_elektronikbiletizin'=>$ucusRoutes[0]->route_elektronikbiletizin,
        'route_tekyonizin'=>$ucusRoutes[0]->route_tekyonizin,
        'route_gecerliliktarih1'=>$ucusRoutes[0]->route_gecerliliktarih1,
        'route_gecerliliktarih2'=>$ucusRoutes[0]->route_gecerliliktarih2,
        'route_yedekkoltuk'=>$ucusRoutes[0]->route_yedekkoltuk,
        'route_uyarikoltuk'=>$ucusRoutes[0]->route_uyarikoltuk,
    ];
}else{

}
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/plugin/bootstrap-datepicker/css/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/plugin/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css">
<style>
    .form-group{
        margin-bottom:15px;
    }
    .select2-fixed .select2{
        /*float: left;*/
        min-width: 180px;
    }
</style>
<div class="widget-body">
    <form id="routeForm" autocomplete="off" class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/<?=$model->ucus_id > 0 ? "routes/update/$model->ucus_id" : "routes/addRoute"?>">
        <div class="col-xs-12">
            <fieldset>
                <legend>Route Genel Bilgileri</legend>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <input type="radio" name="kayit_tur" value="tek" checked class="kayit_tur">
                        <label>Tek Kayıt</label>&nbsp;&nbsp;
                        <input type="radio" name="kayit_tur" value="seri" class="kayit_tur">
                        <label>Seri Kayıt</label>
                    </div>
                    <div class="col-xs-12 kayit_tur_seri" style="display: none;">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="col-md-5 control-label">Başlangıç Tarihi</label>
                                <div class="col-md-7">
                                    <div class="input-group input-group-sm">
                                        <input type="text" placeholder="Başlangıç Tarihi" required disabled class="form-control datepicker disabled_attr" name="seri_start_1" value="<?=date('Y-m-d')?>">
                                        <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="col-md-5 control-label">Bitiş Tarihi</label>
                                <div class="col-md-7">
                                    <div class="input-group input-group-sm">
                                        <input type="text" placeholder="Bitiş Tarihi" required disabled class="form-control datepicker disabled_attr" name="seri_start_2" value="<?=date('Y-m-d')?>">
                                        <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <input type="checkbox" name="Seri[1]" id="seri_1"> <label for="seri_1">Pazartesi</label>&nbsp;
                            <input type="checkbox" name="Seri[2]" id="seri_2"> <label for="seri_2">Salı</label>&nbsp;
                            <input type="checkbox" name="Seri[3]" id="seri_3"> <label for="seri_3">Çarşamba</label>&nbsp;
                            <input type="checkbox" name="Seri[4]" id="seri_4"> <label for="seri_4">Perşembe</label>&nbsp;
                            <input type="checkbox" name="Seri[5]" id="seri_5"> <label for="seri_5">Cuma</label>&nbsp;
                            <input type="checkbox" name="Seri[6]" id="seri_6"> <label for="seri_6">Cumartesi</label>&nbsp;
                            <input type="checkbox" name="Seri[0]" id="seri_7"> <label for="seri_7">Pazar</label>&nbsp;
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tur Operatörü*</label>
                            <div class="col-md-8">
                                <select id="tur_operator" required name="Ucus[ucus_turoperator]" class="form-control">
                                    <option value="">Lütfen Seçiniz</option>
                                    <?foreach ($turOperators as $turoperator) {?>
                                        <option value="<?=$turoperator->turop_id?>" <?=$model->ucus_turoperator == $turoperator->turop_id ? 'selected' : ''?>><?=$turoperator->turop_ad?></option>
                                    <?}?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Uyarı Notu</label>
                            <div class="col-md-8">
                                <input class="form-control" placeholder="Uyarı Notu Giriniz" type="text" value="<?=$model->ucus_uyarinot?>" name="Ucus[ucus_uyarinot]">
                                <div class="pull-right">
                                    <input type="checkbox" class="" <?=$model->ucus_uyarinotgoster == 'E' ? 'checked' : ''?> name="Ucus[ucus_uyarinotgoster]"> Uyarı Notu Göster
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Değişiklik Notu</label>
                            <div class="col-md-8">
                                <textarea class="form-control" placeholder="Değişiklik Notu Giriniz" rows="4" name="Ucus[ucus_degisikliknot]"><?=$model->ucus_degisikliknot?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Durum</label>
                                    <div class="col-md-7">
                                        <select class="form-control" name="Ucus[ucus_durum]">
                                            <option value="A" <?=$model->ucus_durum == 'A' ? 'selected' : ''?>>Aktif</option>
                                            <option value="P" <?=$model->ucus_durum == 'P' ? 'selected' : ''?>>Pasif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">İzin</label>
                                    <div class="col-md-7">
                                        <select class="form-control" name="UcusRoute[route_goruntulemeizin]">
                                            <option value="-1">Seç</option>
                                            <option value="0" selected="selected">Herkes</option>
                                            <option value="1">Müşteri</option>
                                            <option value="2">Acente</option>
                                            <option value="3">Müşteri&amp;Acente</option>
                                            <option value="7">Yönetici</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Başlangıç Tarihi</label>
                            <div class="col-md-9">
                                <div class="input-group input-group-sm">
                                    <input type="text" placeholder="Başlangıç Tarihi" required disabled class="form-control datetimepicker disabled_attr" name="UcusRoute[route_gecerliliktarih1]" value="<?=$globalRoute['route_gecerliliktarih1']?>">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Bitiş Tarihi</label>
                            <div class="col-md-9">
                                <div class="input-group input-group-sm">
                                    <input type="text" placeholder="Bitiş Tarihi" required disabled class="form-control datetimepicker disabled_attr" name="UcusRoute[route_gecerliliktarih2]" value="<?=$globalRoute['route_gecerliliktarih2']?>">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Opsiyon Tarihi <input type="checkbox" disabled class="disabled_attr route_opsiyontarih_C" <?=$globalRoute['route_opsiyonizin'] == 'E' ?'checked' : ''?>></label>
                            <div class="col-md-9">
                                <div class="input-group input-group-sm">
                                    <input type="text" placeholder="Opsiyon Tarihi" class="form-control datetimepicker route_opsiyontarih" name="UcusRoute[route_opsiyontarih]" <?=$globalRoute['route_opsiyonizin'] == 'E' ?'' : 'disabled'?> value="<?=$globalRoute['route_opsiyonizin'] == 'E' ? $globalRoute['route_opsiyontarih'] : ''?>">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Uçuş Bilgisi</legend>
                <table class="table table-striped table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th>Uçuş Tarihi</th>
                        <th>Uçuş Kod-No</th>
                        <th>Parkur</th>
                        <th>Saat</th>
                        <th>Varış Tarihi</th>
                        <!--                        <th></th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <input type="text" placeholder="Uçuş Tarihi" required="" disabled name="Ucus[ucus_tarih]" class="form-control datepicker disabled_attr" value="<?=$model->ucus_tarih?>">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                </span>
                            </div>
                        </td>
                        <td class="select2-fixed">
                            <select required id="ucus_kod" class="form-control" name="Ucus[ucus_kod]" style="width: 50%;float: left;" >
                                <option value=""></option>
                                <?foreach ($airlines as $airline) {?>
                                    <option value="<?=$airline->code?>"><?=$airline->code?> - <?=$airline->name?></option>
                                <?}?>
                            </select>
                            <br>
                            <input type="text" placeholder="Uçuş No" required class="form-control" name="Ucus[ucus_no]" style="width: 180px; float: left;" value="<?=$model->ucus_no?>">
                        </td>
                        <td class="select2-fixed">
                            <select class="form-control" id="ucus_parkur1" required name="Ucus[ucus_parkur1]" style="width: 45%; float: left;">
                                <option value=""></option>
                                <?foreach ($airports as $i=>$airport) {?>
                                    <option value="<?=$airport->code?>"> <?=$airport->code?> - <?=$airport->name?></option>
                                    <?if($i>10) break;}?>
                            </select>
                            <br>
                            <select class="form-control" id="ucus_parkur2" required name="Ucus[ucus_parkur2]" style="width: 45%; float: left;">
                                <option value=""></option>
                                <?foreach ($airports as $i=>$airport) {?>
                                    <option value="<?=$airport->code?>"><?=$airport->code?> - <?=$airport->name?></option>
                                    <?if($i>10) break;}?>
                            </select>
                        </td>
                        <td>
                            <input type="text" placeholder="Kalkış Saati" class="form-control timepicker" required name="Ucus[ucus_saat1]" style="width: 100%; *float: left;" value="<?=$model->ucus_saat1?>">
                            <input type="text" placeholder="İniş Saati" class="form-control timepicker" required name="Ucus[ucus_saat2]" style="width: 100%; *float: left;" value="<?=$model->ucus_saat2?>">
                        </td>
                        <td>
                            <div class="input-group input-group-sm">
                                <input type="text" placeholder="Varış Tarihi" disabled name="Ucus[ucus_varistarih]" class="form-control datepicker disabled_attr" value="<?=$model->ucus_varistarih?>">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                </span>
                            </div>
                        </td>
                        <!--                        <td>-->
                        <!--                            <button class="btn btn-danger btn-xs" type="button"><i class="glyphicon glyphicon-remove"></i> </button>-->
                        <!--                        </td>-->
                    </tr>


                    </tbody>
                    <!--<tfoot>
                    <tr>
                        <td colspan="7">
                            <button class="btn btn-primary btn-xs" type="button"><i class="glyphicon glyphicon-plus"></i> Yeni Alan Ekle</button>
                        </td>
                    </tr>
                    </tfoot>-->
                </table>

            </fieldset>

            <fieldset>
                <legend>Uçuş Koltuk Bilgileri</legend>
                <table class="table table-striped table-condensed table-bordered" id="ucus_tablo">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Koltuk Sayısı</th>
                        <th>Sınıf</th>
                        <th>Oncelik</th>
                        <th>Acente</th>
                        <th>Koltuk Maliyeti €</th>
                        <th>
                            Satış Fiyatı €
                            <div class="row">
                                <div class="col-md-4">Yetişkin</div>
                                <div class="col-md-4">Çocuk</div>
                                <div class="col-md-4">Bebek</div>
                            </div>
                        </th>
                        <th>
                            Bagaj KG
                            <div class="row">
                                <div class="col-md-4">Yetişkin</div>
                                <div class="col-md-4">Çocuk</div>
                                <div class="col-md-4">Bebek</div>
                            </div>
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    if(count($ucusRoutes)){
                        foreach ($ucusRoutes as $ucusRoute) {?>
                            <tr>
                                <td>
                                    <?=$ucusRoute->ucusfiyat_id?>
                                    <input type="hidden" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_id]" value="<?=$ucusRoute->ucusfiyat_id?>">
                                </td>
                                <td>
                                    <input name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_koltuksayi]" type="number" min="0" max="99" placeholder="Koltuk Sayısı" class="form-control input-xs ucusfiyat_koltuksayi" value="<?=$ucusRoute->ucusfiyat_koltuksayi?>"  style="width: 60px;">
                                </td>
                                <td>
                                    <select class="form-control input-xs" style="width: 60px;" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_sinifid]">
                                        <option value="">Lütfen Seçiniz</option>
                                        <?foreach ($sinifAds as $sinifAd) {?>
                                            <option value="<?=$sinifAd->sinif_id?>" <?=$sinifAd->sinif_id == $ucusRoute->ucusfiyat_sinifid ? 'selected' : ''?>><?=$sinifAd->sinif_tanim?></option>
                                        <?}?>
                                    </select>
                                </td>
                                <td>
                                    <input style="max-width: 50px;" type="number" placeholder="Öncelik" min="0" max="9" value="<?=$ucusRoute->ucusfiyat_oncelikderece?>" class="form-control input-xs" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_oncelikderece]">
                                </td>
                                <td>
                                    <select class="form-control input-xs" style="max-width: 80px;" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_acente]">
                                        <option value="">Lütfen Seçiniz</option>
                                        <?foreach ($acentes as $acente) {?>
                                            <option value="<?=$acente->acente_id?>" <?=$acente->acente_id == $ucusRoute->ucusfiyat_acente ? 'selected' : ''?>><?=$acente->acente_ad?></option>
                                        <?}?>
                                    </select>
                                </td>
                                <td>
                                    <input style="max-width: 80px;" type="number" placeholder="Koltuk Maliyeti" class="form-control input-xs" value="<?=$ucusRoute->ucusfiyat_koltukmaliyet?>" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_koltukmaliyet]">
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input style="max-width: 80px;" class="form-control input-xs" value="<?=$ucusRoute->ucusfiyat_yetiskinsatisfiyat?>" placeholder="Yetişkin" type="number" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_yetiskinsatisfiyat]">
                                        </div>
                                        <div class="col-md-4">
                                            <input style="max-width: 80px;" class="form-control input-xs" value="<?=$ucusRoute->ucusfiyat_cocuksatisfiyat?>" placeholder="Çocuk" type="number" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_cocuksatisfiyat]">
                                        </div>
                                        <div class="col-md-4">
                                            <input style="max-width: 80px;" class="form-control input-xs" value="<?=$ucusRoute->ucusfiyat_bebeksatisfiyat?>" placeholder="Bebek" type="number" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_bebeksatisfiyat]">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input style="max-width: 80px;" class="form-control input-xs" value="<?=$ucusRoute->ucusfiyat_yetiskinbagaj?>" placeholder="Yetişkin" type="number" min="0" max="99" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_yetiskinbagaj]">
                                        </div>
                                        <div class="col-md-4">
                                            <input style="max-width: 80px;" class="form-control input-xs" value="<?=$ucusRoute->ucusfiyat_cocukbagaj?>" placeholder="Çocuk" type="number" min="0" max="99" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_cocukbagaj]">
                                        </div>
                                        <div class="col-md-4">
                                            <input style="max-width: 80px;" class="form-control input-xs" value="<?=$ucusRoute->ucusfiyat_bebekbagaj?>" placeholder="Bebek" type="number" min="0" max="99" name="UcusRouteFiyat[<?=$ucusRoute->ucusfiyat_id?>][ucusfiyat_bebekbagaj]">
                                        </div>
                                    </div>
                                </td>

                                <td>
                                    <button class="btn btn-danger btn-xs" type="button"><i class="glyphicon glyphicon-remove ucusroute_remove ucusroutefiyat_<?=$ucusRoute->ucusfiyat_id?>"></i> </button>
                                </td>
                            </tr>
                            <?
                        }
                    }else{?>
                        <tr>
                            <td>
                                #
                            </td>
                            <td>
                                <input name="UcusRouteFiyat[0][ucusfiyat_koltuksayi]" type="number" min="0" max="99" placeholder="Koltuk Sayısı" class="form-control input-xs ucusfiyat_koltuksayi" value=""  style="width: 60px;">
                            </td>
                            <td>
                                <select class="form-control input-xs" style="width: 60px;" name="UcusRouteFiyat[0][ucusfiyat_sinifid]">
                                    <option value="">Lütfen Seçiniz</option>
                                    <?foreach ($sinifAds as $sinifAd) {?>
                                        <option value="<?=$sinifAd->sinif_id?>"><?=$sinifAd->sinif_tanim?></option>
                                    <?}?>
                                </select>
                            </td>
                            <td>
                                <input style="max-width: 50px;" type="number" placeholder="Öncelik" min="0" max="9" class="form-control input-xs" name="UcusRouteFiyat[0][ucusfiyat_oncelikderece]">
                            </td>
                            <td>
                                <select class="form-control input-xs" style="max-width: 80px;" name="UcusRouteFiyat[0][ucusfiyat_acente]">
                                    <option value="">Lütfen Seçiniz</option>
                                    <?foreach ($acentes as $acente) {?>
                                        <option value="<?=$acente->acente_id?>"><?=$acente->acente_ad?></option>
                                    <?}?>
                                </select>
                            </td>
                            <td>
                                <input style="max-width: 80px;" type="number" placeholder="Koltuk Maliyeti" class="form-control input-xs" name="UcusRouteFiyat[0][ucusfiyat_koltukmaliyet]">
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <input style="max-width: 80px;" class="form-control input-xs" placeholder="Yetişkin" type="number" name="UcusRouteFiyat[0][ucusfiyat_yetiskinsatisfiyat]">
                                    </div>
                                    <div class="col-md-4">
                                        <input style="max-width: 80px;" class="form-control input-xs" placeholder="Çocuk" type="number" name="UcusRouteFiyat[0][ucusfiyat_cocuksatisfiyat]">
                                    </div>
                                    <div class="col-md-4">
                                        <input style="max-width: 80px;" class="form-control input-xs" placeholder="Bebek" type="number" name="UcusRouteFiyat[0][ucusfiyat_bebeksatisfiyat]">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <input style="max-width: 80px;" class="form-control input-xs" placeholder="Yetişkin" type="number" min="0" max="99" name="UcusRouteFiyat[0][ucusfiyat_yetiskinbagaj]">
                                    </div>
                                    <div class="col-md-4">
                                        <input style="max-width: 80px;" class="form-control input-xs" placeholder="Çocuk" type="number" min="0" max="99" name="UcusRouteFiyat[0][ucusfiyat_cocukbagaj]">
                                    </div>
                                    <div class="col-md-4">
                                        <input style="max-width: 80px;" class="form-control input-xs" placeholder="Bebek" type="number" min="0" max="99" name="UcusRouteFiyat[0][ucusfiyat_bebekbagaj]">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <button class="btn btn-danger btn-xs ucusroute_remove" disabled type="button"><i class="glyphicon glyphicon-remove"></i> </button>
                            </td>
                        </tr>
                    <?}?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="9">
                            <button class="btn btn-primary btn-xs add_route" type="button" disabled><i class="glyphicon glyphicon-plus"></i> Yeni Alan Ekle</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">Toplam Koltuk: <span class="total_koltuk">0</span></td>
                        <td colspan="2"><span class="pull-left" style="line-height: 30px;">Yedek Koltuk : </span><input class="form-control pull-left" name="UcusRoute[route_yedekkoltuk]" value="<?=$globalRoute['route_yedekkoltuk']?>" style="width: 100px;"></td>
                        <td colspan="2"><span class="pull-left" style="line-height: 30px;">Koltuk Uyarı : </span><input class="form-control pull-left" name="UcusRoute[route_uyarikoltuk]" value="<?=$globalRoute['route_uyarikoltuk']?>" style="width: 100px;"></td>
                    </tr>
                    </tfoot>
                </table>

            </fieldset>
            <fieldset>
                <legend>Diğer Bilgiler</legend>
                <div class="row">
                    <div class="col-sm-6">
                        <input value="E" type="checkbox" name="UcusRoute[route_farklituropizin]" <?=$globalRoute['route_farklituropizin'] == 'E' ? 'checked' : ''?>> <label><b>Gidiş-Dönüşte Turoperatorler Farklı Olursa Ayrı Kayıt Yap </b><i class="fa fa-question-circle"></i> </label><br>
                        <input value="E" type="checkbox" name="UcusRoute[route_farklihsirketizin]" <?=$globalRoute['route_farklihsirketizin'] == 'E' ? 'checked' : ''?>> <label><b>Gidiş-Dönüşte Hava Şirketleri Aynı Olmalıdır </b></label>
                    </div>
                    <div class="col-sm-6">
                        <input value="E" type="checkbox" name="UcusRoute[route_elektronikbiletizin]" <?=$globalRoute['route_elektronikbiletizin'] == 'E' ? 'checked' : ''?>> <label><b>E-Bilet & Uçuş Kuponu Çıktısına İzin Verilmesin </b></label><br>
                        <input value="E" type="checkbox" name="UcusRoute[route_tekyonizin]" <?=$globalRoute['route_tekyonizin'] == 'E' ? 'checked' : ''?>> <label><b>Tek Yöne İzin Verilmesin </b></label>
                    </div>
                    <div class="col-sm-12">
                        <b>Geri Dönüş Süre İzin:</b> Gün olarak <i class="fa fa-question-circle"></i> <input value="G" type="radio" disabled class="route_donussureizintur disabled_attr" name="UcusRoute[route_donussureizintur]" <?=$globalRoute['route_donussureizintur'] == 'G' ? 'checked' : ''?>>
                        Min: <input type="number" min="0" max="365" disabled class="form-control route_donussureizintur_G" name="UcusRoute[route_donussureizinmin]" value="<?=$globalRoute['route_donussureizinmin']?>" style="width: 70px;    display: inline-flex;">
                        Max: <input type="number" min="0" max="365" disabled class="form-control route_donussureizintur_G" name="UcusRoute[route_donussureizinmax]" value="<?=$globalRoute['route_donussureizinmax']?>" style="width: 70px;    display: inline-flex;">
                        &nbsp;&nbsp;&nbsp;
                        Tarih Olarak <i class="fa fa-question-circle"></i> <input type="radio" name="UcusRoute[route_donussureizintur]" disabled class="route_donussureizintur disabled_attr" value="T" <?=$globalRoute['route_donussureizintur'] == 'T' ? 'checked' : ''?>>
                        <div class="input-group input-group-sm"   style="display: inline-flex;">
                            <input type="text" disabled placeholder="" style="width: 150px;" class="form-control datepicker route_donussureizintur_T" name="UcusRoute[route_donussureizintarih]" value="<?=$globalRoute['route_donussureizintarih']?>">
                            <span class="input-group-addon">
                               <i class="glyphicon glyphicon-calendar"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Not</legend>
                <div class="form-group">
                    <textarea class="form-control"></textarea>
                </div>
            </fieldset>

        </div>
        <footer>
            <button type="submit" class="btn btn-primary pull-right">
                <i class="fa fa-save"></i> Kaydet
            </button>
            <div class="clearfix"></div>
        </footer>
    </form>

</div>
<script src="<?php echo base_url(); ?>js/plugin/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>js/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/plugin/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>js/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

<script>
    $(function(){
        $("#ucus_kod").select2({
            placeholder: 'Havayolu Ara',
            minimumInputLength: 2,
            ajax: {
                url: "index.php/reservation/searchHavayolu",
                dataType: 'json',
                quietMillis: 250,
                cache: true
            }
        });
        $("#ucus_parkur1").select2({
            placeholder:'Kalkış Parkur Ara',
            minimumInputLength: 2,
            ajax: {
                url: "index.php/reservation/searchParkur",
                dataType: 'json',
                quietMillis: 250,
                cache: true
            }
        });
        $("#ucus_parkur2").select2({
            placeholder: 'Varış Parkur Ara',
            minimumInputLength: 2,
            ajax: {
                url: "index.php/reservation/searchParkur",
                dataType: 'json',
                quietMillis: 250,
                cache: true
            }
        });

        $("body").on('click','.ucusroute_remove',removeRoute);
        $("body").on('change','.ucusfiyat_koltuksayi',calcKoltuk);
        $(".add_route").on('click',addRoute);
        $(".ucusroute_remove").removeAttr('disabled');
        $(".add_route").removeAttr('disabled');
        $(".disabled_attr").removeAttr('disabled');

        $(".route_donussureizintur").change(function () {
            $(".route_donussureizintur_G").val('').attr('disabled','true');
            $(".route_donussureizintur_T").val('').attr('disabled','true');
            var type = $(this).val();
            if(type === 'G'){
                $(".route_donussureizintur_G").removeAttr('disabled','false');
            } else if(type === 'T'){
                $(".route_donussureizintur_T").removeAttr('disabled','false');
            }
        });

        $(".kayit_tur").change(function () {
            var type = $(this).val();
            if(type === 'tek'){
                $(".kayit_tur_seri").hide();
            } else{
                $(".kayit_tur_seri").show();
            }
        });

        $(".route_opsiyontarih_C").change(function () {
            if($(this).prop("checked")){
                $(".route_opsiyontarih").removeAttr('disabled')
            }else{
                $(".route_opsiyontarih").attr('disabled','true')
            }
        });
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";

        $("#routeForm").on('submit',submitRoute);
        $(".timepicker").timepicker({
            autoclose:true,
            defaultTime:false,
            minuteStep:1,
            showMeridian:false
        });
        $(".datetimepicker").datetimepicker({
            autoclose:true,
            format:'yyyy-mm-dd hh:ii',
            startDate:'<?=date('Y-m-d 00:00:00')?>',
            lang:'tr',
            minuteStep:1
        });
        $(".datepicker").datepicker({
            autoclose:true,
            format:'yyyy-mm-dd',
            startDate:'<?=date('Y-m-d')?>',
            lang:'tr'
        });
    });

    function calcKoltuk() {
        var total_koltuk = 0;
        $.each($(".ucusfiyat_koltuksayi"),function (k,v) {
            total_koltuk = total_koltuk + (parseInt($(v).val()) > 0 ? parseInt($(v).val()) : 0);
        });
        $(".total_koltuk").html(total_koltuk);
    }

    function submitRoute(e) {
        e.preventDefault();
        $.post($("#routeForm").attr('action'), $("#routeForm").serializeArray(), function (data) {
            window.location = "index.php#index.php/routes";
        }).fail(function () {
            alert('Bir hata ile karşılaşıldı. Lütfen tekrar deneyiniz');
        });
    }

    function removeRoute (){
        if(confirm('Emin misiniz?')){
            $(this).parent().parent().remove();
            calcKoltuk();
        }
    }

    function addRoute(){
        var randId = Math.floor((Math.random() * 1000) + 1);
        var html = '<tr>\n' +
            '<td>\n' + '#\n' + '</td>\n' +
            '<td>\n' + '<input name="UcusRouteFiyat['+randId+'][ucusfiyat_koltuksayi]" type="number" min="0" max="99" placeholder="Koltuk Sayısı" class="form-control input-xs ucusfiyat_koltuksayi" value=""  style="width: 60px;">\n' + '</td>\n' +
            '<td>\n' +
            '<select class="form-control input-xs" style="width: 60px;" name="UcusRouteFiyat['+randId+'][ucusfiyat_sinifid]">\n' +
            '<option value="">Lütfen Seçiniz</option>\n' +
            '<?foreach ($sinifAds as $sinifAd) {echo '<option value="'.$sinifAd->sinif_id.'">'.$sinifAd->sinif_tanim.'</option>';}?>'+
            '</select>\n' +
            '</td>\n' +
            '<td>\n' +
            '<input style="max-width: 50px;" type="number" placeholder="Öncelik" class="form-control input-xs" min="0" max="9" name="UcusRouteFiyat['+randId+'][ucusfiyat_oncelikderece]">\n' +
            '</td>\n' +
            '<td>\n' +
            '<select class="form-control input-xs" style="max-width: 80px;" name="UcusRouteFiyat['+randId+'][ucusfiyat_acente]">\n' +
            '<option value="">Lütfen Seçiniz</option>\n' +
            '<?foreach ($acentes as $acente) {echo '<option value="'.$acente->acente_id.'">'.$acente->acente_ad.'</option>';}?>'+
            '</select>\n' + '</td>\n' +
            '<td>\n' +
            '<input style="max-width: 80px;" type="number" placeholder="Koltuk Maliyeti" class="form-control input-xs" name="UcusRouteFiyat['+randId+'][ucusfiyat_koltukmaliyet]">\n' +
            '</td>\n' +
            '<td>\n' +
            '<div class="row">\n' +
            '<div class="col-md-4">\n' +
            '<input style="max-width: 80px;" class="form-control input-xs" placeholder="Yetişkin" type="number" name="UcusRouteFiyat['+randId+'][ucusfiyat_yetiskinsatisfiyat]">\n' +
            '</div>\n' +
            '<div class="col-md-4">\n' +
            '<input style="max-width: 80px;" class="form-control input-xs" placeholder="Çocuk" type="number" name="UcusRouteFiyat['+randId+'][ucusfiyat_cocuksatisfiyat]">\n' +
            '</div>\n' +
            '<div class="col-md-4">\n' +
            '<input style="max-width: 80px;" class="form-control input-xs" placeholder="Bebek" type="number" name="UcusRouteFiyat['+randId+'][ucusfiyat_bebeksatisfiyat]">\n' +
            '</div>\n' +
            '</div>\n' +
            '</td>\n' +
            '<td>\n' +
            '<div class="row">\n' +
            '<div class="col-md-4">\n' +
            '<input style="max-width: 80px;" class="form-control input-xs" placeholder="Yetişkin" type="number" min="0" max="99" name="UcusRouteFiyat['+randId+'][ucusfiyat_yetiskinbagaj]">\n' +
            '</div>\n' +
            '<div class="col-md-4">\n' +
            '<input style="max-width: 80px;" class="form-control input-xs" placeholder="Çocuk" type="number" min="0" max="99" name="UcusRouteFiyat['+randId+'][ucusfiyat_cocukbagaj]">\n' +
            '</div>\n' +
            '<div class="col-md-4">\n' +
            '<input style="max-width: 80px;" class="form-control input-xs" placeholder="Bebek" type="number" min="0" max="99" name="UcusRouteFiyat['+randId+'][ucusfiyat_bebekbagaj]">\n' +
            '</div>\n' +
            '</div>\n' +
            '</td>\n' +
            '<td>\n' +
            '<button class="btn btn-danger btn-xs ucusroute_remove" type="button"><i class="glyphicon glyphicon-remove"></i> </button>\n' +
            '</td>\n' +
            '</tr>\n';

        $("#ucus_tablo tbody").append(html);

    }
</script>