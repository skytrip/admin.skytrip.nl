<div id="appRoutesItems">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa-fw fa fa-plane"></i> Routes <span>> Route Details</span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <style media="screen">
                #sparks li{
                    max-width: 220px;
                    max-height: 32px;
                }
                #calendar td div{
                    min-height: 38px !important;
                }
            </style>
            <ul id="sparks" class="">
                <li class="sparks-info">
                    <h5> Rotalar <span class="txt-color-blue"><?php echo $routes_sum; ?></span></h5>
                </li>

                <li class="sparks-info">
                    <h5> Yolcular <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;<?php echo $passenger_sum; ?></span></h5>
                </li>
            </ul>
        </div>
    </div>



    <section id="widget-grid" class="">
        <!-- row -->
        <div class="row">
            <article class="col-sm-12">
                <!-- new widget -->
                <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-plane txt-color-darken"></i> </span>
                        <h2>Routes </h2>
                    </header>

                    <!-- widget div-->
                    <div class="no-padding">
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                        </div>
                        <!-- end widget edit box -->

                        <div class="widget-body">
                            <!-- content -->
                            <div ng-controller="routesList">
                                <div  ui-grid="routesGrid" bind-scroll-vertical="true" class="grid"></div>
                                <div class="dt-toolbar-footer">
                                    <div class="col-sm-6 col-xs-12 hidden-xs">
                                        <div class="dataTables_info" id="dt_basic_info" role="status" aria-live="polite">
                                            Showing <span class="txt-color-darken">{{firstinpage}}</span> to <span class="txt-color-darken">{{tumu > lastinpage ? lastinpage : tumu }}</span> of <span class="text-primary">{{tumu}}</span> entries
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="dataTables_paginate paging_simple_numbers" id="dt_basic_paginate">
                                            <ul class="pagination pagination-xs no-margin pull-right">
                                                <li class="prev">
                                                    <a href="javascript:;" ng-click="pageFirst()">Ilk</a>
                                                </li>
                                                <li ng-repeat="p in range(page-5,page)" ng-class="p == page ? 'active' : ''">
                                                    <a href="javascript:void(0);" ng-click="setPage(p)">{{p}}</a>
                                                </li>
                                                <li ng-repeat="p in range(page+1,page+5)">
                                                    <a href="javascript:void(0);" ng-click="setPage(p)">{{p}}</a>
                                                </li>
                                                <li class="next">
                                                    <a href="javascript:;" ng-click="pageLast()">Son</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end content -->
                        </div>
                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->
            </article>
        </div>


    </section>

</div>
<div aria-hidden="true" aria-labelledby="detailViewLabel" role="dialog" tabindex="-1" id="detailView" class="modal fade" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/plugin/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/plugin/moment/moment-with-locales.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/plugin/moment/angular-moment.js"></script>

<script>

</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/pagescripts/routes.app.js"></script>
