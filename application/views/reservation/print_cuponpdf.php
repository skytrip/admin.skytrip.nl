<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<style>
	body {
		color: #000000;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	td {
		color: #3d444b;
		font-family: Arial, Helvetica, sans-serif;
	}

	.yazi1 {
		font-size:10px;
		color: #3d444b;
		font-family: Arial, Helvetica, sans-serif;
	}
	.yazi2 {
		font-size:11px;
		color: #000000;
		font-family: Arial, Helvetica, sans-serif;
	}
	.yazi3 {
		font-size:13px;
		color: #3d444b;
		font-family: Arial, Helvetica, sans-serif
	}
	.renkli_alan {
		background-color: #d9d9d9;
	}

	.kenarlik_table
	{
		border-top:1px solid #666666;
		border-left:1px solid #666666;
	}
	.kenarlik_td td
	{
		border-right:1px solid #666666;
		border-bottom:1px solid #666666;
	}
	.kupon_baslik {
		font-size:15px;
		font-weight:bold;
	}
	</style>
</head>
<body>
	<div style="page-break-after:always">
		<table width="742" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" valign="top" style="text-align:center;vertical-align:top">
					<table cellspacing="0" cellpadding="0" width="742">
						<tr>
							<td align="left" valign="top" style="text-align:left;vertical-align:top"><img src="./skytripimg/ucuskuponhavasirketlogo/bos.gif" width="220" height="80" alt="" /></td>
							<td align="center" valign="top">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tr>
										<td align="left" valign="top">Telefon : 010 - 425 30 70 / 010 - 476 42 32</td>
									</tr>
									<tr>
										<td align="left" valign="top">Web : www.ankafly.nl</td>
									</tr>
									<tr>
										<td align="left" valign="top">Fax : 010 - 425 35 68</td>
									</tr>
									<tr>
										<td align="left" valign="top">E-posta : info@skytrip.nl</td>
									</tr>
								</table>
							</td>
							<td align="right" valign="top" style="text-align:right;vertical-align:top"><img src="skytripimg/ucuskuponturoperatorlogo/bos.gif" width="220" height="80" alt="" /></td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td align="left" nowrap="nowrap" style="padding-left:5px" valign="bottom"><span class="kupon_baslik">Uçuş Kuponu 1</span></td>
			</tr>
			<tr>
				<td>
					<table cellspacing="0" cellpadding="0" width="742" class="kenarlik_table">

						<tr class="kenarlik_td">
							<td height="20" style="padding-left:5px" valign="middle"><span class="yazi1">Hava Şirketi</span></td>
							<td valign="top" colspan="3" height="30" rowspan="2" style="padding-left:5px"><span class="yazi1">PNR&nbsp;:&nbsp;</span><span class="yazi2"><?php echo $cupon->pnr; ?></span><br />
								<span class="yazi1">Rezervasyon No&nbsp;:&nbsp;</span><br /><span class="yazi2"><?php echo $cupon->ucusno; ?></span>
							</td>
							<td valign="top" colspan="4" height="30" rowspan="2" style="padding-left:5px"><span class="yazi1">Yolcu Bileti ve Bagaj Kontrolu :</span></td>
						</tr>

						<tr class="kenarlik_td">
							<td height="20" style="padding-left:5px" valign="middle"><span class="yazi2"><?php echo $cupon->alname; ?></span></td>
						</tr>

						<tr class="kenarlik_td">
							<td style="padding-left:5px" valign="middle"><span class="yazi1">Yolcu Adı</span><br /><span class="yazi2"><?php echo $cupon->cinsiyet == "M" ? "MR" : "MS"; ?> <?php echo $cupon->yolcuad; ?></span></td>
							<td valign="top" colspan="2" style="padding-left:5px"><span class="yazi1">Doğum Tarihi<br /><b><?php echo $cupon->dogumtarih; ?></b></span></td>
							<td valign="top" align="left" style="padding-left:5px" colspan="3"><span class="yazi1">Acente :</span><br /><span class="yazi2"><?php echo $cupon->acente; ?></span></td>
							<td nowrap="nowrap" valign="top" colspan="2" style="padding-left:5px"><span class="yazi1">Bilet No :</span><br /><span class="yazi2"><?php echo $cupon->biletno; ?></span></td>
						</tr>

						<tr class="kenarlik_td">
							<td align="left" style="padding-left:4px;font-size:9px"><span class="yazi1">Kalkış</span></td>
							<td width="67" align="center" nowrap="nowrap"><span class="yazi1">Uçuş Kodu</span></td>
							<td width="63" align="center" nowrap="nowrap"><span class="yazi1">Uçuş No</span></td>
							<td width="69" align="center" nowrap="nowrap"><span class="yazi1">Tarih</span></td>
							<td width="80" align="center" nowrap="nowrap"><span class="yazi1">Kalkış Saati</span></td>
							<td width="77" align="center" nowrap="nowrap"><span class="yazi1">Varış Saati</span></td>
							<td width="80" align="center" nowrap="nowrap"><span class="yazi1">Bagaj</span></td>
							<td width="47" align="center" nowrap="nowrap"><span class="yazi1">Durum</span></td>
						</tr>
						<tr class="kenarlik_td">
							<td valign="middle" height="30" style="padding-left:5px"><span class="yazi2"><?php echo $cupon->ai1name; ?> - <?php echo $cupon->ai2name; ?></span></td>

							<td align="center"><span class="yazi2"><?php echo $cupon->ucus_kod; ?></span></td>
							<td align="center"><span class="yazi2"><?php echo $cupon->ucus_no; ?></span></td>
							<td align="center"><span class="yazi2"><?php echo $cupon->ucus_tarih; ?></span></td>
							<td align="center"><span class="yazi2"><?php echo $cupon->ucus_saat1; ?></span></td>
							<td align="center"><span class="yazi2"><?php echo $cupon->ucus_saat2; ?></span></td>
							<td align="center"><span class="yazi2"><?php echo $cupon->bagaj; ?></span></td>
							<td align="center"><span class="yazi2"><?php echo $cupon->ucusdurum; ?></span></td>
						</tr>

						<tr class="kenarlik_td">
							<td colspan="9" style="font-size: 9px; color: #000000; background-color: #ffffff;padding-left:5px;">Bu Bilet Başkasına Devredilemez /           <br />
								Yolcuların Uçuştan 3 Saat Önce Havalimanında Olmaları Zorunludur.          <br />
								Uçuştan 2 gün önce verilen irtibat numaralarından uçuş bilgilerinizi lütfen onaylatınız.          </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding-bottom:15px">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" background="./img/nokta.gif"><img height="22" src="./img/makas.png" width="33" /></td>
				</tr>
			</table>

			<div style=""></div>
			<table width="742" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" valign="top" style="text-align:center;vertical-align:top">
						<table cellspacing="0" cellpadding="0" width="742">
							<tr>
								<td align="left" valign="top" style="text-align:left;vertical-align:top"></td>
								<td align="center" valign="top">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td align="left" valign="top">Telefon : 010 - 425 30 70 / 010 - 476 42 32                </td>
										</tr>
										<tr>
											<td align="left" valign="top">Web : www.ankafly.nl</td>
										</tr>
										<tr>
											<td align="left" valign="top">Fax : 010 - 425 35 68</td>
										</tr>
										<tr>
											<td align="left" valign="top">E-posta : info@skytrip.nl</td>
										</tr>
									</table>
								</td>
								<td align="right" valign="top" style="text-align:right;vertical-align:top"><img src="skytripimg/ucuskuponturoperatorlogo/bos.gif" width="220" height="80" alt="" /></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td align="left" nowrap="nowrap" style="padding-left:5px" valign="bottom"><span class="kupon_baslik">Yolcu Kuponu</span></td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="742" class="kenarlik_table">

							<tr class="kenarlik_td">
								<td height="20" style="padding-left:5px" valign="middle"><span class="yazi1">Yolcu Adı</span><br /><span class="yazi2"><?php echo $cupon->cinsiyet == "M" ? "MR" : "MS"; ?> <?php echo $cupon->yolcuad; ?></span></td>

								<td valign="top" colspan="4" height="30" style="padding-left:5px"><span class="yazi1">PNR&nbsp;:&nbsp;</span><span class="yazi2"><?php echo $cupon->pnr; ?></span><br />

									<span class="yazi1">Rezervasyon No&nbsp;:&nbsp;</span><br /><span class="yazi2"><?php echo $cupon->ucusno; ?></span>
								</td>

								<td valign="top" colspan="4" height="30" style="padding-left:5px"><span class="yazi1">Yolcu Bileti ve Bagaj Kontrolu :</span></td>
							</tr>


							<tr class="kenarlik_td">
								<td valign="middle" style="padding-left:5px"><span class="yazi1">Doğum Tarihi : <b><?php echo $cupon->dogumtarih; ?></b></span></td>

								<td valign="top" align="left" style="padding-left:5px" colspan="4"><span class="yazi1">Acente :</span><br /><span class="yazi2"><?php echo $cupon->acente; ?></span></td>

								<td nowrap="nowrap" valign="top" colspan="4" style="padding-left:5px"><span class="yazi1">Bilet No :</span><br /><span class="yazi2"><?php echo $cupon->biletno; ?></span></td>
							</tr>
							<tr class="kenarlik_td">
								<td align="left" style="padding-left:4px;font-size:9px"><span class="yazi1">Kalkış</span></td>

								<td width="67" align="center" nowrap="nowrap"><span class="yazi1">Hava Şirketi</span></td>
								<td width="67" align="center" nowrap="nowrap"><span class="yazi1">Uçuş Kodu</span></td>
								<td width="63" align="center" nowrap="nowrap"><span class="yazi1">Uçuş No</span></td>
								<td width="69" align="center" nowrap="nowrap"><span class="yazi1">Tarih</span></td>
								<td width="80" align="center" nowrap="nowrap"><span class="yazi1">Kalkış Saati</span></td>
								<td width="77" align="center" nowrap="nowrap"><span class="yazi1">Varış Saati</span></td>
								<td width="80" align="center" nowrap="nowrap"><span class="yazi1">Bagaj</span></td>
								<td width="47" align="center" nowrap="nowrap"><span class="yazi1">Durum</span></td>
							</tr>

							<tr class="kenarlik_td">
								<td valign="middle" height="30" style="padding-left:5px"><span class="yazi2"><?php echo $cupon->ai1name; ?> - <?php echo $cupon->ai2name; ?></span></td>
								<td align="center"><span class="yazi2"><?php echo $cupon->alname; ?></span></td>
								<td align="center"><span class="yazi2"><?php echo $cupon->ucus_kod; ?></span></td>
								<td align="center"><span class="yazi2"><?php echo $cupon->ucus_no; ?></span></td>
								<td align="center"><span class="yazi2"><?php echo $cupon->ucus_tarih; ?></span></td>
								<td align="center"><span class="yazi2"><?php echo $cupon->ucus_saat1; ?></span></td>
								<td align="center"><span class="yazi2"><?php echo $cupon->ucus_saat2; ?></span></td>
								<td align="center"><span class="yazi2"><?php echo $cupon->bagaj; ?></span></td>
								<td align="center"><span class="yazi2"><?php echo $cupon->ucusdurum; ?></span></td>
							</tr>

							<tr class="kenarlik_td">
								<td colspan="9" style="font-size: 9px; color: #000000; background-color: #ffffff;padding-left:5px;">Bu Bilet Başkasına Devredilemez /           <br />
									Yolcuların Uçuştan 3 Saat Önce Havalimanında Olmaları Zorunludur.          <br />
									Uçuştan 2 gün önce verilen irtibat numaralarından uçuş bilgilerinizi lütfen onaylatınız.          </td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
		</html>
