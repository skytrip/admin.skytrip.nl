<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 3.12.2018
 * Time: 20:34
 */

?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/plugin/bootstrap-datepicker/css/datepicker.css">

<style>
    .form-group{
        margin-bottom:15px;
    }

    .airport_country{
        font-weight: bold;
    }

    .airports{
        padding-left: 10px;
    }

    .error{
        border: 1px solid red;
    }
</style>
<div class="widget-body">
    <form id="routeForm" autocomplete="off" class="form-horizontal" method="post">
        <div class="col-xs-12">
            <fieldset>
                <legend>Rezervasyon Oluştur(Koltuk) >Kayıt No:<span class="text-warning">Yeni Kayıt</span></legend>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Acente:</label>
                            <div class="col-sm-8">
                                <select class="form-control"  name="acente" id="acente" required>
                                    <option value="">Lütfen Seçiniz</option>
                                    <?foreach ($acentes as $acente) {?>
                                        <option value="<?=$acente->acente_id?>" <?=$acente->acente_id == 13 ?'selected':''?>><?=$acente->acente_ad?></option>
                                    <?}?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Müşteri ID:</label>
                            <div class="col-sm-8">
                                <select required id="musteri" class="form-control" name="musteri" style="*width: 50%;float: left;" >
                                    <option value=""></option>
                                </select>
                            </div>
                            <!--                            <div class="col-sm-4"><button type="button" class="btn btn-default"><i class="fa fa-eye"></i> Göster</button></div>-->
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-4">
                            <select class="form-control" name="name">
                                <option>Ad + Soyad</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-default"><i class="fa fa-search"></i> Ara</button>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="row">
                <div class="col-sm-3">
                    <fieldset>
                        <legend>Yolcular</legend>
                        <div class="form-group">
                            <label class="control-label col-sm-7">Yetişkin:</label>
                            <div class="col-sm-5">
                                <select class="form-control" id="yetiskin" name="yetiskin" onchange="listRoutes();">
                                    <option>0</option>
                                    <option selected>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-7">Çocuk(2-12 Yaş Arası):</label>
                            <div class="col-sm-5">
                                <select class="form-control" id="cocuk" name="cocuk" onchange="listRoutes();">
                                    <option>0</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-7">Bebek(0-2 Yaş Arası):</label>
                            <div class="col-sm-5">
                                <select class="form-control" id="bebek" name="bebek" onchange="listRoutes();">
                                    <option>0</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-sm-6">
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-offset-1 col-sm-5 g-row">
                                <fieldset>
                                    <legend>Gidiş</legend>
                                    <small>Kalkış</small>
                                    <?$first = true;?>
                                    <select class="form-control disabled_attr" disabled id="gidis_kalkis" name="gidis_kalkis" onchange="return loadGidisVaris()">
                                        <?
                                        if(!empty($airports1)){
                                            foreach ($airports1 as $k=>$airportc) {
                                                ?>
                                                <option class="airport_country" value="country-<?=$k?>"><b><?=$countries[$k]?> - HEPSI</b></option>
                                                <?
                                                foreach ($airportc as $airport) {
                                                    ?>
                                                    <option class="airports" <?=$first? 'selected':''?> value="<?=$airport->code?>">&nbsp;&nbsp;&nbsp;&nbsp;<?=$airport->city?> (<?=$airport->code?>)</option>
                                                    <?
                                                    $first = false;
                                                }
                                                ?>
                                                <?
                                            }
                                        }
                                        ?>
                                    </select>
                                    <small>Varış</small>
                                    <select class="form-control" disabled id="gidis_varis" name="gidis_varis" onchange="return loadDonusKalkis();">
                                        <option class="airport_country" >HEPSI</option>
                                    </select>
                                    <br>
                                    <small>Gidiş Tarihi</small>
                                    <input type="text" placeholder="Gidiş Tarihi" required="" value="<?=!empty($firstDate) ? $firstDate : ''?>" id="range1" disabled name="gidis_tarih" class="form-control *datepicker g">
                                </fieldset>
                            </div>
                            <div class="col-sm-5 gd-row">
                                <fieldset>
                                    <legend>Dönüş</legend>
                                    <small>Kalkış</small>
                                    <select class="form-control gd" disabled id="donus_kalkis" name="donus_kalkis" onchange="return loadDonusVaris();">
                                    </select>
                                    <small>Kalkış</small>
                                    <select class="form-control gd" disabled id="donus_varis" name="donus_varis">
                                    </select>
                                    <br>
                                    <small>Dönüş Tarihi</small>
                                    <input type="text" placeholder="Dönüş Tarihi" required="" id="range2" disabled name="donus_tarih" class="form-control *datepicker gd">
                                </fieldset>
                            </div>
                            <div class="clearfix"></div>
                    </fieldset>
                </div>
                <div class="col-sm-3">
                    <fieldset>
                        <legend>Yön</legend>
                        <div class="form-group">
                            <input class="disabled_attr" type="radio" value="g" name="yon" disabled >
                            <label class="control-label">Tek Yön</label>
                        </div>
                        <div class="form-group">
                            <input class="disabled_attr" type="radio" value="gd" name="yon" disabled checked>
                            <label class="control-label">Gidiş-Dönüş</label>
                        </div>
                        <div class="form-group">
                            <button type="button" onclick="listRoutes();" class="btn btn-default disabled_attr" disabled><i class="fa fa-search"></i> Uçuş Ara</button>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="col-xs-12" id="route_container">
        </div>
        <div class="col-xs-12" id="musteri_container">
        </div>
        <div class="clearfix"></div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<script src="<?php echo base_url(); ?>js/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/plugin/select2/select2.min.js"></script>

<script>
    var range1,range2;
    var firstDate = "<?=!empty($firstDate) ? $firstDate : ''?>";
    var endDate = "<?=!empty($endDate) ? $endDate : ''?>";
    var existDates = <?=json_encode(!empty($existsDates) ? $existsDates : [])?>;
    $.fn.datepicker.dates['tr'] = {
        months: ['Ocak','Şubat','Mart','Nisan','Mayıs','Haziran','Temmuz','Ağustos','Eylül','Ekim','Kasım','Aralık'],
        monthsShort: ['Oca','Şub','Mar','Nis','May','Haz','Tem','Ağu','Eyl','Eki','Kas','Ara'],
        days: ['Pazar','Pazartesi','Salı','Çarşamba','Perşembe','Cuma','Cumartesi'],
        daysShort: ['Paz','Pts','Sal','Çar','Per','Cum','Cts'],
        daysMin: ['Pz','Pt','Sa','Ça','Pe','Cu','Ct'],
        today:'Bugün',
        clear:'Temizle'
    };

    $(function(){


        $(".disabled_attr").removeAttr('disabled');

        $("[name=yon]").on('change',function(){
            changeYon();
        });

        initRange1();
        changeYon();
        loadGidisVaris();

        $("#gidis_kalkis").select2();
        $("#musteri").select2({
            placeholder: 'Müşteri Ara',
            minimumInputLength: 1,
            ajax: {
                url: "index.php/reservation/searchMusteri",
                dataType: 'json',
                quietMillis: 250,
                cache: true
            }
        });

    });

    function loadGidisVaris(){
        var el,tar;
        el = $("#gidis_kalkis");
        tar = $("#gidis_varis");

        tar.empty();
        $.getJSON("index.php/reservation/searchUcus",{ucus:el.val()},function (data) {
            tar.removeAttr('disabled');
            var result = data.results;
            existDates = result.existsDates;
            firstDate = result.firstDate;
            endDate = result.endDate;
            initRange1();
            var firstS = true;
            $.each(result.airports,function (k,v) {
                tar.append('<option class="airport_country" value="country-'+ k +'">'+ result.countries[k] +' - HEPSI</option>');
                for(var i = 0; i < v.length;i++){
                    tar.append('<option class="airports" '+ (firstS ? 'selected' : '' ) +' value="'+ v[i].code +'">&nbsp;&nbsp;&nbsp;&nbsp;'+v[i].city + ' ' +(v[i].code) +'</option>');
                    firstS = false;
                }
            });
            tar.select2();
            loadDonusKalkis();
        });
    }

    function loadDonusKalkis() {
        if($("[name=yon]:checked").val() === 'gd') {
            var el = $("#gidis_kalkis");
            var el2 = $("#gidis_varis");
            var source = $("#donus_kalkis");
            var tar = $("#donus_varis");
            source.attr('disabled',true);
            tar.attr('disabled',true);
            source.empty();
            tar.empty();

            $.getJSON("index.php/reservation/searchDonus",{ucus:el.val(),varis:el2.val()},function (data) {
                source.removeAttr('disabled');
                var result = data.results;
                existDates = result.existsDates;
                firstDate = result.firstDate;
                endDate = result.endDate;
                initRange2();
                var firstS = true;
                if(Object.keys(result.airports).length>0) {
                    $.each(result.airports, function (k, v) {
                        source.append('<option class="airport_country" value="country-' + k + '">' + result.countries[k] + ' - HEPSI</option>');
                        for (var i = 0; i < v.length; i++) {
                            source.append('<option class="airports" ' + (firstS ? 'selected' : '') + ' value="' + v[i].code + '">&nbsp;&nbsp;&nbsp;&nbsp;' + v[i].city + ' ' + (v[i].code) + '</option>');
                            firstS = false;
                        }
                    });
                    source.select2();
                    firstS = true;
                    tar.removeAttr('disabled');
                    $.each(result.airports2, function (k, v) {
                        tar.append('<option class="airport_country" value="country-' + k + '">' + result.countries2[k] + ' - HEPSI</option>');
                        for (var i = 0; i < v.length; i++) {
                            tar.append('<option class="airports" ' + (firstS ? 'selected' : '') + ' value="' + v[i].code + '">&nbsp;&nbsp;&nbsp;&nbsp;' + v[i].city + ' ' + (v[i].code) + '</option>');
                            firstS = false;
                        }
                    });
                    tar.select2();
                }
            });
        }
    }

    function loadDonusVaris(){
        var el,tar;
        el = $("#donus_kalkis");
        tar = $("#donus_varis");

        tar.empty();
        $.getJSON("index.php/reservation/searchUcus",{ucus:el.val()},function (data) {
            tar.removeAttr('disabled');
            var result = data.results;
            endDate = result.endDate;
            var firstS = true;
            $.each(result.airports,function (k,v) {
                tar.append('<option class="airport_country" value="country-'+ k +'">'+ result.countries[k] +' - HEPSI</option>');
                for(var i = 0; i < v.length;i++){
                    tar.append('<option class="airports" '+ (firstS ? 'selected' : '' ) +' value="'+ v[i].code +'">&nbsp;&nbsp;&nbsp;&nbsp;'+v[i].city + ' ' +(v[i].code) +'</option>');
                    firstS = false;
                }
            });
            tar.select2();
        });
    }

    function initRange1(){
        $('#range1').datepicker('remove').datepicker({
            beforeShowDay: function(date){
                var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                return existDates[string] !== undefined;
            },
            autoclose:true,
            dateFormat:"yyyy-mm-dd",
            format:"yyyy-mm-dd",
            language:'tr',
            startDate:firstDate,
            endDate:endDate,
            todayHighlight: true
        });
    }

    function initRange2(startDate){
        $('#range2').datepicker('remove').datepicker({
            beforeShowDay: function(date){
                var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                return existDates[string] !== undefined;
            },
            autoclose:true,
            format: "yyyy-mm-dd",
            language:'tr',
            startDate: startDate,
            todayHighlight: true
        });
    }

    function listRoutes(reset) {
        if(reset === undefined){
            $("#gidis_page").val(0);
            $("#donus_page").val(0);
        }
        $(".error").removeClass('error');
        var acente = $("#acente");
        var yetiskin = $("#yetiskin");
        var cocuk = $("#cocuk");
        var bebek = $("#bebek");
        var gidis_kalkis = $("#gidis_kalkis");
        var donus_kalkis = $("#donus_kalkis");
        var gidis_varis = $("#gidis_varis");
        var donus_varis = $("#donus_varis");
        var gidis_page = $("#gidis_page").val() ? $("#gidis_page").val() : 0;
        var donus_page = $("#donus_page").val() ? $("#donus_page").val() : 0;

        var gidis_tarih = $("#range1");
        var donus_tarih = $("#range2");

        var musteri = $("#musteri");
        var yon = $("[name=yon]:checked");

        var koltuk = 0;
        koltuk = koltuk + parseInt(yetiskin.val());
        koltuk = koltuk + parseInt(cocuk.val());
        koltuk = koltuk + parseInt(bebek.val());
        // $("#route_container").html("");
        if(koltuk < 1 || koltuk === undefined || koltuk === ''){
            yetiskin.addClass('error');
            cocuk.addClass('error');
            bebek.addClass('error');
            showErrorMsg('Lütfen yolcu sayısı seçiniz');
            return;
        }else if(gidis_kalkis.find(':selected').val() === undefined){
            gidis_kalkis.addClass('error');
            showErrorMsg('Lütfen Gidiş için kalkış parkur seçiniz!');
            return;
        }else if(gidis_varis.find(':selected').val() === undefined){
            showErrorMsg('Lütfen Gidiş için Varış parkur seçiniz!');
            gidis_varis.addClass('error');
            return;
        }else if(gidis_kalkis.find(':selected').val() === gidis_varis.find(':selected').val()) {
            showErrorMsg("Lütfen Gidiş için Kalkış ve Varış aynı olamaz!");
            gidis_varis.addClass('error');
            gidis_kalkis.addClass('error');
            return;
        }else if(yon.val() === 'gd' && (donus_kalkis.find(':selected').val() === undefined)){
            showErrorMsg('Lütfen Dönüş için Kalkış parkur seçiniz!');
            donus_kalkis.addClass('error');
            return;
        }else if(yon.val() === 'gd' && (donus_varis.find(':selected').val() === undefined)){
            showErrorMsg('Lütfen Dönüş için Varış parkur seçiniz!');
            donus_varis.addClass('error');
            return;
        }else if(yon.val() === 'gd' && (gidis_kalkis.find(':selected').val() === donus_kalkis.find(':selected').val())){
            showErrorMsg("Lütfen Gidiş ve Dönüş aynı olamaz!");
            gidis_kalkis.addClass('error');
            donus_kalkis.addClass('error');
            return;
        }else if(yon.val() === 'gd' && (gidis_varis.find(':selected').val() === donus_varis.find(':selected').val())){
            showErrorMsg("Lütfen Gidiş ve Dönüş aynı olamaz!");
            gidis_varis.addClass('error');
            donus_varis.addClass('error');
            return;
        }else if(yon.val() === 'gd' && donus_kalkis.find(':selected').val() === donus_varis.find(':selected').val()) {
            showErrorMsg("Lütfen Dönüş için Kalkış ve Varış aynı olamaz!");
            donus_kalkis.addClass('error');
            donus_varis.addClass('error');
            return;
        }else if(gidis_tarih.val() === ""){
            showErrorMsg("Lütfen Gidiş tarihi seçiniz!");
            gidis_tarih.addClass('error');
            return;
        }else if(acente.val() === "" || acente.val() === undefined){
            showErrorMsg("Lütfen acente seçiniz!");
            acente.addClass('error');
            return;
        }else if(yon.val() === 'gd' && donus_tarih.val() === ""){
            showErrorMsg("Lütfen Dönüş tarihi seçiniz!");
            donus_tarih.addClass('error');
            return;
        }

        // $("#route_container").html("<div class='col-xs-12 text-center'><br><br><br><b>Lütfen Bekleyiniz!</b> <br> <i class='fa fa-refresh fa-spin fa-3x'></i> </div>");
        $.post("index.php/reservation/getRoutePrices",{
            gidis_kalkis:gidis_kalkis.find(':selected').val(),
            donus_kalkis:donus_kalkis.find(':selected').val(),
            gidis_varis:gidis_varis.find(':selected').val(),
            donus_varis:donus_varis.find(':selected').val(),
            gidis_tarih:gidis_tarih.val(),
            donus_tarih:donus_tarih.val(),
            musteri:musteri.val(),
            acente:acente.val(),
            yon:yon.val(),
            yetiskin:yetiskin.val(),
            cocuk:cocuk.val(),
            bebek:bebek.val(),
            gidis_page:gidis_page,
            donus_page:donus_page
        },function (data) {
            $("#route_container").html(data);
        }).fail(function(err){
            showErrorMsg("Bir hata ile karşılaşıldı");
        }).done(function () {

        });
    }

    function changeYon(){
        $(".gd").attr('disabled',true);
        $(".g").attr('disabled',true);
        var yon = $("[name=yon]:checked").val();
        if(yon === 'g'){
            $(".g").removeAttr('disabled');
        }else if(yon === 'gd'){
            initRange2();
            $(".g").removeAttr('disabled');
            $(".gd").removeAttr('disabled');
        }else{
            showErrorMsg('Geçersiz Yön!');
        }
    }

    function showErrorMsg(msg) {
        $.smallBox({
            title : "Uyarı",
            content : msg,
            color : "rgb(196, 106, 105)",
            timeout : 4000
        });
    }
</script>