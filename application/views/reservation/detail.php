<?php $acente_musteri = 2;?>
<link rel="stylesheet" href="js/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css" type="text/css" />
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-edit fa-fw "></i>
			Rezervasyon
			<span>>
				Rezervasyon Detayı
			</span>
		</h1>
	</div>
</div>
<section id="widget-grid">
	<div class="row">
		<article class="col-sm-4">
			<div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"
			data-widget-collapsed="true">
			<header>
				<span class="widget-icon"> <i class="fa fa-user"></i> </span>
				<h2>Acente</h2>
				<!-- Acente/Müşteri -->
			</header>
			<div>
				<div class="widget-body" style="min-height: 275px">
					<dl class="dl-list">
						<dt>
							<?php
							echo "Acente";
							// if (!is_nullempty($reservation_detail->acentekullanici)) {
							// 	$acente_musteri = 1;
							// 	echo "Acente";
							// } elseif (!is_nullempty($reservation_detail->musteriadsoyad)) {
							// 	$acente_musteri = 2;
							// 	echo "Müşteri";
							// }
							?>
						</dt>
						<dd>
							<?php
							echo $reservation_detail->acentekod . " - " . $reservation_detail->acentead;
							// if ($acente_musteri == 1) {
							// 	echo $reservation_detail->acentekod . " - " . $reservation_detail->acentead;
							// } elseif ($acente_musteri == 2) {
							// 	echo $reservation_detail->musteriadsoyad;
							// }
							?>
						</dd>
						<?php
						// if ($acente_musteri == 1) {
							if (!is_nullempty($reservation_detail->acentetel)) {
								?>
								<dt>Telefon</dt>
								<dd><?php echo $reservation_detail->acentetel; ?></dd>
								<?php
							}
							if (!is_nullempty($reservation_detail->acentegsm)) {
								?>
								<dt>GSM</dt>
								<dd><?php echo $reservation_detail->acentegsm; ?></dd>
								<?php
							}
							if (!is_nullempty($reservation_detail->acenteemail)) {
								?>
								<dt>E-Posta</dt>
								<dd><?php echo $reservation_detail->acenteemail; ?></dd>
								<?php
							}
						// }
						// elseif ($acente_musteri == 2) {
						// 	if (!is_nullempty($reservation_detail->musteritel1) && $reservation_detail->musteritel1 != "0") {
						// 		?>
						<!-- // 		<dt>Telefon 1</dt>
						// 		<dd><?php //echo $reservation_detail->musteritel1; ?></dd> -->
						 		<?php
						// 	}
						// 	if (!is_nullempty($reservation_detail->musteritel2) && $reservation_detail->musteritel2 != "0") {
						// 		?>
						<!-- // 		<dt>Telefon 2</dt> -->
						<!-- // 		<dd><?php //echo $reservation_detail->musteritel2; ?></dd> -->
						 		<?php
						// 	}
						// 	if (!is_nullempty($reservation_detail->musterigsm) && $reservation_detail->musterigsm != "0") {
						// 		?>
						<!-- // 		<dt>GSM</dt> -->
						<!-- // 		<dd><?php //echo $reservation_detail->musterigsm; ?></dd> -->
						 		<?php
						// 	}
						// 	if (!is_nullempty($reservation_detail->musteriemail1)) {
						// 		?>
						<!-- // 		<dt>E-Posta 1</dt> -->
						<!-- // 		<dd><?php //echo $reservation_detail->musteriemail1; ?></dd> -->
						 		<?php
						// 	}
						// 	if (!is_nullempty($reservation_detail->musteriemail2)) {
						// 		?>
						<!-- // 		<dt>E-Posta 2</dt> -->
						<!-- // 		<dd><?php //echo $reservation_detail->musteriemail2; ?></dd> -->
						 		<?php
						// 	}
						// }
						?>
						<dt>Yolcu Sayısı: <?php echo $reservation_detail->youlcucount; ?> / Bakiye: <?php echo $toplam; ?> €</dt>
						<table class="table table-bordered">
							<tbody>
								<tr>
									<td>
										<a class="btn btn-default" href="javascript:void(0);">
											<i class="fa fa-pencil" aria-hidden="true"></i> Acente Bilgilerini Düzenle
										</a>
									</td>
								</tr>
							</tbody>
						</table>
						<a href="http://adminpanel.skytrip.nl/#index.php/reservation?acente=<?php echo $reservation_detail->acente_id;?>" style="position: absolute;bottom: 30px;font-size:15px">Rezervasyon Listesi</a>
					</dl>
				</div>
			</div>
		</div>
	</article>
	<article class="col-sm-4">
		<div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"
		data-widget-collapsed="true">
		<header>
			<span class="widget-icon"> <i class="fa fa-home"></i> </span>
			<h2>Bilgiler</h2>
		</header>
		<div>
			<div class="widget-body" style="min-height: 275px">
				<?php //if ($acente_musteri == 1) : ?>
					<!-- <div>
						<strong> -->
							<?php
							// echo $reservation_detail->acentecinsiyet == "E" ? "Bay" : ($reservation_detail->acentecinsiyet == "K" ? "Bayan" : "");
							?>
							<?php
							// if($reservation_detail->acenteadsoyad != 0) {
							// 	echo $reservation_detail->acenteadsoyad;
							// }else{
							// 	echo "..";
							// }
							?>
						<!-- </strong>
					</div>
					<div><i class="fa fa-envelope"></i> <?php //echo $reservation_detail->acenteemail; ?></div>
					<div>
						<i class="fa fa-phone"></i> -->
						<?php
						// if($reservation_detail->acentetel != null) {
						// 	echo $reservation_detail->acentetel;
						// }else{
						// 	echo "..";
						// }?>
					<!-- </div> -->
				<?php //elseif ($acente_musteri == 2) : ?>
					<div>
						<strong>
							<?php echo $reservation_detail->mustericinsiyet == "E" ? "Bay" : ($reservation_detail->mustericinsiyet == "K" ? "Bayan" : ""); ?>
							<?php echo $reservation_detail->musteriadsoyad; ?>
						</strong>
						<div><i class="fa fa-home"></i> <?php echo $reservation_detail->musteriadres; ?></div>
						<div><?php echo $reservation_detail->musteri_sehir . " " . $reservation_detail->musteri_ulke; ?></div>
						<div><i class="fa fa-envelope"></i> <?php echo $reservation_detail->musteriemail1; ?></div>
						<div><i class="fa fa-phone"></i> <?php echo $reservation_detail->musteritel1; ?></div>
					</div>
				<?php //endif; ?>
			</div>
		</div>
	</div>
</article>
<article class="col-sm-4">
	<div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"
	data-widget-collapsed="true">
	<header>
		<span class="widget-icon"> <i class="fa fa-info-circle"></i> </span>
		<h2>Detay</h2>
	</header>
	<div>
		<div class="widget-body" style="min-height: 275px">
			<dl class="dl-horizontal left30">
				<dt>Rezervasyon-ID</dt><dd><?php echo $reservation_detail->rez_id; ?></dd>
				<dt>Kayıt Tarihi</dt><dd><?php echo date("d/m/Y D h:i", strtotime($reservation_detail->kayittarih)); ?></dd>
				<dt>Kaydeden</dt><dd><?php
				if ($acente_musteri == 1) {
					echo $reservation_detail->acenteadsoyad;
				} elseif ($acente_musteri == 2) {
					echo $reservation_detail->musteriadsoyad;
				}
				?></dd>
				<dt>Düzenleme Tar.</dt><dd><?php echo $reservation_detail->duzentarih == 0 ? "Düzenleme Yapılmamış" : date("d/m/Y D h:i", strtotime($reservation_detail->duzentarih)); ?></dd>
				<dt>Tur Operatorü</dt><dd><?php echo $reservation_detail->turoperator; ?></dd>
				<dt>Acente</dt><dd><?php echo $reservation_detail->acentead; ?></dd>
				<dt>Kayıt No</dt><dd><?php echo date("Y", strtotime($reservation_detail->kayittarih)); ?> - <?php echo $reservation_detail->rez_kayitno; ?></dd>
				<dt>Rezervasyon No</dt><dd><?php echo $reservation_detail->rez_pnr; ?></dd>
				<dt><?php echo $reservation_detail->turoperator; ?> PNR</dt><dd><?php echo $reservation_detail->rez_turoperatorpnr; ?></dd>
				<dt>PTA</dt><dd><?php echo $reservation_detail->rez_pta == "N" ? "Hayır" : "Evet"; ?></dd>
				<dt>Yön</dt><dd><?php echo $reservation_detail->rez_yon == "g" ? "Tek Yön" : "Gidiş Dönüş"; ?></dd>
				<dt>Durum</dt><dd><span class="<?php
				switch ($reservation_detail->rez_durum) {
					case "OK":
					echo "text-success";
					break;
					case "OP":
					echo "text-primary";
					break;
					case "CA":
					echo "text-danger";
					break;
					case "WA":
					echo "text-info";
					break;
				}
				?>"><?php echo $reservation_detail->rez_durum; ?></span></dd>
				<dt>Opsion Tarihi</dt><dd><?php echo date("d/m/Y D H:i", strtotime($reservation_detail->rez_opsiyontarih)); ?></dd>
			</dl>
		</div>
	</div>
</div>
</article>
<article class="col-sm-12">
	<div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"
	data-widget-collapsed="true">
	<header>
		<span class="widget-icon"> <i class="fa fa-bars"></i> </span>
		<h2>Uçuş Bilgisi</h2>
	</header>
	<div class="table-container borderedd">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Yolcu</th>
					<th>Uçuş ID</th>
					<th>Tarih</th>
					<th>Uçuş No</th>
					<th>Parkur</th>
					<th>Hava Şir.</th>
					<th>Var.Tarihi</th>
					<th>Kal.Saat</th>
					<th>Var.Saat</th>
					<th>Sınıf</th>
					<th>Durum</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php $srno=1; foreach ($ucus_list as $ucus) : ?>
					<tr>
						<td><?php echo $srno++; ?></td>
						<td><?php
						$GLOBALS["yolcu_list"] = $yolcu_list;
						$yolcular = explode(",", $ucus->yolcuad);
						$yolcular = array_map(
							function ($item) {
								$yolcu_list_first = $GLOBALS["yolcu_list"][0];
								return $item - $yolcu_list_first->yolcu_id + 1;
							}, $yolcular
						);
						sort($yolcular);
						if (count($yolcular) < 4) {
							echo implode(",", $yolcular);
						} else {
							echo '<span rel="tooltip" data-placement="top" data-original-title="' . implode(", ", $yolcular) . '">'
							. '<i class="fa fa-info-circle"></i> ' . count($yolcular) . 'pcx</span>';
						}
						?></td>
						<td><?php echo $ucus->ucus_id; ?></td>
						<td style="white-space: nowrap;"><?php echo sqldate_to_dmya($ucus->ucus_tarih); ?></td>
						<td><?php echo $ucus->ucus_kod . " " . $ucus->ucus_no; ?></td>
						<td><?php echo $ucus->ai1name . " (" . $ucus->ai1airportcode . ") - " . $ucus->ai2name . " (" .
						$ucus->ai2airportcode . ")"; ?></td>
						<td><?php echo $ucus->alname; ?></td>
						<td style="white-space: nowrap;"><?php echo $ucus->ucus_varistarih != null ? sqldate_to_dmya($ucus->ucus_varistarih) : ""; ?></td>
						<td><?php echo $ucus->ucus_saat1; ?></td>
						<td><?php echo $ucus->ucus_saat2; ?></td>
						<td><?php echo $ucus->sinif_tanim; ?></td>
						<td><?php
						$color = "";
						if ($ucus->ucusdurum == "OK") {
							$color = "text-success";
						} elseif ($ucus->ucusdurum == "OP") {
							$color = "text-primary";
						} elseif ($ucus->ucusdurum == "CA") {
							$color = "text-danger";
						}
						echo '<span class="' . $color . '"><strong>' . $ucus->ucusdurum . '</strong></span>';
						?></td>
						<td>
							<a href="#" class="label label-primary" title="E-Mail Gönder"><i class="fa fa-envelope"></i>&nbsp;<span class="badge">0</span></a>
							<a href="#"><i class="fa fa-pencil" title="Düzenle"></i></a>
							<a href="#"><i class="fa fa-power-off" title="Çıkar"></i></a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
</article>
<article class="col-sm-12">
	<div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"
	data-widget-collapsed="true">
	<header>
		<span class="widget-icon"> <i class="fa fa-bars"></i> </span>
		<h2>Yolcu Bilgisi</h2>
	</header>
	<div class="table-container borderedd">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Cins.</th>
					<th>Ad</th>
					<th>Soyad</th>
					<th>Doğ.Tar.</th>
					<th>Bilet No</th>
					<th>Kolt.Fiy.</th>
					<th>Komisyon</th>
					<th>Vergi</th>
					<th>Net Mly.</th>
					<th>Sat.Fiy.</th>
					<th>Kâr</th>
					<th>Bilet</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php $srno=1; foreach ($yolcu_list as $yolcu) : ?>
					<tr>
						<td><?php echo $srno; ?> <input type="checkbox" id="srno<?php echo $srno; ?>" /></td>
						<td><?php echo $yolcu->yolcu_cinsiyet . " (" . $yolcu->yolcu_tipi . ")"; ?></td>
						<td><?php echo $yolcu->yolcu_ad; ?></td>
						<td><?php echo $yolcu->yolcu_soyad; ?></td>
						<td><?php echo $yolcu->yolcu_dtarih; ?></td>
						<td><?php echo $yolcu->yolcu_biletno; ?></td>
						<td class="text-right"><?php echo $yolcu->yolcu_koltukfiyat; ?></td>
						<td class="text-right"></td>
						<td class="text-right"><?php echo $yolcu->yolcu_vergi; ?></td>
						<td class="text-right"><?php echo $yolcu->yolcu_koltukfiyat + $yolcu->yolcu_vergi; ?></td>
						<td class="text-right"><?php echo $yolcu->yolcu_satisfiyat; ?></td>
						<td class="text-right"><?php echo number_format($yolcu->yolcu_satisfiyat - $yolcu->yolcu_koltukfiyat - $yolcu->yolcu_vergi, 2); ?></td>
						<td>
							<div class="dropdown">
								<a class="label label-primary" data-toggle="dropdown" aria-haspoup="true">İşlemler<span class="caret"></span></a>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="<?php echo site_url(); ?>/reservation/print_cupon/<?php echo $yolcu->yolcu_id; ?>" target="_blank"><i class="fa fa-print"></i> Uçuş Kuponu (Yazdır)</a></li>
									<li><a href="<?php echo site_url(); ?>/reservation/pdf_cupon/<?php echo $yolcu->yolcu_id; ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> Uçuş Kuponu (PDF)</a></li>
									<li><a href="<?php echo site_url(); ?>/reservation/print_ticket/<?php echo $yolcu->yolcu_id; ?>" target="_blank"><i class="fa fa-print"></i> Elektronik Bilet (Yazdır)</a></li>
									<li><a href="#"><i class="fa fa-file-pdf-o"></i> Elektronik Bilet (PDF)</a></li>
								</ul>
							</div>
						</td>
						<td>
							<a href="#"><i class="fa fa-pencil" title="Düzenle"></i></a>
							<a href="#"><i class="fa fa-trash-o" title="Sil"></i></a>
						</td>
					</tr>
				<?php endforeach; ?>
				<tr>
					<td colspan="14">
						&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-long-arrow-right"></i> <a class="label label-primary">Yolcu Ayır</a>
					</td>
				</tr>
				<tr>
					<td colspan="6" class="text-right">Rezervasyon Toplam:</td>
					<td class="text-right"><?php echo number_format($fiyatlar["koltukfiyat"], 2); ?></td>
					<td class="text-right"><?php echo number_format(0, 2); ?></td>
					<td class="text-right"><?php echo number_format($fiyatlar["vergi"], 2); ?></td>
					<td class="text-right"><?php echo number_format($fiyatlar["koltukfiyat"] + $fiyatlar["vergi"], 2); ?></td>
					<td class="text-right"><?php echo number_format($fiyatlar["satis"], 2); ?></td>
					<td class="text-right"><?php echo number_format($fiyatlar["kar"], 2); ?></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td colspan="14"><hr /></td>
				</tr>
				<tr>
					<td colspan="7"></td>
					<td colspan="2" class="text-right warning">Genel Toplam:</td>
					<td class="text-right warning"><?php echo number_format($fiyatlar["koltukfiyat"] + $fiyatlar["vergi"], 2); ?></td>
					<td class="text-right warning"><?php echo number_format($fiyatlar["satis"], 2); ?></td>
					<td class="text-right warning"><?php echo number_format($fiyatlar["satis"] - $fiyatlar["koltukfiyat"] - $fiyatlar["vergi"], 2); ?></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td colspan="7"></td>
					<td colspan="2" class="text-right warning"><a href="#"><i class="fa fa-plus-circle"></i></a> Alınan:</td>
					<td colspan="3" class="text-center warning"><?php echo $gelirgider_data->gelir === null ? "0.00" : number_format($gelirgider_data->gelir, 2); ?></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td colspan="7"></td>
					<td colspan="2" class="text-right warning"><a href="#"><i class="fa fa-plus-circle"></i></a> İade:</td>
					<td colspan="3" class="text-center warning"><?php echo $gelirgider_data->gider === null ? "0.00" : number_format($gelirgider_data->gider, 2); ?></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td colspan="7"></td>
					<td colspan="2" class="text-right warning">Kalan:</td>
					<td colspan="3" class="text-center warning <?php
					$kalan = $fiyatlar["satis"] - ($gelirgider_data->gelir === null ? 0 : $gelirgider_data->gelir)
					+ ($gelirgider_data->gider === null ? 0 : $gelirgider_data->gider);
					if ($kalan > 0) {
						echo "text-danger";
					} elseif ($kalan < 0) {
						echo "text-primary";
					} else {
						echo "text-success";
					}
					?>"><?php echo number_format($kalan, 2); ?></td>
					<td colspan="2"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
</article>
</div>
</section>
<script type="text/javascript" src="js/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$("body").removeClass("modal-open");
	// if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$(".borderedd").mCustomScrollbar({
			axis: "x",
			setWidth: "100%",
			theme: "dark"
		});
	// }
	loadScript("js/plugin/slimscroll/jquery.slimscroll.min.js");
})
</script>
