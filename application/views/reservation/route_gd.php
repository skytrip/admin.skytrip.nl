<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 8.12.2018
 * Time: 17:10
 */
error_reporting(E_ALL);
$donus_routes = isset($donus_routes) ? $donus_routes : null;
$yon = isset($title2) ? 'gd' : 'g';
?>
<style>
    .flash_label td{
        background: #F8DEAB!important;
    }
</style>
<div class="row">
    <?if(isset($title2)){?>

        <div class="col-sm-6">
            <?php
            $this->load->view("reservation/route_g",array('data'=>isset($gidis_routes) ? $gidis_routes : null,'title'=>$title,'yon'=>'g','toplam_koltuk'=>$postedData['toplam_koltuk'],'page'=>$gidis_page));
            ?>
        </div>
        <div class="col-sm-6">
            <?php
            $this->load->view("reservation/route_g",array('data'=>isset($donus_routes) ? $donus_routes : null,'title'=>$title2,'yon'=>'d','toplam_koltuk'=>$postedData['toplam_koltuk'],'page'=>$donus_page));
            ?>
        </div>
    <?}else{?>
        <div class="col-sm-12">
            <?php
            $this->load->view("reservation/route_g",array('data'=>isset($gidis_routes) ? $gidis_routes : null,'title'=>$title,'yon'=>'g','toplam_koltuk'=>$postedData['toplam_koltuk'],'page'=>$gidis_page));
            ?>
        </div>
    <?}?>
    <div class="clearfix"></div>
    <hr>

    <?if(isset($title2)){?>

        <div class="col-sm-5"  id="costTbl">

        </div>
        <div class="col-sm-2 text-center">
            <h3 style="font-weight: bold;margin-top:0;">Toplam</h3>
            <h1 id="toplamTbl" style="font-weight: bold"></h1>
            <button type="button" class="btn btn-default goToStep2" onclick="goToStep2(this)">Rezervasyon <i class="fa fa-arrow-circle-right"></i></button>

        </div>
        <div class="col-sm-5" id="costTbl2">

        </div>
    <?}else{?>
        <div class="col-sm-8" id="costTbl">

        </div>
        <div class="col-sm-4 text-center">
            <h3 style="font-weight: bold;margin-top:0;">Toplam</h3>
            <h1 id="toplamTbl" style="font-weight: bold">0.00 €</h1>
            <button type="button" class="btn btn-default goToStep2" onclick="goToStep2(this)">Rezervasyon <i class="fa fa-arrow-circle-right"></i></button>
        </div>

    <?}?>
    <div class="clearfix"></div>
    <br>
    <div class="col-xs-12" id="musteriForm">

    </div>
</div>
<br><br><br>
<div class="clearfix"></div>
<script>
    var gidis = <?=json_encode(isset($gidis_routes) ? $gidis_routes : [])?>;
    var donus = <?=json_encode(isset($donus_routes) ? $donus_routes : [])?>;
    var ucretler = <?=json_encode([])?>;
    var yon = '<?=$yon?>';
    var postedData = <?=json_encode($postedData)?>;

    $(function(){
        $("[name=route_yon_g],[name=route_yon_d]").on('change',function () {
            $(".goToStep2").removeAttr('disabled');
            $("#musteriForm").html("");
            $(".flash_label_"+ $(this).data('yon')).find(".flash_label").removeClass('flash_label');
            $(this).parent().parent().parent().addClass('flash_label');
            initCosts();
        });
        initCosts();

        $(".click_label").on('click',function(){

            $("#"+$(this).attr('for')).trigger('click');
        });
    });

    function changePage(el,action) {
        var element = $(el);
        var curPage = parseInt(element.val());
        if(action === 1){
            curPage = curPage + 1;
            element.val(curPage);
            listRoutes(true);
        }else if(action === 0){
            if(curPage > 0){
                curPage = curPage - 1;
                element.val(curPage);
                listRoutes(true);
            }
        }
    }

    function goToStep2(element){
        var gidisRoute = $("[name=route_yon_g]:checked").val();
        if(gidisRoute === undefined || gidisRoute === "" || gidis[gidisRoute] === undefined){
            showErrorMsg('Lütfen gidiş için route seçiniz.');
            return false;
        }
        var donusRoute = "";
        var donusYasTarih = "";
        if(yon === 'gd'){
            donusRoute = $("[name=route_yon_d]:checked").val();
            if(donusRoute === undefined || donusRoute === "" || donus[donusRoute] === undefined){
                showErrorMsg('Lütfen dönüş için route seçiniz.');
                return false;
            }
            if(gidis[gidisRoute].route_tekyonizin === 'E'){
                showErrorMsg('Gidiş için seçilen route tek yönlüdür.');
                return false;
            }
            if(donus[donusRoute].route_tekyonizin === 'E'){
                showErrorMsg('Dönüş için seçilen route tek yönlüdür.');
                return false;
            }
            if(donusRoute !== "" && gidis[gidisRoute].route_farklihsirketizin === 'E' && gidis[gidisRoute].ucus_kod !== donus[donusRoute].ucus_kod){
                showErrorMsg('Dönüş için seçilen route hava yolu dönüş için aynı olmalıdır.');
                return false;
            }
            donusYasTarih = donus[donusRoute].ucus_tarih;
        }

        var musteri = $("#musteri").val();
        $(element).attr('disabled',1);
        $("#musteriForm").html("<div class='col-xs-12 text-center'><br><br><br><b>Lütfen Bekleyiniz!</b> <br> <i class='fa fa-refresh fa-spin fa-3x'></i> </div>");
        $.post("index.php/reservation/getMusteriForm",{postedData:postedData,gidisRoute:gidisRoute,donusRoute:donusRoute,yon:yon,gidisTarih:gidis[gidisRoute].ucus_tarih,donusTarih:donusYasTarih},function (data) {
            $("#musteriForm").html(data)
        }).fail(function (err) {
            $(this).removeAttr('disabled');
            $("#musteriForm").html("Bir hata ile karşılaşıldı");
        });
    }

    function initCosts(){
        var route = $("[name=route_yon_g]:checked").val();
        var selectedRoute = gidis[route];
        var toplam = calculateCosts(postedData.title,selectedRoute,postedData,'costTbl');
        if(yon === 'gd'){
            route = $("[name=route_yon_d]:checked").val();
            selectedRoute = donus[route];
            toplam = toplam + calculateCosts(postedData.title2,selectedRoute,postedData,'costTbl2');
        }
        if(toplam > 0){
            $("#toplamTbl").html(toplam.toFixed(2) + ' €');
        }else{
            $(".goToStep2").attr('disabled',true);
        }
    }

    function calculateCosts(title,selectedRoute,postedData,costElement){
        if(selectedRoute === undefined)
            return false;
        var costs = [];
        var toplam = 0;
        var i = 0;
        if(postedData.yetiskin > 0){
            costs[i++] = [
                "Yetişkin - Koltuk Fiyatı",
                postedData.yetiskin + " / " + selectedRoute.ucusfiyat_yetiskinsatisfiyat + " €",
                (postedData.yetiskin * selectedRoute.ucusfiyat_yetiskinsatisfiyat).toFixed(2) + " €"
            ];
            toplam = toplam + (postedData.yetiskin * selectedRoute.ucusfiyat_yetiskinsatisfiyat);
        }
        if(postedData.cocuk > 0) {
            costs[i++] = [
                "Çocuk - Koltuk Fiyatı",
                postedData.cocuk + " / " + selectedRoute.ucusfiyat_cocuksatisfiyat + " €",
                (postedData.cocuk * selectedRoute.ucusfiyat_cocuksatisfiyat).toFixed(2) + " €"
            ];
            toplam = toplam + (postedData.cocuk * selectedRoute.ucusfiyat_cocuksatisfiyat);
        }
        if(postedData.bebek > 0) {
            costs[i++] = [
                "Bebek - Koltuk Fiyatı",
                postedData.bebek + " / " + selectedRoute.ucusfiyat_bebeksatisfiyat + " €",
                (postedData.bebek * selectedRoute.ucusfiyat_bebeksatisfiyat).toFixed(2) + " €"
            ];
            toplam = toplam + (postedData.bebek * selectedRoute.ucusfiyat_bebeksatisfiyat);
        }

        costs[i++] = [
            '',
            'Toplam: ',
            toplam.toFixed(2) + " €"
        ];
        createTable(postedData.title,costs,costElement);
        return toplam;
    }

    function createTable(title,costs,element) {
        if(costs === undefined || !costs.length)
            return;
        var body = document.getElementById(element);
        var tbl = document.createElement("table");
        var tblHead = document.createElement("thead");
        var row = document.createElement("tr");
        var cell = document.createElement("th");
        var cellText = document.createTextNode(title);
        cell.setAttribute('colspan','3');
        cell.appendChild(cellText);
        row.appendChild(cell);
        tblHead.appendChild(row);
        tbl.appendChild(tblHead);
        var tblBody = document.createElement("tbody");
        for (var j = 0; j < costs.length; j++) {
            var cost = costs[j];
            row = document.createElement("tr");
            for (var i = 0; i < cost.length; i++) {
                var c = cost[i];
                cell = document.createElement("td");
                cellText = document.createTextNode(c);
                cell.appendChild(cellText);
                row.appendChild(cell);
            }
            tblBody.appendChild(row);
        }
        tbl.appendChild(tblBody);
        body.innerHTML = "";
        body.appendChild(tbl);
        tbl.classList.add("table");
        tbl.classList.add("table-striped");
        tbl.classList.add("table-condensed");
        tbl.classList.add("table-bordered");
        tbl.classList.add("table-hover");
    }

</script>
