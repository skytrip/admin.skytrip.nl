<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 8.12.2018
 * Time: 16:12
 */

$daysShort = ['Paz','Pts','Sal','Çar','Per','Cum','Cts'];

?>
<div class="widget-body">
    <div class="row">
        <div class="col-xs-12">
            <fieldset>
                <legend class="flight-header">
                    <?if($yon == 'g'){?>
                        <div class="going">Gidiş</div>
                    <?}else{?>
                        <div class="return">Dönüş</div>
                    <?}?>
                    <span class="rota">
                    <b style="line-height: 32px;"><?=strtoupper($title)?></b>
                    </span>
                </legend>
                <table class="table table-striped *table-condensed table-bordered table-hover flash_label_<?=$yon?>">
                    <thead>
                    <tr>
                        <th></th>
                        <th><small>Tarih</small></th>
                        <th><small>Parkur</small></th>
                        <th><small>Uçuş No</small></th>
                        <th><small>Kalkış</small></th>
                        <th><small>Varış</small></th>
                        <th><small>Koltuk</small></th>
                        <th><small>Fiyat</small></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?
                    if(!count($data)){
                        echo "<tr><td colspan='10'>Kayıt Bulunamadı!</td></tr>";
                    }else {
                        $i = 0;
                        foreach ($data as $item) {
                            $disabled = $toplam_koltuk > $item->ucusfiyat_kalankoltuksayi;
                            ?>
                            <tr>
                                <td><small><input type="radio" <?=$disabled? 'disabled' : ''?> data-yon="<?= $yon ?>" id="route_click_<?= $yon ?>_<?=$item->ucusfiyat_id?>" name="route_yon_<?= $yon ?>" value="<?=$item->ucusfiyat_id?>"></small></td>
                                <td class="click_label click_label_<?=$yon?>" data-yon="<?=$yon?>" for="route_click_<?= $yon ?>_<?=$item->ucusfiyat_id?>"><small><?= date('d-m-Y',strtotime($item->ucus_tarih)). " ".$daysShort[date('w',strtotime($item->ucus_tarih))]?></small></td>
                                <td class="click_label click_label_<?=$yon?>" data-yon="<?=$yon?>" for="route_click_<?= $yon ?>_<?=$item->ucusfiyat_id?>"><small><?= "$item->ap1city-$item->ap2city" ?></small></td>
                                <td class="click_label click_label_<?=$yon?>" data-yon="<?=$yon?>" for="route_click_<?= $yon ?>_<?=$item->ucusfiyat_id?>"><small><?= "$item->ucus_kod $item->ucus_no" ?></small></td>
                                <td class="click_label click_label_<?=$yon?>" data-yon="<?=$yon?>" for="route_click_<?= $yon ?>_<?=$item->ucusfiyat_id?>"><small><?= date('H:i',strtotime($item->ucus_saat1))?></small></td>
                                <td class="click_label click_label_<?=$yon?>" data-yon="<?=$yon?>" for="route_click_<?= $yon ?>_<?=$item->ucusfiyat_id?>"><small><?= date('H:i',strtotime($item->ucus_saat2))?></small></td>
                                <td class="click_label click_label_<?=$yon?>" data-yon="<?=$yon?>" for="route_click_<?= $yon ?>_<?=$item->ucusfiyat_id?>"><small><?= "{$item->ucusfiyat_kalankoltuksayi}" ?></small></td>
                                <td class="click_label click_label_<?=$yon?>" data-yon="<?=$yon?>" for="route_click_<?= $yon ?>_<?=$item->ucusfiyat_id?>"><small><?= "$item->ucusfiyat_yetiskinsatisfiyat €" ?></small></td>
                            </tr>
                            <?
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-xs-6">
                        <a style="cursor: pointer;" onclick="return changePage('#<?=$yon == 'g' ? 'gidis_page': 'donus_page' ?>',0)"><i class="fa fa-arrow-circle-left"></i> <i class="fa fa-angle-double-left"></i> Önceki</a>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a style="cursor: pointer;" onclick="return changePage('#<?=$yon == 'g' ? 'gidis_page': 'donus_page' ?>',1)" >Sonraki <i class="fa fa-angle-double-right"></i> <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    <input id="<?=$yon == 'g' ? 'gidis_page': 'donus_page' ?>" name="<?=$yon == 'g' ? 'gidis_page': 'donus_page' ?>" type="hidden" value="<?=$page ? : 0?>">
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class="clearfix"></div>
