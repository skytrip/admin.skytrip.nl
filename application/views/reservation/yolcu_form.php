<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 15.12.2018
 * Time: 13:59
 */
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/plugin/bootstrap-datepicker/css/datepicker.css">
<div class="row">
    <div class="col-xs-12">
        <table class="table table-striped table-condensed table-bordered table-hover">
            <tr>
                <th>#</th>
                <th>Cinsiyet</th>
                <th>Soyad</th>
                <th>Ad</th>
                <th>Doğum Tarihi</th>
                <th>Bilet No</th>
            </tr>

            <?for($i=1;$i<=$postedData['yetiskin'];$i++) {?>
                <tr>
                    <td><?=$i?></td>
                    <td>
                        <select name="Yolcu[<?=$i?>][cinsiyet]" id="cinsiyet_<?=$i?>" class="form-control yolcu_input" data-queue="<?=$i?>">
                            <option value="ADT_M">MR.</option>
                            <option value="ADT_F">MRS.</option>
                        </select>
                    </td>
                    <td><input class="form-control yolcu_input" name="Yolcu[<?=$i?>][soyisim]" id="soyisim_<?=$i?>" placeholder="Soyad"></td>
                    <td><input class="form-control yolcu_input" name="Yolcu[<?=$i?>][isim]" id="isim_<?=$i?>" placeholder="Ad"></td>
                    <td>
                        <div class="input-group input-group-sm">
                            <input class="form-control datepicker1 yolcu_input" name="Yolcu[<?=$i?>][dogumtarih]" id="dogumtarih_<?=$i?>" placeholder="Doğum Tarihi">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </td>
                    <td><input class="form-control" name="yolcu[<?=$i?>][biletno]" id="biletno_<?=$i?>" placeholder="Bilet no"></td>
                </tr>
            <?}?>
            <?for($i=1;$i<=$postedData['cocuk'];$i++) {?>
                <tr>
                    <td><?=$i?></td>
                    <td>
                        <select name="Yolcu[<?=$i?>][cinsiyet]" id="cinsiyet_<?=$i?>" class="form-control yolcu_input" data-queue="<?=$i?>">
                            <option value="CHD_M">MSTR-CHD.</option>
                            <option value="CHD_F">MISS-CHD.</option>
                        </select>
                    </td>
                    <td><input class="form-control yolcu_input" name="Yolcu[<?=$i?>][soyisim]" id="soyisim_<?=$i?>" placeholder="Soyad"></td>
                    <td><input class="form-control yolcu_input" name="Yolcu[<?=$i?>][isim]" id="isim_<?=$i?>" placeholder="Ad"></td>
                    <td>
                        <div class="input-group input-group-sm">
                            <input class="form-control datepicker2 yolcu_input" name="Yolcu[<?=$i?>][dogumtarih]" id="dogumtarih_<?=$i?>" placeholder="Doğum Tarihi">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </td>
                    <td><input class="form-control" name="yolcu[<?=$i?>][biletno]" id="biletno_<?=$i?>" placeholder="Bilet no"></td>
                </tr>
            <?}?>
            <?for($i=1;$i<=$postedData['bebek'];$i++) {?>
                <tr>
                    <td><?=$i?></td>
                    <td>
                        <select name="Yolcu[<?=$i?>][cinsiyet]" id="cinsiyet_<?=$i?>" class="form-control yolcu_input" data-queue="<?=$i?>">
                            <option value="INF_M">MSTR-INF.</option>
                            <option value="INF_F">MISS-INF.</option>
                        </select>
                    </td>
                    <td><input class="form-control yolcu_input" name="Yolcu[<?=$i?>][soyisim]" id="soyisim_<?=$i?>" placeholder="Soyad"></td>
                    <td><input class="form-control yolcu_input" name="Yolcu[<?=$i?>][isim]" id="isim_<?=$i?>" placeholder="Ad"></td>
                    <td>
                        <div class="input-group input-group-sm">
                            <input class="form-control datepicker3 yolcu_input" name="Yolcu[<?=$i?>][dogumtarih]" id="dogumtarih_<?=$i?>" placeholder="Doğum Tarihi">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </td>
                    <td><input class="form-control" name="yolcu[<?=$i?>][biletno]" id="biletno_<?=$i?>" placeholder="Bilet no"></td>
                </tr>
            <?}?>
        </table>
        <button type="button" class="btn btn-success pull-right" onclick="musteriSave(this);"><i class="fa fa-save"></i> Kaydet</button>
    </div>
    <div class="col-xs-12" id="rez_detail">

    </div>
</div>
<script src="<?php echo base_url(); ?>js/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script>
    var postedData = <?=json_encode($postedData)?>;
    var gidisRoute = '<?=$gidisRoute?>';
    var donusRoute = '<?=isset($donusRoute) ? $donusRoute : ''?>';
    $('.datepicker1').datepicker({
        autoclose:true,
        dateFormat:"yyyy-mm-dd",
        format:"yyyy-mm-dd",
        lang:'tr'
    });
    $('.datepicker2').datepicker({
        autoclose:true,
        dateFormat:"yyyy-mm-dd",
        format:"yyyy-mm-dd",
        lang:'tr'
    });
    $('.datepicker3').datepicker({
        autoclose:true,
        dateFormat:"yyyy-mm-dd",
        format:"yyyy-mm-dd",
        lang:'tr'
    });

    function musteriSave(btn) {
        $(".error").removeClass('error');
        var save = true;
        $.each($(".yolcu_input"),function (k,v) {
            element = $(v);
            if(element.val() === undefined || element.val() === ''){
                if(save)
                    showErrorMsg("Lütfen yolcu bilgilerini doldurunuz!");
                save = false;
                element.addClass('error');
            }
        });
        if(save){
            if(confirm('Rezervasyon oluşturmak istediğinizden emin misiniz?')){
                // $(btn).attr('disabled',1);
                var data = $("#musteriForm input,#musteriForm select").serializeArray();
                data.push({name:'gidisRoute',value:gidisRoute});
                data.push({name:'donusRoute',value:donusRoute});
                $.each(postedData,function (k,v) {
                    data.push({name:'postedData['+k+']',value:v});
                });
                console.log(data);

                $.post('index.php/reservation/saveRez',data,function (data) {
                    var json = JSON.parse(data);
                    if(json.status === 'NOK'){
                        showErrorMsg(json.message);
                        $("#rez_detail").html(json.message);
                        $(btn).removeAttr('disabled');
                    }else{
                        window.location = "index.php#index.php/reservation/detail/"+json.rez_id;
                    }
                }).fail(function (err) {
                    showErrorMsg("Bir hata ile karşılaşıldı. Lütfen tekrar deneyiniz.");
                    $("#rez_detail").html("Bir hata ile karşılaşıldı");
                    $(btn).removeAttr('disabled');
                });
            }
        }else{
            console.log("eksik element");
        }
    }
</script>
