<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title></title>
<style>
body {
  color: #000000;
  font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
h1
{
  font-family: Arial, Helvetica, Sans-Serif;
  font-size: 16px;
  font-style: normal;
  font-weight: bold;
  text-align: left;
}

.baslik td {
  border:1px solid #000000;
}
.renk1 {
	background-color:#cbc9d1;
}

.renk2 {
	background-color:#e5e4e8;
}
.anabaslik {
  font-size:18px;
	color:#000000;
	font-family: Arial, Helvetica, sans-serif
}
.kenarlik_td td {
  border-left:1px solid #000000;
	border-bottom:1px solid #000000;
}
.renkli_alan {
  background-color: #d9d9d9;
}
@page{
	size:A4;
}
</style>
</head>
<body>
<div style="page-break-after:always">
<table cellspacing="0" cellpadding="0" border="0" id="table1" align="center">
  <tr>
    <td align="center" nowrap="nowrap"><h3><b>ELEKTRONİK YOLCU BİLETİ<br /></b></h3></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td width="100%" align="center">
      <table cellspacing="2" cellpadding="2" width="100%" border="0" id="table2">
        <tr>
          <td width="60%" align="left" nowrap="nowrap">
            <table cellspacing="0" cellpadding="0" width="0" border="0" id="table3">
              <tr>
                <td width="100%" align="left" valign="top"><img src="skytripimg/elektronikbiletturoperatorlogo/bos.gif" alt="" width="220" height="100" /></td>
              </tr>
            </table>
          </td>
					<td align="right" width="40%" nowrap="nowrap" valign="middle">
					  <table>
						  <tr>
							  <td align="left" valign="top">SkyTrip<br />MATHENESERPLEIN 99<br />Telefon : 010 - 425 30 70 / 010 - 476 42 32<br />Fax : 010 - 425 35 68<br />info@skytrip.nl<br />http://www.ankafly.nl</td>
              </tr>
						</table>
					</td>
        </tr>
      </table>
    </td>
  </tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table cellspacing="2" cellpadding="2" width="0" border="0" id="table4">
			<tr>
				<td>Yolcu Adı</td>
				<td align="middle" width="15">:</td>
				<td><?php echo $ticket->cinsiyet == "M" ? "MR" : "MS"; ?> <?php echo $ticket->yolcuad; ?> <?php echo $ticket->dogumtarih; ?></td>
			</tr>
			<tr>
				<td>Rezervasyon No</td>
				<td align="middle">:</td>
				<td><b><?php echo $ticket->ucusno; ?></b></td>
			</tr>
      <tr>
				<td>PNR</td>
				<td align="middle">:</td>
				<td><b><?php echo $ticket->pnr; ?></b></td>
			</tr>
			<tr>
				<td>Bilet No</td>
				<td align="middle">:</td>
				<td><?php echo $ticket->biletno; ?></td>
			</tr>
			<tr>
				<td>Tarih</td>
				<td align="middle">:</td>
				<td><?php echo $ticket->kayittarih; ?></td>
			</tr>
					</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
      <table cellspacing="0" cellpadding="0" width="100%" class="kenarlik_td" style="border-top:1px solid #000000;border-right:1px solid #000000">
			  <tr class="renkli_alan">
          <td align="center" nowrap="nowrap">Hava Şirketi<br /></td>
          <td align="center" nowrap="nowrap">Uçuş No</td>
          <td align="center" nowrap="nowrap">Kalkış / Varış<br /></td>
          <td align="center" nowrap="nowrap">Uçuş Tarihi</td>
          <td align="center" nowrap="nowrap">Kalkış Saati</td>
          <td align="center" nowrap="nowrap">Varış Saati</td>
          <td align="center" nowrap="nowrap">Bagaj</td>
          <td align="center" nowrap="nowrap">Durum</td>
			  </tr>
        			  <tr>
          <td align="center" nowrap="nowrap" valign="middle"><b><?php echo $ticket->alname; ?></b></td>
				  <td align="center" nowrap="nowrap" valign="middle"><b><?php echo $ticket->ucus_no; ?></b></td>
          <td align="center" nowrap="nowrap" valign="middle"><b>Charles De Gaulle Intl Arpt(CDG) - Golubovci Arpt(TGD)</b></td>

				  <td align="center" nowrap="nowrap" valign="middle"><b>13/05/2016</b></td>
				  <td align="center" nowrap="nowrap" valign="middle"><b>15:40</b></td>
          <td align="center" nowrap="nowrap" valign="middle"><b>18:05</b></td>
				  <td align="center" nowrap="nowrap" valign="middle"><b>1 KG</b></td>
				  <td align="center" nowrap="nowrap" valign="middle"><b>OK</b></td>
			  </tr>
        		  </table>
      <table width="100%" cellspacing="0" cellpadding="0" border="0" id="table1">
      <tr>
		    <td height="15">&nbsp;</td>
	    </tr>
	    <tr>
		    <td><p>
	<b><b>U&Ccedil;UŞLA İLGİLİ BİLGİLER</b></b></p>
<p>
	<b><b>&nbsp;</b></b><b>Not 1 :</b> U&ccedil;unuzdan 2 g&uuml;n &ouml;nce (48 saat) u&ccedil;uş saatinizi mutlaka kontrol ettiriniz (konfirme). Bundan dolayı doğacak sorunlardan seyahat acentesi sorumlu tutulamaz.<br />
	<b>Not 2 : </b>Yolcunun u&ccedil;uş saatinden en az 2 saat evvelinden (yuksek sezonda 3 saat) hava limanında hazır bulunması gerekir. Herhangi bir gecikme vukuunda yolcular bilete ilişkin u&ccedil;uş haklarını kaybederler.<br />
	<b>Not 3 :</b> Biletlerde bulunan telefon numaralarından yeterli bilgi alınmadığı taktirde biletin alındığı acenteyle irtibat kurunuz.</p></td>
	    </tr>
      </table>
		</td>
	</tr>
</table>


</div>
</body>
</html>
