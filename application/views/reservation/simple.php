<?php
function yolcu_nr($item) {
     $yolcu_list = $GLOBALS["yolcu_list"];
     $yolcu_list_first = $yolcu_list[0];
     return $item - $yolcu_list_first->yolcu_id + 1;
}
?>
<link rel="stylesheet" href="js/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css" type="text/css" />
<div class="modal-header">
     <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
          ×
     </button>
     <h4 id="detailViewLabel" class="modal-title">Detay</h4>
</div>
<div class="modal-body">
     <div class="jarviswidget jarviswidget-color-greenLight" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
          <header>
               <h2>Rezervasyon Bilgileri</h2>
          </header>
          <div>
               <style media="screen">
                    @media (min-width:361px) and (max-width:420px){
                         .ikincitablo {
                              margin-top: -17px;
                         }
                         .dl-horizontal dt, .dl-horizontal dd {
                              width: 120px
                         }
                         .dl-horizontal dt {
                              float: left;
                         }
                         .dl-horizontal dd {
                              float: right;
                         }
                         .dl-horizontal dd::after{
                              clear: both;
                         }
                         .mCSB_horizontal.mCSB_inside>.mCSB_container {
                              margin-bottom: 0;
                         }
                    }
                    @media (max-width:360px){
                         .ikincitablo {
                              margin-top: -17px;
                         }
                         .dl-horizontal dt, .dl-horizontal dd {
                              width: 100px
                         }
                         .dl-horizontal dt {
                              float: left;
                         }
                         .dl-horizontal dd {
                              float: right;
                         }
                         .dl-horizontal dd::after{
                              clear: both;
                              /*content: '\A';*/
                         }
                         #mCSB_2_container .table {
                              margin-bottom: 0!important;
                         }
                         .mCSB_horizontal.mCSB_inside>.mCSB_container {
                              margin-bottom: 0;
                         }
                         .mCSB_dragger_bar {
                              height: 8px!important
                         }
                    }
               </style>
               <div class="widget-body">
                    <div class="col-md-6">
                         <dl class="dl-horizontal">
                              <table class="tablo tablo1">
                                   <tr>
                                        <td>
                                             <dt>Rezervasyon ID:</dt>
                                             <dd><?php echo $rezervasyon_data->rez_id; ?></dd>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt>Kayıt Tarihi:</dt>
                                             <dd><?php echo sqldate_to_longdate($rezervasyon_data->kayittarih); ?></dd>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt>Kaydeden:</dt>
                                             <dd><?php
                                             echo $rezervasyon_data->kayitedenyonetici == NULL || $rezervasyon_data->kayitedenyonetici == "" ?
                                             "Müşteri - " . $rezervasyon_data->kayitedenmusteri : $rezervasyon_data->kayitedenyonetici;
                                             ?></dd>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt>Düzenlenme Tarihi:</dt>
                                             <dd><?php
                                             if ($rezervasyon_data->rez_duzenleyen == "0") {
                                                  echo "Kayıt düzenlenmemiş";
                                             } else {
                                                  echo $rezervasyon_data->duzentarih;
                                             }
                                             ?></dd>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt>Tur Opertörü:</dt>
                                             <dd><?php echo $rezervasyon_data->turoperator; ?></dd>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt>Acente:</dt>
                                             <dd><?php echo $rezervasyon_data->acentead; ?></dd>
                                        </td>
                                   </tr>
                              </table>
                         </dl>
                    </div>
                    <div class="col-md-6 ikincitablo">
                         <dl class="dl-horizontal">
                              <table class="tablo tablo2">
                                   <tr>
                                        <td>
                                             <dt>Kayıt No:</dt>
                                             <dd><?php echo date("Y", strtotime($rezervasyon_data->rez_kayittarih)) . "-" . $rezervasyon_data->rez_kayitno; ?></dd>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt><?php echo $rezervasyon_data->turoperator; ?> PNR:</dt>
                                             <dd><?php echo $rezervasyon_data->rez_turoperatorpnr; ?></dd>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt>Rezervasyon PNR:</dt>
                                             <dd><?php echo $rezervasyon_data->rez_pnr; ?></dd>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt>Yön:</dt>
                                             <dd><?php
                                             if ($rezervasyon_data->rez_yon == "gd") {
                                                  echo "Gidiş-Dönüş";
                                             } elseif ($rezervasyon_data->rez_yon == "g") {
                                                  echo "Gidiş";
                                             } elseif ($rezervasyon_data->rez_yon == "d") {
                                                  echo "Dönüş";
                                             } elseif ($rezervasyon_data->rez_yon == "da") {
                                                  echo "Dönüş Açık";
                                             }
                                             ?></dd>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt>Durum:</dt>
                                             <?php
                                             $color = "";
                                             if ($rezervasyon_data->rez_durum == "OK") {
                                                  $color = "text-success";
                                             } elseif ($rezervasyon_data->rez_durum == "OP") {
                                                  $color = "text-primary";
                                             } elseif ($rezervasyon_data->rez_durum == "CA") {
                                                  $color = "text-danger";
                                             }
                                             echo '<dd class="' . $color . '" style="font-weight: bold">' . $rezervasyon_data->rez_durum . '</dd>';?>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <dt>Opsiyon Tarihi:</dt>
                                             <dd><?php echo sqldate_to_longdate($rezervasyon_data->opsiyontarih); ?></dd>
                                        </td>
                                   </tr>
                              </table>
                         </dl>
                    </div>
               </div>
          </div>
     </div>
     <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
          <header>
               <h2>Uçuş Listesi</h2>
          </header>
          <div class="table-container">
               <table class="table table-bordered">
                    <thead>
                         <tr>
                              <th>No</th>
                              <th>Yolcu</th>
                              <th>Tarih</th>
                              <th>Uçuş No</th>
                              <th>Parkur</th>
                              <th>Kal.Saat</th>
                              <th>Var.Saat</th>
                              <th>Sınıf</th>
                              <th>Durum</th>
                         </tr>
                    </thead>
                    <tbody>
                         <?php $srno = 0; foreach ($ucus_list as $ucus) : ?>
                              <tr>
                                   <td><?php echo ++$srno; ?></td>
                                   <td><?php
                                   $GLOBALS["yolcu_list"] = $yolcu_list;
                                   $yolcular = explode(",", $ucus->yolcuad);
                                   $yolcular = array_map("yolcu_nr", $yolcular);
                                   sort($yolcular);
                                   if (count($yolcular) < 4) {
                                        echo implode(",", $yolcular);
                                   } else {
                                        echo '<span rel="tooltip" data-placement="top" data-original-title="' . implode(", ", $yolcular) . '">'
                                        . '<i class="fa fa-info-circle"></i> ' . count($yolcular) . 'pcx</span>';
                                   }
                                   ?></td>
                                   <td style="white-space: nowrap;"><?php echo sqldate_to_dmya($ucus->ucus_tarih); ?></td>
                                   <td style="white-space: nowrap;">
                                        <span rel="tooltip" data-placement="top" data-original-title="<?php echo $ucus->alname; ?>">
                                             <i class="fa fa-info-circle"></i>
                                             <?php echo $ucus->ucus_kod . " " . $ucus->ucus_no; ?>
                                        </span>
                                   </td>
                                   <td>
                                        <?php echo $ucus->ai1name . " (" . $ucus->ai1airportcode . ") - " . $ucus->ai2name . " (" .
                                        $ucus->ai2airportcode . ")"; ?>
                                   </td>
                                   <td><?php echo $ucus->ucus_saat1; ?></td>
                                   <td><?php echo $ucus->ucus_saat2; ?></td>
                                   <td><?php echo $ucus->sinif_tanim; ?></td>
                                   <td><?php
                                   $color = "";
                                   if ($ucus->ucusdurum == "OK") {
                                        $color = "text-success";
                                   } elseif ($ucus->ucusdurum == "OP") {
                                        $color = "text-primary";
                                   } elseif ($ucus->ucusdurum == "CA") {
                                        $color = "text-danger";
                                   }
                                   echo '<span class="' . $color . '"><strong>' . $ucus->ucusdurum . '</strong></span>';
                                   ?></td>
                              </tr>
                         <?php endforeach; ?>
                    </tbody>
               </table>
          </div>
     </div>
     <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
          <header>
               <h2>Yolcu Listesi</h2>
          </header>
          <div class="table-container">
               <table class="table table-bordered">
                    <thead>
                         <tr>
                              <th>No</th>
                              <th>Cinsiyet</th>
                              <th>Ad</th>
                              <th>Soyad</th>
                              <th>Doğ.Tar.</th>
                              <th>BiletNo</th>
                              <th>K.Fiyatı</th>
                              <th>Vergi</th>
                              <th>Satis</th>
                              <th>Kar</th>
                         </tr>
                    </thead>
                    <tbody>
                         <?php $srno=0; foreach ($yolcu_list as $yolcu) : ?>
                              <tr>
                                   <td><?php echo ++$srno; ?></td>
                                   <td><?php echo $yolcu->yolcu_cinsiyet . " (" . $yolcu->yolcu_tipi . ")"; ?></td>
                                   <td><?php echo $yolcu->yolcu_ad; ?></td>
                                   <td><?php echo $yolcu->yolcu_soyad; ?></td>
                                   <td><?php echo $yolcu->yolcu_dtarih; ?></td>
                                   <td><?php echo $yolcu->yolcu_biletno; ?></td>
                                   <td class="text-right"><?php echo $yolcu->yolcu_koltukfiyat . "€"; ?></td>
                                   <td class="text-right"><?php echo $yolcu->yolcu_vergi . "€"; ?></td>
                                   <td class="text-right"><?php echo $yolcu->yolcu_satisfiyat . "€"; ?></td>
                                   <td class="text-right"><?php echo number_format($yolcu->yolcu_satisfiyat - $yolcu->yolcu_koltukfiyat - $yolcu->yolcu_vergi, 2) . "€"; ?></td>
                              </tr>
                         <?php endforeach; ?>
                         <tr>
                              <td colspan="6" class="text-right">Rezervasyon Toplam:</td>
                              <td class="text-right"><?php echo number_format($fiyatlar["koltukfiyat"], 2) . "€"; ?></td>
                              <td class="text-right"><?php echo number_format($fiyatlar["vergi"], 2) . "€"; ?></td>
                              <td class="text-right"><?php echo number_format($fiyatlar["satis"], 2) . "€"; ?></td>
                              <td class="text-right"><?php echo number_format($fiyatlar["kar"], 2) . "€"; ?></td>
                         </tr>
                    </tbody>
               </table>
          </div>
     </div>
     <div class="jarviswidget jarviswidget-color-orangeDark" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
          <header>
               <h2>Ücretlendirme</h2>
          </header>
          <div class="table-container">
               <table class="table table-bordered">
                    <thead>
                         <tr>
                              <th></th>
                              <th>Maliyet</th>
                              <th>Satış</th>
                              <th>Kâr</th>
                         </tr>
                    </thead>
                    <tbody class="text-right">
                         <tr>
                              <td style="width: 60%">Genel Toplam</td>
                              <td><?php echo number_format($fiyatlar["koltukfiyat"] + $fiyatlar["vergi"], 2) . "€"; ?></td>
                              <td><?php echo number_format($fiyatlar["satis"], 2) . "€"; ?></td>
                              <td><?php echo number_format($fiyatlar["satis"] - $fiyatlar["koltukfiyat"] - $fiyatlar["vergi"], 2) . "€"; ?></td>
                         </tr>
                         <tr>
                              <td>Alınan</td>
                              <td colspan="3"><?php echo $gelirgider_data->gelir === NULL ? "0,00" : number_format($gelirgider_data->gelir, 2) . "€"; ?></td>
                         </tr>
                         <tr>
                              <td>İade</td>
                              <td colspan="3"><?php echo $gelirgider_data->gider === NULL ? "0,00" : number_format($gelirgider_data->gider, 2) . "€"; ?></td>
                         </tr>
                         <tr>
                              <td>Kalan</td>
                              <td colspan="3"><span class="<?php
                              $kalan = $fiyatlar["satis"] - ($gelirgider_data->gelir === NULL ? 0 : $gelirgider_data->gelir)
                              + ($gelirgider_data->gider === NULL ? 0 : $gelirgider_data->gider);
                              if ($kalan > 0) {
                                   echo "text-danger";
                              } elseif ($kalan < 0) {
                                   echo "text-primary";
                              } else {
                                   echo "text-success";
                              }
                              ?>"><?php echo number_format($kalan, 2) . "€"; ?></span></td>
                         </tr>
                    </tbody>
               </table>
          </div>
     </div>
</div>
<div class="modal-footer">
     <a href="<?php echo base_url();?>#index.php/reservation/detail/<?php echo $rezervasyon_data->rez_id; ?>" class="btn btn-primary">
          Detay
     </a>
     <button data-dismiss="modal" class="btn btn-default" type="button">
          OK
     </button>
</div>
<script type="text/javascript" src="js/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
     $("[rel=tooltip]").tooltip();

     $(".modal-body").slimScroll({
          height: (window.innerHeight - 200).toString() + "px",
          railVisible: true,
          alwaysVisible: true,
          wheelStep: 25,
          touchScrollStep: 25
     });

     $(".modal-body .table-container").mCustomScrollbar({
          axis: "x",
          setWidth: "100%",
          theme: "dark"
     });
});

function gopage(url) {
     console.log("ok");
     $("body").removeClass("modal-open");
}
</script>
