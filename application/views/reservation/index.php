<?php
$tip = null;
$tarih = null;
$acentepost = null;
if($_GET){
	if(isset($_GET["tip"])){
		$tip = strtoupper($_GET["tip"]);
	}
	if(isset($_GET["tarih"])){
		$tarihi = $_GET["tarih"];
		$tariha = explode("-", $tarihi);
		$tarih = $tariha[0] . "/" . $tariha[1] . "/" . $tariha[2];
	}
	if(isset($_GET["acente"])){
		$acentepost = $_GET["acente"];
	}
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/plugin/bootstrap-datepicker/css/datepicker.css" />
<!-- <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/plugin/datepicker/jquery-ui.css" /> -->
<div id="appSkytripPages">
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-edit fa-fw "></i>
				Rezervasyon
				<span>>
					Rezervasyon Listesi
				</span>
			</h1>
		</div>
	</div>
	<section id="widget-grid" class="" ng-controller="skytripSearch">
		<div id="postparam" class="hidden">
			<span id="tip"><?php echo $tip;?></span>
			<span id="tarih"><?php echo $tarih;?></span>
			<span id="acentepost"><?php echo $acentepost;?></span>
		</div>
		<div class="row">
			<article class="col-md-12">
				<div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false" data-widget-collapsed="true" data-widget-deletebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-filter"></i> </span>
						<h2>Filtreler</h2>
					</header>
					<div>
						<div class="widget-body no-padding">
							<form action="#" class="smart-form" my-enter="FormSearch()">
								<fieldset>
									<div class="col-sm-4">
										<section>
											<label class="input">
												<input type="text" name="rezid" class="input-sm" ng-model="FormElements.rezid" placeholder="Rezervasyon ID" />
											</label>
										</section>
									</div>
									<div class="col-sm-4">
										<section>
											<label class="input">
												<input type="text" name="rezno" class="input-sm" ng-model="FormElements.rezno" placeholder="Rezervasyon No" />
											</label>
										</section>
									</div>
									<div class="col-sm-4">
										<section>
											<label class="input">
												<input type="text" name="pnr" class="input-sm"  ng-model="FormElements.pnr" placeholder="PNR" />
											</label>
										</section>
									</div>
									<div class="col-sm-4">
										<section>
											<label class="select">
												<select name="turop" ng-model="FormElements.turop" class="input-sm">
													<option value="">Tur Operatörü</option>
													<?php
													foreach ($turop_list as $turop) :
														?>
														<option value="<?php echo $turop->id; ?>"><?php echo $turop->title; ?></option>
														<?php
													endforeach;
													?>
												</select>
												<i></i>
											</label>
										</section>
									</div>
									<div class="col-sm-4">
										<section>
											<label class="select">
												<select name="acente" <?php if($acentepost === null){ ?>ng-model="FormElements.acente"<?php } ?> class="input-sm">
													<option value="">Acente</option>
													<?php foreach ($acente_list as $acente) { ?>
														<?php if($acentepost !== null){ ?>
															<?php if($acentepost == $acente->id){ ?>
																<option value="<?php echo $acente->id; ?>" selected><?php echo $acente->title; ?></option>
															<?php }else{ ?>
																<option value="<?php echo $acente->id; ?>"><?php echo $acente->title; ?></option>
															<?php } ?>
														<?php }else{ ?>
														<option value="<?php echo $acente->id; ?>"><?php echo $acente->title; ?></option>
														<?php } ?>
													<?php } ?>
												</select>
												<i></i>
											</label>
										</section>
									</div>
									<div class="col-sm-4">
										<section>
											<label class="input">
												<input type="text" name="musteriid" class="input-sm" ng-model="FormElements.musteriid" placeholder="Müşteri ID" />
											</label>
										</section>
									</div>
									<div class="col-sm-4">
										<section>
											<div class="col-xs-5">
												<label class="select">
													<select name="kayitno_pre" class="input-sm"  ng-model="FormElements.kayitno_pre">
														<option value="">Yıl</option>
														<?php
														foreach ($yil as $yil) {
															?>
															<option value="<?php echo $yil;?>"><?php echo $yil;?></option>
															<?php
														}
														?>
													</select>
													<i></i>
												</label>
											</div>
											<div class="col-xs-1"></div>
											<div class="col-xs-6">
												<label class="input">
													<input type="text" name="kayitno_post" class="input-sm" ng-model="FormElements.kayitno_post" placeholder="Kayıt No" />
												</label>
											</div>
											<div class="clearfix"></div>
										</section>
									</div>
									<div class="col-sm-4">
										<section>
											<div class="col-xs-5">
												<div class="form-group">
													<div class="input-group">
														<label class="input">
															<input class="form-control hasDatepicker input-sm datepicker2" type="text" name="kayittar_start"  <?php if($tarih === null){ ?> ng-model="FormElements.kayittar_start" <?php } ?>  placeholder="Kay.Tr.Başlangıç" value="<?php echo $tarih;?>">
														</label>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<!-- <label class="input">
												<input type="text" name="kayittar_start" class="input-sm" ng-model="FormElements.kayittar_start" placeholder="Kay.Tr.Başlangıç" />
											</label> -->
										</div>
										<div class="col-xs-2 text-center">
											ile
										</div>
										<div class="col-xs-5">
											<div class="form-group">
												<div class="input-group">
													<label class="input">
														<input class="form-control hasDatepicker input-sm datepicker2" type="text" name="kayittar_end" <?php if($tarih === null){ ?> ng-model="FormElements.kayittar_end" <?php } ?> placeholder="Kay.Tr.Bitiş" value="<?php echo $tarih;?>">
													</label>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
											<!-- <label class="input">
											<input type="text" name="kayittar_end" class="input-sm" ng-model="FormElements.kayittar_end" placeholder="Kay.Tr.Bitiş" />
										</label> -->
									</div>
									<div class="clearfix"></div>
								</section>
							</div>
							<div class="col-sm-4">
								<section>
									<div class="col-xs-5">
										<div class="form-group">
											<div class="input-group">
												<label class="input">
													<input class="form-control hasDatepicker input-sm datepicker2" type="text" name="ucustar_start" ng-model="FormElements.ucustar_start" placeholder="Uçş.Tr.Başlangıç">
												</label>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
										<!-- <label class="input">
										<input name="ucustar_start" ng-model="FormElements.ucustar_start" class="input-sm" placeholder="Uçş.Tr.Başlangıç" />
									</label> -->
								</div>
								<div class="col-xs-2 text-center">ile</div>
								<div class="col-xs-5">
									<div class="form-group">
										<div class="input-group">
											<label class="input">
												<input class="form-control hasDatepicker input-sm datepicker2" type="text" name="ucustar_end" ng-model="FormElements.ucustar_end" placeholder="Uçş.Tr.Bitiş">
											</label>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<!-- <label class="input">
									<input name="ucustar_end" ng-model="FormElements.ucustar_end" class="input-sm" placeholder="Uçş.Tr.Bitiş" />
								</label> -->
							</div>
							<div class="clearfix"></div>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<div class="col-xs-5">
								<label class="select">
									<select name="ucusno_pre" ng-model="FormElements.ucusno_pre" class="input-sm">
										<option value="">Havayolu Şirketi</option>
										<?php
										foreach ($turop_list as $turop) {
											?>
											<option value="<?php echo $turop->id; ?>"><?php echo $turop->title; ?></option>
											<?php
										}
										?>
									</select>
									<i></i>
								</label>
							</div>
							<div class="col-xs-1"></div>
							<div class="col-xs-6">
								<label class="input">
									<input name="ucusno_post" ng-model="FormElements.ucusno_post" class="input-sm" placeholder="Uçuş No" />
								</label>
							</div>
							<div class="clearfix"></div>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="select">
								<select name="kaydeden" ng-model="FormElements.kaydeden" class="input-sm">
									<option value="">Kaydeden</option>
									<option value="m">Müşteri</option>
									<option value="a">Acente</option>
									<option value="y">Yönetici</option>
									<?php foreach ($yonetici_list as $yonetici) : ?>
										<option value="<?php echo $yonetici->id; ?>">-- <?php echo $yonetici->title; ?></option>
									<?php endforeach; ?>
								</select>
								<i></i>
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="input">
								<input name="aciklama" ng-model="FormElements.aciklama" class="input-sm" placeholder="Açıklama" />
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="select">
								<select name="iade" ng-model="FormElements.iade" class="input-sm">
									<option value="">İade</option>
									<option value="1">İade > 0</option>
								</select>
								<i></i>
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="select">
								<select name="ekstraucr" ng-model="FormElements.ekstraucr" class="input-sm">
									<option value="">Ekstra Ücret</option>
									<option value="-999" disabled="disabled">Sabit Tanımlı</option>
									<?php
									$minus = TRUE;
									foreach ($extraucret_list as $extraucret) :
										if ($extraucret->id > 0 && $minus) :
											$minus = FALSE;
											?>
											<option value="-999" disabled="disabled">Kullanıcı Tanımlı</option>
											<?php
										endif;
										?>
										<option value="<?php echo $extraucret->id; ?>">-- <?php echo $extraucret->title; ?></option>
										<?php
									endforeach;
									?>
								</select>
								<i></i>
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="select">
								<select name="kalan" ng-model="FormElements.kalan" class="input-sm">
									<option value="">Kalan</option>
									<option value="b">Kalan > 0</option>
									<option value="s">Kalan < 0</option>
									<option value="e">Kalan = 0</option>
									<option value="n">Kalan <> 0</option>
								</select>
								<i></i>
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="select">
								<select name="webapp" ng-model="FormElements.webapp" class="input-sm">
									<option value="">WebApp</option>
									<?php foreach ($webapp_list as $webapp) : ?>
										<option value="<?php echo $webapp->id; ?>"><?php echo $webapp->title; ?></option>
									<?php endforeach; ?>
								</select>
								<i></i>
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="select">
								<select name="kayit" ng-model="FormElements.kayit" class="input-sm">
									<option value="">Kayıt Tipi</option>
									<option value="L">Lokal</option>
									<option value="O">Online</option>
								</select>
								<i></i>
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="select">
								<select name="durum"
								<?php
								if($tip === null){ ?>
									ng-model="FormElements.durum"
									<?php } ?> class="input-sm">
									<?php if($tip !== null){
										?>
										<option value="<?php echo $tip;?>" selected><?php echo $tip;?></option>
										<?php
									} ?>
									<option value="">Durum</option>
									<option value="OK">OK</option>
									<option value="OP">OP</option>
									<option value="WA">WA</option>
									<option value="CA">CA</option>
								</select>
								<i></i>
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="select">
								<select name="mailbilgisi" ng-model="FormElements.mailbilgisi" class="input-sm">
									<option value="">Mail Bilgisi</option>
									<option value="var">Var</option>
									<option value="yok">Yok</option>
									<?php
									foreach ($mail as $m) {
										?>
										<option value="<?php echo $m;?>"><?php echo $m;?></option>
										<?php
									}
									?>
								</select>
								<i></i>
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="checkbox">
								<input type="checkbox" name="degisen" ng-model="FormElements.degisen" />
								<i></i> Değişen Kayıtlar
							</label>
						</section>
					</div>
					<div class="col-sm-4">
						<section>
							<label class="checkbox">
								<input type="checkbox" name="sifirucret" ng-model="FormElements.sifirucret" />
								<i></i> Boş Rezervasyonlar
							</label>
						</section>
					</div>
				</fieldset>
				<footer>
					<button type="button" id="listele" ng-click="FormSearch()" class="btn btn-primary">Listele</button>
					<button type="button"
					<?php if(!$_GET){
						?>
						ng-click="FormClear()"
						<?php
					}else{
						?>
						onclick="window.location.href = '#index.php/reservation';"
						<?php
					}?> class="btn btn-danger">Temizle</button>
				</footer>
			</form>
		</div>
	</div>
</div>
<div class="jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false" data-widget-deletebutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
		<h2>Rezervasyonlar</h2>

	</header>
	<div>
		<div class="widget-body no-padding">
			<table id="rezdt" class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr>
						<td></td>
						<td>#</td>
						<td>RezID</td>
						<td>KN</td>
						<td>Rez.No</td>
						<td>PNR</td>
						<td>Tarih</td>
						<td>Acente</td>
						<td>Tur Opt.</td>
						<td>İlk Ucs.Tar.</td>
						<td>Yolcu S.</td>
						<td>WebApp</td>
						<td>Tutar</td>
						<td>Ek Ücr.</td>
						<td>Alınan</td>
						<td>İade</td>
						<td>Kalan</td>
						<td>Durum</td>
						<td>A</td>
					</tr>
				</thead>
				<tbody>

				</tbody>
				<tfoot>
					<tr style="width:56px;background-color:#E4E4E4;font-weight:bold">
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style="color:#b94a48;">Toplam:</td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
						<td style=""></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
</article>
</div>
</section>
</div>
<div aria-hidden="true" aria-labelledby="detailViewLabel" role="dialog" tabindex="-1" id="detailView" class="modal fade" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<style type="text/css">
#rezdt td {
	white-space: nowrap;
}
#rezdt tbody tr.active {
	background-color: #ffcc99!important;
}
.footertablediv{
	width:100%;
	height:22px;
}
.footertablediv .listana{
	float:right;
	font-size:12px;
}
.footertablediv .listt {
	float: left;
	margin-left: 20px;
}
.footertablediv .last{
	margin-left: 10px;
}
</style>
<script type="text/javascript">
pageSetUp();

var referance_dt_rezdt = undefined;
var referance_dt_filter = {};

var breakpointDefinition = {
	tablet : 1024,
	phone : 480
};

var pagefunction = function () {
	moment.locale('tr');
	<?php echo $this->activemenu; ?>
	referance_dt_rezdt = $("#rezdt").DataTable({
		serverSide: true,
		bFilter: false,
		scrollX: true,
		responsive: true,
		ajax: function (data, callback, settings) {
			var tip = $("#tip").text();
			var tarih = $("#tarih").text();
			var acentepost = $("#acentepost").text();
			if(tip !== ""){
				referance_dt_filter.durum = tip;
			}
			if(tarih !== ""){
				referance_dt_filter.kayittar = tarih;
			}
			if(acentepost !== ""){
				referance_dt_filter.acente = acentepost;
			}
			referance_dt_filter.start = data.start;
			referance_dt_filter.length = data.length;
			referance_dt_filter.draw = data.draw;
			referance_dt_filter.order = data.columns[data.order[0].column].data + " " + data.order[0].dir;
			$.ajax({
				url: 'index.php/reservation/api_list',
				method: "POST",
				data: referance_dt_filter
			}).done(callback);
		},
		columns: [
			{data: "rez_id", searchable: false, orderable: false, className: "dt-body-center", render: function (data, type, full, meta){
				return '<input id="data' + full.rez_id + '" type="checkbox">';
			}},
			{data: "rez_id", render: function(data, type, full, meta) {
				return '<a href="index.php/reservation/simple/' + data + '" data-toggle="modal" data-target="#detailView"><i class="fa fa-external-link"></i></a>';
			}},
			{data: "rez_id", render: function(data, type, full, meta) {
				return '<a href="index.php/reservation/simple/' + data + '" data-toggle="modal" data-target="#detailView">' + data + '</a>';
			}},
			{data: "rez_kayitno"},
			{data: "rez_pnr"},
			{data: "rez_turoperatorpnr"},
			{data: "rez_kayittarih", render: function(data, type, full, meta) {
				return moment(data, "YYYY-MM-DD").format("DD/MM/YYYY dd");
			}},
			{data: "acente_ad"},
			{data: "turop_ad"},
			{data: "ucus_tarih", className: "ucus", render: function(data, type, full, meta) {
				if (!moment(data, "YYYY-MM-DD").isValid()) {
					return "00/00/0000";
				}
				var kalan = parseFloat(full.kalan);
				if (kalan > 0) {
					if (moment(full.ucus_tarih, "YYYY-MM-DD").isBefore(new Date(), 'day'))
					{
						return '<span class="text-danger">' + moment(data, "YYYY-MM-DD").format("DD/MM/YYYY dd") + '</span>';
					}
					else if (moment(full.ucus_tarih, "YYYY-MM-DD").isSame(new Date(), 'day'))
					{
						return '<strong>' + moment(data, "YYYY-MM-DD").format("DD/MM/YYYY dd") + '</strong>'
					}
				}
				return moment(data, "YYYY-MM-DD").format("DD/MM/YYYY dd");
			}},
			{data: "yolcu_sayi", render: function(data, type, full, meta){
				var yolcuadt = 0;
				var yolcuchd = 0;
				var yolcuinf = 0;
				if(full.yolcu_adt != null){
					yolcuadt = full.yolcu_adt;
				}
				if(full.yolcu_chd != null){
					yolcuchd = full.yolcu_chd;
				}
				if(full.yolcu_inf != null) {
					yolcuinf = full.yolcu_inf;
				}
				return yolcuadt + "/" + yolcuchd + "/" + yolcuinf + "=" + full.yolcu_sayi;
			}},
			{data: "rez_webapp"},
			{data: "sum_tutar", className: "text-right", render: function(data, type, full, meta) {
				if (data === null || data == undefined || data == "") {
					return "0,00";
				} else {
					return data + "€";
				}
			}},
			{data: "rezek_tutar", className: "text-right", render: function(data, type, full, meta) {
				if (data === null || data == undefined || data == "") {
					return "0,00";
				} else {
					return data + "€";
				}
			}},
			{data: "alinan", className: "text-right", render: function(data, type, full, meta) {
				if (data === null || data == undefined || data == "") {
					return "0,00";
				} else {
					if(data == "0.00"){
						return data;
					}else{
						return data + "€";
					}
				}
			}},
			{data: "odenen", className: "text-right", render: function(data, type, full, meta) {
				if (data === null || data == undefined || data == "") {
					return "0,00";
				} else {
					if(data == "0.00"){
						return data;
					}else{
						return data + "€";
					}
				}
			}},
			{data: "kalan", className: "text-right", render: function(data, type, full, meta) {
				// $kalan = $fiyatlar["satis"] - ($gelirgider_data->gelir === null ? 0 : $gelirgider_data->gelir) + ($gelirgider_data->gider === null ? 0 : $gelirgider_data->gider);
				if (data === null || data == undefined) {
					return "0,00";
				}
				if (parseFloat(data) > 0) {
					return '<span class="text-danger">' + data + "€" + '</span>';
				} else {
					return '<span class="text-success">' + data + "€" + '</span>';
				}
			}},
			{data: "rez_durum", render: function(data, type, full, meta) {
				var durum_css = "";
				if (data == "OK") {
					durum_css = "text-success";
				} else if (data == "OP") {
					durum_css = "text-primary";
				} else if (data == "CA") {
					durum_css = "text-danger";
				}
				return '<span class="' + durum_css + '"><strong>' + data + '</strong></span>';
			}},
			{data: "rez_aciklama2", render: function(data, type, full, meta) {
				if (data !== null && data != undefined && data != "") {
					return '<a href="javascript:;" data-toggle="popover" data-placement="left" data-trigger="hover" title="Detay" data-content="' + data + '"><i class="fa fa-comment"></i></a>';
				} else {
					return "";
				}
			}}
		],
		order: [[1, 'desc']],
		sDom: "<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-6 hidden-xs'CT>r>"+
		"t"+
		"<'dt-toolbar-footer'<'footerrapor'><'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		oTableTools: {
			sRowSelect: "multi",
			aButtons: [
				{
					sExtends: "select_all",
					fnComplete: function ( nButton, oConfig, oFlash, sFlash ) {
						var oTT = TableTools.fnGetInstance( 'rezdt' );
						var nRow = $('#rezdt tbody tr');
						var input = nRow.find("input[type='checkbox']");
						if ( nRow.hasClass('active') ) {
							input.prop('checked', 'checked');
						}else {
							input.removeAttr('checked');
						}
						oTT.fnSelect(nRow);
					}
				},
				{
					sExtends: "select_none",
					fnComplete: function ( nButton, oConfig, oFlash, sFlash ) {
						var oTT = TableTools.fnGetInstance( 'rezdt' );
						var nRow = $('#rezdt tbody tr');
						var input = nRow.find("input[type='checkbox']");
						if ( nRow.hasClass('active') ) {
							input.prop('checked', 'checked');
						}else {
							input.removeAttr('checked');
						}
						oTT.fnDeselect(nRow);
					}
				},
				"copy",
				"csv",
				"xls",
				{
					sExtends: "pdf",
					sTitle: "SkyTrip PDF",
					sPdfMessage: "SkyTrip PDF Export",
					bBomInc: true,
					// sPdfSize: "letter",
					// fnComplete: function ( nButton, oConfig, oFlash, sFlash ) {
					// 	var oTT = TableTools.fnGetInstance( 'rezdt' );
					// 	var nRow = $('#rezdt tbody tr');
					// 	oTT.fnDeselect(nRow);
					// }
				},
				{
					sExtends: "print",
					sTitle: "SkyTrip Print",
					sMessage: "Rezervation List",
					// fnComplete: function ( nButton, oConfig, oFlash, sFlash ) {
					// 	var oTT = TableTools.fnGetInstance( 'rezdt' );
					// 	var nRow = $('#rezdt tbody tr');
					// 	oTT.fnDeselect(nRow);
					// }
				}
			],
			sSwfPath: "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
		},
		fnDrawCallback: function( oSettings ) {
			$('[data-toggle="popover"]').popover();
		},
		footerCallback: function (row, data, start, end, display) {
			var api = this.api(), data;
			console.log("ok");

			// Remove the formatting to get integer data for summation
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			// Kalan Toplamı
			kalantoplam = api
			.column( 16, { page: 'current'} )
			.data()
			.reduce( function (a, b) {
				var deger = intVal(a) + intVal(b);
				return parseFloat(Math.round(deger * 100) / 100).toFixed(2);
			}, 0 );

			$( api.column( 16 ).footer() ).html(
				(kalantoplam + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +'€'
			);

			// İade Toplamı
			iadetoplam = api
			.column( 15, { page: 'current'} )
			.data()
			.reduce( function (a, b) {
				var deger = intVal(a) + intVal(b);
				return parseFloat(Math.round(deger * 100) / 100).toFixed(2);
			}, 0 );

			$( api.column( 15 ).footer() ).html(
				(iadetoplam + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +'€'
			);

			// Alınan Toplamı
			alinantoplam = api
			.column( 14, { page: 'current'} )
			.data()
			.reduce( function (a, b) {
				var deger = intVal(a) + intVal(b);
				return parseFloat(Math.round(deger * 100) / 100).toFixed(2);
			}, 0 );

			$( api.column( 14 ).footer() ).html(
				(alinantoplam + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +'€'
			);

			// Ek Ücret Toplamı
			ekucrettoplam = api
			.column( 13, { page: 'current'} )
			.data()
			.reduce( function (a, b) {
				var deger = intVal(a) + intVal(b);
				return parseFloat(Math.round(deger * 100) / 100).toFixed(2);
			}, 0 );

			$( api.column( 13 ).footer() ).html(
				(ekucrettoplam + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +'€'
			);

			// Tutar Toplamı
			tutartoplam = api
			.column( 12, { page: 'current'} )
			.data()
			.reduce( function (a, b) {
				var deger = intVal(a) + intVal(b);
				return parseFloat(Math.round(deger * 100) / 100).toFixed(2);
			}, 0 );

			$( api.column( 12 ).footer() ).html(
				(tutartoplam + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +'€'
			);
		}
	});
	$(".datepicker2").datepicker({
		format: "dd/mm/yyyy",
		autoclose: true,
		numberOfMonths: 2
	});
	// $('.input').keypress(function (e) {
	// 	if (e.which == 13) {
	// 		console.log("ok");
	// 		$('#listele').submit();
	// 		return false;    //<---- Add this line
	// 	}
	// });
	$('#rezdt tbody').on('click', 'tr', function () {
		var input = $(this).find("input[type='checkbox']");
		if ( $(this).hasClass('active') ) {
			input.prop('checked', 'checked');
		}else {
			input.removeAttr('checked');
		}
	} );
};

loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
	loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
		loadScript("js/plugin/datatables/dataTables.tableTools.min.js", function(){
			loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
				loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", function() {
					loadScript("js/plugin/moment/moment-with-locales.js", function() {
						loadScript("js/plugin/slimscroll/jquery.slimscroll.min.js", function() {
							loadScript("js/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js", pagefunction);
						});
					});
				});
			});
		});
	});
});
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/pagescripts/reservation.search.app.js"></script>
