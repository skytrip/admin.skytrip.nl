<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" media="screen" title="no title">
<div class="widget-body">
     <table style="width:100%" id="tablocheck">
          <tr>
               <td>
                    <input type="radio" name="kisayolpnr" class="inputcheck" id="kisayolpnr" checked="checked">Pnr
               </td>
               <td>
                    <input type="radio" name="kisayolsoyad" class="inputcheck" id="kisayolsoyad">Soyad
               </td>
               <td>
                    <input type="radio" name="kisayolad" class="inputcheck" id="kisayolad">Ad
               </td>
               <td>
                    <input type="radio" name="kisayolextra" class="inputcheck" id="kisayolextra">Extra Ücret
               </td>
          </tr>
          <tr>
               <td>
                    <input type="radio" name="kisayolmusteri" class="inputcheck" id="kisayolmusteri">Müşteri
               </td>
               <td>
                    <input type="radio" name="kisayolreznot" class="inputcheck" id="kisayolreznot">Rez.Not
               </td>
               <td>
                    <input type="radio" name="kisyaolyolcunot" class="inputcheck" id="kisyaolyolcunot">Yolcu Not
               </td>
               <td></td>
          </tr>
     </table>
     <script type="text/javascript">
          var sayfano = <?php echo $sayfano; ?>;
          var kriter = '<?php echo $kriter; ?>';
          var tip = '<?php echo $tip; ?>';
          var anascript = function () {
               $('[data-toggle="tooltip"]').tooltip({'placement': 'right', 'z-index': '3000'});
               $("#kisayolpnr").click(function () {
                    if($(this).attr("checked")){
                         $("#kisayolpnrtable").removeClass("hidden");
                    }
               })

               function veri(kriter,sayfano,tip) {
                    if(tip == "pnr" || tip == "extra" || tip == "reznot" || tip == "yolcunot"){
                         $.ajax({
          				url: "index.php/dashboard/aramakisayolalt",
          				method: "POST",
          				data: {
          					kriter: kriter,
          					sayfano: sayfano,
                                   tip: tip
          				}
          			}).done(function (result) {
                              $("#kisayolpnrtable").html("");
                              if(result != "null"){
                                   var htmlt = '<thead>'+
                                        '<tr>'+
                                             '<th style="max-width:10px">#</th>'+
                                             '<th>RezID</th>'+
                                             '<th>Kayit No</th>'+
                                             '<th>PNR</th>'+
                                        '</tr>'+
                                   '</thead>'+
                                   '<tbody>';
                                   var i = 0;
                                   $.each(result.result, function (key, value) {
                                        i++;
                                        htmlt += '<tr>'+
                                                  '<td>'+i+
                                                  '</td>'+
                                                  '<td>'+value.rez_id+
                                                  '</td>'+
                                                  '<td>'+value.rez_kayitno+
                                                  '</td>'+
                                                  '<td>'+value.rez_pnr+
                                                  '</td>';
                                   })
                                   htmlt += '</tbody>';
                                   $("#kisayolpnrtable").html(htmlt);
                              }else{
                                   var htmlt = '<thead>'+
                                        '<tr>'+
                                             '<th style="text-align:center;">#</th>'+
                                        '</tr>'+
                                   '</thead>'+
                                   '<tbody><tr><td style="text-align:center;">Kayıt Yok ya da Bulunamadı</td></tr></tbody>';
                                   $("#kisayolpnrtable").html(htmlt);
                              }
                         });
                    }
                    if(tip == "soyad" || tip == "ad"){
                         $.ajax({
          				url: "index.php/dashboard/aramakisayolalt",
          				method: "POST",
          				data: {
          					kriter: kriter,
          					sayfano: sayfano,
                                   tip: tip
          				}
          			}).done(function (result) {
                              $("#kisayolpnrtable").html("");
                              if(result != "null"){
                                   var htmlt = '<thead>'+
                                        '<tr>'+
                                             '<th style="width:10px">#</th>'+
                                             '<th>RezID</th>'+
                                             '<th style="width: 80px;">Kayit No</th>'+
                                             '<th>PNR</th>'+
                                             '<th>Yolcu</th>'+
                                        '</tr>'+
                                   '</thead>'+
                                   '<tbody>';
                                   var i = 0;
                                   $.each(result.result, function (key, value) {
                                        i++;
                                        htmlt += '<tr>'+
                                                  '<td>'+i+
                                                  '</td>'+
                                                  '<td>'+value.rez_id+
                                                  '</td>'+
                                                  '<td>'+value.rez_kayitno+
                                                  '</td>'+
                                                  '<td>'+value.rez_pnr+
                                                  '</td>'+
                                                  '<td>'+value.yolcu_ad.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                                       return letter.toUpperCase();
                                                  })+' '+value.yolcu_soyad.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                                       return letter.toUpperCase();
                                                  })+
                                                  '</td>';
                                   })
                                   htmlt += '</tbody>';
                                   $("#kisayolpnrtable").html(htmlt);
                              }else{
                                   var htmlt = '<thead>'+
                                        '<tr>'+
                                             '<th style="text-align:center;">#</th>'+
                                        '</tr>'+
                                   '</thead>'+
                                   '<tbody><tr><td style="text-align:center;">Kayıt Yok ya da Bulunamadı</td></tr></tbody>';
                                   $("#kisayolpnrtable").html(htmlt);
                              }
                         });
                    }

                    if(tip == "musteri"){
                         $.ajax({
          				url: "index.php/dashboard/aramakisayolalt",
          				method: "POST",
          				data: {
          					kriter: kriter,
          					sayfano: sayfano,
                                   tip: tip
          				}
          			}).done(function (result) {
                              $("#kisayolpnrtable").html("");
                              if(result != "null"){
                                   var htmlt = '<thead>'+
                                        '<tr>'+
                                             '<th style="width:10px">#</th>'+
                                             '<th>ID</th>'+
                                             '<th>Soyad</th>'+
                                             '<th>Ad</th>'+
                                             '<th>Doğumtarihi</th>'+
                                             '<th>İletişim</th>'+
                                        '</tr>'+
                                   '</thead>'+
                                   '<tbody>';
                                   var i = 0;
                                   $.each(result.result, function (key, value) {
                                        i++;
                                        var dtarih = "";
                                        if(value.musteri_dtarih == "0000-00-00"){
                                             dtarih = "";
                                        }else {
                                             dtarih = value.musteri_dtarih;
                                        }

                                        htmlt += '<tr>'+
                                                  '<td>'+i+
                                                  '</td>'+
                                                  '<td>'+value.musteri_id+
                                                  '</td>'+
                                                  '<td>'+value.musteri_soyad.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                                       return letter.toUpperCase();
                                                  })+
                                                  '</td>'+
                                                  '<td>'+value.musteri_ad.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                                       return letter.toUpperCase();
                                                  })+
                                                  '</td>'+
                                                  '<td>('+dtarih+
                                                  ')</td>'+
                                                  '<td><span data-toggle="tooltip" title="Tel:'+value.musteri_tel1+',Tel:'+value.musteri_tel2+'"><i class="fa fa-phone-square" aria-hidden="true"></i></span></td>';
                                   })
                                   htmlt += '</tbody>';
                                   $("#kisayolpnrtable").html(htmlt);
                              }else{
                                   var htmlt = '<thead>'+
                                        '<tr>'+
                                             '<th style="text-align:center;">#</th>'+
                                        '</tr>'+
                                   '</thead>'+
                                   '<tbody><tr><td style="text-align:center;">Kayıt Yok ya da Bulunamadı</td></tr></tbody>';
                                   $("#kisayolpnrtable").html(htmlt);
                              }
                         });
                    }
               }
               veri(kriter, sayfano, tip);
               $(".inputcheck").click(function () {
                    $(".inputcheck").each(function () {
                         $(this).prop('checked', false);
                    });
                    $(this).prop('checked', true);
                    if($(this).attr("id") == "kisayolpnr"){
                         tip = "pnr";
                         veri(kriter, 0, tip);
                    }
                    if($(this).attr("id") == "kisayolsoyad"){
                         tip = "soyad";
                         veri(kriter, 0, tip);
                    }
                    if($(this).attr("id") == "kisayolad"){
                         tip = "ad";
                         veri(kriter, 0, tip);
                    }
                    if($(this).attr("id") == "kisayolextra"){
                         tip = "extra";
                         veri(kriter, 0, tip);
                    }
                    if($(this).attr("id") == "kisayolmusteri"){
                         tip = "musteri";
                         veri(kriter, 0, tip);
                    }
                    if($(this).attr("id") == "kisayolreznot"){
                         tip = "reznot";
                         veri(kriter, 0, tip);
                    }
                    if($(this).attr("id") == "kisyaolyolcunot"){
                         tip = "yolcunot";
                         veri(kriter, 0, tip);
                    }
               })
          }
          loadScript("js/libs/jquery-2.1.1.min.js",function () {
               loadScript("js/libs/jquery-ui-1.10.3.min.js", function () {
                    loadScript("js/bootstrap/bootstrap.min.js", anascript)
               });
          });
     </script>
     <div class="table-responsive" id="ana">
          <table class="table table-bordered" id="kisayolpnrtable"></table>
     </div>
</div>
