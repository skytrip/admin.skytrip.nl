<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link rel="stylesheet" href="/js/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css" type="text/css" />
<style media="screen">
    .eventa {
        display: inline-block;
        position:relative;
        width:63px;
        height:21px;
        color:#fff;
    }
    .eventa:hover, .eventa:visited{
        color:#fff;
    }
    .page-toolbar .btn {
        padding: 8px 12px;
    }
    .btn.btn-lightred {
        background-color: #e05d6f;
        color: #fff;
        border-color: #dc485c;
    }
</style>
<div id="appDashboardItems">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa-fw fa fa-home"></i> Dashboard <span>> My Dashboard</span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <style media="screen">
                #sparks li{
                    max-width: 220px;
                    max-height: 32px;
                }
                #calendar td div{
                    min-height: 38px !important;
                }
            </style>
            <ul id="sparks" class="">
                <li class="sparks-info">
                    <h5> Tutarlar <span class="txt-color-blue">&euro; <?php echo $renevues_last30_days_sum; ?></span></h5>
                    <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                        <?php echo implode(",", $renevues_last15_days); ?>
                    </div>
                </li>
                <li class="sparks-info">
                    <h5> Site Traffic <span class="txt-color-purple"><i class="fa fa-arrow-circle-up"></i>&nbsp;<?php echo $traffic_last30days_sum; ?></span></h5>
                    <div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">
                        <?php echo implode(",", $traffic_last15_days); ?>
                    </div>
                </li>
                <li class="sparks-info">
                    <h5> Yolcular <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;<?php echo $passenger_last30days_sum; ?></span></h5>
                    <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
                        <?php echo implode(",", $passenger_last15_days); ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- widget grid -->
    <section id="widget-grid" class="">
        <!-- row -->
        <div class="row">
            <article class="col-sm-12">
                <!-- new widget -->
                <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                    <header>
                        <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>
                        <h2>Live Feeds </h2>
                        <ul class="nav nav-tabs pull-right in" id="myTab">
                            <li class="active">
                                <a data-toggle="tab" href="#s3"><i class="fa fa-euro"></i> <span class="hidden-mobile hidden-tablet">Tutarlar</span></a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#s1"><i class="fa fa-clock-o"></i> <span class="hidden-mobile hidden-tablet">İstatistik</span></a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#s2"><i class="fa fa-facebook"></i> <span class="hidden-mobile hidden-tablet">Sosyal Ag</span></a>
                            </li>
                        </ul>
                    </header>

                    <!-- widget div-->
                    <div class="no-padding">
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            test
                        </div>
                        <!-- end widget edit box -->

                        <div class="widget-body">
                            <!-- content -->
                            <div id="myTabContent" class="tab-content">
                                <div class="tab-pane fade padding-10 no-padding-bottom" id="s1">
                                    <div class="row no-space">
                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<span class="demo-liveupdate-1">
										<span class="onoffswitch-title">Canlı Veri</span>
										<span class="onoffswitch">
											<input type="checkbox" name="start_interval" class="onoffswitch-checkbox" id="start_interval">
											<label class="onoffswitch-label" for="start_interval">
												<span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</span>
									</span>
                                            <div id="updating-chart" class="chart-large txt-color-blue"></div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 show-stats">
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
											<span class="text">
												My Tasks <span class="pull-right">130/200</span>
											</span>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-color-blueDark" style="width: 65%;"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> <span class="text"> Transfered <span class="pull-right">440 GB</span> </span>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-color-blue" style="width: 34%;"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> <span class="text"> Bugs Squashed<span class="pull-right">77%</span> </span>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-color-blue" style="width: 77%;"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> <span class="text"> User Testing <span class="pull-right">7 Days</span> </span>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-color-greenLight" style="width: 84%;"></div>
                                                    </div>
                                                </div>

                                                <span class="show-stat-buttons"> <span class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> <a href="javascript:void(0);" class="btn btn-default btn-block hidden-xs">Generate PDF</a> </span> <span class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> <a href="javascript:void(0);" class="btn btn-default btn-block hidden-xs">Report a bug</a> </span> </span>

                                            </div>

                                        </div>
                                    </div>

                                    <div class="show-stat-microcharts">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                                            <div class="easy-pie-chart txt-color-orangeDark" data-percent="33" data-pie-size="50">
                                                <span class="percent percent-sign">35</span>
                                            </div>
                                            <span class="easy-pie-title"> Server Load <i class="fa fa-caret-up icon-color-bad"></i> </span>
                                            <ul class="smaller-stat hidden-sm pull-right">
                                                <li>
                                                    <span class="label bg-color-greenLight"><i class="fa fa-caret-up"></i> 97%</span>
                                                </li>
                                                <li>
                                                    <span class="label bg-color-blueLight"><i class="fa fa-caret-down"></i> 44%</span>
                                                </li>
                                            </ul>
                                            <div class="sparkline txt-color-greenLight hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">
                                                130, 187, 250, 257, 200, 210, 300, 270, 363, 247, 270, 363, 247
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="easy-pie-chart txt-color-greenLight" data-percent="78.9" data-pie-size="50">
                                                <span class="percent percent-sign">78.9 </span>
                                            </div>
                                            <span class="easy-pie-title"> Disk Space <i class="fa fa-caret-down icon-color-good"></i></span>
                                            <ul class="smaller-stat hidden-sm pull-right">
                                                <li>
                                                    <span class="label bg-color-blueDark"><i class="fa fa-caret-up"></i> 76%</span>
                                                </li>
                                                <li>
                                                    <span class="label bg-color-blue"><i class="fa fa-caret-down"></i> 3%</span>
                                                </li>
                                            </ul>
                                            <div class="sparkline txt-color-blue hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">
                                                257, 200, 210, 300, 270, 363, 130, 187, 250, 247, 270, 363, 247
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="easy-pie-chart txt-color-blue" data-percent="23" data-pie-size="50">
                                                <span class="percent percent-sign">23 </span>
                                            </div>
                                            <span class="easy-pie-title"> Transfered <i class="fa fa-caret-up icon-color-good"></i></span>
                                            <ul class="smaller-stat hidden-sm pull-right">
                                                <li>
                                                    <span class="label bg-color-darken">10GB</span>
                                                </li>
                                                <li>
                                                    <span class="label bg-color-blueDark"><i class="fa fa-caret-up"></i> 10%</span>
                                                </li>
                                            </ul>
                                            <div class="sparkline txt-color-darken hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">
                                                200, 210, 363, 247, 300, 270, 130, 187, 250, 257, 363, 247, 270
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="easy-pie-chart txt-color-darken" data-percent="36" data-pie-size="50">
                                                <span class="percent degree-sign">36 <i class="fa fa-caret-up"></i></span>
                                            </div>
                                            <span class="easy-pie-title"> Temperature <i class="fa fa-caret-down icon-color-good"></i></span>
                                            <ul class="smaller-stat hidden-sm pull-right">
                                                <li>
                                                    <span class="label bg-color-red"><i class="fa fa-caret-up"></i> 124</span>
                                                </li>
                                                <li>
                                                    <span class="label bg-color-blue"><i class="fa fa-caret-down"></i> 40 F</span>
                                                </li>
                                            </ul>
                                            <div class="sparkline txt-color-red hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">
                                                2700, 3631, 2471, 2700, 3631, 2471, 1300, 1877, 2500, 2577, 2000, 2100, 3000
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end s1 tab pane -->
                                <div class="tab-pane fade" id="s2">
                                    <div class="widget-body-toolbar bg-color-white">
                                        <form class="form-inline" role="form">
                                            <div class="form-group">
                                                <label class="sr-only" for="s123">Show From</label>
                                                <input type="email" class="form-control input-sm" id="s123" placeholder="Show From">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control input-sm" id="s124" placeholder="To">
                                            </div>

                                            <div class="btn-group hidden-phone pull-right">
                                                <a class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown"><i class="fa fa-cog"></i> More <span class="caret"> </span> </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:void(0);"><i class="fa fa-file-text-alt"></i> Export to PDF</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);"><i class="fa fa-question-sign"></i> Help</a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </form>

                                    </div>
                                    <div class="padding-10">
                                        <div id="statsChart" class="chart-large has-legend-unique"></div>
                                    </div>

                                </div>
                                <!-- end s2 tab pane -->

                                <div class="tab-pane fade active in" id="s3">
                                    <div class="widget-body-toolbar bg-color-white smart-form" id="rev-toggles">
                                        <div class="inline-group hidden-mobile">
                                            <label for="gra-0" class="checkbox">
                                                <input type="checkbox" name="gra-0" id="gra-0" checked="checked">
                                                <i></i> Müşteri Kaydı
                                            </label>
                                            <label for="gra-1" class="checkbox">
                                                <input type="checkbox" name="gra-1" id="gra-1" checked="checked">
                                                <i></i> Yolcu
                                            </label>
                                            <label for="gra-2" class="checkbox">
                                                <input type="checkbox" name="gra-2" id="gra-2" checked="checked">
                                                <i></i> Gelir
                                            </label>
                                        </div>
                                        <div class="btn-group hidden-phone pull-right">
                                            <a class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown"><i class="fa fa-cog"></i> More <span class="caret"> </span> </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:void(0);"><i class="fa fa-file-text-alt"></i> Export to PDF</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);"><i class="fa fa-question-sign"></i> Help</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="reportrange" class="page-toolbar pull-right" style="margin:-4px 4px 0 0">
                                            <a href="" class="btn btn-lightred no-border">
                                                <i class="fa fa-calendar"></i>&nbsp;&nbsp;
                                                <span id="reportrangespan"></span>&nbsp;&nbsp;
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="padding-10">
                                        <div id="flotcontainer" class="chart-large has-legend-unique"></div>
                                    </div>
                                </div>
                                <!-- end s3 tab pane -->
                            </div>
                            <!-- end content -->
                        </div>
                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->
            </article>
        </div>

        <!-- end row -->

        <!-- row -->

        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-6">
                <!-- new widget -->
                <div role="widget" style="" class="jarviswidget jarviswidget-sortable" id="wid-id-11" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                    <style media="screen">
                        .hizliislema {
                            padding: 7px 8px 6px !important;
                        }
                    </style>
                    <header role="heading">
                        <div class="jarviswidget-ctrls" role="menu">
                            <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse">
                                <i class="fa fa-minus "></i>
                            </a>
                            <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                <i class="fa fa-expand "></i>
                            </a>
                            <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Standard Data Tables </h2>
                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                    </header>
                    <header role="heading">
                        <!-- <h2>Hızlı İşlemler</h2> -->
                        <ul id="widget-tab-1" class="nav pull-right nav-tabs hizliislemul">
                            <li class="hizliislemli active">
                                <a class="hizliislema" aria-expanded="true" data-toggle="tab" href="#rls1">
                                    <i class="fa fa-lg fa-clock-o"></i>
                                    <span class="hidden-mobile hidden-tablet"> Opsiyon </span>
                                </a>
                            </li>
                            <li class="hizliislemli">
                                <a class="hizliislema" aria-expanded="false" data-toggle="tab" href="#rls2">
                                    <i class="fa fa-lg fa-ban"></i>
                                    <span class="hidden-mobile hidden-tablet"> İptal </span>
                                </a>
                            </li>
                            <li class="hizliislemli">
                                <a class="hizliislema" aria-expanded="false" data-toggle="tab" href="#rls3">
                                    <i class="fa fa-lg fa-pause"></i>
                                    <span class="hidden-mobile hidden-tablet"> Bekleyen </span>
                                </a>
                            </li>
                            <li class="hizliislemli">
                                <a class="hizliislema" aria-expanded="false" data-toggle="tab" href="#rls4">
                                    <i class="fa fa-lg fa-envelope"></i>
                                    <span class="hidden-mobile hidden-tablet"> Özel Mesaj </span>
                                </a>
                            </li>
                            <li class="hizliislemli">
                                <a class="hizliislema" aria-expanded="false" data-toggle="tab" href="#rls5">
                                    <i class="fa fa-lg fa-comments"></i>
                                    <span class="hidden-mobile hidden-tablet"> PNR Mesaj </span>
                                </a>
                            </li>
                            <li class="hizliislemli">
                                <a class="hizliislema" aria-expanded="false" data-toggle="tab" href="#rls6">
                                    <i class="fa fa-lg fa-plane"></i>
                                    <span class="hidden-mobile hidden-tablet"> Uçuşlar </span>
                                </a>
                            </li>
                        </ul>

                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                    </header>
                    <!-- widget div-->
                    <div role="content">
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                            <!-- widget body text-->
                            <div class="tab-content padding-10">
                                <div class="tab-pane fade active in" id="rls1">
                                    <div ng-controller="optionList">
                                        <!-- class="opsiyonlist"> -->
                                        <div  ui-grid="optionGrid" bind-scroll-vertical="true" class="grid"></div>
                                        <div class="dt-toolbar-footer">
                                            <div class="col-sm-6 col-xs-12 hidden-xs">
                                                <div class="dataTables_info" id="dt_basic_info" role="status" aria-live="polite">
                                                    Showing <span class="txt-color-darken">{{firstinpage}}</span> to <span class="txt-color-darken">{{tumu > lastinpage ? lastinpage : tumu }}</span> of <span class="text-primary">{{tumu}}</span> entries
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dt_basic_paginate">
                                                    <ul class="pagination pagination-xs no-margin pull-right">
                                                        <li class="prev">
                                                            <a href="javascript:;" ng-click="pageFirst()">Ilk</a>
                                                        </li>
                                                        <li ng-repeat="p in range(page-5,page)" ng-class="p == page ? 'active' : ''">
                                                            <a href="javascript:void(0);" ng-click="setPage(p)">{{p}}</a>
                                                        </li>
                                                        <li ng-repeat="p in range(page+1,page+5)">
                                                            <a href="javascript:void(0);" ng-click="setPage(p)">{{p}}</a>
                                                        </li>
                                                        <li class="next">
                                                            <a href="javascript:;" ng-click="pageLast()">Son</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="rls2">
                                    <div ng-controller="cancelList">
                                        <!-- class="opsiyonlist"> -->
                                        <div  ui-grid="cancelGrid" bind-scroll-vertical="true" class="grid"></div>
                                        <div class="dt-toolbar-footer">
                                            <div class="col-sm-6 col-xs-12 hidden-xs">
                                                <div class="dataTables_info" id="dt_basic_info" role="status" aria-live="polite">
                                                    Showing <span class="txt-color-darken">{{firstinpage}}</span> to <span class="txt-color-darken">{{tumu > lastinpage ? lastinpage : tumu }}</span> of <span class="text-primary">{{tumu}}</span> entries
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dt_basic_paginate">
                                                    <ul class="pagination pagination-xs no-margin">
                                                        <li class="prev">
                                                            <a href="javascript:;" ng-click="pageFirst()">Ilk</a>
                                                        </li>
                                                        <li ng-repeat="p in range(page-5,page)" ng-class="p == page ? 'active' : ''">
                                                            <a href="javascript:void(0);" ng-click="setPage(p)">{{p}}</a>
                                                        </li>
                                                        <li ng-repeat="p in range(page+1,page+5)">
                                                            <a href="javascript:void(0);" ng-click="setPage(p)">{{p}}</a>
                                                        </li>
                                                        <li class="next">
                                                            <a href="javascript:;" ng-click="pageLast()">Son</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="rls3">
                                    <div ng-controller="waitList">
                                        <!-- class="opsiyonlist"> -->
                                        <div  ui-grid="waitGrid" bind-scroll-vertical="true" class="grid"></div>
                                        <div class="dt-toolbar-footer">
                                            <div class="col-sm-6 col-xs-12 hidden-xs">
                                                <div class="dataTables_info" id="dt_basic_info" role="status" aria-live="polite">
                                                    Showing <span class="txt-color-darken">{{firstinpage}}</span> to <span class="txt-color-darken">{{tumu > lastinpage ? lastinpage : tumu }}</span> of <span class="text-primary">{{tumu}}</span> entries
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="dt_basic_paginate">
                                                    <ul class="pagination pagination-xs no-margin">
                                                        <li class="prev">
                                                            <a href="javascript:;" ng-click="pageFirst()">Ilk</a>
                                                        </li>
                                                        <li ng-repeat="p in range(page-5,page)" ng-class="p == page ? 'active' : ''">
                                                            <a href="javascript:void(0);" ng-click="setPage(p)">{{p}}</a>
                                                        </li>
                                                        <li ng-repeat="p in range(page+1,page+5)">
                                                            <a href="javascript:void(0);" ng-click="setPage(p)">{{p}}</a>
                                                        </li>
                                                        <li class="next">
                                                            <a href="javascript:;" ng-click="pageLast()">Son</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="rls4">
                                    <div ng-controller="specmessage">
                                        <!-- class="opsiyonlist"> -->
                                        <div  ui-grid="specmessageGrid" bind-scroll-vertical="true" class="grid"></div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="rls5">
                                    <div ng-controller="resmessage">
                                        <!-- class="opsiyonlist"> -->
                                        <div  ui-grid="resmessageGrid" bind-scroll-vertical="true" class="grid"></div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="rls6">
                                    <div ng-controller="flightwithpassenger" class="opsiyonlist">
                                        <div class="text-right"><input type="text" class="input-sm" ng-model="selectedDate" /> <button class="btn btn-default" ng-click="dateChanged()">Listele</button></div>
                                        <div  ui-grid="flightwithpassengerGrid" bind-scroll-vertical="true" class="grid"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- end widget body text-->

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->
                <!-- new widget -->
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false" data-widget-deletebutton="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"-->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>
                        <h2> Durum Takvimi </h2>
                        <div class="widget-toolbar">
                            <!-- add: non-hidden - to disable auto hide -->
                            <div class="btn-group">
                                <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
                                    Showing <i class="fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu js-status-update pull-right">
                                    <li>
                                        <a href="javascript:void(0);" id="mt">Month</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" id="ag">Week</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" id="td">Today</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </header>
                    <!-- widget div-->
                    <div>
                        <div class="widget-body no-padding">
                            <!-- content goes here -->
                            <div class="widget-body-toolbar"></div>
                            <div id="calendar" style=""></div>
                            <!-- end content -->
                        </div>

                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->
            </article>

            <article class="col-sm-12 col-md-12 col-lg-6">

                <!-- new widget -->
                <div class="jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false">

                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"-->

                    <header>
                        <span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>
                        <h2>Kuş bakışı ziyaretler</h2>
                    </header>

                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <div>
                                <label>Title:</label>
                                <input type="text" />
                            </div>
                        </div>
                        <!-- end widget edit box -->

                        <div class="widget-body no-padding">
                            <!-- content goes here -->

                            <div id="vector-map" class="vector-map"></div>
                            <div id="heat-fill">
                                <span class="fill-a">0</span>

                                <span class="fill-b">5,000</span>
                            </div>

                            <table class="table table-striped table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th>Country</th>
                                    <th>Visits</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($google_analytics_country)) : ?>
                                    <?php
                                    uasort($google_analytics_country, function ($primary_row, $secondary_row) {
                                        if (intval($primary_row[1]) > intval($secondary_row[1])) {
                                            return -1;
                                        } elseif (intval($primary_row[1]) < intval($secondary_row[1])) {
                                            return 1;
                                        } else {
                                            return 0;
                                        }
                                    });
                                    ?>
                                    <?php foreach ($google_analytics_country as $ga_row) : ?>
                                        <tr>
                                            <td><?php echo $ga_row[0]; ?></td>
                                            <td><?php echo $ga_row[1]; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>

                            <!-- end content -->

                        </div>

                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->

                <!-- new widget -->
                <div class="jarviswidget jarviswidget-color-blue" id="wid-id-4" data-widget-editbutton="false" data-widget-colorbutton="false">

                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"-->

                    <header>
                        <span class="widget-icon"> <i class="fa fa-check txt-color-white"></i> </span>
                        <h2> ToDo's </h2>
                        <!-- <div class="widget-toolbar">
                        add: non-hidden - to disable auto hide</div>-->
                    </header>

                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <div>
                                <label>Title:</label>
                                <input type="text" />
                            </div>
                        </div>
                        <!-- end widget edit box -->

                        <div class="widget-body no-padding smart-form">
                            <!-- content goes here -->
                            <h5 class="todo-group-title">
                                <i class="fa fa-warning"></i> Critical Tasks (<small class="num-of-tasks">1</small>)
                            </h5>
                            <ul id="sortable1" class="todo">
                                <li>
						<span class="handle"> <label class="checkbox">
							<input type="checkbox" name="checkbox-inline">
							<i></i> </label>
						</span>
                                    <p>
                                        <strong>Ticket #17643</strong> - Hotfix for WebApp interface issue [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="text-muted">Sea deep blessed bearing under darkness from God air living isn't. </span>
                                        <span class="date">Jan 1, 2014</span>
                                    </p>
                                </li>
                            </ul>
                            <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Important Tasks (<small class="num-of-tasks">3</small>)</h5>
                            <ul id="sortable2" class="todo">
                                <li>
						<span class="handle"> <label class="checkbox">
							<input type="checkbox" name="checkbox-inline">
							<i></i> </label>
						</span>
                                    <p>
                                        <strong>Ticket #1347</strong> - Inbox email is being sent twice <small>(bug fix)</small> [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="date">Nov 22, 2013</span>
                                    </p>
                                </li>
                                <li>
						<span class="handle"> <label class="checkbox">
							<input type="checkbox" name="checkbox-inline">
							<i></i> </label>
						</span>
                                    <p>
                                        <strong>Ticket #1314</strong> - Call customer support re: Issue <a href="javascript:void(0);" class="font-xs">#6134</a><small> (code review)</small>
                                        <span class="date">Nov 22, 2013</span>
                                    </p>
                                </li>
                                <li>
						<span class="handle"> <label class="checkbox">
							<input type="checkbox" name="checkbox-inline">
							<i></i> </label>
						</span>
                                    <p>
                                        <strong>Ticket #17643</strong> - Hotfix for WebApp interface issue [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="text-muted">Sea deep blessed bearing under darkness from God air living isn't. </span>
                                        <span class="date">Jan 1, 2014</span>
                                    </p>
                                </li>
                            </ul>

                            <h5 class="todo-group-title"><i class="fa fa-check"></i> Completed Tasks (<small class="num-of-tasks">1</small>)</h5>
                            <ul id="sortable3" class="todo">
                                <li class="complete">
						<span class="handle"> <label class="checkbox state-disabled" style="display:none">
							<input type="checkbox" name="checkbox-inline" checked="checked" disabled="disabled">
							<i></i> </label>
						</span>
                                    <p>
                                        <strong>Ticket #17643</strong> - Hotfix for WebApp interface issue [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="text-muted">Sea deep blessed bearing under darkness from God air living isn't. </span>
                                        <span class="date">Jan 1, 2014</span>
                                    </p>
                                </li>
                            </ul>

                            <!-- end content -->
                        </div>

                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->

            </article>

        </div>

        <!-- end row -->

    </section>
    <!-- end widget grid -->
</div>
<div aria-hidden="true" aria-labelledby="detailViewLabel" role="dialog" tabindex="-1" id="detailView" class="modal fade" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--js/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>js/plugin/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/plugin/moment/moment-with-locales.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/plugin/moment/angular-moment.js"></script>
<script type="text/javascript">
    /* DO NOT REMOVE : GLOBAL FUNCTIONS!
    *
    * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
    *
    * // activate tooltips
    * $("[rel=tooltip]").tooltip();
    *
    * // activate popovers
    * $("[rel=popover]").popover();
    *
    * // activate popovers with hover states
    * $("[rel=popover-hover]").popover({ trigger: "hover" });
    *
    * // activate inline charts
    * runAllCharts();
    *
    * // setup widgets
    * setup_widgets_desktop();
    *
    * // run form elements
    * runAllForms();
    *
    ********************************
    *
    * pageSetUp() is needed whenever you load a page.
    * It initializes and checks for all basic elements of the page
    * and makes rendering easier.
    *
    */

    var flot_updating_chart, flot_statsChart, flot_multigraph, calendar;

    // pageSetUp();

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        /*$(".opsiyonlist").mCustomScrollbar({
            axis: "x",
            setWidth: "100%",
            theme: "dark",
            mouseWheelPixels: 1000,
            contentTouchScroll: 50
        });*/
    }

    /*
    * PAGE RELATED SCRIPTS
    */

    // pagefunction
    var pagefunction = function() {
        <?php echo $this->activemenu; ?>
        $(".js-status-update a").click(function () {
            var selText = $(this).text();
            var $this = $(this);
            $this.parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
            $this.parents('.dropdown-menu').find('li').removeClass('active');
            $this.parent().addClass('active');
        });

        /*
        * TODO: add a way to add more todo's to list
        */

        // initialize sortable

        $("#sortable1, #sortable2").sortable({
            handle: '.handle',
            connectWith: ".todo",
            update: countTasks
        }).disableSelection();


        // check and uncheck
        $('.todo .checkbox > input[type="checkbox"]').click(function () {
            var $this = $(this).parent().parent().parent();

            if ($(this).prop('checked')) {
                $this.addClass("complete");

                // remove this if you want to undo a check list once checked
                //$(this).attr("disabled", true);
                $(this).parent().hide();

                // once clicked - add class, copy to memory then remove and add to sortable3
                $this.slideUp(500, function () {
                    $this.clone().prependTo("#sortable3").effect("highlight", {}, 800);
                    $this.remove();
                    countTasks();
                });
            } else {
                // insert undo code here...
            }

        })
        // count tasks
        function countTasks() {
            $('.todo-group-title').each(function () {
                var $this = $(this);
                $this.find(".num-of-tasks").text($this.next().find("li").size());
            });
        }

        /*
        * RUN PAGE GRAPHS
        */

        // load all flot plugins
        loadScript("js/plugin/flot/jquery.flot.cust.min.js", function(){
            loadScript("js/plugin/flot/jquery.flot.resize.min.js", function(){
                loadScript("js/plugin/flot/jquery.flot.time.min.js", function(){
                    loadScript("js/plugin/flot/jquery.flot.tooltip.min.js", generatePageGraphs);
                });
            });
        });
        loadScript("js/plugin/slimscroll/jquery.slimscroll.min.js");

        // loadScript("js/libs/angular.min.js", function(){
        // 	loadScript("js/pagescripts/dashboard.app.js", function () {
        // loadScript("js/plugin/angular-moment/angular-moment.js");
        // 	});
        // });
        function generatePageGraphs() {

            /* TAB 1: UPDATING CHART */
            // For the demo we use generated data, but normally it would be coming from the server

            var data = [],
                totalPoints = 200,
                $UpdatingChartColors = $("#updating-chart").css('color');

            function initAnalyticsData() {
                $.ajax({
                    url: "index.php/analytics/index"
                })
                    .done(function (result) {
                        console.log(result);
                        data = result;
                        setupAnalyticsData();
                    });
            }

            // setup control widget
            var updateInterval = 1500;
            $("#updating-chart").val(updateInterval).change(function () {

                var v = $(this).val();
                if (v && !isNaN(+v)) {
                    updateInterval = +v;
                    $(this).val("" + updateInterval);
                }

            });

            // setup plot
            var options = {
                yaxis: {
                    //min: 0,
                    //max: 100
                    tickSize: 1,
                    tickDecimals: 0
                },
                xaxis: {
                    //min: 0,
                    //max: 100
                    tickSize: 1,
                    tickDecimals: 0
                },
                colors: [$UpdatingChartColors],
                series: {
                    lines: {
                        lineWidth: 1,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0.4
                            }, {
                                opacity: 0
                            }]
                        },
                        steps: false,
                        bars: {
                            show: true
                        },
                        bars: {
                            align: "center",
                            barWidth: 0.5
                        },
                    }
                }
            };

            function setupAnalyticsData() {
                flot_updating_chart = $.plot($("#updating-chart"), [data], options);
            }
            /* live switch */
            $('input[type="checkbox"]#start_interval').click(function () {
                if ($(this).prop('checked')) {
                    $on = true;
                    updateInterval = 15000;
                    update();
                } else {
                    clearInterval(updateInterval);
                    $on = false;
                }
            });

            function update() {

                try {
                    if ($on == true) {
                        updateAnalyticsData();
                        //flot_updating_chart.setData([getRandomData()]);
                        //flot_updating_chart.draw();
                        setTimeout(update, updateInterval);

                    } else {
                        clearInterval(updateInterval)
                    }
                }
                catch(err) {
                    clearInterval(updateInterval);
                }

            }

            function updateAnalyticsData() {
                $.ajax({
                    url: "index.php/analytics/index"
                })
                    .done(function (result) {
                        console.log(result);
                        data = result;
                        flot_updating_chart.setData([data]);
                        flot_updating_chart.draw();
                    });
            }

            var $on = false;

            // initAnalyticsData();

            /*end updating chart*/

            /* TAB 2: Social Network  */

            $(function () {
                // jQuery Flot Chart
                var twitter = [
                        [1, 27],
                        [2, 34],
                        [3, 51],
                        [4, 48],
                        [5, 55],
                        [6, 65],
                        [7, 61],
                        [8, 70],
                        [9, 65],
                        [10, 75],
                        [11, 57],
                        [12, 59],
                        [13, 62]
                    ],
                    facebook = [
                        [1, 25],
                        [2, 31],
                        [3, 45],
                        [4, 37],
                        [5, 38],
                        [6, 40],
                        [7, 47],
                        [8, 55],
                        [9, 43],
                        [10, 50],
                        [11, 47],
                        [12, 39],
                        [13, 47]
                    ],
                    data = [{
                        label: "Twitter",
                        data: twitter,
                        lines: {
                            show: true,
                            lineWidth: 1,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.1
                                }, {
                                    opacity: 0.13
                                }]
                            }
                        },
                        points: {
                            show: true
                        }
                    }, {
                        label: "Facebook",
                        data: facebook,
                        lines: {
                            show: true,
                            lineWidth: 1,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.1
                                }, {
                                    opacity: 0.13
                                }]
                            }
                        },
                        points: {
                            show: true
                        }
                    }];

                var options = {
                    grid: {
                        hoverable: true
                    },
                    colors: ["#568A89", "#3276B1"],
                    tooltip: true,
                    tooltipOpts: {
                        //content : "Value <b>$x</b> Value <span>$y</span>",
                        defaultTheme: false
                    },
                    xaxis: {
                        ticks: [
                            [1, "JAN"],
                            [2, "FEB"],
                            [3, "MAR"],
                            [4, "APR"],
                            [5, "MAY"],
                            [6, "JUN"],
                            [7, "JUL"],
                            [8, "AUG"],
                            [9, "SEP"],
                            [10, "OCT"],
                            [11, "NOV"],
                            [12, "DEC"],
                            [13, "JAN+1"]
                        ]
                    },
                    yaxes: {

                    }
                };

                flot_statsChart = $.plot($("#statsChart"), data, options);
            });

            // END TAB 2

            // TAB THREE GRAPH //
            /* TAB 3: Revenew  */

            $(function () {

                var trgt = [
                    ],
                    prft = [
                    ],
                    sgnups = [
                    ],
                    toggles = $("#rev-toggles"),
                    target = $("#flotcontainer");

                var data = [{
                    label: "Müşteri",
                    data: trgt,
                    color: '#6e587a',
                    lines: {
                        show: true,
                        lineWidth: 2
                    },
                    points: {
                        show: true
                    }
                }, {
                    label: "Yolcu",
                    data: prft,
                    color: '#71843F',
                    lines: {
                        show: true,
                        lineWidth: 3
                    },
                    points: {
                        show: true
                    }
                }, {
                    label: "Gelir",
                    data: sgnups,
                    color: '#57889c',
                    lines: {
                        show: false
                        // lineWidth: 2
                    },
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 3*24*60*60*300,
                        fill: true,
                        lineWidth: 1,
                        order: 3,
                        fillColor:  {
                            colors: [{ opacity: 0.7 }, { opacity: 0.1}]
                        }
                    },
                    points: {
                        show: true
                    }
                }]

                var options = {
                    grid: {
                        hoverable: true
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '%x - %y',
                        dateFormat: '%b %d'
                    },
                    xaxis: {
                        mode: "time"
                    },
                    yaxes: {
                        tickDecimals: 0
                    }

                };

                flot_multigraph = null;

                function plotNow() {
                    if (prft.length == 0) return;
                    if (sgnups.length == 0) return;
                    if (trgt.length == 0) return;
                    data[0].data = trgt;
                    data[1].data = prft;
                    data[2].data = sgnups;
                    var d = [];
                    toggles.find(':checkbox').each(function () {
                        if ($(this).is(':checked')) {
                            d.push(data[$(this).attr("name").substr(4, 1)]);
                        }
                    });
                    if (d.length > 0) {
                        // console.log(options);
                        // if (flot_multigraph) {
                        // 	flot_multigraph.setData(d);
                        // 	flot_multigraph.draw();
                        // } else {
                        // 	flot_multigraph = $.plot(target, d, options);
                        // }
                        flot_multigraph = $.plot(target, d, options);
                    }

                };

                toggles.find(':checkbox').on('change', function () {
                    plotNow();
                });
                function passenger_count(day1, day2) {
                    $.ajax({
                        url: "index.php/dashboard/passenger_count",
                        data: {
                            day1: day1,
                            day2: day2
                        }
                    }).done(function(result) {
                        prft = result;
                        plotNow();
                    });
                }

                function revenue_summary(day1, day2) {
                    $.ajax({
                        url: "index.php/dashboard/revenue_summary",
                        data: {
                            day1: day1,
                            day2: day2
                        }
                    }).done(function(result) {
                        sgnups = result;
                        plotNow();
                    });
                }

                function daily(day1, day2) {
                    $.ajax({
                        url: "index.php/mongoctrl/daily",
                        data: {
                            day1: day1,
                            day2: day2
                        }
                    }).done(function(result) {
                        trgt = result;
                        plotNow();
                    });
                }

                passenger_count();
                revenue_summary();
                daily();

                var daterangeformat = 'MMMM D, YYYY';
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    daterangeformat = 'MM D, YYYY';
                    // $("#reportrange").css("margin","-4px 4px 0 0").css("width", "100%");
                }

                $('#reportrangespan').text(moment().startOf("month").format(daterangeformat) + ' - ' + moment().format(daterangeformat));

                $('#reportrange').daterangepicker({
                        "ranges": {
                            "Today":  [moment(), moment()],
                            "Yesterday": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            "Last 7 Days": [moment().subtract(6, 'days'), moment()],
                            "Last 30 Days": [moment().subtract(29, 'days'), moment()],
                            "This Month": [moment().startOf('month'), moment().endOf('month')],
                            "Last Month": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        "locale": {
                            "format": "DD/MM/YYYY",
                            "separator": " - ",
                            "applyLabel": "Apply",
                            "cancelLabel": "Cancel",
                            "fromLabel": "From",
                            "toLabel": "To",
                            "customRangeLabel": "Custom",
                            "weekLabel": "W",
                            "daysOfWeek": [
                                "Paz",
                                "Pazt",
                                "Sal",
                                "Çar",
                                "Per",
                                "Cum",
                                "Cumr"
                            ],
                            "monthNames": [
                                "Ocak",
                                "Şubat",
                                "Mart",
                                "Nisan",
                                "Mayıs",
                                "Haziran",
                                "Temmuz",
                                "Ağustos",
                                "Eylül",
                                "Ekim",
                                "Kasım",
                                "Aralık"
                            ],
                            "firstDay": 1
                        },
                        "alwaysShowCalendars": true,
                        "opens": "left"
                    },
                    function(start, end, label) {
                        $('#reportrangespan').text(start.format(daterangeformat) + ' - ' + end.format(daterangeformat));
                        start = start.format('YYYY-MM-DD');
                        end = end.format('YYYY-MM-DD');
                        passenger_count(start,end);
                        revenue_summary(start,end);
                        daily(start,end);
                    });
            });

        }

        /*
        * VECTOR MAP
        */

        data_array = {
            "US": 4977,
            "AU": 4873,
            "IN": 3671,
            "BR": 2476,
            "TR": 1476,
            "CN": 146,
            "CA": 134,
            "BD": 100
        };

        // Load Map dependency 1 then call for dependency 2 and then run renderVectorMap function
        loadScript("js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js", function(){
            loadScript("js/plugin/vectormap/jquery-jvectormap-world-mill-en.js", renderVectorMap);
        });

        function loadVectorMap() {
            $.ajax({
                url: "index.php/analytics/map_list" // link ölü: 404
            }).done(function(result) {
                data_array = result;
                renderVectorMap();
            });
        }


        function renderVectorMap() {
            $('#vector-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: '#fff',
                regionStyle: {
                    initial: {
                        fill: '#c4c4c4'
                    },
                    hover: {
                        "fill-opacity": 1
                    }
                },
                series: {
                    regions: [{
                        values: data_array,
                        scale: ['#85a8b6', '#4d7686'],
                        normalizeFunction: 'polynomial'
                    }]
                },
                onRegionLabelShow: function (e, el, code) {
                    if (typeof data_array[code] == 'undefined') {
                        e.preventDefault();
                    } else {
                        var countrylbl = data_array[code];
                        el.html(el.html() + ': ' + countrylbl + ' visits');
                    }
                }
            });
        }

        /*
        * FULL CALENDAR JS
        */

        // Load Calendar dependency then setup calendar
        // loadScript("//cdn.jsdelivr.net/momentjs/latest/moment.min.js", function () {
        // 	loadScript("//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js")
        // });

        loadScript("js/plugin/moment/moment.min.js", function(){
            loadScript("js/plugin/fullcalendar/jquery.fullcalendar.min.js", function(){
                loadScript("//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js", setupCalendar);
            });
        });

        function setupCalendar() {
            if ($("#calendar").length) {
                var date = new Date();
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();

                calendar = $('#calendar').fullCalendar({
                    editable: false,
                    draggable: false,
                    selectable: false,
                    selectHelper: true,
                    unselectAuto: false,
                    disableResizing: false,
                    height: "auto",

                    header: {
                        left: 'title', //,today
                        center: 'prev, next, today',
                        right: 'month, agendaWeek, agenDay' //month, agendaDay,
                    },

                    select: function (start, end, allDay) {
                        var title = prompt('Event Title:');
                        if (title) {
                            calendar.fullCalendar('renderEvent', {
                                title: title,
                                start: start,
                                end: end,
                                allDay: allDay
                            }, true); // make the event "stick"
                        }
                        calendar.fullCalendar('unselect');
                    },

                    events: <?php echo $calendar_data; ?>,

                    eventRender: function (event, element, icon) {
                        //   console.log(event.kontrol[0]);
                        if(event.tip == "qr"){
                            if(event.kontrol[0] != 0) {
                                if(event.kontrol[0] == "false"){
                                    var da = event.start._d;
                                    var day = da.getDate();
                                    var month = da.getMonth()+1;
                                    var year = da.getFullYear();
                                    if(day < 10){
                                        day = "0" + day;
                                    }
                                    if(month < 10){
                                        month = "0" + month;
                                    }
                                    var tarih = year + "-" + month + "-" + day;

                                    $("td[data-date='" + tarih + "']").css("background-color", "#d68d8d");
                                }
                                if(event.kontrol[0] == "ok"){
                                    var da = event.start._d;
                                    var day = da.getDate();
                                    var month = da.getMonth()+1;
                                    var year = da.getFullYear();
                                    if(day < 10){
                                        day = "0" + day;
                                    }
                                    if(month < 10){
                                        month = "0" + month;
                                    }
                                    var tarih = year + "-" + month + "-" + day;

                                    $("td[data-date='" + tarih + "']").css({
                                        'background-color' : '#00ff00',
                                        'opacity' : '0.2'
                                    });
                                }
                            }
                        }
                        if(event.tip == "qr"){
                            var span = element.find(".fc-event-title");
                            var tarih = moment(event.start._d).format("DD-MM-YYYY");
                            span.replaceWith("<a target='_blank' href='http://adminpanel.skytrip.nl/#index.php/webservicelog/?tip=qr&tarih=" + tarih + "' class='eventa'><span class='fc-event-title'>" + span.text() + "</span></a>")
                        }
                        if(event.tip == "op"){
                            var span = element.find(".fc-event-title");
                            var tarih = moment(event.start._d).format("DD-MM-YYYY");
                            span.replaceWith("<a target='_blank' href='<?php echo base_url(); ?>#index.php/reservation?tip=op&tarih=" + tarih + "' class='eventa'><span class='fc-event-title'>" + span.text() + "</span></a>")
                        }
                        if(event.tip == "ca"){
                            var span = element.find(".fc-event-title");
                            var tarih = moment(event.start._d).format("DD-MM-YYYY");
                            span.replaceWith("<a target='_blank' href='<?php echo base_url(); ?>#index.php/reservation?tip=ca&tarih=" + tarih + "' class='eventa'><span class='fc-event-title'>" + span.text() + "</span></a>")
                        }
                        if(event.tip == "ok"){
                            var span = element.find(".fc-event-title");
                            var tarih = moment(event.start._d).format("DD-MM-YYYY");
                            span.replaceWith("<a target='_blank' href='<?php echo base_url(); ?>#index.php/reservation?tip=ok&tarih=" + tarih + "' class='eventa'><span class='fc-event-title'>" + span.text() + "</span></a>")
                        }
                        if (!event.description == "") {
                            element.find('.fc-title').append("<br/><span class='ultra-light'>" + event.description + "</span>");
                        }
                        if (!event.icon == "") {
                            element.find('.fc-title').append("<i class='air air-top-right fa " + event.icon + " '></i>");
                        }
                        element.find(".fc-event-time").remove();
                    }
                });
            };
            /* hide default buttons */
            $('.fc-toolbar .fc-right, .fc-toolbar .fc-center').hide();
        }

        // calendar prev
        $('#calendar-buttons #btn-prev').click(function () {
            $('.fc-prev-button').click();
            return false;
        });

        // calendar next
        $('#calendar-buttons #btn-next').click(function () {
            $('.fc-next-button').click();
            return false;
        });

        // calendar today
        $('#calendar-buttons #btn-today').click(function () {
            $('.fc-button-today').click();
            return false;
        });

        // calendar month
        $('#mt').click(function () {
            $('#calendar').fullCalendar('changeView', 'month');
        });

        // calendar agenda week
        $('#ag').click(function () {
            $('#calendar').fullCalendar('changeView', 'agendaWeek');
        });

        // calendar agenda day
        $('#td').click(function () {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
        });
    };

    // end pagefunction

    // destroy generated instances
    // pagedestroy is called automatically before loading a new page
    // only usable in AJAX version!

    var pagedestroy = function(){

        // destroy calendar
        calendar.fullCalendar('destroy');
        calendar = null;

        //destroy flots
        flot_updating_chart.shutdown();
        flot_updating_chart=null;
        flot_statsChart.shutdown();
        flot_statsChart = null;

        flot_multigraph.shutdown();
        flot_multigraph = null;

        // destroy vector map objects
        $('#vector-map').find('*').addBack().off().remove();

        // destroy todo
        $("#sortable1, #sortable2").sortable("destroy");
        $('.todo .checkbox > input[type="checkbox"]').off();

        // destroy misc events
        $("#rev-toggles").find(':checkbox').off();

        // debug msg
        if (debugState){
            root.console.log("✔ Calendar, Flot Charts, Vector map, misc events destroyed");
        }

        loadScript("js/plugin/ui-grid/ui-grid.min.js");
    }

    // end destroy

    // run pagefunction on load
    pagefunction();


</script>
<!-- <script type="text/javascript" src="--><?php //echo base_url(); ?><!--js/libs/angular.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>js/pagescripts/dashboard.app.js"></script>
<!-- <script type="text/javascript" src="--><?php //echo base_url(); ?><!--js/plugin/daterangepicker/daterangepicker.js"></script>-->
