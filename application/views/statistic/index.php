<div id="appStatistic">
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 ng-isolate-scope">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa-fw fa fa-home"></i> Statistic
			</h1>
		</div>
		<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
			<ul id="sparks" class="">
				<li class="sparks-info">
					<h5> Tutarlar <span class="txt-color-blue">&euro; <?php echo $renevues_last30_days_sum; ?></span></h5>
					<div id="sparktutar" class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
						h
					</div>
				</li>
				<li class="sparks-info">
					<h5> Site Traffic <span class="txt-color-purple"><i class="fa fa-arrow-circle-up"></i>&nbsp;<?php echo $traffic_last30days_sum; ?></span></h5>
					<div id="sparktraf" class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">
						h
					</div>
				</li>
				<li class="sparks-info">
					<h5> Yolcular <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;<?php echo $passenger_last30days_sum; ?></span></h5>
					<div id="sparkyolcu" class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
						h
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<article class="col-sm-12">
				<div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
					<header>
						<span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>
						<h2>7 Günlük Raporlar </h2>
						<ul class="nav nav-tabs pull-right in" id="myTab">
							<li class="active">
								<a data-toggle="tab" href="#s1"><i class="fa fa-plane" aria-hidden="true"></i> <span class="hidden-mobile hidden-tablet">Uçuş Aramaları</span></a>
							</li>
							<li>
								<a data-toggle="tab" href="#s2"><i class="fa fa-clock-o"></i> <span class="hidden-mobile hidden-tablet">Rezervasyonlar</span></a>
							</li>
							<li>
								<a data-toggle="tab" href="#s3"><i class="fa fa-eur" aria-hidden="true"></i> <span class="hidden-mobile hidden-tablet">Biletler</span></a>
							</li>
						</ul>
					</header>
					<div class="no-padding">
						<div class="widget-body">
							<div id="myTabContent" class="tab-content">
								<div class="tab-pane fade active in" id="s1">
									<div class="row no-space">
										<div class="col-sm-12 well">
											<div class="col-sm-6" id="ucusaramalar7daytable"></div>
											<div class="col-sm-6" style="">
												<div style="" id="ucusaramalar7day"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="s2">
									<div class="row no-space">
										<div class="col-sm-12 well">
											<div class="col-sm-6" id="reservationlast7daytable"></div>
											<div class="col-sm-6" style="">
												<div style="" id="reservationlast7day"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="s3">
									<div class="row no-space">
										<div class="col-sm-12 well">
											<div class="col-sm-6" id="biletlerlast7daytable"></div>
											<div class="col-sm-6" style="">
												<div style="" id="biletlerlast7day"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
			<!-- end row -->
			<div class="row no-space padding-10">
				<div class="col-sm-12 well">
					<div class="col-sm-6" id="genelrezervsatis12monthtable"></div>
					<div class="col-sm-6" style="">
						<div style="" id="genelrezervsatis12month"></div>
					</div>
				</div>
			</div>
			<div class="row no-space padding-10">
				<div class="col-sm-12 well">
					<div class="col-sm-6" id="turopyuzdetable"></div>
					<div class="col-sm-6" style="">
						<div style="" id="turopyuzde"></div>
					</div>
				</div>
			</div>
			<article class="col-sm-12">
				<div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
					<header>
						<span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>
						<h2>Gidiş Dönüş Raporları </h2>
						<ul class="nav nav-tabs pull-right in" id="myTab">
							<li class="active">
								<a data-toggle="tab" href="#s4"><i class="fa fa-plane" aria-hidden="true"></i> <span class="hidden-mobile hidden-tablet">Gidiş Havaalanları</span></a>
							</li>
							<li>
								<a data-toggle="tab" href="#s5"><i class="fa fa-clock-o"></i> <span class="hidden-mobile hidden-tablet">Dönüş Havaalanları</span></a>
							</li>
							<li>
								<a data-toggle="tab" href="#s6"><i class="fa fa-eur" aria-hidden="true"></i> <span class="hidden-mobile hidden-tablet">Güzergah</span></a>
							</li>
						</ul>
					</header>
					<div class="no-padding">
						<div class="widget-body">
							<div id="myTabContent" class="tab-content">
								<div class="tab-pane fade active in" id="s4">
									<div class="row no-space">
										<div class="col-sm-12 well">
											<div class="col-sm-6" id="gidishavaalanlaritable"></div>
											<div class="col-sm-6" style="">
												<div style="" id="gidishavaalanlari"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="s5">
									<div class="row no-space">
										<div class="col-sm-12 well">
											<div class="col-sm-6" id="donushavalanlaritable"></div>
											<div class="col-sm-6" style="">
												<div style="" id="donushavalanlari"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="s6">
									<div class="row no-space">
										<div class="col-sm-12 well">
											<div class="col-sm-6" id="guzergahtable"></div>
											<div class="col-sm-6" style="">
												<div style="" id="guzergah"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>

			<style media="screen">
			.list-item {
				list-style-type: none;
				margin-left: -40px;
				display: block;
				position: relative;
				padding: 12px 16px;
				min-height: 60px
			}
			.list-left {
				float: left;
			}
			.list-left + .list-body {
				margin-left: 56px;
			}
			.info {
				color: rgba(255, 255, 255, 0.87);
				background-color: #6887ff!important;
			}
			.accent {
				color: rgba(255, 255, 255, 0.87);
				background-color: #a88add!important;
			}
			.danger {
				color: rgba(255, 255, 255, 0.87);
				background-color: #f44455!important;
			}
			.labela {
				padding: 0.25em 0.5em;
				font-weight: bold;
				background-color: #b8b8b8;
				display: inline-block;
				padding: .25em .4em;
				font-size: 75%;
				font-weight: 700;
				line-height: 1;
				color: #fff;
				text-align: center;
				white-space: nowrap;
				vertical-align: baseline;
				border-radius: .25rem;
			}
			.box-header h3 {
				font-size: 16px;
			}
			.box-header h2, .box-header h3, .box-header h4 {
				margin: 0;
				font-size: 18px;
				line-height: 1;
			}
			.text {
				font-size: 1.5rem;
			}
			.text-u-c {
				text-transform: uppercase;
			}
			.text-muted {
				color: inherit;
				opacity: 0.6;
			}
			.box-header {
				position: relative;
				padding: 13px;
			}
			.text-muted {
				color: #818a91;
			}
			.b-b {
				border-bottom: 1px solid rgba(120, 130, 140, 0.13);
			}
			.box-shadow-z0, .box-shadow-z0 .box, .box-shadow-z0 .box-color {
				box-shadow: 0 0px 1px rgba(0, 0, 0, 0.15);
			}
			.box, .box-color {
				background-color: #fff;
				position: relative;
				margin-bottom: 1.5rem;
			}
			.w-40 {
				width: 40px;
				height: 40px;
				line-height: 40px;
				display: inline-block;
				text-align: center;
			}

			.rounded, .circle {
				border-radius: 500px;
			}
			.primary {
				color: rgba(255, 255, 255, 0.87);
				background-color: #0cc2aa;
			}
			.blueprimary {
				background-color: #2196f3;
				color: rgba(255, 255, 255, 0.87);
			}
			.reservationa{
				color: inherit!important;
				text-decoration: none;
				cursor: pointer;
				outline: 0;
			}
			.dashbord-list-top-div {
				margin-top: -10px;
			}
			.dashbord-list-a {
				position: relative;
				display: inline-block;
			}
			.pointer {
				cursor: pointer;
			}
			.success {
				color: rgba(255, 255, 255, 0.87);
				background-color: #6cc788;
			}
			.dark {
				color: rgba(255, 255, 255, 0.87);
				background-color: #2e3e4e;
			}
			.tumunugor {
				display: table;
				table-layout: fixed;
				border-spacing: 0;
				width: 100%;
				height: 100%;
			}
			.tumunugora{
				color: inherit;
				text-decoration: none;
				cursor: pointer;
				outline: 0;
			}
			.p-a {
				padding: 1rem !important;
			}
			.black {
				color: rgba(255, 255, 255, 0.87)!important;
				background-color: #2a2b3c;
			}
			</style>
			<div class="row-col" style="border-top: 1px solid #CCC">
				<div class="col-md-4 col-lg-4 white">
					<div class="padding">
						<div class="box">
							<div class="box-header b-b">
								<h3 class="text-u-c text-muted">Uçuş Aramaları</h3>
							</div>
							<ul class="list inset" id="ucusaramalari"></ul>
						</div>
						<div class="box row-col tumunugor">
							<div class="row-cell p-a black lt">
								<a href="" class="tumunugora">Tümünü Görüntüle</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 white">
					<div class="padding">
						<div class="box">
							<div class="box-header b-b">
								<h3 class="text-u-c text-muted">Rezervasyonlar</h3>
							</div>
							<ul class="list inset" id="rezervasyonlar"></ul>
						</div>
						<div class="box row-col tumunugor">
							<div class="row-cell p-a black lt">
								<a href="" class="tumunugora">Tümünü Görüntüle</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-lg-4 white">
					<div class="padding">
						<div class="box">
							<div class="box-header b-b">
								<h3 class="text-u-c text-muted">Biletler</h3>
							</div>
							<ul class="list inset" id="biletler"></ul>
						</div>
						<div class="box row-col tumunugor">
							<div class="row-cell p-a black lt">
								<a href="" class="tumunugora">Tümünü Görüntüle</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end row -->
	</section>
</div>
<div aria-hidden="true" aria-labelledby="detailViewLabel" role="dialog" tabindex="-1" id="detailView" class="modal fade" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<script type="text/javascript" src="js/plugin/moment/moment.min.js"></script>
<script type="text/javascript">
function anafonk(){
	moment.locale('tr');
	<?php echo $activemenu; ?>
	function rezervasyonlast7days(day1,day2) {
		$.ajax({
			url: "index.php/dashboard/lastreservationcount",
			data: {
				day1: day1,
				day2: day2
			},
			cache: false
		}).done(function(result) {
			var week = [];
			var counts = [];
			for (var i = 0; i < 7; i++) {
				var day = moment(new Date(day2).getTime() - i * 24 * 3600 * 1000).format('DD/MM/YY');
				week.push(day);
				var mevcut = false;
				var countinarray = 0;
				for (var a = 0; a < result.result.length; a++) {
					if(moment(result.result[a]["reztarih"]).format('DD/MM/YY') === day){
						mevcut = true;
						counts.push(result.result[a]["count"]);
					}
				}
				if(mevcut === false){
					counts.push(0);
				}
			}
			week.reverse();
			counts.reverse();

			var table = '<table class="highchart table table-hover table-bordered" data-highchart-table=""'+ 'data-graph-container="reservationlast7day" data-graph-type="column">'+
			'<caption>Rezervasyonlar 7 Gün</caption>'+
			'<thead>'+
			'<tr>';
			for (var b = 0; b < week.length; b++) {
				table += '<th>' + week[b] + '</th>';
			}
			table += '</tr>'+
			'</thead>'+
			'<tbody><tr>';
			for (var c = 0; c < counts.length; c++) {
				table += '<td>' + counts[c] + '</td>';
			}
			table += '</tbody></table>';
			$("#reservationlast7daytable").html(table);
			var chart = new Highcharts.Chart({
				chart: {
					renderTo: 'reservationlast7day',
					type: 'column'
				},
				title: {
					text: "Rezervasyonlar Son 7 Gün"
				},
				xAxis: {
					categories: week
				},
				yAxis: {
					title: {
						text: 'Value',
						margin: 55
					},
					tickInterval: 1
				},
				series: [{
					name:"Rezervasyon Count",
					data: counts,
					pointWidth: 54
				}]
			});
		});
	}

	function biletlast7days(day1,day2) {
		$.ajax({
			url: "index.php/dashboard/lastbiletcount",
			data: {
				day1: day1,
				day2: day2
			},
			cache: false
		}).done(function(result) {
			var week = [];
			var counts = [];
			for (var i = 0; i < 7; i++) {
				var day = moment(new Date(day2).getTime() - i * 24 * 3600 * 1000).format('DD/MM/YY');
				week.push(day);
				var mevcut = false;
				var countinarray = 0;
				for (var a = 0; a < result.result.length; a++) {
					if(moment(result.result[a]["reztarih"]).format('DD/MM/YY') === day){
						mevcut = true;
						counts.push(result.result[a]["count"]);
					}
				}
				if(mevcut === false){
					counts.push(0);
				}
			}
			week.reverse();
			counts.reverse();

			var table = '<table class="highchart table table-hover table-bordered" data-highchart-table=""'+ 'data-graph-container="biletlerlast7day" data-graph-type="column">'+
			'<caption>Biletler 7 Gün</caption>'+
			'<thead>'+
			'<tr>';
			for (var b = 0; b < week.length; b++) {
				table += '<th>' + week[b] + '</th>';
			}
			table += '</tr>'+
			'</thead>'+
			'<tbody><tr>';
			for (var c = 0; c < counts.length; c++) {
				table += '<td>' + counts[c] + '</td>';
			}
			table += '</tbody></table>';
			$("#biletlerlast7daytable").html(table);
			var chart = new Highcharts.Chart({
				chart: {
					renderTo: 'biletlerlast7day',
					type: 'column'
				},
				title: {
					text: "Biletler Son 7 Gün"
				},
				xAxis: {
					categories: week
				},
				yAxis: {
					title: {
						text: 'Value',
						margin: 55
					},
					tickInterval: 1
				},
				series: [{
					name:"Bilet Count",
					data: counts,
					pointWidth: 54
				}]
			});
		});
	}

	function ucusaramalaricount7days(day1,day2) {
		$.ajax({
			url: "index.php/mongoctrl/daily",
			data: {
				day1: day1,
				day2: day2
			},
			cache: false
		}).done(function(result) {
			var week = [];
			var counts = [];
			for (var i = 0; i < 7; i++) {
				var day = moment(new Date(day2).getTime() - i * 24 * 3600 * 1000).format('DD/MM/YY');
				week.push(day);
				var mevcut = false;
				var countinarray = 0;
				for (var a = 0; a < result.length; a++) {
					if(moment(result[a][0]).format('DD/MM/YY') === day){
						mevcut = true;
						counts.push(result[a][1]);
					}
				}
				if(mevcut === false){
					counts.push(0);
				}
			}
			week.reverse();
			counts.reverse();

			var table = '<table class="highchart table table-hover table-bordered" data-highchart-table=""'+ 'data-graph-container="biletlerlast7day" data-graph-type="column">'+
			'<caption>Uçuş Aramaları 7 Gün</caption>'+
			'<thead>'+
			'<tr>';
			for (var b = 0; b < week.length; b++) {
				table += '<th>' + week[b] + '</th>';
			}
			table += '</tr>'+
			'</thead>'+
			'<tbody><tr>';
			for (var c = 0; c < counts.length; c++) {
				table += '<td>' + counts[c] + '</td>';
			}
			table += '</tbody></table>';
			$("#ucusaramalar7daytable").html(table);
			var chart = new Highcharts.Chart({
				chart: {
					renderTo: 'ucusaramalar7day',
					type: 'column'
				},
				title: {
					text: "Uçuş Aramaları 7 Gün"
				},
				xAxis: {
					categories: week
				},
				yAxis: {
					title: {
						text: 'Value',
						margin: 55
					},
					tickInterval: 1
				},
				series: [{
					name:"Arama Count",
					data: counts,
					pointWidth: 54
				}]
			});
		});
	}

	function rezervasyonlast12month(month1,month2) {
		$.ajax({
			url: "index.php/reservation/reservationlistmonthsatiscount",
			data: {
				month1: month1,
				month2: month2
			},
			cache: false
		}).done(function(result) {
			var months = [];
			var counts = [];
			var countsok = [];
			var sorgu = [];
			var yolcuarr = [];
			var kararr = [];
			for (var i = 0; i < result.rezervation.length; i++) {
				counts.push(result.rezervation[i].count);
				countsok.push(result.rezervation[i].countok);
				yolcuarr.push(result.rezervation[i].yolcu_sayi);
				kararr.push(result.rezervation[i].kazanc);
			}
			for (var i = 0; i < result.sorgu.length; i++) {
				sorgu.push(result.sorgu[i][1]);
				months.push(moment(result.sorgu[i][0]).format("MMM") + " " + moment(result.sorgu[i][0]).format("YYYY"));
			}

			result.rezervation.reverse();
			result.sorgu.reverse();
			var table = '<table class="highchart table table-hover table-bordered" data-highchart-table=""'+ 'data-graph-container="genelrezervsatis12month" data-graph-type="column">'+
			'<caption>Rezervasyon ve Satışlar 12 Ay</caption>'+
			'<thead>'+
			'<tr>'+
			'<th>Aylar</th>'+
			'<th class="" style="">Genel Rezervasyon</th>'+
			'<th class="">Satışlar</th>'+
			'<th class="">Sorgu</th>'+
			'<th class="">Yolcu</th>'+
			'<th class="">Kâr</th>'+
			'</tr>'+
			'</thead>'+
			'<tbody>';
			for (var b = 0; b < result.sorgu.length; b++) {
				var rezcount = "";
				var rezcountok = "";
				var yolcu = "";
				var kar = "";
				for (var c = 0; c < result.rezervation.length; c++) {
					if(result.rezervation[c].reztarih == moment(result.sorgu[b][0]).format("YYYY-MM")){
						rezcount = result.rezervation[c].count;
						rezcountok = result.rezervation[c].countok;
						yolcu = result.rezervation[c].yolcu_sayi;
						kar = result.rezervation[c].kazanc + "€";
					}
				}
				table += '<tr><td>' + moment(result.sorgu[b][0]).format("MMM YYYY") + '</td><td>' + rezcount + '</td><td style="text-align:center">' + rezcountok + '</td><td style="text-align:center">' + result.sorgu[b][1] + '</td><td style="text-align:center">' + yolcu + '</td><td style="text-align:right">' + kar + '</td></tr>';
			}
			table += '</tbody></table>';
			$("#genelrezervsatis12monthtable").html(table);
			var chart = new Highcharts.Chart({
				chart: {
					renderTo: 'genelrezervsatis12month',
					type: 'column',
					// width: 600
				},
				title: {
					text: "Rezervasyon ve Satışlar 12 Ay"
				},
				xAxis: {
					categories: months
				},
				yAxis: {
					title: {
						text: 'Value',
						margin: 100
					},
					tickInterval: 1
				},
				series: [{
					name:"Rezervasyonlar",
					data: counts,
					pointWidth: 10,
					color: "#aa4643"
				},{
					name:"Satışlar",
					data: countsok,
					pointWidth: 10,
				},{
					name:"Sorgu",
					data: sorgu,
					pointWidth: 10,
					color: "#4572a7"
				},{
					name:"Yolcu",
					data: yolcuarr,
					pointWidth: 10
				},{
					name:"Kâr",
					data: kararr,
					pointWidth: 10,
					color: "#80699b"
				}]
				// ,
				// tooltip: {
				// 	shared: true
				// },
			});
		});
	}

	function rezervasyonturopyuzde() {
		$.ajax({
			url: "index.php/reservation/reservationlistturopyuzde",
			cache: false
		}).done(function(result) {
			var toplam = 0;
			for (var i = 0; i < result.length; i++) {
				toplam = parseInt(toplam) + parseInt(result[i].turopcount);
			}

			Highcharts.getOptions().plotOptions.pie.colors = (function () {
				var colors = [],
				base = Highcharts.getOptions().colors[0],
				i;

				for (i = 0; i < 10; i += 1) {
					// Start out with a darkened base color (negative brighten), and end
					// up with a much brighter color
					colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
				}
				return colors;
			}());
			var options = {
				chart: {
					renderTo: 'turopyuzde',
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false,
					type: 'pie'
				},
				title: {
					text: 'En İyi Havayolu Firmaları'
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							format: '<b>{point.name}</b>: {point.percentage:.1f} %',
							style: {
								color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
							}
						}
					}
				},
				series: [{
					name: 'Brands',
					colorByPoint: true,
					data: []
				}]
			};

			var table = '<table class="highchart table table-hover table-bordered" data-highchart-table=""'+ 'data-graph-container="turopyuzde" data-graph-type="column">'+
			'<caption>En İyi Havayolu Firmaları</caption>'+
			'<thead>'+
			'<tr>'+
			'<th>Aylar</th>'+
			'<th class="" style="">Yüzde</th>'+
			'<th class="">Satış</th>'+
			'</tr>'+
			'</thead>'+
			'<tbody>';
			for (var i = 0; i < result.length; i++) {
				var yuzde = parseInt(result[i].turopcount) * 100 / parseInt(toplam);
				options.series[0].data.push({name: result[i].turop_ad, y: yuzde});
				table += '<tr><td>' + result[i].turop_ad + '</td><td>' + yuzde + '</td><td>' + result[i].turopcount + '</td></tr>';
			}
			table += '</tbody>';

			$("#turopyuzdetable").html(table);

			var chart = new Highcharts.Chart(options);
		});
	}

	function gidishavaalanieniyi(month1,month2) {
		$.ajax({
			url: "index.php/reservation/gidishavaalani",
			data: {
				month1: month1,
				month2: month2
			},
			cache: false
		}).done(function(result) {
			var provider = [];
			var counts = [];
			for (var i = 0; i < result.length; i++) {
				provider.push(result[i][1]);
				counts.push(result[i][0]);
			}

			var table = '<table class="highchart table table-hover table-bordered" data-highchart-table=""'+ 'data-graph-container="biletlerlast7day" data-graph-type="column">'+
			'<caption>Gidiş Havaalanları 12 Ay</caption>'+
			'<thead>'+
			'<tr>';
			for (var b = 0; b < result.length; b++) {
				table += '<th>' + result[b][1] + '</th>';
			}
			table += '</tr>'+
			'</thead>'+
			'<tbody><tr>';
			for (var c = 0; c < result.length; c++) {
				table += '<td>' + result[c][0] + '</td>';
			}
			table += '<tr></tbody></table>';
			$("#gidishavaalanlaritable").html(table);
			var chart = new Highcharts.Chart({
				chart: {
					renderTo: 'gidishavaalanlari',
					type: 'column'
				},
				title: {
					text: "Gidiş Havaalanları 12 Ay"
				},
				xAxis: {
					categories: provider
				},
				yAxis: {
					title: {
						text: 'Value',
						margin: 100
					},
					tickInterval: 1
				},
				series: [{
					name:"Gidiş Havaalanları",
					data: counts,
					pointWidth: 15
				}]
			});
		});
	}

	function donushavaalanieniyi(month1,month2) {
		$.ajax({
			url: "index.php/reservation/donushavaalani",
			data: {
				month1: month1,
				month2: month2
			},
			cache: false
		}).done(function(result) {
			// if(result.length == 0){
			// 	result = [0,0]
			// }
			var provider = [];
			var counts = [];
			for (var i = 0; i < result.length; i++) {
				provider.push(result[i][1]);
				counts.push(result[i][0]);
			}
			if(result.length != 0){
				var table = '<table class="highchart table table-hover table-bordered" data-highchart-table=""'+ 'data-graph-container="biletlerlast7day" data-graph-type="column">'+
				'<caption>Dönüş Havaalanları 12 Ay</caption>'+
				'<thead>'+
				'<tr>';
				for (var b = 0; b < result.length; b++) {
					table += '<th>' + result[b][1] + '</th>';
				}
				table += '</tr>'+
				'</thead>'+
				'<tbody><tr>';
				for (var c = 0; c < result.length; c++) {
					table += '<td>' + result[c][0] + '</td>';
				}
				table += '</tr></tbody></table>';
			}else {
				var table = '<table class="highchart table table-hover table-bordered" data-highchart-table=""'+ 'data-graph-container="biletlerlast7day" data-graph-type="column">'+
				'<caption>Dönüş Havaalanları 12 Ay</caption>'+
				'<thead>'+
				'<tr><td>0</td><td>0</td></tr>'+
				'</thead>'+
				'<tbody><tr><td>0</td><td>0</td></tbody></table>';
			}
			$("#donushavalanlaritable").html(table);
			var chart = new Highcharts.Chart({
				chart: {
					renderTo: 'donushavalanlari',
					type: 'column',
					width: 525
				},
				title: {
					text: "Dönüş Havaalanları 12 Ay"
				},
				xAxis: {
					categories: provider
				},
				yAxis: {
					title: {
						text: 'Value',
						margin: 100
					},
					tickInterval: 1
				},
				series: [{
					name:"Dönüş Havaalanları",
					data: counts,
					pointWidth: 15
				}]
			});
		});
	}

	function guzergaheniyi(month1,month2) {
		$.ajax({
			url: "index.php/reservation/guzergaheniyi",
			data: {
				month1: month1,
				month2: month2
			},
			cache: false
		}).done(function(result) {
			var guzergah = [];
			var counts = [];
			$.each(result, function(k, v) {
				guzergah.push(k);
				counts.push(v);
			});
			var table = '<table class="highchart table table-hover table-bordered" data-highchart-table=""'+ 'data-graph-container="guzergah" data-graph-type="column">'+
			'<caption>Güzergah En İyi</caption>'+
			'<thead>'+
			'<tr>';
			$.each(guzergah, function(k, v) {
				table += '<th>' + v + '</th>';
			});
			table += '</tr>'+
			'</thead>'+
			'<tbody><tr>';
			$.each(counts, function(k, v) {
				table += '<td>' + v + '</td>';
			});
			table += '</tr></tbody></table>';
			$("#guzergahtable").html(table);
			var chart = new Highcharts.Chart({
				chart: {
					renderTo: 'guzergah',
					type: 'column',
					width: 525
				},
				title: {
					text: "Güzergah En İyi"
				},
				xAxis: {
					categories: guzergah
				},
				yAxis: {
					title: {
						text: 'Value',
						margin: 100
					},
					tickInterval: 1
				},
				series: [{
					name:"Güzergahlar",
					data: counts,
					pointWidth: 15
				}]
			});
		});
	}

	function ucusaramalari(day1, day2) {
		$.ajax({
			url: "index.php/mongoctrl/sorgu",
			data: {
				day1: day1,
				day2: day2
			},
			cache: false
		}).done(function(result) {
			$("#ucusaramalari").html("");
			var guzergah = "";
			var gidisdonus = "";
			var gidistarih = "";
			var gidissaat = "";
			var gidistarih = null;
			var gidissaat = null;
			var sorgusaat = "";
							console.log(result);
			$.each(result, function(k, v) {
				var baslik = "";
				var baslikclass = "";
				var gidistarih = "";
				var donustarih = "";
				var rota = "";

				var detay = v[2][0]["legs"]["detay"];
				var legrota = v[2][0]["legs"]["legrota"];
				var fiyat = v[2][0]["fiyat"];

				if(v[2][0]["gidisdonus"] == 0){
					baslik = "ONE";
					donustarih = "";
					baslikclass = "blueprimary";

					rota = legrota[0][0] + ' » ' + legrota[0][1];

					gidistarih = moment(detay[0][0]["kalkistarih"]).format('DD/MM/YYYY dd');
				}
				if(v[2][0]["gidisdonus"] == 1){
					baslik = "RND";
					baslikclass = "primary";

					rota = legrota[0][0] + ' » ' + legrota[0][1] + ' » ' + legrota[1][1];

					gidistarih = moment(detay[0][0]["kalkistarih"]).format('DD/MM/YYYY dd');

					donustarih = ' <span class="labela accent">D: ' + moment(detay[detay.length - 1][detay[detay.length - 1].length - 1]["varistarih"]).format('DD/MM/YYYY dd') + '</span>';
				}
				var listitem = '<li class="list-item">'+
					'<div herf="" class="list-left" style="">'+
						'<a href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Gidiş Dönüş">'+
							'<span class="w-40 circle ' + baslikclass + '">'+
								'<strong class="text">' + baslik + '</strong>'+
							'</span>'+
						'</a>'+
					'</div>'+
					'<div class="list-body" style="">'+
						'<div>'+
							rota + '<small class="text-muted pull-right"><span class="labela danger">' + moment(new Date(v[0]).getTime()).format('DD/MM/YYYY dd') + ' ' + v[3] + '</span></small>'+
						'</div>'+
						'<div>'+
							'<span class="labela info">G: ' + gidistarih + '</span>' + donustarih +
							'<strong style="float:right;">' + fiyat + '€</strong><div class="clear"></div>'+
						'</div>'+
						'<div></div>'+
					'</div>'+
					'<div class="clear"></div>'+
				'</li>';
				$("#ucusaramalari").append(listitem);
			});
		});
	}

	function rezervasyonlar(day1, day2) {
		$.ajax({
			url: "index.php/reservation/rezervasyonlar_detayli",
			data: {
				day1: day1,
				day2: day2
			},
			cache: false
		}).done(function(result) {
			$("#rezervasyonlar").html("");
			var rezid = "";
			var clas = "";
			var baslik = "";
			var donus = "";
			var yolcuadsoyad = "";
			console.log(result);
			$.each(result, function(k, v) {
				rezid = v["rez_id"];
				ucusparkur = v["ucus_parkur1"] + " - " + v["ucus_parkur2"];
				yolcuadsoyad = v["yolcu_ad"] + ' ' + v["yolcu_soyad"];
				yolcuadsoyad = yolcuadsoyad.substring(0, 24);
				if(v["rez_yon"] == "g"){
					clas = "blueprimary";
					baslik = "ONE";
					donus = "";
				}else{
					clas = "primary";
					baslik = "RND";
					donus = '<span class="labela accent">D: ' + moment(v["ucus_tarih"], "YYYY-MM-DD").format("DD/MM/YYYY dd") + '</span>';
				}
				var listitem = '<li class="list-item" style="">'+
					'<div herf="" class="list-left">'+
						'<a href="index.php/reservation/simple/' + v["rez_id"] + '" data-toggle="modal" title="" data-target="#detailView" data-original-title="Gidiş Dönüş" data-rezid="' + rezid + '">'+
							'<span class="w-40 circle ' + clas + '"><strong class="text">' + baslik + '</strong></span>'+
						'</a>'+
					'</div>'+
					'<div class="list-body">'+
						'<div class="col-md-12 no-padding dashbord-list-top-div">'+
							'<a href="#" data-toggle="tooltip" title="" data-original-title="' + ucusparkur + '" class="reservationa">' + v["ucus_parkur1"] + '</a> » <a href="#" data-toggle="tooltip" title="" data-original-title="' + ucusparkur + '" class="reservationa">' + v["ucus_parkur2"] + '</a>'+
							'<div class="pull-right">'+
								'<small class="text-muted"><span class="labela dark">' + moment(v["rez_kayittarih"], "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY dd HH:mm:ss") + '</span></small>'+
							'</div>'+
						'</div>'+
						'<div class="col-md-12 no-padding">' + yolcuadsoyad +
							'<a href="#" class="dashbord-list-a pull-right" data-toggle="tooltip" title="" data-original-title="Rezervasyon Detayı">'+
								'<a href="index.php/reservation/simple/' + v["rez_id"] + '" data-toggle="modal" data-target="#detailView"><label class="pull-right labela success pointer">PNR: ' + v["rez_pnr"] + '</label></a>'+
							'</a>'+
						'</div>'+
						'<div class="col-md-12 no-padding dashbord-list-bottom-div">'+
							'<span class="labela info" style="margin-right:3px">G: ' + moment(v["ucus_tarih"], "YYYY-MM-DD").format("DD/MM/YYYY dd") + '</span>' + donus + '<strong style="float:right;">' + v["fiyat"] + '€</strong><div class="clear"></div>'
						'</div>'+
					'</div>'+
					'<div class="clear"></div>'+
				'</li>';
				$("#rezervasyonlar").append(listitem);
			});
		});
	}

	function biletler(day1, day2) {
		$.ajax({
			url: "index.php/reservation/biletler_detayli",
			data: {
				day1: day1,
				day2: day2
			},
			cache: false
		}).done(function(result) {
			$("#biletler").html("");
			var rezid = "";
			var clas = "";
			var baslik = "";
			var donus = "";
			var yolcuadsoyad = "";
			console.log(result);
			$.each(result, function(k, v) {
				rezid = v["rez_id"];
				ucusparkur = v["ucus_parkur1"] + " - " + v["ucus_parkur2"];
				yolcuadsoyad = v["yolcu_ad"] + ' ' + v["yolcu_soyad"];
				yolcuadsoyad = yolcuadsoyad.substring(0, 24);
				if(v["rez_yon"] == "g"){
					clas = "blueprimary";
					baslik = "ONE";
					donus = "";
				}else{
					clas = "primary";
					baslik = "RND";
					donus = '<span class="labela accent">D: ' + moment(v["ucus_tarih"], "YYYY-MM-DD").format("DD/MM/YYYY dd") + '</span>';
				}
				var listitem = '<li class="list-item" style="">'+
					'<div herf="" class="list-left">'+
						'<a href="index.php/reservation/simple/' + v["rez_id"] + '" data-toggle="modal" title="" data-target="#detailView" data-original-title="Gidiş Dönüş" data-rezid="' + rezid + '">'+
							'<span class="w-40 circle ' + clas + '"><strong class="text">' + baslik + '</strong></span>'+
						'</a>'+
					'</div>'+
					'<div class="list-body">'+
						'<div class="col-md-12 no-padding dashbord-list-top-div">'+
							'<a href="#" data-toggle="tooltip" title="" data-original-title="' + ucusparkur + '" class="reservationa">' + v["ucus_parkur1"] + '</a> » <a href="#" data-toggle="tooltip" title="" data-original-title="' + ucusparkur + '" class="reservationa">' + v["ucus_parkur2"] + '</a>'+
							'<div class="pull-right">'+
								'<small class="text-muted"><span class="labela dark">' + moment(v["rez_kayittarih"], "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY dd HH:mm:ss") + '</span></small>'+
							'</div>'+
						'</div>'+
						'<div class="col-md-12 no-padding">' + yolcuadsoyad +
							'<a href="#" class="dashbord-list-a pull-right" data-toggle="tooltip" title="" data-original-title="Rezervasyon Detayı">'+
								'<a href="index.php/reservation/simple/' + v["rez_id"] + '" data-toggle="modal" data-target="#detailView"><label class="pull-right labela success pointer">PNR: ' + v["rez_pnr"] + '</label></a>'+
							'</a>'+
						'</div>'+
						'<div class="col-md-12 no-padding dashbord-list-bottom-div">'+
							'<span class="labela info" style="margin-right:3px">G: ' + moment(v["ucus_tarih"], "YYYY-MM-DD").format("DD/MM/YYYY dd") + '</span>' + donus + '<strong style="float:right;">' + v["fiyat"] + '€</strong><div class="clear"></div>'
						'</div>'+
					'</div>'+
					'<div class="clear"></div>'+
				'</li>';
				$("#biletler").append(listitem);
			});
		});
	}


	// rezervasyonlast7days("2016-10-08", "2016-10-17");
	rezervasyonlast7days(moment().subtract(6,'d').format('YYYY-MM-DD'), moment().format('YYYY-MM-DD'));
	biletlast7days(moment().subtract(6,'d').format('YYYY-MM-DD'), moment().format('YYYY-MM-DD'));
	ucusaramalaricount7days(moment().subtract(6,'d').format('YYYY-MM-DD'), moment().format('YYYY-MM-DD'));
	rezervasyonlast12month(moment().subtract(12,'months').format('YYYY-MM'), moment().format('YYYY-MM'));
	gidishavaalanieniyi(moment().subtract(12,'months').format('YYYY-MM'), moment().format('YYYY-MM'));
	donushavaalanieniyi(moment().subtract(12,'months').format('YYYY-MM'), moment().format('YYYY-MM'));
	guzergaheniyi(moment().subtract(12,'months').format('YYYY-MM'), moment().format('YYYY-MM'));
	rezervasyonturopyuzde();
	ucusaramalari(moment().subtract(1,'d').format('YYYY-MM-DD'), moment().format('YYYY-MM-DD'));
	rezervasyonlar(moment().subtract(30,'d').format('YYYY-MM-DD'), moment().format('YYYY-MM-DD'));
	biletler(moment().subtract(30,'d').format('YYYY-MM-DD'), moment().format('YYYY-MM-DD'));

	$("#sparktutar").sparkline([<?php echo implode(",", $renevues_last15_days); ?>], {
		type: 'bar',
		height: '26px',
		barWidth: 5,
		barColor: '#57889c'
	});
	$("#sparktraf").sparkline([<?php echo implode(",", $traffic_last15_days); ?>], {
		type: 'bar',
		height: '26px',
		barWidth: 5,
		barColor: '#6e587a'
	});
	$("#sparkyolcu").sparkline([<?php echo implode(",", $passenger_last15_days); ?>], {
		type: 'bar',
		height: '26px',
		barWidth: 5,
		barColor: '#496949'
	});
};
loadScript("js/plugin/sparkline/jquery.sparkline.min.js", function () {
	loadScript("https://code.highcharts.com/highcharts.js", function () {
		loadScript("https://code.highcharts.com/modules/data.js", function () {
			loadScript("js/plugin/slimscroll/jquery.slimscroll.min.js", function () {
				loadScript("https://code.highcharts.com/modules/exporting.js", anafonk());
			})
		});
	});
});

</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/pagescripts/statistic.search.app.js"></script>
