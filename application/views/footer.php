

<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white">SmartAdmin 1.9 <span class="hidden-xs"></span> © 2017-2019</span>
        </div>

        <div class="col-xs-6 col-sm-6 text-right hidden-xs">
            <div class="txt-color-white inline-block">
<!--                <i class="txt-color-blueLight hidden-mobile">Last account activity <i class="fa fa-clock-o"></i> <strong>52 mins ago &nbsp;</strong> </i>-->
                <div class="btn-group dropup">
                  <!--  <button class="btn btn-xs dropdown-toggle bg-color-blue txt-color-white" data-toggle="dropdown">
                        <i class="fa fa-link"></i> <span class="caret"></span>
                    </button>-->
                    <ul class="dropdown-menu pull-right text-left">
                        <li>
                            <div class="padding-5">
                                <p class="txt-color-darken font-sm no-margin">Download Progress</p>
                                <div class="progress progress-micro no-margin">
                                    <div class="progress-bar progress-bar-success" style="width: 50%;"></div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="padding-5">
                                <p class="txt-color-darken font-sm no-margin">Server Load</p>
                                <div class="progress progress-micro no-margin">
                                    <div class="progress-bar progress-bar-success" style="width: 20%;"></div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="padding-5">
                                <p class="txt-color-darken font-sm no-margin">Memory Load <span class="text-danger">*critical*</span></p>
                                <div class="progress progress-micro no-margin">
                                    <div class="progress-bar progress-bar-danger" style="width: 70%;"></div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="padding-5">
                                <button class="btn btn-block btn-default">refresh</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->

<!-- These scripts will be located in Header So we can add scripts inside body (used in class.datatables.php) -->
<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->


<!--<script data-pace-options='{ "restartOnRequestAfter": true }' src="--><?php //echo base_url(); ?><!--js/plugin/pace/pace.min.js"></script>-->


<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script type="text/javascript" src="/js/libs/jquery-ui.min.js"></script>

<script src="<?php echo base_url(); ?>js/app.ori.js"></script>


<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>js/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo base_url(); ?>/js/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="<?php echo base_url(); ?>js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="<?php echo base_url(); ?>js/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>js/plugin/fastclick/fastclick.min.js"></script>

<!--[if IE 8]>
<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
<![endif]-->

<!-- Demo purpose only -->
<!--<script src="--><?php //echo base_url(); ?><!--js/demo.min.js"></script>-->

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>js/speech/voicecommand.min.js"></script>

<!-- SmartChat UI : plugin -->
<!--<script src="--><?php //echo base_url(); ?><!--js/smart-chat-ui/smart.chat.ui.min.js"></script>-->
<!--<script src="--><?php //echo base_url(); ?><!--js/smart-chat-ui/smart.chat.manager.min.js"></script>-->

<script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    pageSetUp();
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#logouta").attr("href", "<?php echo base_url(); ?>index.php/auth/logout" + document.location.hash);
        $("#dialog").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            position: ["right",50,"button"],
            title: "Arama Sonuçları",
            closeOnEscape: true,
            close: function(event, ui){
                $(this).empty();
            }
        });
        $('#search-fld').keypress(function (e) {
            if (e.which == 13) {
                var veri = $(this).val();
                $.ajax({
                    url: "index.php/dashboard/aramakisayol",
                    method: "POST",
                    data: {
                        kriter: veri,
                        sayfano: 0,
                        tip: "pnr"
                    }
                }).done(function (result) {
                    $("#dialog").dialog({
                        autoOpen: false,
                        height: 400,
                        width: 500,
                        position: ["right",50,"button"],
                        title: "Arama Sonuçları",
                        closeOnEscape: true,
                        close: function(event, ui){
                            $(this).empty();
                        }
                    }).dialog("open").html(result);
                });
            }
        });
        $("#searchheader").click(function (e) {
            var veri = $(this).val();
            $.ajax({
                url: "index.php/dashboard/aramakisayol",
                method: "POST",
                data: {
                    kriter: veri,
                    sayfano: 0,
                    tip: "pnr"
                }
            }).done(function (result) {
                $("#dialog").dialog({
                    autoOpen: false,
                    height: 400,
                    width: 500,
                    position: ["right",50,"button"],
                    title: "Arama Sonuçları",
                    closeOnEscape: true,
                    close: function(event, ui){
                        $(this).empty();
                    }
                }).dialog("open").html(result);
            });
        });
    })
</script>




<script src="//ui-grid.info/docs/grunt-scripts/csv.js"></script>
<script src="//ui-grid.info/docs/grunt-scripts/pdfmake.js"></script>
<script src="//ui-grid.info/docs/grunt-scripts/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/plugin/ui-grid/ui-grid.min.js"></script>

</body>
</html>