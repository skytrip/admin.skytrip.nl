<link rel="stylesheet" href="js/plugin/jquery-custom-scrollbar/jquery.custom-scrollbar.css" type="text/css" />
<div id="appWebservice">
     <div class="row" style="width:100%">
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
               <h1 class="page-title txt-color-blueDark">
                    <i class="fa-fw fa fa-home"></i> Dashboard <span>> Webservice Logs</span>
               </h1>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
          <style media="screen">
          #sparks li{
               max-width: 220px;
               max-height: 32px;
          }
          #calendar td div{
               min-height: 38px !important;
          }
          </style>
          <ul id="sparks" class="">
               <li class="sparks-info">
                    <h5> Tutarlar <span class="txt-color-blue">&euro; <?php echo $renevues_last30_days_sum; ?></span></h5>
                    <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                         <?php echo implode(",", $renevues_last15_days); ?>
                    </div>
               </li>
               <li class="sparks-info">
                    <h5> Site Traffic <span class="txt-color-purple"><i class="fa fa-arrow-circle-up"></i>&nbsp;<?php echo $traffic_last30days_sum; ?></span></h5>
                    <div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">
                         <?php echo implode(",", $traffic_last15_days); ?>
                    </div>
               </li>
               <li class="sparks-info">
                    <h5> Yolcular <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;<?php echo $passenger_last30days_sum; ?></span></h5>
                    <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
                         <?php echo implode(",", $passenger_last15_days); ?>
                    </div>
               </li>
          </ul>
     </div>
     <!--
     The ID "widget-grid" will start to initialize all widgets below
     You do not need to use widgets if you dont want to. Simply remove
     the <section></section> and you can use wells or panels instead
-->
<!-- widget grid -->
<section widget-grid id="widget-grid">
     <!-- row -->
     <div class="row">
          <!-- NEW WIDGET START -->
          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <!-- Widget ID (each widget will need unique ID)-->
               <div class="" data-jarvis-widget style="margin-left:20px">
                    <!-- widget options:
                    usage: <div data-jarvis-widget id="wid-id-0" data-widget-editbutton="false">
                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"
               -->
               <!-- <header></header> -->
               <!-- widget div-->
               <div>
                    <!-- widget content -->
                    <div class="widget-body no-padding">
                         <table datatable-basic data-table-options="tableOptions" bind-filters="true"
                         class="display projects-table table table-striped table-bordered table-hover"
                         cellspacing="0" width="100%" id="tablewebservice">
                         <thead>
                              <tr>
                                   <th></th>
                                   <th></th>
                                   <th class="hasinput" style="width:10%">
                                        <input type="text" class="form-control" placeholder="Trnx ID" id="trnxid" data-type="trnxid" data-index="2" />
                                   </th>
                                   <th class="hasinput" style="min-width:180px">
                                        <section class="select">
                                             <select class="input-sm" data-type="servicename" data-index="3" id="serviceselect">
                                                  <option style="color:#cccccc!important">Service Name</option>
                                                  <option value="low_fare_search">low_fare_search</option>
                                                  <option value="ONUR-FareDisplay">ONUR-FareDisplay</option>
                                                  <option value="SUNEX-FlightSearch">SUNEX-FlightSearch</option>
                                                  <option value="KOMFL-GetAvailableFares">KOMFL-GetAvailableFares</option>
                                                  <option value="CRNDN-GetAvailableFares">CRNDN-GetAvailableFares</option>
                                             </select>
                                        </section>
                                        <!-- <input type="text" class="form-control" placeholder="Service Name" data-type="servicename" data-index="3" /> -->
                                   </th>
                                   <th class="hasinput">
                                        <section class="select">
                                             <select class="input-sm" data-type="error" data-index="4" id="errorselect">
                                                  <option style="color:#cccccc!important">Error</option>
                                                  <option value="00000">00000</option>
                                                  <option value="1005">1005</option>
                                                  <option value="3037">3037</option>
                                                  <option value="4413">4413</option>
                                                  <option value="700003">700003</option>
                                             </select>
                                        </section>
                                        <!-- <input type="text" class="form-control" placeholder="Error" data-type="error" data-index="4" /> -->
                                   </th>
                                   <th class="hasinput" style="min-width:100px">
                                        <input type="text" class="form-control" placeholder="Trnx Time" id="trnxtime" data-type="trnxtime" data-index="5" id="datepicker"/>
                                   </th>
                                   <th class="hasinput" style="width:12%">
                                        <input type="text" class="form-control" placeholder="Request File" id="requestfile" data-type="requestfile" data-index="6" />
                                   </th>
                                   <th class="hasinput" style="width:10%">
                                        <input type="text" class="form-control" placeholder="Response File" id="responsefile" data-type="responsefile" data-index="7" />
                                   </th>
                                   <th><a href="http://adminpanel.skytrip.nl/#index.php/webservicelog" id="temizle" class="btn btn-danger">Temizle</a></th>
                              </tr>
                              <tr>
                                   <th></th>
                                   <th></th>
                                   <th data-class="expand">
                                        TrnxID
                                   </th>
                                   <th data-class="expand">
                                        Service Name
                                   </th>
                                   <th data-class="expand">
                                        Error
                                   </th>
                                   <th data-class="expand">
                                        Trnx Time
                                   </th>
                                   <th data-class="expand">
                                        Request File
                                   </th>
                                   <th data-class="expand">
                                        Response File
                                   </th>
                                   <th data-class="expand">
                                        Child Count
                                   </th>
                              </tr>
                         </thead>
                    </table>
                    <div id="showbilgi">

                    </div>
               </div>
               <!-- end widget content -->
          </div>
          <!-- end widget div -->
     </div>
     <!-- end widget -->
</article>
<!-- WIDGET END -->
</div>
<!-- end row -->
</section>
<!-- end widget grid -->
<!-- end row -->
</section>
</div>
<style media="screen">
.childtable{
     width: 100%;
}
.childtable tr td {
     min-width: 36px;
}
</style>
<script type="text/javascript" src="js/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript">
/* DO NOT REMOVE : GLOBAL FUNCTIONS!
*
* pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
*
* // activate tooltips
* $("[rel=tooltip]").tooltip();
*
* // activate popovers
* $("[rel=popover]").popover();
*
* // activate popovers with hover states
* $("[rel=popover-hover]").popover({ trigger: "hover" });
*
* // activate inline charts
* runAllCharts();
*
* // setup widgets
* setup_widgets_desktop();
*
* // run form elements
* runAllForms();
*
********************************
*
* pageSetUp() is needed whenever you load a page.
* It initializes and checks for all basic elements of the page
* and makes rendering easier.
*
*/
pageSetUp();

/*
* ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
* eg alert("my home function");
*
* var pagefunction = function() {
*   ...
* }
* loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
*
*/

// PAGE RELATED SCRIPTS

// pagefunction
var otable = undefined;
var referance_dt_filter = {};
var responsiveHelper_dt_basic = undefined;
var responsiveHelper_datatable_fixed_column = undefined;
var responsiveHelper_datatable_col_reorder = undefined;
var responsiveHelper_datatable_tabletools = undefined;

var breakpointDefinition = {
     tablet : 1024,
     phone : 480
};

var pagefunction = function() {

     /* // DOM Position key index //

     l - Length changing (dropdown)
     f - Filtering input (search)
     t - The Table! (datatable)
     i - Information (records)
     p - Pagination (paging)
     r - pRocessing
     < and > - div elements
     <"#id" and > - div with an id
     <"class" and > - div with a class
     <"#id.class" and > - div with an id and class

     Also see: http://legacy.datatables.net/usage/features
     */
     <?php echo $this->activemenu; ?>
     function format ( d ) {
          var data = '<table class="childtable"><thead><tr><th></th><th></th><th>TrnxID</th><th>Service Name</th><th>Error</th><th>Trnx Time</th><th>Request File</th><th>Response File</th></tr></thead><tbody>';
          for(i in d["detaylar"]){
               data += '<tr>'+
               '<td> </td>'+
               '<td> </td>'+
               '<td>' + d["trnxid"] + '</td>'+
               '<td>'+d["detaylar"][i].servicename+'</td>'+
               '<td>'+d["detaylar"][i].error+'</td>'+
               '<td>'+moment(d["detaylar"][i].trnx_year+'-'+d["detaylar"][i].trnx_month+'-'+d["detaylar"][i].trnx_day, "YYYY-MM-DD").format("DD/MM/YYYY dd")+'</td>'+
               '<td>'+d["detaylar"][i].requestfile+'</td>'+
               '<td>'+d["detaylar"][i].responsefile+'</td>'+
               '</tr>';
          }
          data += '</tbody></table>';
          return data;
     }
     /* COLUMN FILTER  */
     otable = $('#tablewebservice').DataTable({
          serverSide: true,
          bFilter: false,
          scrollX: true,
          responsive: true,
          // keys: true,
          // iDisplayStart : 0,
          ajax: function (data, callback, settings) {
               <?php
               if($_GET){
                    if(isset($_GET["tarih"])){
                         ?>
                         referance_dt_filter.tarih = '<?php echo $_GET["tarih"];?>';
                         <?php
                    }
               } ?>
               referance_dt_filter.start = data.start;
               referance_dt_filter.length = data.length;
               referance_dt_filter.draw = data.draw;
               referance_dt_filter.order = data.columns[data.order[0].column].data + " " + data.order[0].dir;
               $.ajax({
                    url: 'index.php/webservicelog/get_pagdata',
                    dataType: 'JSON',
                    method: "POST",
                    data: referance_dt_filter
               }).done(callback);
          },
          oLanguage:{
               sSearch: "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> ",
               sLengthMenu: "_MENU_"
          },
          iDisplayLength: 30,
          columns: [
               {data: "rownum", "orderable": false, "sortable": false},
               {data: "plus", className: 'details-control', orderable: false, defaultContent: '', render: function (data, type, full, meta){
                    return "";
               }},
               {data: "trnxid", searchable: true},
               {data: "servicename", searchable: true, render: function (data, type, full, meta){
                    return full.detaylar[0].servicename;
               }},
               {data: "error", searchable: true, render: function (data, type, full, meta){
                    return full.detaylar[0].error;
               }},
               {data: "trnxtime", searchable: true, render: function (data, type, full, meta){
                    return moment(full.detaylar[0].trnx_year+'-'+full.detaylar[0].trnx_month+'-'+full.detaylar[0].trnx_day, "YYYY-MM-DD").format("DD/MM/YYYY dd");
               }},//trnxtime
               {data: "requestfile", searchable: true, render: function (data, type, full, meta){
                    return '<a target="_blank" href="http://www.skytrip.nl/webservicelogs/' + full.detaylar[0].requestfile + '">' + full.detaylar[0].requestfile + '</a>';
               }},
               {data: "responsefile", searchable: true, render: function (data, type, full, meta){
                    return '<a target="_blank" href="http://www.skytrip.nl/webservicelogs/' + full.detaylar[0].responsefile + '">' + full.detaylar[0].responsefile + '</a>';
               }},
               {data: "detaycount", "orderable": false, "sortable": false}
          ],
          order: [[5, 'desc']],
          //"bFilter": false,
          //"bInfo": false,
          //"bLengthChange": false
          //"bAutoWidth": false,
          //"bPaginate": false,
          //"bStateSave": true // saves sort state using localStorage
          sDom: ""+//<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>
          "t"+
          "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
          autoWidth : true,
          preDrawCallback : function() {
               // Initialize the responsive datatables helper once.
               // if (!responsiveHelper_datatable_fixed_column) {
               responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#tablewebservice'), breakpointDefinition);
               // }
          },
          rowCallback : function(nRow) {
               responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
          },
          drawCallback : function(oSettings) {
               responsiveHelper_datatable_fixed_column.respond();
          },
          fnDrawCallback: function( oSettings ) {
               $('[data-toggle="popover"]').popover();
          },
          footerCallback: function ( row, data, start, end, display ) {
               var api = this.api(), data;

               // Remove the formatting to get integer data for summation
               var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
               };

               // Total over all pages
               total = api
               .column( 8 )
               .data()
               .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
               }, 0 );

               // Total over this page
               pageTotal = api
               .column( 8, { page: 'current'} )
               .data()
               .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
               }, 0 );

               // Update footer
               $( '#showbilgi' ).html(
                    "Sayfa child toplamı: " + pageTotal
               );
          }
     });
     $('#tablewebservice tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = otable.row( tr );

          if ( row.child.isShown() ) {
               // This row is already open - close it
               row.child.hide();
               tr.removeClass('shown');
          }
          else {
               // Open this row
               row.child( format(row.data()) ).show();
               tr.addClass('shown');
          }
     } );

     // custom toolbar
     // $("div.toolbar").html('<?php if($_GET){if(isset($_GET["tarih"])){?><div class="text-right"><a href="http://adminpanel.skytrip.nl/#index.php/webservicelog" class="btn btn-danger">Temizle</a></div><?php }}else{ ?><div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div><?php }?>');
     $( otable.table().container() ).on( 'keyup', 'thead input', function () {
          var type = $(this).data('type');
          var gonderilen = {};
          if(type == "trnxid"){
               gonderilen = {
                    trnxid: this.value,
               }
          }
          if(type == "servicename"){
               gonderilen = {
                    servicename: this.value,
               }
          }
          if(type == "error"){
               gonderilen = {
                    error: this.value,
               }
          }
          if(type == "trnxtime"){
               gonderilen = {
                    trnxtime: this.value,
               }
          }
          if(type == "requestfile"){
               gonderilen = {
                    requestfile: this.value,
               }
          }
          if(type == "responsefile"){
               gonderilen = {
                    responsefile: this.value,
               }
          }
          referance_dt_filter = gonderilen;
          otable.draw();
     });
     $( otable.table().container() ).on( 'change', 'thead select', function () {
          var type = $(this).data('type');
          var val = $.fn.dataTable.util.escapeRegex(
               $(this).val()
          );
          var gonderilen = {};
          if(type == "servicename"){
               gonderilen = {
                    servicename: this.value,
               }
               $("#errorselect").val($("#errorselect option:first").val());
          }
          if(type == "error"){
               gonderilen = {
                    error: this.value,
               }
               $("#serviceselect").val($("#serviceselect option:first").val());
          }
          referance_dt_filter = gonderilen;
          otable.draw();
     });
     $( "#datepicker" ).datepicker({
          dateFormat: "dd/mm/yy",
          onSelect: function(dateText, inst) {
               var gonderilen = {
                    trnxtime: dateText,
               }
               referance_dt_filter = gonderilen;
               otable.draw();
          }
     });
     $("#temizle").on("click", function () {
          $("#errorselect").val($("#errorselect option:first").val());
          $("#serviceselect").val($("#serviceselect option:first").val());
          $("#trnxid").val("");
          $("#servicename").val("");
          $("#error").val("");
          $("#trnxtime").val("");
          $("#requestfile").val("");
          $("#responsefile").val("");
          gonderilen = {
               trnxid: "",
               servicename: "",
               error: "",
               trnxtime: "",
               requestfile: "",
               responsefile: "",
          }
          referance_dt_filter = gonderilen;
          otable.draw();
     })
     /* END COLUMN FILTER */
};

// load related plugins

loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
     loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
          loadScript("js/plugin/datatables/dataTables.tableTools.min.js", function(){
               loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
                    loadScript("js/plugin/moment/moment-with-locales.js", function() {
                         loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction);
                    });
               });
          });
     });
});
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/pagescripts/webservice.search.app.js"></script>
