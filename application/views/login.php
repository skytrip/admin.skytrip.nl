<header id="header">
    <div id="logo-group">
        <span id="logo"> <img src="/img/logo.png" alt="Skytrip"> </span>
    </div>
</header>
<div id="content" class="container">
    <div class="row">
        <div class="col-xs-4 col-xs-offset-4">
            <div class="well no-padding">
                <form id="login-form" class="smart-form client-form" method="post">
                    <header>
                        Sign In
                    </header>
                    <fieldset>
                        <section>
                            <label class="label">Username</label>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="username">
                                <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter username</b></label>
                        </section>
                        <section>
                            <label class="label">Password</label>
                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <input type="password" name="password">
                                <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
                        </section>
                        <section>
                            <label class="checkbox">
                                <input type="checkbox" name="remember" checked="">
                                <i></i>Stay signed in</label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Sign in
                        </button>
                    </footer>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/libs/jquery-2.1.1.min.js"></script>
