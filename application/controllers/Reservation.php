<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation extends CI_Controller
{
    public $activemenu;

    public function __construct()
    {
        parent::__construct();
        include_once APPPATH."vendor/tecnickcom/tcpdf/tcpdf.php";
        $this->load->helper("jsglobal");
        $this->activemenu = activemenu("Reservation");
    }

    public function index()
    {
        $this->load->model("reservation_model");
        $model = array(
            "turop_list" => $this->reservation_model->turop_list(),
            "extraucret_list" => $this->reservation_model->extraucret_list(),
            "acente_list" => $this->reservation_model->acente_list(),
            "yonetici_list" => $this->reservation_model->yonetici_list(),
            "webapp_list" => $this->reservation_model->webapp_list(),
            "yil" => $this->reservation_model->rezervasyon_yil_list(),
            "mail" => $this->reservation_model->musteri_email()
        );
        $this->load->view("reservation/index", $model);
    }

    public function simple($id)
    {
        $this->load->model("reservation_model");
        $this->load->helper("dateext");
        $model = array(
            "rezervasyon_data" => $this->reservation_model->rezervasyon_simple($id),
            "ucus_list" => $this->reservation_model->rezervasyon_simple_ucus($id),
            "yolcu_list" => $this->reservation_model->rezervasyon_simple_yolcu($id),
            "ek_list" => $this->reservation_model->rezervasyon_simple_ek($id),
            "gelirgider_data" => $this->reservation_model->rezervasyon_simple_gelirgider($id),
        );
        $totals = array(
            "koltukfiyat" => 0,
            "vergi" => 0,
            "satis" => 0,
            "kar" => 0
        );
        foreach ($model["yolcu_list"] as $yolcu) {
            $totals["koltukfiyat"] += $yolcu->yolcu_koltukfiyat;
            $totals["vergi"] += $yolcu->yolcu_vergi;
            $totals["satis"] += $yolcu->yolcu_satisfiyat;
            $totals["kar"] += ($yolcu->yolcu_satisfiyat - $yolcu->yolcu_koltukfiyat - $yolcu->yolcu_vergi);
        }
        $model["fiyatlar"] = $totals;
        $this->load->view("reservation/simple", $model);
    }

    public function detail($id,$reload = null)
    {
        $this->load->helper("stringop");
        $this->load->helper("dateext");
        $this->load->model("reservation_model");
        $model = array(
            "reservation_detail" => $this->reservation_model->rezervasyon_detail($id),
            "reservation_contact" => $this->reservation_model->rezervasyon_detail_contact($id),
            "yolcu_list" => $this->reservation_model->rezervasyon_detail_yolcu($id),
            "ucus_list" => $this->reservation_model->rezervasyon_detail_ucus($id),
            "gelirgider_data" => $this->reservation_model->rezervasyon_detail_gelirgider($id)
        );
        $totals = array(
            "koltukfiyat" => 0,
            "vergi" => 0,
            "satis" => 0,
            "kar" => 0
        );
        foreach ($model["yolcu_list"] as $yolcu) {
            $totals["koltukfiyat"] += $yolcu->yolcu_koltukfiyat;
            $totals["vergi"] += $yolcu->yolcu_vergi;
            $totals["satis"] += $yolcu->yolcu_satisfiyat;
            $totals["kar"] += ($yolcu->yolcu_satisfiyat - $yolcu->yolcu_koltukfiyat - $yolcu->yolcu_vergi);
        }
        $model["fiyatlar"] = $totals;
        $yolculist = $this->reservation_model->rezervasyon_simple_yolcutotal($model["reservation_detail"]->acente_id);
        $gelirgider = $this->reservation_model->rezervasyon_toplam_gelirgider($model["reservation_detail"]->acente_id);
        $toplam = 0;
        foreach ($yolculist as $a) {
            $toplam += $a->yolcu_satisfiyat;
        }
        foreach ($gelirgider as $b) {
            if($b->gelirgider_grup == "g"){
                $toplam = $toplam - $b->gelirgider_tutar;
            }elseif ($b->gelirgider_grup == "c") {
                $toplam = $toplam + $b->gelirgider_tutar;
            }
        }
        $model["toplam"] = $toplam;
        $this->load->view("reservation/detail", $model);
    }

    public function api_list()
    {
        $this->load->model("reservation_model");
        $this->load->helper("dateext");

        $filter = array();
        $likes = array();
        $filter_datas = $this->input->post(null, true);

        if (isset($filter_datas["rezid"]) && $filter_datas["rezid"] != "") {
            $filter["rez_id"] = $filter_datas["rezid"];
        }
        if (isset($filter_datas["rezno"]) && $filter_datas["rezno"] != "") {
            $filter["rez_pnr"] = $filter_datas["rezno"];
        }
        if (isset($filter_datas["pnr"]) && $filter_datas["pnr"] != "") {
            $filter["rez_turoperatorpnr"] = $filter_datas["pnr"];
        }
        if (isset($filter_datas["kayitno_pre"]) && $filter_datas["kayitno_pre"] != "") {
            $filter["date_format(rez_kayittarih, '%Y') = "] = $filter_datas["kayitno_pre"];
        }
        if (isset($filter_datas["kayitno_post"]) && $filter_datas["kayitno_post"] != "") {
            $filter["rez_kayitno"] = $filter_datas["kayitno_post"];
        }
        if (isset($filter_datas["musteriid"]) && $filter_datas["musteriid"] != "") {
            $filter["rez_musteri"] = $filter_datas["musteriid"];
        }
        if (isset($filter_datas["kayittar_start"]) && $filter_datas["kayittar_start"] != "") {
            $filter["rez_kayittarih >="] = dmywsl_to_sqldate($filter_datas["kayittar_start"]);
        }
        if (isset($filter_datas["kayittar_end"]) && $filter_datas["kayittar_end"] != "") {
            $filter["rez_kayittarih <="] = dmywsl_to_sqldate($filter_datas["kayittar_end"]);
        }
        if (isset($filter_datas["kayittar"]) && $filter_datas["kayittar"] != "") {
            $filter["date_format(rez_kayittarih, '%Y-%m-%d') = "] = dmywsl_to_sqldate($filter_datas["kayittar"]);
        }
        if (isset($filter_datas["ucusno_pre"]) && $filter_datas["ucusno_pre"] != "") {
            $filter["ucus_kod"] = $filter_datas["ucusno_pre"];
        }
        if (isset($filter_datas["ucusno_post"]) && $filter_datas["ucusno_post"] != "") {
            $filter["ucus_no"] = $filter_datas["ucusno_post"];
        }
        if (isset($filter_datas["ucustar_start"]) && $filter_datas["ucustar_start"] != "") {
            $filter["ucus_tarih >= "] = dmywsl_to_sqldate($filter_datas["ucustar_start"]);
        }
        if (isset($filter_datas["ucustar_end"]) && $filter_datas["ucustar_end"] != "") {
            $filter["ucus_tarih <="] = dmywsl_to_sqldate($filter_datas["ucustar_end"]);
        }
        if (isset($filter_datas["turop"]) && $filter_datas["turop"] != "") {
            $filter["rez_turoperator"] = $filter_datas["turop"];
        }
        if (isset($filter_datas["mailbilgisi"]) && $filter_datas["mailbilgisi"] != "") {
            if($filter_datas["mailbilgisi"] == "var") {
                $filter["CHAR_LENGTH(musteri_email1) > "] = "1";
            }
            if($filter_datas["mailbilgisi"] == "yok") {
                $filter["CHAR_LENGTH(musteri_email1) = "] = "4";
            }
            if($filter_datas["mailbilgisi"] != "var" && $filter_datas["mailbilgisi"] != "yok") {
                $filter["musteri_email1"] = $filter_datas["mailbilgisi"];
            }
        }
        if (isset($filter_datas["degisen"]) && $filter_datas["degisen"] != "") {
            $filter["rez_duzenseviye > "] = "0";
        }
        if (isset($filter_datas["sifirucret"]) && $filter_datas["sifirucret"] == "true") {
            $filter["yolcu_satisfiyat = "] = "0";
        }
        if (isset($filter_datas["ekstraucr"]) && $filter_datas["ekstraucr"] != "") {
            $filter["rezek_tanimid"] = $filter_datas["ekstraucr"];
        }
        if (isset($filter_datas["kalan"]) && $filter_datas["kalan"] != "") {
            if ($filter_datas["kalan"] == "b") {
                $filter["(y.yolcu_satisfiyat - IF((g.gelirgider_grup = 'g'), g.gelirgider_tutar, 0) + IF((g.gelirgider_grup = 'c'), g.gelirgider_tutar, 0)) >"] = "0";
            } elseif ($filter_datas["kalan"] == "s") {
                $filter["(y.yolcu_satisfiyat - IF((g.gelirgider_grup = 'g'), g.gelirgider_tutar, 0) + IF((g.gelirgider_grup = 'c'), g.gelirgider_tutar, 0)) <"] = "0";
            } elseif ($filter_datas["kalan"] == "e") {
                $filter["(y.yolcu_satisfiyat - IF((g.gelirgider_grup = 'g'), g.gelirgider_tutar, 0) + IF((g.gelirgider_grup = 'c'), g.gelirgider_tutar, 0)) ="] = "0";
            } elseif ($filter_datas["kalan"] == "n") {
                $filter["(y.yolcu_satisfiyat - IF((g.gelirgider_grup = 'g'), g.gelirgider_tutar, 0) + IF((g.gelirgider_grup = 'c'), g.gelirgider_tutar, 0)) <>"] = "0";
            }
        }
        if (isset($filter_datas["iade"]) && $filter_datas["iade"] != "") {
            $filter["rez_durum"] = "CA";
            $filter["(y.yolcu_satisfiyat - IF((g.gelirgider_grup = 'g'), g.gelirgider_tutar, 0) + IF((g.gelirgider_grup = 'c'), g.gelirgider_tutar, 0)) >"] = "0";
        }
        if (isset($filter_datas["acente"]) && $filter_datas["acente"] != "") {
            $filter["rez_acente"] = $filter_datas["acente"];
        }
        if (isset($filter_datas["aciklama"]) && $filter_datas["aciklama"] != "") {
            $likes["rez_aciklama2"] = $filter_datas["aciklama"];
        }
        if (isset($filter_datas["kaydeden"]) && $filter_datas["kaydeden"] != "") {
            switch ($filter_datas["kaydeden"]) {
                case 'b':
                    $filter["rez_kayitseviye"] = "1";
                    break;
                case 'a':
                    $filter["rez_kayitseviye"] = "2";
                    break;
                case 'y':
                    $filter["rez_kayitseviye"] = "4";
                default:
                    $filter["rez_kaydeden"] = $filter_datas["kaydeden"];
                    break;
            }
        }
        if (isset($filter_datas["durum"]) && $filter_datas["durum"] != "") {
            $likes["rez_durum"] = $filter_datas["durum"];
        }
        if (isset($filter_datas["yon"]) && $filter_datas["yon"] != "") {
            $likes["rez_yon"] = $filter_datas["yon"];
        }
        if (isset($filter_datas["webapp"]) && $filter_datas["webapp"] != "") {
            $likes["rez_webapp"] = $filter_datas["webapp"];
        }
        if (isset($filter_datas["kayit"]) && $filter_datas["kayit"] != "") {
            $likes["rez_kayittipi"] = $filter_datas["kayit"];
        }

        if(isset($filter_datas["start"]) && isset($filter_datas["length"])) {
            $page = array(intval($filter_datas["start"]), intval($filter_datas["length"]));
        }elseif(isset($filter_datas["start"])) {
            $page = array(intval($filter_datas["start"]),0);
        }elseif (isset($filter_datas["length"])) {
            $page = array(0,intval($filter_datas["length"]));
        }elseif(!isset($filter_datas["start"]) && !isset($filter_datas["length"])) {
            $page = array(0, 0);
        }
        $order = "rez_id";
        if (isset($filter_datas["order"]) && $filter_datas["order"] != "") {
            $order = $filter_datas["order"];
        }
        if(isset($filter_datas["draw"])) {
            $draw = $filter_datas["draw"];
        }else {
            $draw = "";
        }

        $data = $this->reservation_model->reservation_list($filter, $likes, $page, $order);
        $count = $this->reservation_model->reservation_list_count($filter, $likes, $page, $order);
        $result = '{ "data": ' . json_encode($data["result"]) . ', "recordsTotal": ' . $count["count_all"] . ', "recordsFiltered": ' . $count["count_all"] . ', "draw": ' . $draw . '}';

        $this->output->set_content_type('application/json')->set_output($result);
    }

    public function print_cupon($id)
    {
        $this->load->model("reservation_model");
        $model = array(
            'cupon' => $this->reservation_model->flight_cupon($id)[0]
        );
        $this->load->view('reservation/print_cupon', $model);
    }

    public function pdf_cupon($id)
    {
        $this->load->model("reservation_model");
        $this->load->library("dompdf");
        $model = array(
            'cupon' => $this->reservation_model->flight_cupon($id)[0]
        );
        $html = $this->load->view('reservation/print_cuponpdf', $model, true);
        //    use Dompdf\Dompdf;
        $obj_pdf = new Dompdf();
        $obj_pdf->load_html($html);
        // (Optional) Setup the paper size and orientation
        $obj_pdf->setPaper('A4', 'landscape');
        // Render the HTML as PDF
        $obj_pdf->render();
        // Output the generated PDF to Browser
        $obj_pdf->stream("coupon.pdf", array("Attachment" => 0));
    }

    public function print_ticket($id)
    {
        $this->load->model("reservation_model");
        $model = array(
            'ticket' => $this->reservation_model->flight_ticket($id)
        );
        var_dump($model);
        exit();
        $this->load->view('reservation/print_ticket');
    }

    public function pdf_ticket($id)
    {
        $this->load->model("reservation_model");
        $model = array(
            'ticket' => $this->reservation_model->flight_ticket($id)
        );
        $html = $this->load->view('reservation/print_ticket', $model, true);
        $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $obj_pdf->SetCreator(PDF_CREATOR);
        $title = "PDF Report";
        $obj_pdf->SetTitle($title);
        $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
        $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $obj_pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $obj_pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $obj_pdf->SetFont('helvetica', '', 9);
        $obj_pdf->setFontSubsetting(false);
        $obj_pdf->setPrintHeader(false);
        $obj_pdf->setPrintFooter(false);
        $obj_pdf->AddPage();
        @$obj_pdf->writeHTML($html, true, false, true, false, '');
        $obj_pdf->Output('output.pdf', 'I');
    }

    public function ucustarih()
    {
        $this->load->model("reservation_model");
        echo $this->reservation_model->ucustarih($this->input->post("yolcuid"));
    }

    public function reservationlistmonthsatiscount()
    {
        $this->load->model('mongolog_model');
        $this->load->model("reservation_model");
        $this->load->helper("dateext");

        $month1 = $_GET["month1"];
        $month2 = $_GET["month2"];

        $months = array($month1, $month2);

        // Rezervasyon
        $aramaarr["r.rez_kayittarih >="] = dmywsl_to_sqldate($month1 . "-01");
        $aramaarr["r.rez_kayittarih <="] = dmywsl_to_sqldate($month2 . "-01");

        // print_r($aramaarr);

        $result["rezervation"] = $this->reservation_model->rezervasyonlist_monthcount($aramaarr);

        //Sorgu
        if($month1 != null && $month2 != null) {
            $model = $this->mongolog_model->get_trnx_group_monthly_counts($month1 . "-01", $month2);
        }

        $result["sorgu"] = array_map(
            function ($row) {
                return array(strtotime($row->_id->trnx_year."-".$row->_id->trnx_month)*1000, intval($row->count));//strtotime($row->_id->trnx_year."-".$row->_id->trnx_month."-".$row->_id->trnx_day)*1000
            }, $model
        );

        $this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_NUMERIC_CHECK));
    }

    public function reservationlistturopyuzde()
    {
        $this->load->model("reservation_model");

        $result = $this->reservation_model->rezervasyonlist_turopcount();

        $this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_NUMERIC_CHECK));
    }

    public function gidishavaalani()
    {
        $this->load->model("reservation_model");
        $this->load->helper("dateext");

        $month1 = $_GET["month1"];
        $month2 = $_GET["month2"];

        $aramaarr["r.rez_kayittarih >="] = dmywsl_to_sqldate($month1 . "-01");
        $aramaarr["r.rez_kayittarih <="] = dmywsl_to_sqldate($month2 . "-31");

        $result = $this->reservation_model->rezervasyonlist_gidiscount($aramaarr);

        $this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_NUMERIC_CHECK));
    }

    public function donushavaalani()
    {
        $this->load->model("reservation_model");
        $this->load->helper("dateext");

        $month1 = $_GET["month1"];
        $month2 = $_GET["month2"];

        $aramaarr["r.rez_kayittarih >="] = dmywsl_to_sqldate($month1 . "-01");
        $aramaarr["r.rez_kayittarih <="] = dmywsl_to_sqldate($month2 . "-31");

        $result = $this->reservation_model->rezervasyonlist_donuscount($aramaarr);

        $this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_NUMERIC_CHECK));
    }

    public function guzergaheniyi()
    {
        $this->load->model("reservation_model");
        $this->load->helper("dateext");

        $month1 = $_GET["month1"];
        $month2 = $_GET["month2"];

        // $aramaarr["r.rez_kayittarih >="] = dmywsl_to_sqldate($month1 . "-01");
        // $aramaarr["r.rez_kayittarih <="] = dmywsl_to_sqldate($month2 . "-31");

        $aramaarr["ucus_tarih >="] = dmywsl_to_sqldate($month1 . "-01");
        $aramaarr["ucus_tarih <="] = dmywsl_to_sqldate($month2 . "-31");

        $result = $this->reservation_model->rezervasyonlist_guzergaheniyi($aramaarr);

        $this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_NUMERIC_CHECK));
    }

    public function rezervasyonlar_detayli()
    {
        $this->load->model("reservation_model");
        $this->load->helper("dateext");

        $day1 = null;
        $day2 = null;

        if($_GET){
            if(isset($_GET["day1"])){
                $day1 = $_GET["day1"];
            }
            if(isset($_GET["day2"])){
                $day2 = $_GET["day2"];
            }
        }
        // echo $day1 . $day2;
        $aramaarr["r.rez_kayittarih >="] = dmywsl_to_sqldate($day1);
        $aramaarr["r.rez_kayittarih <="] = dmywsl_to_sqldate($day2);
        $limit = 10;
        // print_r($aramaarr);

        $result = $this->reservation_model->reservation_list_simple($aramaarr, $limit);

        $this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_NUMERIC_CHECK));
    }

    public function biletler_detayli()
    {
        $this->load->model("reservation_model");
        $this->load->helper("dateext");

        $day1 = null;
        $day2 = null;

        if($_GET){
            if(isset($_GET["day1"])){
                $day1 = $_GET["day1"];
            }
            if(isset($_GET["day2"])){
                $day2 = $_GET["day2"];
            }
        }
        // echo $day1 . $day2;
        $aramaarr["r.rez_kayittarih >="] = dmywsl_to_sqldate($day1);
        $aramaarr["r.rez_kayittarih <="] = dmywsl_to_sqldate($day2);
        $aramaarr["r.rez_durum ="] = "OK";
        $limit = 10;
        // print_r($aramaarr);

        $result = $this->reservation_model->reservation_list_simple($aramaarr, $limit);

        $this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_NUMERIC_CHECK));
    }

    public function addReservation()
    {
        $this->load->model("reservation_model");
        $this->load->model( 'acente_model');
        $this->load->model( 'airports_model');
        $this->load->model( 'ucus_model');
        $data = [];

        $data['acentes'] = $this->acente_model->getAll();
        $airports1 = $this->airports_model->getGidis();

        $days = null;
        if(!empty($airports1)) {
            foreach ($airports1 as $airport) {
                $data['countries'][$airport->countrycode] = $airport->country;
                $data['airports1'][$airport->countrycode][] = $airport;
                $data['existsDates'][$airport->ucus_tarih] = $airport->ucus_tarih;
                if (!$days)
                    $days = $this->ucus_model->getDays($airport->code);
            }
        }
        if(!empty($days)) {
            foreach ($days as $day) {
                $data['existsDates'][$day->ucus_tarih] = $day->ucus_tarih;
            }
        }
        if(!empty($data['existsDates'])) {
            $data['firstDate'] = key($data['existsDates']);
            $data['endDate'] = end($data['existsDates']);
        }


        $airports2 = $this->airports_model->getVaris();

        $days = null;
        if(!empty($airports2)) {
            foreach ($airports2 as $airport) {
                $data['countries2'][$airport->countrycode] = $airport->country;
                $data['airports2'][$airport->countrycode][] = $airport;
                $data['existsDates2'][$airport->ucus_tarih] = $airport->ucus_tarih;
                if (!$days)
                    $days = $this->ucus_model->getDays($airport->code);
            }
        }
        if(!empty($days)) {
            foreach ($days as $day) {
                $data['existsDates2'][$day->ucus_tarih] = $day->ucus_tarih;
            }
        }
        if(!empty($data['existsDates'])) {
            $data['firstDate2'] = key($data['existsDates2']);
            $data['endDate2'] = end($data['existsDates2']);
        }
        $this->load->view("reservation/form",$data);
    }

    public function getRoutePrices()
    {
        $this->load->model("airports_model");
        $this->load->model("ucusroute_model");
        $gidis_kalkis = $_POST['gidis_kalkis'];
        $donus_kalkis = isset($_POST['donus_kalkis']) ? $_POST['donus_kalkis'] : "";
        $gidis_varis = $_POST['gidis_varis'];
        $donus_varis = isset($_POST['donus_varis']) ? $_POST['donus_varis'] : "";
        $gidis_page = isset($_POST['gidis_page']) ? $_POST['gidis_page'] : 2;
        $donus_page = isset($_POST['donus_page']) ? $_POST['donus_page'] : 0;

        $gidis_tarih = $_POST['gidis_tarih'];
        $donus_tarih = $_POST['donus_tarih'];
        $yon = $_POST['yon'];
        $toplam_koltuk = $_POST['yetiskin']  + $_POST['cocuk'];
        $byCountry1 = strpos($gidis_kalkis,'country') !== false ? str_replace('country-','',$gidis_kalkis) : false;
        if($byCountry1)
            $country1 = $this->airports_model->getCountry($byCountry1);
        $byCountry2 = strpos($gidis_varis,'country') !== false ? str_replace('country-','',$gidis_varis) : false;
        if($byCountry2)
            $country2 = $this->airports_model->getCountry($byCountry2);

        $byCountry3 = strpos($donus_kalkis,'country') !== false ? str_replace('country-','',$donus_kalkis) : false;
        if($byCountry3)
            $country3 = $this->airports_model->getCountry($byCountry3);
        $byCountry4 = strpos($donus_varis,'country') !== false ? str_replace('country-','',$donus_varis) : false;
        if($byCountry4)
            $country4 = $this->airports_model->getCountry($byCountry4);

        $data = [];
        $data['postedData'] = $_POST;
        $data['postedData']['toplam_koltuk'] = $toplam_koltuk;
        if(!$byCountry1)
            $data['gidis_kalkis_parkur'] = $this->airports_model->getByKod($gidis_kalkis);
        if(!$byCountry2)
            $data['gidis_varis_parkur'] = $this->airports_model->getByKod($gidis_varis);

        if(!$byCountry3)
            $data['donus_kalkis_parkur'] = $this->airports_model->getByKod($donus_kalkis);
        if(!$byCountry4)
            $data['donus_varis_parkur'] = $this->airports_model->getByKod($donus_varis);

        $temp_gidis_routes = $this->ucusroute_model->getByUcusKod($gidis_kalkis,$gidis_varis,$gidis_tarih,$byCountry1,$byCountry2,$toplam_koltuk,$gidis_page);
        foreach ($temp_gidis_routes as $temp_gidis_route) {
            $data['gidis_routes'][$temp_gidis_route->ucusfiyat_id] = $temp_gidis_route;
        }
        $data['yon'] = 'g';
        $data['postedData']['title'] = $data['title'] = ($byCountry1 ? $country1." HEPSİ" : $data['gidis_kalkis_parkur']->city)." - ".($byCountry2 ? $country2." HEPSİ" : $data['gidis_varis_parkur']->city);
        if($yon == 'gd'){
            $data['yon2'] = 'd';
            $data['postedData']['title2'] = $data['title2'] = ($byCountry3 ? $country3." HEPSİ" : $data['donus_kalkis_parkur']->city)." - ".($byCountry4 ? $country4." HEPSİ" : $data['donus_varis_parkur']->city);
            $temp_donus_routes = $this->ucusroute_model->getByUcusKod($donus_kalkis,$donus_varis,$donus_tarih,$byCountry3,$byCountry4,$toplam_koltuk,$donus_page);
            foreach ($temp_donus_routes as $temp_donus_route) {
                $data['donus_routes'][$temp_donus_route->ucusfiyat_id] = $temp_donus_route;
            }
        }

        $data['gidis_page'] = $gidis_page;
        $data['donus_page'] = $donus_page;
        $this->load->view("reservation/route_gd",$data);

    }

    public function getMusteriForm()
    {
        $data = $_POST;
        $this->load->view('reservation/yolcu_form',$data);
    }

    public function searchDonus()
    {
        $this->load->model("airports_model");
        $this->load->model("ucus_model");
        $ucus = isset($_GET['ucus']) ? $_GET['ucus'] : "";
        $varis = isset($_GET['varis']) ? $_GET['varis'] : "";
        $byCountry1 = strpos($_GET['ucus'],'country') !== false ? str_replace('country-','',$_GET['ucus']) : false;
        $byCountry2 = strpos($_GET['varis'],'country') !== false ? str_replace('country-','',$_GET['varis']) : false;

        $temp = $this->airports_model->getAllByCountryForDonus($ucus,$byCountry1,$varis,$byCountry2);
        $days = $this->ucus_model->getDays($byCountry1,$byCountry1 ? : false);
        $data = [];
        foreach ($temp as $airport) {
            $data['countries'][$airport->countrycode] = $airport->country;
            $data['airports'][$airport->countrycode][] = $airport;
        }
        foreach ($days as $day) {
            $data['existsDates'][$day->ucus_tarih] = $day->ucus_tarih;
        }
        if(!empty($data['existsDates'])) {
            $data['firstDate'] = key($data['existsDates']);
            $data['endDate'] = end($data['existsDates']);
        }

        $temp = $this->airports_model->getAllByCountryForDonusVaris($varis,$byCountry2,$ucus,$byCountry1);
        foreach ($temp as $airport) {
            $data['countries2'][$airport->countrycode] = $airport->country;
            $data['airports2'][$airport->countrycode][] = $airport;
        }
        echo json_encode(array('results'=>$data));
    }

    public function searchUcus()
    {
        $this->load->model("airports_model");
        $this->load->model("ucus_model");
        $ucus = isset($_GET['ucus']) ? $_GET['ucus'] : "";
        $byCountry = strpos($ucus,'country') !== false ? str_replace('country-','',$ucus) : false;
        if($byCountry) {
            $temp = $this->airports_model->getAllByCountryForVaris($byCountry);
            $days = $this->ucus_model->getDays($byCountry,true);
        }else {
            $temp = $this->airports_model->getAllByParkurForVaris($ucus);
            $days = $this->ucus_model->getDays($byCountry);
        }
        $data = [];
        foreach ($temp as $airport) {
            $data['countries'][$airport->countrycode] = $airport->country;
            $data['airports'][$airport->countrycode][] = $airport;
        }
        foreach ($days as $day) {
            $data['existsDates'][$day->ucus_tarih] = $day->ucus_tarih;
        }

        $data['firstDate'] = key($data['existsDates']);
        $data['endDate'] = end($data['existsDates']);
        echo json_encode(array('results'=>$data));
    }

    public function searchParkur()
    {
        $key = $_GET['q'];
        $this->load->model("airports_model");
        echo json_encode(array('results'=>$this->airports_model->getByKey($key)));
    }

    public function searchHavayolu()
    {
        $key = $_GET['q'];
        $this->load->model("airlines_model");
        echo json_encode(array('results'=>$this->airlines_model->getByKey($key)));
    }

    public function searchMusteri()
    {
        $key = $_GET['q'];
        $this->load->model("musteri_model");
        echo json_encode(array('results'=>$this->musteri_model->search($key)));
    }

    public function saveRez()
    {
        $this->load->model("reservation_model");
        $this->load->model("ucus_model");
        $this->load->model("ucusroute_model");
        $this->load->model("ucusroutefiyat_model");
        $this->load->model("yolcu_model");
        $this->load->model("yolcuroute_model");
        $this->load->model("yolcuucus_model");
        $gidisRoute = $_POST['gidisRoute'];

        $fiyat = $this->ucusroutefiyat_model->get($gidisRoute);
        $route = $this->ucusroute_model->getWithUcus($fiyat->ucusfiyat_routeid);
        $postedData = $_POST['postedData'];

        if($route) {
            $rez = $this->saveRezervation($fiyat,$route,$postedData);
            if($rez['rez_id']>0){
                echo json_encode(array('status'=>'OK','rez_id'=>$rez['rez_id'],'message'=>'Done'));
            } else {
                echo json_encode(array('status'=>'NOK','message'=>$rez['message']));
            }
        } else {
            echo json_encode(array('status'=>'NOK','message'=>'Rezervasyon oluştururken hata ile karşılaşıldı.'));
        }
    }

    private function saveRezervation($fiyat,$route,$postedData){
        $yolcular = $_POST['Yolcu'];
        $donusRoute = isset($_POST['donusRoute']) ? $_POST['donusRoute'] : false;
        $yon = $postedData['yon'];

        if($donusRoute){
            if($route->route_tekyonizin === 'E'){
                return array('rez_id'=>0,'message'=>'Gidiş için seçilen route tek yönlüdür.');
            }
            $fiyat_donus = $this->ucusroutefiyat_model->get($donusRoute);
            $route_donus = $this->ucusroute_model->getWithUcus($fiyat_donus->ucusfiyat_routeid);
            if($route_donus->route_tekyonizin === 'E'){
                return array('rez_id'=>0,'message'=>'Dönüş için seçilen route tek yönlüdür.');
            }
            if($route_donus->route_farklihsirketizin === 'E' && $route_donus->ucus_kod !== $route_donus->ucus_kod) {
                return array('rez_id'=>0,'message'=>'Dönüş için seçilen route hava yolu dönüş için aynı olmalıdır.');
            }
        }

        $toplamKoltuk = $postedData['toplam_koltuk'];
        $rez_koltukfiyat = $toplamKoltuk * ($fiyat->ucusfiyat_koltukmaliyet + ($donusRoute ? $fiyat_donus->ucusfiyat_koltukmaliyet : 0));
        $rez_komisyonoran = 0;
        $rez_vergi = 0;
        $rez_odemeid = 0;
        $rez_satisfiyat = 0;
        $rez_satisfiyat += ($postedData['yetiskin'] * ($fiyat->ucusfiyat_yetiskinsatisfiyat + ($donusRoute ? $fiyat_donus->ucusfiyat_yetiskinsatisfiyat : 0)));
        $rez_satisfiyat += ($postedData['cocuk'] * $fiyat->ucusfiyat_cocuksatisfiyat + ($donusRoute ? $fiyat_donus->ucusfiyat_cocuksatisfiyat : 0));
        $rez_satisfiyat += ($postedData['bebek'] * $fiyat->ucusfiyat_bebeksatisfiyat + ($donusRoute ? $fiyat_donus->ucusfiyat_bebeksatisfiyat : 0));
        $cinsiyet = [
            'ADT_M' => 'M',
            'CHD_M' => 'M',
            'INF_M' => 'M',
            'ADT_F' => 'F',
            'CHD_F' => 'F',
            'INF_F' => 'F'
        ];
        $koltukFiyatlari = [
            'ADT' => $fiyat->ucusfiyat_yetiskinsatisfiyat + ($donusRoute ? $fiyat_donus->ucusfiyat_yetiskinsatisfiyat : 0),
            'CHD' => $fiyat->ucusfiyat_cocuksatisfiyat + ($donusRoute ? $fiyat_donus->ucusfiyat_cocuksatisfiyat : 0),
            'INF' => $fiyat->ucusfiyat_bebeksatisfiyat + ($donusRoute ? $fiyat_donus->ucusfiyat_bebeksatisfiyat : 0),
        ];
        $bagajBilgisi = [
            'ADT' => $fiyat->ucusfiyat_yetiskinbagaj,
            'CHD' => $fiyat->ucusfiyat_cocukbagaj,
            'INF' => $fiyat->ucusfiyat_bebekbagaj,
        ];
        $rezData = [
            'rez_musteri' => $postedData['musteri'],
            'rez_yon' => $postedData['yon'],
            'rez_kayittipi' => 'L',
            'rez_turoperator' => $route->route_turoperator,
            'rez_acente' => $postedData['acente'],
            'rez_durum' => 'OK',
            'rez_pta' => 'N',
            'rez_koltukfiyat' => $rez_koltukfiyat,
            'rez_komisyonoran' => $rez_komisyonoran,
            'rez_vergi' => $rez_vergi,
            'rez_satisfiyat' => $rez_satisfiyat,
            'rez_odemeid' => $rez_odemeid,
            'rez_webapp' => 'SKYTRIP',
            'rez_kayitseviye' => '1',//TODO: sor
            'rez_kayittarih' => date('Y-m-d H:i:s'),
            'rez_kaydeden' => $this->session->userdata('logged_user')->yonetici_id,
        ];
        $rez_id = $this->reservation_model->insert($rezData);
        if ($rez_id) {

            foreach ($yolcular as $yolcu) {
                $yolcuData = [
                    'yolcu_rezid' => $rez_id,
                    'yolcu_koltukfiyat' => $fiyat->ucusfiyat_koltukmaliyet + ($donusRoute ? $fiyat_donus->ucusfiyat_koltukmaliyet : 0),
                    'yolcu_komisyonoran' => $rez_komisyonoran,
                    'yolcu_vergi' => $rez_vergi,
                    'yolcu_soyad' => $yolcu['soyisim'],
                    'yolcu_ad' => $yolcu['isim'],
                    'yolcu_dtarih' => $yolcu['dogumtarih'],
                    'yolcu_biletno' => isset($yolcu['biletno']) ? $yolcu['biletno'] : '',
                    'yolcu_kimlikturu' => 'B',
                    'yolcu_donusdurum' => $donusRoute ? 'OK' : 'NO',
                    'yolcu_cinsiyet' => $cinsiyet[$yolcu['cinsiyet']]
                ];
                $yolcuData['yolcu_tipi'] = str_replace("_" . $yolcuData['yolcu_cinsiyet'], '', $yolcu['cinsiyet']);
                $yolcuData['yolcu_satisfiyat'] = $koltukFiyatlari[$yolcuData['yolcu_tipi']];
                $yolcuId = $this->yolcu_model->insert($yolcuData);
                if ($yolcuId) {
                    $yolcuUcusData = [
                        'yucus_yon' => 'g',
                        'yucus_yolcuid' => $yolcuId,
                        'yucus_ucusid' => $route->ucus_id,
                        'yucus_sinifid' => $fiyat->ucusfiyat_sinifid,
                        'yucus_bagaj_tipi' => 'P',
                        'yucus_durum' => 'OK',
                        'yucus_bagaj' => $bagajBilgisi[$yolcuData['yolcu_tipi']],
                    ];

                    $this->yolcuucus_model->insert($yolcuUcusData);
                    if($donusRoute){

                        $yolcuUcusData['yucus_yon'] = 'd';
                        $yolcuUcusData['yucus_ucusid'] = $route_donus->ucus_id;
                        $yolcuUcusData['yucus_sinifid'] = $fiyat_donus->ucusfiyat_sinifid;
                        $this->yolcuucus_model->insert($yolcuUcusData);
                    }
                }
            }

            return array('rez_id'=>$rez_id,'rez_komisyonoran'=>$rez_komisyonoran);
        }

    }

}
