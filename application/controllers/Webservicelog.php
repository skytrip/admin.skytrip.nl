<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Webservicelog extends CI_Controller
{
     public $activemenu;

     public function __construct()
     {
          parent::__construct();
          $this->load->helper("jsglobal");
          $this->activemenu = activemenu("Web Service Logs");
     }

     public function index()
     {
          $this->load->model( 'dashboard_datas' );
          $this->load->model( 'mongolog_model' );

          $model = array('renevues_last15_days' => array_map( function ( $row ){
               return $row->kazanc;
               }, $this->dashboard_datas->revenue_last15days() ),
               'passenger_last15_days' => array_map( function ( $row )
               {
                    return $row->yolcu_count;
               }, $this->dashboard_datas->passenger_last15days() ),
               'traffic_last15_days' => array_map( function ( $row )
               {
                    return $row;
               }, $this->mongolog_model->get_trnx_last15_days()),//[ "result" ] ),
               'renevues_last30_days_sum' => $this->dashboard_datas->revenue_last30days_sum(),
               'passenger_last30days_sum' => $this->dashboard_datas->passenger_last30days_sum(),
               'traffic_last30days_sum' => $this->mongolog_model->get_trnx_last30_days_sum()[ 0 ]->count
          );

          $this->load->view( "webservice/index", $model );
     }

     public function get_pagdata($limit = null,$start = null,$draw = null)
     {
          $this->load->model( 'mongolog_model' );

          $trnxid = null;
          $servicename = null;
          $error = null;
          $trnxtime = null;
          $requestfile = null;
          $responsefile = null;

          $aramax = array();

          if($_POST){
               if(isset($_POST["start"])){
                    $start = $_POST["start"];
               }
               if(isset($_POST["draw"])){
                    $draw = $_POST["draw"];
               }
               if(isset($_POST["length"])){
                    $limit = $_POST["length"];
               }
               if(isset($_POST["trnxid"]) and $_POST["trnxid"] != ""){
                    $aramax = array('TRNX_ID' => array('$regex' => $_POST["trnxid"]));
               }
               if(isset($_POST["servicename"]) and $_POST["servicename"] != ""){
                    $aramax = array('SERVICE_NAME' => array('$regex' => $_POST["servicename"]));
               }
               if(isset($_POST["error"]) and $_POST["error"] != ""){
                    $aramax = array('ERROR_STATU' => array('$regex' => $_POST["error"]));
               }
               if(isset($_POST["trnxtime"]) and $_POST["trnxtime"] != ""){
                    $time = strtotime(str_replace('/', '-', $_POST["trnxtime"]));
                    $time2 = strtotime(str_replace('/', '-', $_POST["trnxtime"] . ' +1 day'));
                    // echo date("d/m/Y",$time) . " o " . date("d/m/Y",$time2);
                    $aramax = array('TRNX_TIME' => array('$gt' => new \MongoDB\BSON\UTCDateTime($time * 1000),'$lt' => new \MongoDB\BSON\UTCDateTime($time2 * 1000)));
               }
               if(isset($_POST["requestfile"]) and $_POST["requestfile"] != ""){
                    $aramax = array('SERVICE_REQUEST_FILE_ID' => new MongoDB\BSON\ObjectId($_POST["requestfile"]));
               }
               if(isset($_POST["responsefile"]) and $_POST["responsefile"] != ""){
                    $aramax = array('SERVICE_RESPONSE_FILE_ID' => new MongoDB\BSON\ObjectId($_POST["responsefile"]));
               }
               if(isset($_POST["tarih"]) and $_POST["tarih"] != ""){
                    $time = strtotime(date("Y-m-d", strtotime(str_replace('/', '-', $_POST["tarih"]))) . 'T00:00:00.000Z');
                    $time2 = strtotime(date("Y-m-d", strtotime(str_replace('/', '-', $_POST["tarih"] . ' +1 day'))) . 'T00:00:00.000Z');
                    // echo str_replace('/', '-', $_POST["tarih"]);
                    // echo date("Y-m-d", strtotime(str_replace('/', '-', $_POST["tarih"])));
                    $aramax = array('TRNX_TIME' => array('$gte' => new \MongoDB\BSON\UTCDateTime($time * 1000),'$lt' => new \MongoDB\BSON\UTCDateTime($time2 * 1000)));
               }

               // Order
               if(isset($_POST["order"]) and $_POST["order"] != ""){
                    $ham = explode(" ", $_POST["order"]);
                    $tab = $ham[0];
                    $yontem = $ham[1];
                    // echo "tab: " . $tab . "\n yontem: " . $yontem;

                    if($yontem == "desc"){
                         $yontem = -1;
                    }elseif($yontem == "asc"){
                         $yontem = 1;
                    }

                    $sortx = array('detaylar.' . $tab => $yontem);
               }
          }
          $sortx = array();
          //, $trnxid, $servicename, $error, $trnxtime, $requestfile, $responsefile

          echo $this->mongolog_model->get_pagdata($limit, $start, $draw,$aramax,$sortx);
     }
}
