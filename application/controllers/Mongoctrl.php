<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mongoctrl extends CI_Controller
{

     public function daily()
     {
          $this->load->model('mongolog_model');

          $day1 = null;
          $day2 = null;
          if(isset($_GET["day1"]) && isset($_GET["day2"])) {
               $day1 = $_GET["day1"];
               $day2 = $_GET["day2"];
          }
          if($day1 != null && $day2 != null) {
               $model = $this->mongolog_model->get_trnx_group_daily_counts($day1, $day2);
          }else{
               $model = $this->mongolog_model->get_trnx_group_daily_counts(date("Y-m-01"));
          }

          $result = array_map(
               function ($row) {
                    return array(strtotime($row->_id->trnx_year."-".$row->_id->trnx_month."-".$row->_id->trnx_day)*1000, intval($row->count));
               }, $model
          );
          $this->output->set_content_type('application/json')->set_output(json_encode($result));
     }

     public function sorgu($day1 = null,$day2 = null)
     {
          $this->load->model('mongolog_model');

          $day1 = null;
          $day2 = null;
          if($day1 == null and $day2 = null) {
               if(isset($_GET["day1"]) && isset($_GET["day2"])) {
                    $day1 = $_GET["day1"];
                    $day2 = $_GET["day2"];
               }
          }
          if($day1 != null && $day2 != null) {
               $model = $this->mongolog_model->get_trnx_group_sorgu($day1, $day2);
          }else{
               $model = $this->mongolog_model->get_trnx_group_sorgu(date("Y-m-01"));
          }

          $result = array_map(
               function ($row) {
                    $parse = json_decode($row->_id->searchresult, true);
                    $trxId = $parse["trxId"];
                    $apicode = "";
                    $combinedKey = "";
                    $fiyat = "";
                    $parabirimi = "EUR";
                    $airSegmentKey = "";
                    $airSegmentKey2 = "";
                    $airSegmentKeys = "";
                    $carrier = "";
                    $kalkissehir = "";
                    $varissehir = "";
                    $kalkistarih = "";
                    $kalkissaat = "";
                    $varistarih = "";
                    $varissaat = "";
                    $operatingCarrier = "";
                    $gidisdonus = "";
                    $key = "";
                    $airSegmentKeysMax = null;
                    $ucussuresimax = null;
                    $toplamartikzamanMax = null;
                    $toplamucussuresiMax = null;

                    $sorgutarih = date("d.m.Y");
                    $sorgusaat = date("H:i:s");

                    $anaarray = array();

                    $trnxIdKontrol = array();
                    foreach ($parse["combinedAirPriceSolutionArray"] as $a) {
                         // uçuş
                         $combinedKey = $a["combinedKey"];

                         // apikey
                         $apicode = $a["apiCode"];

                         // fiyat
                         foreach ($a["airPricingInfoArray"] as $b) {
                              $fiyat = $b["ADT"]["approximateTotalPriceAmout"] + $b["ADT"]["totalAdditionalPriceAmount"];
                         }

                         $airSegmentKeysana = array();
                         $airSegmentKeysarr = array();

                         $legrotaana = array();
                         $legs = array();
                         $z = -1;

                         $z = 0;
                         foreach ($a["legs"] as $k => $c) {
                              $z++;
                              for ($j=0; $j < count($a["legs"][$k]["avaibleJourneyOptions"]); $j++) {
                                   if(!empty($a["legs"][$k]["avaibleJourneyOptions"][$j]["airSegmentKeys"])){
                                        $airSegmentKeysarr = $a["legs"][$k]["avaibleJourneyOptions"][$j]["airSegmentKeys"];
                                   }
                                   array_push($airSegmentKeysana, $airSegmentKeysarr);
                                   if($j == count($a["legs"][$k]["avaibleJourneyOptions"]) - 1){
                                        $legrota = array($c["origin"], $c["destination"]);
                                        array_push($legrotaana, $legrota);
                                        $legs["airSegmentKeys"] = $airSegmentKeysana;
                                        $legs["legrota"] = $legrotaana;
                                   }
                              }
                         }
                         $toplamucussuresi = 0;
                         $toplamseyehatsuresi = 0;
                         $ucussaati = 0;

                         foreach ($legs["airSegmentKeys"] as $i => $f) {
                              foreach ($f as $k => $value) {
                                   foreach ($parse["airSegmentArray"] as $g) {
                                        if ($value == $g["key"]) {
                                             $key = $g["key"];
                                             $carrier = $g["carrier"] . " " . $g["flightNumber"];
                                             $kalkissehir = $g["origin"];
                                             $varissehir = $g["destination"];
                                             $kalkistarih = $g["departureDate"];
                                             $kalkissaat = $g["departureHours"];
                                             $varistarih = $g["arrivalDate"];
                                             $varissaat = $g["arrivalHours"];
                                             if($g["operatingCarrier"] != null) {
                                                  $operatingCarrier = $g["operatingCarrier"];
                                             }else{
                                                  $operatingCarrier = "null";
                                             }
                                             $ucussaati = $g["flightTime"];

                                             $toplamucussuresi += $ucussaati;

                                             $kalkistarihduzelt = explode(".", $kalkistarih);
                                             $varistarihduzelt = explode(".", $varistarih);

                                             if ($kalkistarihduzelt[2] < 2000) {
                                                  $kalkistarih = $kalkistarihduzelt[0] . "." . $kalkistarihduzelt[1] . "." . "20" . $kalkistarihduzelt[2];
                                             }
                                             if ($varistarihduzelt[2] < 2000) {
                                                  $varistarih = $varistarihduzelt[0] . "." . $varistarihduzelt[1] . "." . "20" . $varistarihduzelt[2];
                                             }
                                             $legs["detay"][$i][$k]["key"] = $key;
                                             $legs["detay"][$i][$k]["carrier"] = $carrier;
                                             $legs["detay"][$i][$k]["kalkissehir"] = $kalkissehir;
                                             $legs["detay"][$i][$k]["varissehir"] = $varissehir;
                                             $legs["detay"][$i][$k]["kalkistarih"] = strtotime($kalkistarih)*1000;
                                             $legs["detay"][$i][$k]["kalkissaat"] = $kalkissaat;
                                             $legs["detay"][$i][$k]["varistarih"] = strtotime($varistarih)*1000;
                                             $legs["detay"][$i][$k]["varissaat"] = $varissaat;
                                             $legs["detay"][$i][$k]["operatingCarrier"] = $operatingCarrier;
                                             $legs["detay"][$i][$k]["ucussaati"] = $ucussaati;

                                             $kontroltarih = explode(".", $kalkistarih);
                                             if ($kontroltarih[2] == date("Y")) {
                                                  if ($kontroltarih[1] == date("m")) {
                                                       if ($kontroltarih[0] < date("d")) {
                                                            break;
                                                       }
                                                  }
                                                  if ($kontroltarih[1] < date("m")) {
                                                       break;
                                                  }
                                             } else {
                                                  if ($kontroltarih[2] < date("Y")) {
                                                       break;
                                                  }
                                             }
                                        }
                                   }
                              }
                         }
                         if ($ucussuresimax != null) {
                              if ($ucussaati > $ucussuresimax) {
                                   break;
                              }
                         }
                         $toplamartikzaman = 0;
                         $toplamyolculuksuresi = 0;
                         $toplamucussuresi = 0;

                         foreach ($legs["detay"] as $key => $value) {
                              if(count($value) > 1) {
                                   $t = -1;
                                   for ($h=0; $h < count($value); $h++) {
                                        $toplamucussuresi += $value[$h]["ucussaati"];
                                        if($h != 0) {
                                             $t++;
                                             $toplamartikzaman = floor(((strtotime(date("d.m.Y", $value[$h]["kalkistarih"]/1000) . " " . $value[$h]["kalkissaat"])*1000) - (strtotime(date("d.m.Y", $value[$h - 1]["varistarih"]/1000) . " " . $value[$h - 1]["varissaat"])*1000)) * 1.66666667 / 100000);
                                        }
                                        if($h == count($value) - 1){
                                             $toplamyolculuksuresi = $toplamartikzaman + $toplamucussuresi;
                                             $legs["zaman"][$key][$t]["yolculuksuresi"] = $toplamyolculuksuresi;
                                             $legs["zaman"][$key][$t]["artikzaman"] = $toplamartikzaman;
                                             $legs["zaman"][$key][$t]["ucussuresi"] = $toplamucussuresi;
                                             $toplamucussuresi = 0;
                                        }
                                   }
                              }else{
                                   $toplamucussuresi = $value[0]["ucussaati"];
                                   $toplamyolculuksuresi = 0 + $toplamucussuresi;
                                   $legs["zaman"][0][0]["yolculuksuresi"] = $toplamyolculuksuresi;
                                   $legs["zaman"][0][0]["artikzaman"] = 0;
                                   $legs["zaman"][0][0]["ucussuresi"] = $toplamucussuresi;
                                   $toplamucussuresi = 0;
                              }
                         }
                         $gidisdonus = 0;
                         if(count($legs["legrota"]) > 1){
                              $gidisdonus = 1;
                         }

                         if(!in_array($trxId, $trnxIdKontrol)) {
                              array_push($trnxIdKontrol, $trxId);
                              array_push($anaarray, array(
                                   "trxId" => $trxId,
                                   "combinedKey" => $combinedKey,
                                   "apicode" => $apicode,
                                   "sorgutarih" => strtotime($sorgutarih) * 1000,
                                   "sorgusaat" => $sorgusaat,
                                   "fiyat" => $fiyat,
                                   "parabirimi" => $parabirimi,
                                   "gidisdonus" => $gidisdonus,
                                   "legs" => $legs,
                                   )
                              );
                         }
                    }

                    return array(strtotime($row->_id->trnx_year."-".$row->_id->trnx_month."-".$row->_id->trnx_day)*1000, intval($row->count), $anaarray, $row->_id->trnx_hour . ":" . $row->_id->trnx_minute . ":" . $row->_id->trnx_second);
               }, $model
          );
          $this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_NUMERIC_CHECK));
     }
}
