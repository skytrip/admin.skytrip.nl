<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Analytics extends CI_Controller {
    
    public function index() {
        $this->load->library("google_analytics");
        $model = $this->google_analytics->daily();
        $data = $model["rows"];
        $result = array();
        for ($i=0; $i<count($data); $i++) {
            $result[] = array($data[$i][0], intval($data[$i][1]));
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }
    
}
