<?php
/**
 * Created by IntelliJ IDEA.
 * User: ekrmn60600
 * Date: 18.11.2018
 * Time: 00:49
 */
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Routes extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
    }

    public function index()
    {
        $model = array(
            'routes_sum'=>0,
            'passenger_sum'=>0,
        );
        $this->load->view( "routes/index", $model );
    }

    public function update($id)
    {
        $this->load->model( 'airlines_model' );
        $this->load->model( 'airports_model' );
        $this->load->model( 'ucus_model' );
        $this->load->model( 'turoperator_model' );
        $this->load->model( 'ucussinifad_model' );
        $this->load->model( 'ucusroute_model');
        $this->load->model( 'acente_model');
        $data['model'] = $this->ucus_model->getById($id);
        if(!empty($_POST)){
            echo "<pre>";
            print_r($_POST);
            die();

        }
        $data['turOperators'] = $this->turoperator_model->getAll();
        $data['sinifAds'] = $this->ucussinifad_model->getAll();
        $data['ucusRoutes'] = $this->ucusroute_model->getAllByUcusId($id);
        $data['acentes'] = $this->acente_model->getAll();
        $data['airlines'] = $this->airlines_model->getAll();
        $data['airports'] = $this->airports_model->getAll();

        $this->load->view("routes/ucus_form",$data);
    }

    public function addRoute()
    {
        $this->load->model( 'airlines_model' );
        $this->load->model( 'airports_model' );
        $this->load->model( 'ucus_model' );
        $this->load->model( 'turoperator_model' );
        $this->load->model( 'ucussinifad_model' );
        $this->load->model( 'ucusroute_model');
        $this->load->model( 'ucussegment_model');
        $this->load->model( 'acente_model');
        $this->load->model( 'ucusroutefiyat_model');

        $data['model'] = new Ucus_model();
        if(!empty($_POST)){
            $ucusData = $_POST['Ucus'];
            if($_POST['kayit_tur'] == 'seri') {
                $varis_tarih_day = isset($ucusData['ucus_varistarih']) ? (strtotime($ucusData['ucus_varistarih'])-strtotime($ucusData['ucus_tarih'])) / ( 60 * 60 * 24) : 0;
                $startDate = $_POST['seri_start_1'];
                $endDate = $_POST['seri_start_2'];
                $dateList = [];
                $whileStart = true;
                while ($whileStart) {
                    if ($startDate <= $endDate) {
                        $dayOfWeek = date("w", strtotime($startDate));
                        if (isset($_POST['Seri'][$dayOfWeek])) {

                            $dateList[$startDate] = date("Y-m-d", strtotime("+$varis_tarih_day day", strtotime($startDate)));
                        }
                        $startDate = date('Y-m-d', strtotime("+1 day", strtotime($startDate)));
                    } else {
                        $whileStart = false;
                    }
                }
            }else{
                $_datasdasd = date('Y-m-d',strtotime($ucusData['ucus_tarih']));
                $dateList = [$_datasdasd=>isset($ucusData['ucus_varistarih']) ? date('Y-m-d H:i:s',strtotime($ucusData['ucus_varistarih'])) : $ucusData['ucus_tarih']];
            }
            foreach ($dateList as $start_date=>$end_date) {

                $ucusData['ucus_tarih'] = date('Y-m-d H:i:s',strtotime($start_date));
                $ucusData['ucus_varistarih'] = $end_date;
                $ucusData['ucus_yil'] = date('Y',strtotime($ucusData['ucus_tarih']));
                $ucusData['ucus_uyarinotgoster'] = isset($ucusData['ucus_uyarinotgoster']) ? 'E' : 'H';
                $ucusData['ucus_kayittarih'] = date('Y-m-d H:i:s');
                $ucusData['ucus_kaydeden'] = $this->session->userdata('logged_user')->yonetici_id;
                $ucusData['ucus_kayitseviye'] = 1;
                $ucus_id = $this->ucus_model->insert($ucusData);
                if($ucus_id){
                    $ucusRouteData = $_POST['UcusRoute'];
                    $ucusRouteFiyatDatas = $_POST['UcusRouteFiyat'];
                    $ucusRouteData['route_turoperator'] = $ucusData['ucus_turoperator'];
                    $ucusRouteData['route_durum'] = $ucusData['ucus_durum'];
                    $ucusRouteData['route_uyarinot'] = $ucusData['ucus_uyarinot'];
                    $ucusRouteData['route_uyarinotgoster'] = $ucusData['ucus_uyarinotgoster'];
                    $ucusRouteData['route_degisikliknot'] = $ucusData['ucus_degisikliknot'];
                    $ucusRouteData['route_aciklama'] = $ucusData['ucus_aciklama'];
                    $ucusRouteData['route_sezonseviye'] = 3;//TODO: nereden
                    $ucusRouteData['route_goruntulemeizin'] = isset($ucusRouteData['route_goruntulemeizin']) ? '1' : '0';
                    $ucusRouteData['route_opsiyonizin'] = isset($ucusRouteData['route_opsiyonizin']) ? 'E' : 'H';
                    $ucusRouteData['route_opsiyontarih'] = $ucusRouteData['route_opsiyonizin'] == 'E' ? $ucusRouteData['route_opsiyontarih'] : '0000-00-00 00:00:00';
                    $ucusRouteData['route_donussureizintur'] = isset($ucusRouteData['route_donussureizintur']) ? $ucusRouteData['route_donussureizintur'] : 'G';
                    $ucusRouteData['route_donussureizinmin'] = isset($ucusRouteData['route_donussureizinmin']) ? $ucusRouteData['route_donussureizinmin'] : '0';
                    $ucusRouteData['route_donussureizinmax'] = isset($ucusRouteData['route_donussureizinmax']) ? $ucusRouteData['route_donussureizinmax'] : '0';
                    $ucusRouteData['route_donussureizintarih'] = $ucusRouteData['route_donussureizintur'] == 'T' ? (isset($ucusRouteData['route_donussureizintarih']) ? $ucusRouteData['route_donussureizinmax'] : '0000-00-00 00:00:00') : '0000-00-00 00:00:00';
                    $ucusRouteData['route_farklituropizin'] = isset($ucusRouteData['route_farklituropizin']) ? 'E' : 'H';
                    $ucusRouteData['route_farklihsirketizin'] = isset($ucusRouteData['route_farklihsirketizin']) ? 'E' : 'H';
                    $ucusRouteData['route_elektronikbiletizin'] = isset($ucusRouteData['route_elektronikbiletizin']) ? 'E' : 'H';
                    $ucusRouteData['route_tekyonizin'] = isset($ucusRouteData['route_tekyonizin']) ? 'E' : 'H';
                    $ucusRouteData['route_kayittarih'] = date('Y-m-d H:i:s');
                    $ucusRouteData['route_kaydeden'] = $this->session->userdata('logged_user')->yonetici_id;
                    //TODO: kilit tarih ve seviye ?

                    $route_id = $this->ucusroute_model->insert($ucusRouteData);
                    if($route_id){
                        $this->ucussegment_model->insert(array('route_id'=>$route_id,'ucus_id'=>$ucus_id));
                        foreach ($ucusRouteFiyatDatas as $ucusRouteFiyatData) {
                            $ucusRouteFiyatData['ucusfiyat_routeid'] = $route_id;
                            $ucusRouteFiyatData['ucusfiyat_kalankoltuksayi'] = $ucusRouteFiyatData['ucusfiyat_koltuksayi'];
                            $ucusRouteFiyatData['ucusfiyat_promosyon'] = 'H';
                            $ucusRouteFiyatData['ucusfiyat_sinifgoster'] = 'H';
                            $ucusRouteFiyatData['ucusfiyat_durum'] = $ucusData['ucus_durum'];
                            $ucusfiyat_id = $this->ucusroutefiyat_model->insert($ucusRouteFiyatData);
                        }
                    }
                }
            }
            die();
        }
        $data['turOperators'] = $this->turoperator_model->getAll();
        $data['sinifAds'] = $this->ucussinifad_model->getAll();
        $data['ucusRoutes'] = [];
        $data['acentes'] = $this->acente_model->getAll();
        $data['airlines'] = $this->airlines_model->getAll();
        $data['airports'] = $this->airports_model->getAll();
        $this->load->view("routes/ucus_form",$data);
    }

    public function sellRoute()
    {
        echo "sellRoute";
    }

    public function getRoutesList($page = 1)
    {
        $this->load->model( 'ucus_model' );
        $model = $this->ucus_model->getUcusList( array( ( $page - 1 ) * 10, 10 ) );
        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $model ) );
    }

}