<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistic extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper("jsglobal");
     }

     public function index() {
          $this->load->model("dashboard_datas");
          $this->load->model("reservation_model");
          $this->load->helper("dateext");
          $this->load->model("mongolog_model");

          $aramaarr["r.rez_kayittarih >="] = dmywsl_to_sqldate("2016-10-01");
          $aramaarr["r.rez_kayittarih <="] = dmywsl_to_sqldate("2016-11-18");
          $limit = 10;

          $model = array(
               'renevues_last15_days' => array_map( function ( $row )
               {
                   return $row->kazanc;
               }, $this->dashboard_datas->revenue_last15days() ),
               'passenger_last15_days' => array_map( function ( $row )
               {
                   return $row->yolcu_count;
               }, $this->dashboard_datas->passenger_last15days() ),
               'traffic_last15_days' => array_map( function ( $row )
               {
                   return $row;
               }, $this->mongolog_model->get_trnx_last15_days()),//[ "result" ] ),
               'renevues_last30_days_sum' => $this->dashboard_datas->revenue_last30days_sum(),
               'passenger_last30days_sum' => $this->dashboard_datas->passenger_last30days_sum(),
               'traffic_last30days_sum' => $this->mongolog_model->get_trnx_last30_days_sum()[ 0 ]->count,
               'activemenu' => activemenu("Statistics")
          );
          $this->load->view("statistic/index", $model);
     }
}
