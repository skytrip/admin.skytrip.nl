<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

    public function __construct() {
        parent::__construct();
        include_once APPPATH."third_party/smartui/UI.php";
        include_once APPPATH."third_party/smartui/Util.php";
        include_once APPPATH."third_party/smartui/Components/Nav.php";

        \SmartUI\UI::register('nav', 'SmartUI\Components\Nav');
    }

    public function index()
    {
        $this->load->helper("url");
        $this->load->model("menu");
        $nav_model = array(
            "page_nav" => $this->menu->get(),
            '_ui'=>new \SmartUI\UI(),
        );

        $header_model = array(
            "page_title" => "Site",
            "page_css" => array(),
            "page_body_prop" => array(),
            "no_main_header" => false,
        );

        $this->load->view("header", $header_model);
        $this->load->view("nav", $nav_model);
        $this->load->view("layout");
        $this->load->view("footer");
    }
}
