<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
     }

     public function index() {
          if ( $_SERVER[ 'REQUEST_METHOD' ] != 'GET' )
          {
               echo "not allowed";
               exit();
          }
          $this->load->view("mail/mail");
     }
}
