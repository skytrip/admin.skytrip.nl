<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Dashboard extends CI_Controller
{
    public $activemenu;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("jsglobal");
        $this->activemenu = activemenu("Dashboard");
    }
    public function index()
    {
        $this->load->model( 'dashboard_datas' );
        $this->load->model( 'mongolog_model' );
        $this->load->library( "google_analytics" );

        //start calendar ok, op, ca recrods
        $calendar_data = $this->dashboard_datas->calendar_summary();
        $calendar_items = array();
        $kontrol = array();
        for ( $i = 0; $i < count( $calendar_data ); $i++ )
        {
            $row = $calendar_data[ $i ];
            $date_day = date( "j", strtotime( $row->rezdate ) );
            $date_mon = date( "n", strtotime( $row->rezdate ) ) - 1;
            $date_yea = date( "Y", strtotime( $row->rezdate ) );
            if ( $row->statusok > 0 )
            {
                $calendar_items[] = array(
                    "@[title]@" => "OK " . $row->statusok, "@[tip]@" => "ok", "@[className]@" => array( "event", "bg-color-greenLight" ), "@[start]@" => "@[new Date({$date_yea}, {$date_mon}, {$date_day})]@", "@[allDay]@" => false, "@[icon]@" => "fa-check", "@[countok]@" => $row->statusok
                );
                $kontrol[] = array(
                    "yil" => $date_yea,
                    "ay" => $date_mon,
                    "gun" => $date_day,
                    "countok" => $row->statusok
                );
            }
            if ( $row->statusca > 0 )
            {
                $calendar_items[] = array(
                    "@[title]@" => "CA " . $row->statusca, "@[tip]@" => "ca", "@[className]@" => array( "event", "bg-color-red" ), "@[start]@" => "@[new Date({$date_yea}, {$date_mon}, {$date_day})]@", "@[allDay]@" => false, "@[icon]@" => "fa-check"
                );
            }
            if ( $row->statusop > 0 )
            {
                $calendar_items[] = array(
                    "@[title]@" => "OP " . $row->statusop, "@[tip]@" => "op", "@[className]@" => array( "event", "bg-color-blue" ), "@[start]@" => "@[new Date({$date_yea}, {$date_mon}, {$date_day})]@", "@[allDay]@" => false, "@[icon]@" => "fa-check"
                );
            }
        }

        //mongolog calendar records
//        $mongo_calendar_records = $this->mongolog_model->get_trnx_yearly_processed_calendar();
        $mongo_calendar_records = [];
        $mongo_calendar_result = $mongo_calendar_records;
        for ( $i = 0; $i < count( $mongo_calendar_result ); $i++ )
        {
            $row = $mongo_calendar_result[ $i ];
            $_id = $row->_id;
            $trnx_year = intval( $_id->trnx_year );
            $trnx_month = intval( $_id->trnx_month ) - 1;
            $trnx_day = intval( $_id->trnx_day );
            $calendar_items[] = array(
                "@[title]@" => "QR " . $row->count, "@[tip]@" => "qr", "@[className]@" => array( "event", "bg-color-orange" ), "@[start]@" => "@[new Date({$trnx_year}, {$trnx_month}, {$trnx_day})]@", "@[allDay]@" => false, "@[icon]@" => "fa-check", "@[count]@" => $row->count, "@[procount]@" => $row->procount, "@[kontrol]@" => array_map( function ( $rowt , $yil, $ay, $gun, $procount)
                {
                    if($rowt["yil"] == $yil && $rowt["ay"] == $ay && $rowt["gun"] == $gun){
                        if($rowt["countok"] == $procount){
                            return "ok";
                        }else{
                            return "false";
                        }
                    }else{
                        return 0;
                    }
                }, $kontrol , [$trnx_year], [$trnx_month], [$trnx_day], [$row->procount])
            );
        }

        $calendar_js = json_encode( $calendar_items );
        $calendar_js = str_replace( "\"@[", "", $calendar_js );
        $calendar_js = str_replace( "]@\"", "", $calendar_js );

        //create model object
        $model = array(
            'renevues_last15_days' => array_map( function ( $row )
            {
                return $row->kazanc;
            }, $this->dashboard_datas->revenue_last15days() ),
            'passenger_last15_days' => array_map( function ( $row )
            {
                return $row->yolcu_count;
            }, $this->dashboard_datas->passenger_last15days() ),
            'traffic_last15_days' => array_map( function ( $row )
            {
                return $row;
            }, $this->mongolog_model->get_trnx_last15_days()),//[ "result" ] ),
            'renevues_last30_days_sum' => $this->dashboard_datas->revenue_last30days_sum(),
            'passenger_last30days_sum' => $this->dashboard_datas->passenger_last30days_sum(),
            'traffic_last30days_sum' => $this->mongolog_model->get_trnx_last30_days_sum()[ 0 ]->count,//[ "result" ][ 0 ][ "count" ],
            'calendar_data' => $calendar_js
        );

        //country based visits
        if ( $this->session->has_userdata( "access_token" ) )
        {
            $google_analytics_country = $this->google_analytics->country();
            $model[ 'google_analytics_country' ] = $google_analytics_country->rows;
        }
        $this->load->view( "dashboard/index", $model );
    }

    public function info()
    {
        echo phpinfo();
    }

    public function passenger_count()
    {
        $this->load->model( 'dashboard_datas' );

        $day1 = null;
        $day2 = null;
        if(isset($_GET["day1"]) && isset($_GET["day2"])){
            $day1 = $_GET["day1"];
            $day2 = $_GET["day2"];
        }
        if($day1 != null && $day2 != null){
            $model = $this->dashboard_datas->passenger_count($day1, $day2);
        }else{
            $model = $this->dashboard_datas->passenger_count();
        }
        $result = array_map( function ( $row )
        {
            return array( strtotime( $row->reztarih ) * 1000, intval( $row->yolcu_count ) );
        }, $model );
        if($result == null){
            $result[] = 0;
        }
        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $result ) );
    }

    public function revenue_summary()
    {
        $this->load->model( 'dashboard_datas' );

        $day1 = null;
        $day2 = null;
        if(isset($_GET["day1"]) && isset($_GET["day2"])){
            $day1 = $_GET["day1"];
            $day2 = $_GET["day2"];
        }
        if($day1 != null && $day2 != null){
            $model = $this->dashboard_datas->revenue_summary($day1, $day2);
        }else{
            $model = $this->dashboard_datas->revenue_summary();
        }
        $result = array_map( function ( $row )
        {
            return array( strtotime( $row->reztarih ) * 1000, floatval( $row->kazanc ) );
        }, $model );
        if($result == null){
            $result[] = 0;
        }
        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $result ) );
    }

    public function query_summary()
    {
        $this->load->model( 'mongolog_model' );
        $model = $this->mongolog_model->get_trnx_booking_daily();

    }

    public function dashboard_optionlist( $page = 1 )
    {
        $this->load->model( 'reservation_model' );
        $model = $this->reservation_model->dashboard_opsionlist( array( ( $page - 1 ) * 10, 10 ) );
        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $model ) );
    }

    public function dashboard_cancellist( $page = 1 )
    {
        $this->load->model( 'reservation_model' );
        $model = $this->reservation_model->dashboard_cancellist( array( ( $page - 1 ) * 10, 10 ) );
        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $model ) );
    }

    public function dashboard_waitlist( $page = 1 )
    {
        $this->load->model( 'reservation_model' );
        $model = $this->reservation_model->dashboard_waitlist( array( ( $page - 1 ) * 10, 10 ) );
        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $model ) );
    }

    public function dashboard_specialmessage()
    {
        $this->load->model( 'dashboard_datas' );
        $model =
            $this->dashboard_datas->special_message( $this->session->userdata( "logged_user" )->yonetici_kullanici );
        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $model ) );
    }

    public function dashboard_reservationmessage()
    {
        $this->load->model( 'dashboard_datas' );
        $model = $this->dashboard_datas->reservation_message( $this->session->userdata( "logged_user" )->yonetici_id );
        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $model ) );
    }

    public function dashboard_flightwithpassengercount( $date = null )
    {
        $this->load->model( 'dashboard_datas' );
        if ( $date === null )
        {
            $date = date( '%Y-%m-%d' );
        }
        $model = $this->dashboard_datas->flight_with_passengercount( $date );
        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $model ) );
    }

    public function lastreservationcount()
    {
        $this->load->model("reservation_model");
        $this->load->helper("dateext");

        $day1 = $_GET["day1"];
        $day2 = date('Y-m-d', strtotime($_GET["day2"] . ' +1 day'));

        $days = array($day1, $day2);

        $aramaarr["r.rez_kayittarih >="] = dmywsl_to_sqldate($day1);
        $aramaarr["r.rez_kayittarih <="] = dmywsl_to_sqldate($day2);

        $result = $this->reservation_model->rezervasyonlist_daycount($aramaarr);

        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $result, JSON_NUMERIC_CHECK ) );
    }

    public function lastbiletcount()
    {
        $this->load->model("reservation_model");
        $this->load->helper("dateext");

        $day1 = $_GET["day1"];
        $day2 = date('Y-m-d', strtotime($_GET["day2"] . ' +1 day'));

        $days = array($day1, $day2);

        $aramaarr["r.rez_kayittarih >="] = dmywsl_to_sqldate($day1);
        $aramaarr["r.rez_kayittarih <="] = dmywsl_to_sqldate($day2);

        $result = $this->reservation_model->bilet_daycount($aramaarr);

        $this->output->set_content_type( 'application/json' )->set_output( json_encode( $result, JSON_NUMERIC_CHECK ) );
    }

    public function aramakisayol($kriter = null, $sayfano = null, $tarz = null)
    {
        $data["kriter"] = $this->input->post("kriter");
        $data["sayfano"] = $this->input->post("sayfano");
        $data["tip"] = $this->input->post("tip");
        $this->load->view("dashboard/aramakisayol", $data);
    }

    public function aramakisayolalt()
    {
        $this->load->model("reservation_model");

        $kriter = $this->input->post("kriter");
        $sayfano = $this->input->post("sayfano");
        $tip = $this->input->post("tip");
        if($tip == "pnr"){
            $likes["rez_pnr"] = $kriter;
        }
        if($tip == "soyad"){
            $likes["yolcu_soyad"] = $kriter;
        }
        if($tip == "ad"){
            $likes["yolcu_ad"] = $kriter;
        }
        if($tip == "extra"){
            $likes["yolcu_soyad"] = $kriter;
        }
        if($tip == "musteri"){
            $likes["musteri_ad"] = $kriter;
        }
        if($tip == "reznot"){
            $likes["rez_not"] = $kriter;
        }
        if($tip == "yolcunot"){
            $likes["yolcu_aciklama"] = $kriter;
        }

        $json["result"] = $this->reservation_model->kisayolarama($likes, $sayfano, $tip);

        if($json["result"] != null){
            $this->output->set_content_type( 'application/json' )->set_output( json_encode( $json ) );
        }else{
            echo "null";
        }
    }
}
