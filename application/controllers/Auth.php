<?php

class Auth extends CI_Controller {

    public function oauth2callback() {
        $this->load->library("google_analytics");
        $this->google_analytics->auth();
    }

    //---------------- begin login ------------------
    public function login() {
        if ($this->input->method() == "post") {
            $this->f_login_post();
        }
        else
        {
            $this->f_login_get();
        }
    }

    private function f_login_get($model = NULL) {
        $header_model = array(
			"page_title" => "Login",
			"page_css" => array(),
			"page_body_prop" => array(),
			"no_main_header" => true
			);
        $this->load->view("header", $header_model);
        $this->load->view("login", $model);
    }

    private function f_login_post() {
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        if ($username === NULL || $username == "" || $password === NULL || $password == "") {
            $this->f_login_get(array("username" => $username));
            return;
        }
        $this->load->model("administrator_model");
        $yonetici = $this->administrator_model->get_yonetici($username, $password);
        if ($yonetici === NULL) {
            $this->f_login_get(array("username" => $username));
            return;
        }
        $this->session->set_userdata(array("logged_user" => $yonetici));
        redirect(base_url());
    }
    //--------------- end login -----------------

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

}
